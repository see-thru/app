#!/usr/bin/env bash
# install watchman
git clone https://github.com/facebook/watchman.git
cd watchman/
git checkout v4.7.0
sudo apt-get install -y autoconf automake build-essential python-dev
./autogen.sh 
./configure 
make
sudo make install
watchman --version
# install yarn
yarn install