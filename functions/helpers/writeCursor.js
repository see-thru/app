const writeCursor = (cursor, res) => {
  console.log('writeCursor')
  cursor.transformStream({ 
    transform: d => JSON.stringify(d)
  }).pipe(res)
}

module.exports = writeCursor
