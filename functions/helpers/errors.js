const logSendError = (res, error) => {
  console.error(error)
  return res.status(400).send({ error: error.message })
}

const asyncHandler = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(logSendError.bind(null, res));

module.exports = { logSendError, asyncHandler }