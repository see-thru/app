const isProd = process.env.GCP_PROJECT !== 'seethru-dev'
const dbName = process.env.GCP_PROJECT === 'claude-care' ? 'claude' : 'common'
const helpers = {
  isProd,
  dbName,
  getStripeKey: () => {
    const stripeKeyDev = process.env.STRIPE_KEY_DEV
    const stripeKeyProd = process.env.STRIPE_KEY_PROD
    return isProd ? stripeKeyProd : stripeKeyDev
  }
}

module.exports = helpers
