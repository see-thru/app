const { MongoClient, ObjectId } = require('mongodb')
const { isProd, dbName } = require('../helpers/env')
const uri = process.env.MONGO_URI
let clientDb

// todo: use retry?
const db = async mongoFunc => {
  if(clientDb){
    return mongoFunc(clientDb)
  } else {
    const client = await MongoClient.connect(uri, { useNewUrlParser: true })
    console.log('Mongo client connected')
    clientDb = client.db(dbName)
    return mongoFunc(clientDb)
  }
}

const getCollection = collectionName => mongoFunc => {
  return db(db => mongoFunc(db.collection(collectionName)))
}

const toId = str => {
  try { return new ObjectId(str) }
  catch(e) {
    console.error("Mongo.toId: ", str)
    throw new Error('There is no object with this ID.')
  }
}

module.exports = { 
  toId,
  db,
  users: getCollection(isProd ? 'users' : 'usersDev'),
  providers: getCollection('providers'),
  services: getCollection('services'),
  serviceLists: getCollection('serviceLists'),
  appointments: getCollection(isProd ? 'appointments' : 'appointmentsDev'),
  actions: getCollection(isProd ? 'actions' : 'actionsDev')
}
