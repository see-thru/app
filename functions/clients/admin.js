const admin = require('firebase-admin')

admin.initializeApp()

const getFirebaseId = (req, res, next) => {
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
    return res.status(401).send('Unauthorized')
  }
  let idToken = req.headers.authorization.split('Bearer ')[1]
  return admin.auth().verifyIdToken(idToken).then(user => {
    req.user = { uid: user.uid }
    return next()
  }).catch(error => res.status(401).send({error}))
}

const getFirebaseIdAllowEmpty = (req, res, next) => {
  req.user = { uid: '' }
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
    return next()
  }
  let idToken = req.headers.authorization.split('Bearer ')[1]
  return admin.auth().verifyIdToken(idToken)
    .then(user => {
      req.user.uid = user.uid
      return next()
    })
    .catch(() => next())
}

module.exports = { admin, getFirebaseId, getFirebaseIdAllowEmpty }