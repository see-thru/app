const sgMail = require('@sendgrid/mail')
const sgKey = require('firebase-functions').config().sendgrid.key
const isProd = require('../helpers/env').isProd

sgMail.setApiKey(sgKey)

const sendgrid = {
  sendEmail: (subject, text, html) => {
    return sgMail.send({
      to: 'vip@seethru.healthcare',
      from: {
        name: 'SeeThru App',
        email: 'app@seethru.healthcare',
      },
      subject,
      text: text || undefined,
      html: html || undefined
    })
  },
  newAppointment: (appointmentId) => {
    const link = `https://admin.seethru.healthcare/appointments/${appointmentId}/show`
    const env = isProd ? 'prod' : 'dev'
    const msg = `View Appointment: <a href="${link}#${env}">${link}</a>`
    return sendgrid.sendEmail('New Appointment', undefined, msg).catch(console.error)
  }
}

module.exports = sendgrid