const { dbName } = require('../helpers/env')
const uri = process.env.MONGO_URI
const mongoose = require('mongoose')

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(uri, { useNewUrlParser: true }).then(() => {
  console.log('Successfully connected to the database');
  return
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

module.exports = mongoose
