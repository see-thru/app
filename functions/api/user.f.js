const functions = require('firebase-functions')
const { getFirebaseId, getFirebaseIdAllowEmpty } = require('../clients/admin')
const mongo = require('../clients/mongo')
const express = require('express')
const { validateUserFields } = require('../middleware/validateUserField')
const { logSendError } = require('../helpers/errors')

const getMe = (req, res) => {
  mongo.users(async collection => {
    const user = await collection.findOne({_id: req.user.uid })
    return res.send({ user })
  }).catch(logSendError.bind(null, res))
}

const createMe = (req, res) => {
  console.log('POST /user/me', req.user, req.body)
  const userInfo = req.body // must validate the user information
  userInfo.createdAt = new Date()
  userInfo._id = req.user.uid
  mongo.users(async collection => {
    await collection.insertOne(userInfo)
    return res.send({ user: userInfo })
  }).catch(logSendError.bind(null, res))
}

const addActivity = (req, res) => {
  console.log('POST /user/activity', req.user, req.body)
  const data = Object.assign({ userId: req.user.uid, createdAt: new Date() }, req.body)
  mongo.actions(async collection => {
    await collection.insertOne(data)
    return res.send('OK')
  }).catch(logSendError.bind(null, res))
}

const router = express.Router()
router.get('/healthz', (req, res) => res.send({status: true}))
router.get('/me', getFirebaseId, getMe)
router.post('/create', getFirebaseId, express.json(), validateUserFields, createMe)
router.post('/activity', getFirebaseIdAllowEmpty, express.json(), addActivity)
const app = express().use(['/api/user',''], router)

exports = module.exports = functions.https.onRequest(app)