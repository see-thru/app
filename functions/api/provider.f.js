const functions = require('firebase-functions')
const express = require('express')
const { Provider } = require('../helpers/schemas')
const queries = require('../helpers/queries')
const { asyncHandler } = require('../helpers/errors')
const { isProd } = require('../helpers/env')
const { markupService } = require('../helpers/utils')

const getProvider = asyncHandler(async (req, res) => {
  console.log(`GET /provider/${req.params.providerId}`)
  if(!req.params.providerId){
    res.status(400).send({ error: 'There is no Provider ID specified.'})
    return
  }
  let query = Provider.findById(req.params.providerId).lean()
  if(isProd){
    query.where('active', true)
  }
  const provider = await query
  if(!provider){
    res.status(400).send({ error: 'There is no provider with that ID.'})
  }
  res.send(provider)
})

const getProviderServices = asyncHandler(async (req, res) => {
  console.log(`GET /provider/${req.params.providerId}/services`)
  if(!req.params.providerId){
    res.status(400).send({ error: 'There is no Provider ID specified.'})
    return
  }
  let services = await Provider.aggregate(queries.providerServices(req.params.providerId))
  services.forEach(s => { s.cost = markupService(s.cost) })
  res.send({ services })
  // if(services.length){
  // } else {
  //   return res.status(400).send({ error: 'There are no services for this provider.'})
  // }
})

const router = express.Router()
router.get('/healthz', (req, res) => res.send({status: true}))
router.get('/provider/:providerId', getProvider)
router.get('/provider/:providerId/services', getProviderServices)
// .use(/(?:\/api\/provider)?/, router)
const app = express().use(['/api/provider',''], router)

exports = module.exports = functions.https.onRequest(app)