const functions = require('firebase-functions')
const express = require('express')
const mongo = require('../clients/mongo')
const { logSendError } = require('../helpers/errors')
const { isProd } = require('../helpers/env')
const { markupService } = require('../helpers/utils')
const METERS_PER_MILE = 1609.34
const RADIANS_PER_MILE = 1 / 3963.2

const cleanSearch = searchText => {
  if(searchText){
    const common =  ["the","and","that","have","for","not","with","you","this","but","his","from","they","say","her","she","will","one","all","would","there","their","what","out","about","who","get","which","when","make","can","like","just","him","know","take","people","into","year","your","some","could","them","see","other","than","then","now","look","only","come","its","it's","over","think","also","back","after","use","two","how","our","first","well","way","even","want","because","any","these","give","most","none","really","don't","dont","didn't","didnt","sure","are","i'm","idk","etc","much","has","lot"]
    var pattern = /[\wÀ-ÿ'‘’]+/g
    // var pattern = /[\wÀ-ÿ'‘’\-]+/g
    var words = searchText.toLowerCase().match(pattern)
    if(words){
      return words.filter(word => word.length > 2 && !common.includes(word)).join(' ')
    }
  }
  return undefined
}

const buildQuery = filters => {
  let query = isProd ? { active: true } : {}
  const distance = filters.distance || 20
  var searchText = cleanSearch(filters.q)
  console.log(`searchText: ${searchText}`)
  if(searchText){
    query['$text'] = { $search: searchText }
    if(filters.location && filters.location.lat && filters.location.lng){
      let coordinates = [ Number(filters.location.lng), Number(filters.location.lat) ]
      query.location = {
        $geoWithin: {
          $centerSphere: [coordinates, distance * RADIANS_PER_MILE ]
        }
      }
    }
  } else { // if no search term, default location to New York
    let coordinates
    if(filters.location && filters.location.lat && filters.location.lng){
      coordinates = [ Number(filters.location.lng), Number(filters.location.lat) ]
    } else {
      coordinates = [ -74.0060, 40.7128 ]
    }
    query.location = {
      $nearSphere: { 
        $geometry: { type: 'Point', coordinates }, 
        $maxDistance: distance * METERS_PER_MILE 
      }
    }
  }
  return query
}

const searchProjection = {
  'providerInfo.name': 1,
  'providerInfo.degrees': 1,
  'providerInfo.specialty': 1,
  'providerInfo.secondarySpecialties': 1,
  'providerInfo.rating': 1,
  'providerInfo.photo': 1,
  address: 1,
  location: 1,
  baseCost: 1
}
const doSearch = (req, res) => {
  const filters = req.body || {}
  const skip = filters.skip || 0
  const limit = filters.limit || 10
  
  const query = buildQuery(filters) // TODO: sort by match score?
  mongo.providers(async collection => {
    let providers = await collection.find(query).project(searchProjection)
      .skip(skip).limit(limit)
      .toArray()
    if(!providers){
      return res.send({ providers: [] })
    }
    providers.forEach(p => { p.baseCost = markupService(p.baseCost) })
    return res.send({ providers })
  }).catch(logSendError.bind(null, res))
}

const router = express.Router()
router.get('/healthz', (req, res) => res.send({status: true}))
router.post('/search', express.json(), doSearch)
const app = express().use(['/api/search',''], router)

exports = module.exports = functions.https.onRequest(app)