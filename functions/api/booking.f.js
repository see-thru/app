const functions = require('firebase-functions')
const { getFirebaseId } = require('../clients/admin')
const { ObjectId, Appointment, User } = require('../helpers/schemas')
const express = require('express')
const generateRandomCheckInCode = require('../helpers/generateRandomCheckInCode')
const { asyncHandler } = require('../helpers/errors')
const { prepare } = require('../helpers/utils')
const { getStripeKey } = require('../helpers/env')
const stripe = require('stripe')(getStripeKey())
const sendgrid = require('../clients/sendgrid')

const STATUS = {
  booked: 'booked',
  checkedIn: 'checkedIn',
  confirmed: 'confirmed'
}

const getAll = asyncHandler(async (req, res) => {
  const appointments = await Appointment.find({ patientId: req.user.uid }).sort({ _id: -1 }).populate('provider').lean()
  return res.send({ appointments })
})

// create stripe customer (can retrieve the user, if we'd like an email here)
const cardError = new Error('There was an error with your credit card information')
const createCustomer = asyncHandler(async (payment, totalCost, patientId) => {
  if(totalCost){
    if(payment && payment.source){
      const user = await User.findById(patientId).lean()
      const customer = await stripe.customers.create({
        description: `${user.email} (${patientId})`,
        source: payment.source // obtained with Stripe.js
      }).catch(() => { throw cardError })
      // user.set('customerId', customer.id) // TODO: Add customerId to mongo user
      // await user.save()
      return customer.id 
    } else throw cardError
  } else return undefined
})

const create = asyncHandler(async (req, res) => {
  console.log('POST /create')
  const { totalCost, details, services, payment, providerId } = req.body
  
  const stripeCustomer = await createCustomer(payment, totalCost, req.user.uid)
  let appointment = new Appointment({
    patientId: req.user.uid,
    providerId: ObjectId(providerId),
    stripeCustomer,
    totalCost,
    details,
    services,
    status: STATUS.booked,
    createdAt: new Date()
  })
  appointment = await appointment.save() // does .save() modify the original object? 
  sendgrid.newAppointment(appointment._id)
  return res.send({ success: true, appointmentId: appointment._id })
})

const checkIn = asyncHandler(async (req, res) => {
  const appointmentId = req.params.appointmentId
  const appointment = await Appointment.findOne({ _id: ObjectId(appointmentId), patientId: req.user.uid }).populate('provider')
  
  if(!appointment){
    return res.status(403).send({ error: 'This appointment could not be found'})
  }

  const date = new Date()
  const dateString = `${date.getMonth() + 1}.${date.getDate()}.`
  
  // if the code was generated today, no need to create a new code
  
  if (appointment.get('checkInCode') && appointment.get('checkInCode').includes(dateString)) {
    return res.send({ appointment: prepare(appointment) })
  }
  
  const checkInCode = dateString + generateRandomCheckInCode()
  appointment.set('checkInCode', checkInCode)
  appointment.set('status', STATUS.checkedIn)
  await appointment.save()
  return res.send({ appointment: prepare(appointment) })
})

const router = express.Router()
router.get('/healthz', (req, res) => res.send({status: true}))
router.get('/all', getFirebaseId, getAll)
router.post('/create', getFirebaseId, express.json(), create)
router.post('/patient/check-in/:appointmentId', getFirebaseId, checkIn)
const app = express().use(['/api/booking',''], router)

exports = module.exports = functions.https.onRequest(app)