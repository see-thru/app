const validFields = [
  'firstName',
  'lastName',
  'email',
  'dob',
  'invitedBy',
  'sex',
  'phone'
]

// const requiredFields = [
//   'firstName',
//   'lastName',
//   'email',
//   'phone'
// ]

const validateUserFields = (req, res, next) => {
  for (let key in req.body) {
    if (validFields.indexOf(key) === -1) {
      return res.status(405).send({error: `Invalid ${key} field. Only allowed fields: ${validFields}`})
    }
  }

  // const missingFields = getMissingFields(req.body, requiredFields)
  // if (missingFields.length > 0) {
  //   return res.status(405).send({error: `Required fields: ${missingFields}`})
  // }

  if (req.body.email && !isEmail(req.body.email)) {
    return res.status(405).send({error: 'Invalid email'})
  }

  if (req.body.dob && !isDate(req.body.dob)) {
    return res.status(405).send({error: 'Not a valid date. Must be in the format mm/dd/yyyy.'})
  }

  return next()
}

const getMissingFields = (fields, requiredFields) => {
  const missingFields = []
  requiredFields.forEach( (field) => {
    if (!fields[field]) {
      missingFields.push(field)
    }
  })
  return missingFields
}

const isEmail = email => {
  if (!email || email.length > 254) {
    return false
  }
  var reg = /^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+$/
  return reg.test(email)
}

const isDate = date => {
  var reg = /^\d{1,2}\/\d{1,2}\/\d{4}$/
  return reg.test(date)
}

module.exports = {validateUserFields, getMissingFields, isEmail, isDate}
