import React from 'react'
import ReactGA from 'react-ga'
import ReactDOM from 'react-dom'
import Modal from 'react-modal'
import { unregister } from './registerServiceWorker'

import App from './App'
import { GOOGLE_ANALYTICS } from './constants/google' 

// import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-bootstrap-typeahead/css/Typeahead.css'
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css'

ReactGA.initialize(GOOGLE_ANALYTICS)
let appElement = document.getElementsByClassName('seethru-wrapper')[0]
Modal.setAppElement(appElement)
Modal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, .65)'
ReactDOM.render(<App />, appElement)
unregister()
