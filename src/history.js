import ReactGA from 'react-ga'
import { createBrowserHistory } from 'history'
import trackAction, { PLATFORM } from './utils/analytics';

const history = createBrowserHistory()
history.listen((location, action) => {
  ReactGA.set({ page: location.pathname })
  ReactGA.pageview(location.pathname)
  if(action === 'PUSH'){
    window.SeeThru.stackSize += 1
  } else if (action === 'POP'){
    window.SeeThru.stackSize -= 1
    trackAction('Navigated back', { detail: location.pathname, link: window.location.href }, PLATFORM.ST)
  }
})

export default history