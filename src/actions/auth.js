import makeActionCreator from '../utils/make-action-creator'
import * as constants from '../constants/auth'
import { auth, getAuthPersisted } from '../firebase'
import { createUser, getUser } from './user';

// Actions creators:
export const toggleSignInModal = makeActionCreator(constants.TOGGLE_SIGN_IN, 'modalIsOpen')
export const toggleSignUpModal = makeActionCreator(constants.TOGGLE_SIGN_UP, 'modalIsOpen')
export const toggleForgotPasswordModal = makeActionCreator(constants.TOGGLE_FORGOT_PASSWORD, 'modalIsOpen')
export const authLoading = makeActionCreator(constants.AUTH_LOADING)
export const loginFailed = makeActionCreator(constants.LOGIN_FAILED, 'error')
export const signupFailed = makeActionCreator(constants.SIGNUP_FAILED, 'error')
export const passwordResetFailed = makeActionCreator(constants.PASSWORD_RESET_FAILED, 'error')
export const passwordResetDone = makeActionCreator(constants.PASSWORD_RESET_DONE, 'data')
export const loggedIn = makeActionCreator(constants.LOGGED_IN)
export const loggedOut = makeActionCreator(constants.LOGGED_OUT)
export const setToken = makeActionCreator(constants.SET_TOKEN, 'token')

const getAuthErrorMessage = e => {
  switch(e.code){
    case 'auth/user-not-found':
      return 'There is no user with that email. Find the link to sign up for SeeThru below.'
    case 'auth/email-already-exists':
      return 'This email address is already in use.'
    default:
      return e.message || 'Sorry, there was an error. PLease try again or contact team@seethru.healthcare.'
  }
}

// async actions 
export const login = e => {
  const { email, password, rememberMe } = e
  return dispatch => {
    if(!email || !password){
      dispatch(loginFailed("There was an error logging you in. Please try again or contact team@seethru.healthcare."))
    } else {
      dispatch(authLoading())
      return getAuthPersisted(rememberMe)
        .then(auth => auth.signInWithEmailAndPassword(email, password))
        .then(() => dispatch(getUser())) // only necessary for freshchat at the moment..
        .catch(error => {
          console.error(error)
          dispatch(loginFailed(getAuthErrorMessage(error)))
        })
    }
  }
}

const getInviter = inviteCode => {
  if(inviteCode.startsWith('Thanksgiving18') || inviteCode.startsWith('thanksgiving18')){
    return inviteCode.slice(14)
  } else return undefined;
}

export const signup = e => {
  const { email, password, firstName, lastName, dob, inviteCode, rememberMe, phone } = e
  return dispatch => {
    const invitedBy = getInviter(inviteCode)
    if(!email || !password){
      dispatch(signupFailed('There was an error signing you up. Please try again or contact team@seethru.healthcare.'))
    } else if(!invitedBy){
      dispatch(signupFailed('Sorry, this invite code is not valid. We‘re still in beta!'))
    } else {
      dispatch(authLoading())
      return getAuthPersisted(rememberMe)
        .then(auth => auth.createUserWithEmailAndPassword(email, password))
        .then(() => {
          let newUser = { email, firstName, lastName, dob, invitedBy, phone }
          dispatch(createUser(newUser)) // catch this and delete auth user?
        })
        .catch(error => {
          console.error(error)
          dispatch(signupFailed(getAuthErrorMessage(error))) 
        })
    }
  }
}

export const logout = history => {
  return dispatch => {
    dispatch(authLoading())
    return auth.signOut()
      .then(() => {
        if (history && history.push) {
          history.push('/')
        } else {
          window.location.replace('/')
        }
        // dispatch(loggedOut()) // no need. firebase listener will handle AuthReducer.isLogged
      })
      .catch(error => console.error(error))
  }
}

// export const refreshToken = () => {
//   return dispatch => {
//     auth.currentUser.getIdToken()
//       .then( idToken => { 
//         dispatch(setToken(idToken))
//         return idToken
//       })
//       .catch( error => dispatch(loggedOut()) )
//   } 
// }

export const sendPasswordReset = email => {
  const url = window.location.origin
  return dispatch => {
    dispatch(authLoading())
    return auth.sendPasswordResetEmail(email, { url }) // set to open login in URL. Needs work
      .then(data => {
        console.log('passwordResetDone', data)
        dispatch(passwordResetDone('Your password has been reset successfully. Check your email for further instructions.')) 
      })
      .catch(error => {
        console.error(error)
        dispatch(passwordResetFailed(getAuthErrorMessage(error)))
      })
  }
} 