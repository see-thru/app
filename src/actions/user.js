import axiosWithAuth from '../api/axios-with-auth'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints'
import * as constants from '../constants/auth'
import { signupFailed } from './auth'
import { setFCUser } from '../utils/fresh-chat'

export const authLoading = makeActionCreator(constants.AUTH_LOADING) // change to page loading
export const userFailed = makeActionCreator(constants.USER_FAILED, 'error')
export const setUser = makeActionCreator(constants.SET_USER, 'data')

export const createUser = newUser => {
  const url = urlGenerator(endpoints.CREATE_USER) 
  return dispatch => {
    axiosWithAuth()
      .then(ax => ax.post(url, newUser))
      .then(response => {
        setFCUser(response.data.user)
        dispatch(setUser(response.data.user))
      })
      .catch(error => {
        console.error(error)
        dispatch(signupFailed(error.message))
      })
  }
}

export const getUser = () => {
  const url = urlGenerator(endpoints.USER)
  return dispatch => {
    dispatch(authLoading())
    axiosWithAuth()
      .then(ax => ax.get(url))
      .then(response => {
        setFCUser(response.data.user)
        dispatch(setUser(response.data.user))
      })
      .catch( error => dispatch(userFailed(error.message)) )
  }
}
