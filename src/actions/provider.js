import axios from 'axios'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import { getErrorMessage } from '../utils/errors'
import trackAction, { PLATFORM } from '../utils/analytics';
import * as endpoints from '../constants/endpoints'
import * as constants from '../constants/provider'
// Actions creators:
export const setProvider = makeActionCreator(constants.SET_PROVIDER, 'data')
export const setProviderServices = makeActionCreator(constants.SET_PROVIDER_SERVICES, 'data')
export const providerLoading = makeActionCreator(constants.PROVIDER_LOADING)
export const providerFailed = makeActionCreator(constants.PROVIDER_FAILED, 'error')

export const getProvider = id => {
  const providerUrl = `${ urlGenerator(endpoints.PROVIDER) }/${ id }`
  const servicesUrl = `${ urlGenerator(endpoints.PROVIDER) }/${ id }/services`
  return (dispatch, getState) => {
    dispatch(providerLoading())
    axios.get(providerUrl)
      .then(response => {
        dispatch(setProvider(response.data))
        if(response.data && response.data.providerInfo){
          trackAction('Viewed Provider', {
            detail: response.data.providerInfo.name,
            link: window.location.href
          }, PLATFORM.ST, PLATFORM.FC)
        }
      })
      .catch( e => {
        console.error(e.response)
        dispatch(providerFailed(getErrorMessage(e.response) || 'There was an error finding this provider.'))
      })
    axios.get(servicesUrl)
      .then( response => dispatch(setProviderServices(response.data.services || [])))
      .catch ( e => {
        console.error(e.response)
        if(!getState().ProviderReducer){
          dispatch(providerFailed(getErrorMessage(e.response) || 'There was an error loading the provider‘s services'))
        }
      })
  }
}

// helper for us. not used by the ui
export const getProviderIds = () => {
  const url = urlGenerator(endpoints.PROVIDER_ALL)
  return axios.get(url)
    .catch(e => {
      console.error(e)
    })
}
