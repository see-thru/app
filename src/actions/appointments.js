import axiosWithAuth from '../api/axios-with-auth'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints'
import * as constants from '../constants/appointments'
import trackAction from '../utils/analytics'
// Actions creators:
export const bookingLoading = makeActionCreator(constants.BOOKING_LOADING)
export const bookingFailed = makeActionCreator(constants.BOOKING_FAILED, 'error')
export const bookingDone = makeActionCreator(constants.BOOKING_DONE)
export const checkInLoading = makeActionCreator(constants.CHECK_IN_LOADING)
export const checkInFailed = makeActionCreator(constants.CHECK_IN_FAILED, 'error')
export const checkInDone = makeActionCreator(constants.CHECK_IN_DONE, 'data')
export const appointmentsLoading = makeActionCreator(constants.APPOINTMENTS_LOADING)
export const appointmentsFailed = makeActionCreator(constants.APPOINTMENTS_FAILED, 'error')
export const setAppointments = makeActionCreator(constants.SET_APPOINTMENTS, 'data')

export const bookAppointment = (appointment, stripeSource) => {
  return async dispatch => {
    try {
      const url = urlGenerator(endpoints.CREATE_APPOINTMENT)
      dispatch(bookingLoading())
      console.log('appointment:', appointment)
      const services = (appointment.services || []).map(s => {
        return { _id: s._id, name: s.name, cost: s.cost, time: s.time, includes: s.includes }
      })
      let payment
      if(stripeSource){
        payment = {
          source: stripeSource.id
        }
      }
      var { totalCost, details } = appointment
      var data = {
        totalCost,
        details,
        services,
        payment,
        providerId: appointment.provider._id
      }
      const axios = await axiosWithAuth()
      const response = await axios.post(url, data)
      const appointmentId = response.data.appointmentId
      dispatch(bookingDone()) // appointment id
      const link = `https://admin.seethru.healthcare/appointments/${appointmentId}/show`
      trackAction('Booked Appointment', { 
        category: 'Appointments',
        label: link,
        detail: appointment.provider.providerInfo.name,
        link
      })
      return appointmentId
    } catch(error) {
      console.error(error)
      dispatch(bookingFailed('There was an error booking the appointment. Please try again or contact team@seethru.healthcare.')) 
    }
  }
}

export const loadAppointments = () => {
  return async dispatch => {
    try {
      dispatch(appointmentsLoading())
      const axios = await axiosWithAuth()
      const response = await axios.get(urlGenerator(endpoints.LOAD_APPOINTMENTS))
      dispatch(setAppointments(response.data.appointments))
    } catch(error) {
      dispatch(appointmentsFailed(error.message))
    }
  }
}

export const doCheckIn = (appointmentId) => {
  return async dispatch => {
    try {
      dispatch(checkInLoading())
      const axios = await axiosWithAuth()
      const response = await axios.post(`${ urlGenerator(endpoints.CHECK_IN) }/${ appointmentId }`)
      dispatch(checkInDone(response.data.appointment))
    } catch(error) {
      dispatch(checkInFailed(error.message))
    }
  }
}