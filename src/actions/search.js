import axios from 'axios'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints'
import * as constants from '../constants/search'
import trackAction from '../utils/analytics'

// Actions creators:
export const searchLoading = makeActionCreator(constants.SEARCH_LOADING)
export const searchFailed = makeActionCreator(constants.SEARCH_FAILED, 'error')
export const setSearchResults = makeActionCreator(constants.SET_SEARCH_RESULTS, 'data', 'noMoreResults', 'clearResults')
export const providerMouseover = makeActionCreator(constants.PROVIDER_MOUSEOVER, 'providerId')
export const searchMarkerMouseover = makeActionCreator(constants.SEARCH_MARKER_MOUSEOVER, 'providerId')
export const searchMarkerMouseout = makeActionCreator(constants.SEARCH_MARKER_MOUSEOUT)

export const search = (data, skip = 0, limit = 10) => {
  const url = urlGenerator(endpoints.SEARCH_PROVIDERS)
  data.skip = skip
  data.limit = limit
  return dispatch => {
    dispatch(searchLoading())
    if(!skip){ 
      trackAction('Searched Providers', { 
        category: 'Search',
        detail: `“${data.q || ''}”`,
        link: window.location.href
      })
    }
    axios.post(url, data).then(response => {
      var providers = response.data.providers
      var noMoreResults = !providers || (providers.length < limit)
      // if skip, concat. Else replace
      dispatch(setSearchResults(providers, noMoreResults, !skip))
    }).catch( error => {
      console.error(error)
      dispatch(searchFailed('There was an error searching the providers')) 
    })
  }
}