const combineSearchData = (data = [], oldData = []) => {
  if(!data.length){
    return oldData
  }
  if(!oldData || !oldData.length){
    return data
  }
  return [ ...oldData, ...data ]
}

export default combineSearchData