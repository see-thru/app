const GetCostPerHour = (
  desiredAnnualIncome = 0,
  annualOverhead = 0,
  totalHours = 0,
  decimals = 2
) => {
  if ( typeof desiredAnnualIncome !== 'number' ) return 'the `desiredAnnualIncome` parameter should be a number'
  if ( typeof annualOverhead !== 'number' ) return 'the `annualOverhead` parameter should be a number'
  if ( typeof totalHours !== 'number' ) return 'the `totalHours` parameter should be a number'
  if ( typeof decimals !== 'number' ) return 'the `decimals` parameter should be a number'
  const subTotal = desiredAnnualIncome + annualOverhead
  const total = subTotal !== 0 && totalHours !== 0 ? subTotal / totalHours : 0
  return Number(total.toFixed(decimals))
}

export default GetCostPerHour