const GetCostPerProcedure = (
  costPerHour = 0,
  hourPerProcedure = 0,
  decimals = 2
) => {
  if ( typeof costPerHour !== 'number' ) return 'the `costPerHour` parameter should be a number'
  if ( typeof hourPerProcedure !== 'number' ) return 'the `hourPerProcedure` parameter should be a number'
  if ( typeof decimals !== 'number' ) return 'the `decimals` parameter should be a number'
  const total = costPerHour * hourPerProcedure
  return Number(total.toFixed(decimals))
}

export default GetCostPerProcedure