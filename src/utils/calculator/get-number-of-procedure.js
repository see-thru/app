const GetNumberOfProcedure = (
  totalHours = 0,
  hourPerProcedure = 0,
  decimals = 2
) => {
  if ( typeof totalHours !== 'number' ) return 'the `totalHours` parameter should be a number'
  if ( typeof hourPerProcedure !== 'number' ) return 'the `hourPerProcedure` parameter should be a number'
  if ( typeof decimals !== 'number' ) return 'the `decimals` parameter should be a number'
  const total = totalHours !== 0 && hourPerProcedure !== 0 ? totalHours / hourPerProcedure : 0
  return Number(total.toFixed(decimals))
}

export default GetNumberOfProcedure