const ConvertToNumbers = (
  annualOverhead = '0',
  hoursPerWeek = '0',
  weeksPerYear = '0',
  minutesPerProcedure = '0',
  desiredAnnualIncome = '0'
) => {
  try {
    let results = {
      annualOverhead: Number(annualOverhead),
      hoursPerWeek: Number(hoursPerWeek),
      weeksPerYear: Number(weeksPerYear),
      minutesPerProcedure: Number(minutesPerProcedure),
      desiredAnnualIncome: Number(desiredAnnualIncome)
    }
    return results
  } catch(e){
    console.error('Error converting fields to numbers', e)
    return {}
  }
}

export default ConvertToNumbers