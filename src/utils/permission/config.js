import { 
  generatePagesForDevelop,
  generatePagesForProduction
} from './pages-generators'

import {
  generateFeaturesForDevelop,
  generateFeaturesForProduction
} from './features-generators'

const config = {
  develop: {
    name: 'dev',
    pages: generatePagesForDevelop(),
    feature: generateFeaturesForDevelop(),
    enableAll: true,
  },
  production: {
    name: 'prod',
    pages: generatePagesForProduction(),
    feature: generateFeaturesForProduction(),
    enableAll: false
  }
}

export default config