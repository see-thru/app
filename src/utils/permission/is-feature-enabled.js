import config from './config'

const isFeatureEnabled = baseUrl => {
  switch (baseUrl) {
    default:
      return config.develop
  }
}

export default isFeatureEnabled