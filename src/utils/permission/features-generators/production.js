import * as features from '../../../constants/permission/features'
import populateObject from '../populate-object'

const generateFeaturesForProduction = () => {
  let object = {}
  populateObject(object, features.HEADER, true)
  populateObject(object, features.ENROLLMENT_FORM, true)
  populateObject(object, features.HOME_SEARCH, true)
  populateObject(object, features.TEST_CREDIT_CARDS_LIST, false)
  return object
}

export default generateFeaturesForProduction