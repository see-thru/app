import generateFeaturesForDevelop from './develop'
import generateFeaturesForProduction from './production'

export {
  generateFeaturesForDevelop,
  generateFeaturesForProduction
}