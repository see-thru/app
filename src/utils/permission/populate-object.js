const populateObject = (
  object = {}, 
  name = '', 
  status = true
) => {
  if ( typeof object !== 'object' ) return 'the `baseUrl` parameter should be an object'
  if ( typeof name !== 'string' ) return 'the `name` parameter should be a string'
  if ( typeof status !== 'boolean' ) return 'the `status` parameter should be a boolean'
  object[name] = status
  return object
}

export default populateObject