import generatePagesForQA from './qa'
import generatePagesForDemo from './demo'
import generatePagesForDevelop from './develop'
import generatePagesForProduction from './production'

export {
  generatePagesForQA,
  generatePagesForDemo,
  generatePagesForDevelop,
  generatePagesForProduction
}