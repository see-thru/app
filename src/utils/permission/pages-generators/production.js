import * as pages from '../../../constants/permission/pages'
import populateObject from '../populate-object'

const generatePagesForProduction = () => {
  let object = {}
  populateObject(object, pages.FAQ, true)
  populateObject(object, pages.HOME, true)
  populateObject(object, pages.SITE_MAP, true)
  populateObject(object, pages.ABOUT_US, true)
  populateObject(object, pages.WHY_SEETHRU, true)
  populateObject(object, pages.PARTNERSHIP, true)
  populateObject(object, pages.EDIT_PROFILE, true)
  populateObject(object, pages.WORK_WITH_US, true)
  populateObject(object, pages.PRIVACY_POLICY, true)
  populateObject(object, pages.PROVIDER_DETAIL, true)
  populateObject(object, pages.SEARCH_PROVIDERS, true)
  populateObject(object, pages.TERMS_OF_SERVICE, true)
  return object
}

export default generatePagesForProduction