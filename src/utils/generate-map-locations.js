const generateMapLocations = (data = []) => {
  if(!Array.isArray(data)){
    console.error('the `data` parameter should be an array', data)
    return []
  }
  let locations = []
  data.forEach(x => {
    if(x.location && x.location.coordinates){
      locations.push({
        provider: {
          _id: x._id,
          name: x.providerInfo.name,
          photo: x.providerInfo.photo
        },
        lat : x.location.coordinates[1], 
        lng : x.location.coordinates[0]
      })
    }
  })
  return locations
}

export default generateMapLocations