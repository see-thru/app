export const setFCUser = user => {
  if(!window.fcWidget.isLoaded){
    console.error('fcWidget has not been initiated')
    return
  }
  if(!user._id){
    console.error('fcWidget error: missing user id')
    return
  }

  window.fcWidget.init({ 
    token: "6dffec3b-01a9-456c-a194-53f9e99b26b8", 
    host: "https://wchat.freshchat.com",
    externalId: user._id,
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email
  })
}

export const initFCEmpty = () => {
  if(!window.fcWidget.isLoaded){
    console.error('fcWidget has not been initiated')
    return
  }
  window.fcWidget.init({ 
    token: "6dffec3b-01a9-456c-a194-53f9e99b26b8", 
    host: "https://wchat.freshchat.com"
  })
}