export const createAlert = (message, e, type) => {
  if(e) { console.error(e) }
  return { message, show: true, type: type || undefined }
}

export const clearAlert = function(){ this.setState({ alert: {} }) }

function isObject(o){
  return typeof o === 'object' && o !== null
}
export const getErrorMessage = e => { // should probably accept full error and return e.message, if e.response.data.error is not available
  return (isObject(e) && isObject(e.data) && e.data.error) || undefined;
}
