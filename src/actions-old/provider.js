import axios from 'axios'
import axiosWithAuth from '../api/axios-with-auth'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints/provider'
import * as constants from '../constants/provider'

// Headers
import APIHeaders from '../api/headers-old'
const Headers = new APIHeaders()

// Actions creators:
export const providerPending = makeActionCreator(constants.PROVIDER_LOADING)
export const providerSuccessFully = makeActionCreator(constants.SET_PROVIDER, 'data')
export const providerRejected = makeActionCreator(constants.PROVIDER_FAILED, 'error')
export const providerProfileRejected = makeActionCreator(constants.PROVIDER_FAILED_GET, 'error')
export const providerProfileSuccessFully = makeActionCreator(constants.SET_PROVIDER_GET, 'data')
export const providerCreatedRejected = makeActionCreator(constants.PROVIDER_CREATED_REJECTED, 'error')
export const providerCreatedSuccessFully = makeActionCreator(constants.PROVIDER_CREATED_SUCCESSFULLY, 'data')
export const providerUpdatedRejected = makeActionCreator(constants.PROVIDER_UPDATED_REJECTED, 'error')
export const providerUpdatedSuccessFully = makeActionCreator(constants.PROVIDER_UPDATED_SUCCESSFULLY, 'data')

export const providerPhotoPending = makeActionCreator(constants.PROVIDER_PHOTO_PENDING)
export const providerPhotoGetRejected = makeActionCreator(constants.PROVIDER_GET_PHOTO_REJECTED, 'error')
export const providerPhotoGetSuccessfully = makeActionCreator(constants.PROVIDER_GET_PHOTO_SUCCESSFULLY, 'data')
export const providerPhotoPostRejected = makeActionCreator(constants.PROVIDER_POST_PHOTO_REJECTED, 'error')
export const providerPhotoPostSuccessfully = makeActionCreator(constants.PROVIDER_POST_PHOTO_SUCCESSFULLY, 'data')

export const providerTimeSchedulePostRejected = makeActionCreator(constants.PROVIDER_TIME_SCHEDULE_REJECTED, 'error')
export const providerTimeSchedulePostSuccessfully = makeActionCreator(constants.PROVIDER_TIME_SCHEDULE_SUCCESSFULLY, 'data')

export const getProvider = id => {
  const url = `${ urlGenerator(endpoints.SEARCH_PROVIDER_GET) }/${ id }`
  return dispatch => {
    dispatch(providerPending())
    // axios.get(url)
    //   .then( response => dispatch(providerSuccessFully(response.data)) )
    //   .catch( error => dispatch(providerRejected(error)) )
    return new Promise(r => r({
      "providerDisplayId": "Federico-Ulfo-22L60",
      "provider": {
        "firstName": "Federico",
        "lastName": "Ulfo",
        "email": "federicoulfo@gmail.com",
        "photo": "https://storage.googleapis.com/seethru-dev.appspot.com/userPhoto/22L60lORIsfxDBiO23PnQunaoMG3.png",
        "specialty": {
          "primary": "Cardiac Electrophysiologist",
          "secondary": [
            "ClinicalInformatics",
            "AdolescentMedicine"
          ]
        },
        "currency": "usd",
        "degree": [
          "M.A.",
          "D.B.H."
        ],
        "languages": [
          "Italian",
          "Spanish; Castilian",
          "English"
        ],
        "medicalTraining": {
          "medicalSchool": {
            "name": "Messina",
            "year": 2000
          },
          "residency": {
            "name": "Pisa",
            "year": 2004
          },
          "fellowship": {
            "year": 2005,
            "name": "NYC"
          }
        },
        "personalNPI": "123456",
        "practiceNPI": "321",
        "practiceName": "AMG",
        "bio": "Creative Software Engineer with a wide experience in building startups and tech team, and in designing and implementing tech solution that scales up to millions"
      },
      "office": {
        "address": "99 Saint Marks Place 5C New York NYC 10009",
        "location": {
          "lng": -73.9844492,
          "lat": 40.72745099999999
        }
      },
      "availabilities": {
        "weekdays": [
          {
            "weekday": "Monday",
            "slots": [
              {
                "to": 10,
                "from": 9
              },
              {
                "to": 15,
                "from": 11
              }
            ]
          }
        ]
      },
      "_id": "22L60lORIsfxDBiO23PnQunaoMG3",
      "servicesAndBundles": {
        "services": [
          {
            "name": "X Ray",
            "cost": 58,
            "time": "15"
          },
          {
            "name": "Blood exam",
            "cost": 35,
            "time": "15"
          }
        ],
        "bundles": [
          {
            "bundle": "Annual Physical",
            "cost": 116,
            "time": "15",
            "services": [
              "Sore Throat",
              "History",
              "Heart Rate"
            ]
          }
        ]
      },
      "_highlight": {
        "services.name": [
          "Sore Throat"
        ]
      },
      "hasAvailableTime": true
    }))
  }
}

export const getProviderProfile = token => {
  const url = urlGenerator(endpoints.PROVIDER)
  const headers = Headers.getWithAuthorization(token)
  return dispatch => {
    dispatch(providerPending())
    axiosWithAuth.get(url, { headers })
      .then( response => {
        const data = response.data.provider ? response.data.provider : response.data
        dispatch(providerProfileSuccessFully(data))
      })
      .catch( error => dispatch(providerProfileRejected(error)) )
  }
}

export const createProvider = ( 
  e,
  token,
) => {
  const url = urlGenerator(endpoints.PROVIDER) 
  const headers = token ? Headers.getWithAuthorization(token) : {}
  const body = e
  return dispatch => {
    dispatch(providerPending())
    axios.post(url, JSON.stringify(body), { headers })
      .then( response => dispatch(providerCreatedSuccessFully()) )
      .catch( error => dispatch(providerCreatedRejected(error.message)) )
  }
}

export const updateProvider = ( 
  e,
  token
) => {
  const url = urlGenerator(endpoints.PROVIDER) 
  const headers = Headers.getWithAuthorization(token)
  const body = e
  return dispatch => {
    dispatch(providerPending())
    axiosWithAuth.put(url, JSON.stringify(body), { headers })
      .then( response => dispatch(providerUpdatedSuccessFully(response.data)) )
      .catch( error => dispatch(providerUpdatedRejected(error.message)) )
  }
}

export const getProviderPhoto = providerId => {
  const url = `${ urlGenerator(endpoints.PROVIDER_PHOTO) }/${ providerId }`
  return dispatch => {
    dispatch(providerPhotoPending())
    axios.get(url)
      .then( response => dispatch(providerPhotoGetSuccessfully(response.data)) )
      .catch( error => dispatch(providerPhotoGetRejected(error.message)) )
  }  
}

export const editProviderPhoto = (
  file,
  token,
  providerId
) => {
  let body = new FormData()
  body.append('photo', file)
  const url = `${ urlGenerator(endpoints.PROVIDER_PHOTO) }/${ providerId }`
  const headers = Headers.getWithAuthorization(token)
  Object.assign(headers, {
    "Content-Type": "application/x-www-form-urlencoded",
    "Cache-Control": "no-cache"
  })
  return dispatch => {
    dispatch(providerPhotoPending())
    axiosWithAuth.post(url, body, { headers })
      .then( response => {
        dispatch(getProviderProfile(token))
        dispatch(providerPhotoPostSuccessfully(response.data)) 
      })
      .catch( error => dispatch(providerPhotoPostRejected(error.message)) )
  }  
}

export const editTimeSchedule = (
  token,
  timeSchedule
) => {
  const url = urlGenerator(endpoints.PROVIDER_TIME_SCHEDULE) 
  const headers = Headers.getWithAuthorization(token)
  const body = timeSchedule
  return dispatch => {
    dispatch(providerPending())
    axiosWithAuth.put(url, JSON.stringify(body), { headers })
      .then( response => {
        dispatch(providerTimeSchedulePostSuccessfully())
        dispatch(getProviderProfile(token))
      })
      .catch( error => dispatch(providerTimeSchedulePostRejected(error.message)) )
  }  
}