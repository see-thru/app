// import makeActionCreator from '../utils/make-action-creator'
// import * as constants from '../constants/auth'
// import { auth } from '../firebase'

// import { getUser, createUser } from './user'

// // Actions creators:
// export const toggleSignInModal = makeActionCreator(constants.TOGGLE_SIGN_IN, 'modalIsOpen')
// export const toggleSignInModalWithBackTo = makeActionCreator(constants.TOGGLE_SIGN_IN, 'modalIsOpen', 'backTo')
// export const toggleSignUpModal = makeActionCreator(constants.TOGGLE_SIGN_UP, 'modalIsOpen')
// export const toggleSignUpModalWithBackTo = makeActionCreator(constants.TOGGLE_SIGN_UP, 'modalIsOpen', 'backTo')
// export const authPending = makeActionCreator(constants.AUTH_LOADING)
// export const authSuccessFully = makeActionCreator(constants.SET_USER, 'data')
// export const registerRejected = makeActionCreator(constants.SIGNUP_FAILED, 'error')
// export const loginRejected = makeActionCreator(constants.LOGIN_FAILED, 'error')
// export const logoutSuccessFully = makeActionCreator(constants.LOGOUT_DONE)
// export const refreshTokenSuccessFully = makeActionCreator(constants.SET_TOKEN, 'token')
// export const toggleForgotPasswordModal = makeActionCreator(constants.TOGGLE_FORGOT_PASSWORD, 'modalIsOpen')
// export const toggleResetPasswordModal = makeActionCreator(constants.TOGGLE_RESET_PASSWORD, 'modalIsOpen')
// export const sendPasswordResetEmailPending = makeActionCreator(constants.SEND_PASSWORD_RESET_EMAIL_PENDING)
// export const sendPasswordResetEmailRejected = makeActionCreator(constants.SEND_PASSWORD_RESET_EMAIL_REJECTED, 'error')
// export const sendPasswordResetEmailSuccessFully = makeActionCreator(constants.SEND_PASSWORD_RESET_EMAIL_SUCCESSFULLY, 'data')

// // async actions 
// export const register = (e, role = 'patient') => {
//   const { email, password } = e
//   return dispatch => {
//     dispatch(authPending())
//     if(!email || !password){
//       dispatch(registerRejected("There was an error signing you up. Please try again or contact team@seethru.healthcare."))
//     }
//     return auth.createUserWithEmailAndPassword(email, password)
//       .then( authUser => {
//         if ( ! authUser.message ) {
//           dispatch(authSuccessFully(authUser))
//           dispatch(createUser(e, role, authUser.qa))
//           dispatch(toggleSignUpModal(false))
//         } else {
//           dispatch(registerRejected(authUser.message))
//         }
//         return authUser
//       })
//       .catch( error => dispatch(registerRejected(error.message)) )
//   }
// }

// export const login = e => {
//   const {
//     email,
//     password
//   } = e
//   return dispatch => {
//     dispatch(authPending())
//     return auth.signInWithEmailAndPassword(email, password)
//       .then( authUser => {
//         if ( !authUser.message ) {
//           console.log('logged in user: ', authUser)
//           dispatch(authSuccessFully(authUser))
//           dispatch(getUser(authUser.qa))
//           dispatch(toggleSignInModal(false))
//         } else {
//           dispatch(loginRejected(authUser.message))
//         }
//         return authUser
//       })
//       .catch( error => dispatch(loginRejected(error.message)) )
//   }
// }

// export const logout = history => {
//   return dispatch => {
//     dispatch(authPending())
//     return auth.signOut()
//       .then( () => {
//         if ( history && history.push ) {
//           history.push('/')
//         } else {
//           window.location.replace('/')
//         }
//         dispatch(logoutSuccessFully())
//       })
//       .catch( error => console.error(error.message) )
//   }
// }

// export const refreshToken = () => {
//   return dispatch => {
//     auth.refreshToken()
//       .then( idToken => dispatch(refreshTokenSuccessFully(idToken)) )
//       .catch( error => dispatch(logoutSuccessFully()) )
//   } 
// }

// export const sendPasswordResetEmail = email => {
//   const baseUrl = window.location.origin
//   return dispatch => {
//     dispatch(sendPasswordResetEmailPending())
//     return auth.sendPasswordResetEmail(email, { 
//       url: `${ baseUrl }?&passwordUpdated=true`,
//       handleCodeInApp: true 
//     })
//       .then( data => {
//         dispatch(sendPasswordResetEmailSuccessFully(data)) 
//       })
//       .catch( error => {
//         if (error.code) {
//           dispatch(sendPasswordResetEmailRejected(error.message))
//         } else {
//           dispatch(sendPasswordResetEmailRejected(error)) 
//         }
//       })
//   }
// } 