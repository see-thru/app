import axios from 'axios'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints/search'
import * as constants from '../constants/search'

// Actions creators:
export const searchPending = makeActionCreator(constants.SEARCH_LOADING)
export const searchRejected = makeActionCreator(constants.SEARCH_FAILED, 'error')
export const searchSuccessFully = makeActionCreator(constants.SEARCH_DONE, 'data')
export const hoveredMarkerChanged = makeActionCreator(constants.SEARCH_MARKER_HOVERED, 'hoveredMarker')
export const activeProviderChanged = makeActionCreator(constants.SEARCH_PROVIDER_CHANGED, 'activeProvider')

export const searchProviders = (
  e, 
  resultsShown = []
) => {
  const url = urlGenerator(endpoints.SEARCH_DOCTORS)
  let body = {}
  if ( e.what ) {
    body.what = e.what 
  }
  if ( e.when ) {
    body.when = e.when
  }
  if ( e.location ) {
    body.location = e.location
  }
  if ( resultsShown >= 1 ) {
    body.from = resultsShown
    body.size = resultsShown + 10
  }
  if(e.distance){
      body.distance = e.distance
  }
  return dispatch => {
    dispatch(searchPending())
    axios.post(url, body)
      .then( response => dispatch(searchSuccessFully(response.data)) )
      .catch( error => dispatch(searchRejected(error)) )
  }
}