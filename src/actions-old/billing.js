import urlGenerator from '../api/url-generator'
import axiosWithAuth from '../api/axios-with-auth'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints/billing'
import * as constants from '../constants/billing'

// Headers
import APIHeaders from '../api/headers-old'
const Headers = new APIHeaders()

// Actions creators:
export const billingPending = makeActionCreator(constants.BILLING_PENDING)
export const billingGetRejected = makeActionCreator(constants.BILLING_GET_REJECTED, 'error')
export const billingGetSuccessFully = makeActionCreator(constants.BILLING_GET_SUCCESSFULLY, 'data')
export const billingPostRejected = makeActionCreator(constants.BILLING_POST_REJECTED, 'error')
export const billingPostSuccessFully = makeActionCreator(constants.BILLING_POST_SUCCESSFULLY, 'data')
export const billingDeleteRejected = makeActionCreator(constants.BILLING_DELETE_REJECTED, 'error')
export const billingDeleteSuccessFully = makeActionCreator(constants.BILLING_DELETE_SUCCESSFULLY, 'data')
export const bullingPayWithCardRejected = makeActionCreator(constants.BILLING_PAY_WITH_CARD_REJECTED, 'error')
export const bullingPayWithCardSuccessFully = makeActionCreator(constants.BILLING_PAY_WITH_CARD_SUCCESSFULLY, 'data')

export const getCreditCardList = token => {
  const url = urlGenerator(endpoints.BILLING) 
  const headers = Headers.getWithAuthorization(token)
  return dispatch => {
    dispatch(billingPending())
    axiosWithAuth.get(url, { headers })
      .then( response => dispatch(billingGetSuccessFully(response.data)) )
      .catch( error => dispatch(billingGetRejected(error.message)) )
  }
}

export const saveCreditCard = (
  e,
  token,
  history, 
  openModalCreditCardList
) => {
  const url = urlGenerator(endpoints.BILLING) 
  const headers = Headers.getWithAuthorization(token)
  const body = e
  return dispatch => {
    dispatch(billingPending())
    axiosWithAuth.post(url, JSON.stringify(body), { headers })
    .then( response => {
        dispatch(billingPostSuccessFully(response.data))
        if ( history ) history.push('/user/payment-info/methods')
        if ( openModalCreditCardList ) openModalCreditCardList()
      })
      .catch( error => {
        const message = error.response.data.error && error.response.data.error.message ? error.response.data.error.message : error.response.data.error
        dispatch(billingPostRejected(message)) 
      })
  }
}

export const deleteCreditCard = (
  cardId,
  token
) => {
  const url = `${ urlGenerator(endpoints.BILLING) }/${ cardId }`
  const headers = Headers.getWithAuthorization(token)
  return dispatch => {
    dispatch(billingPending())
    axiosWithAuth.delete(url, { headers })
      .then( response => {
        dispatch(billingDeleteSuccessFully(response.data))
        dispatch(getCreditCardList(token))
      } )
      .catch( error => dispatch(billingDeleteRejected(error.message)) )
  }
}

export const PayWithCard = (
  e,
  token
) => {
  const url = urlGenerator(endpoints.BILLING_PAY_WITH_CARD)
  const headers = Headers.getWithAuthorization(token)
  const body = e
  return dispatch => {
    dispatch(billingPending())
    axiosWithAuth.post(url, JSON.stringify(body), { headers })
      .then( response => dispatch(bullingPayWithCardSuccessFully(response.data)) )
      .catch( error => dispatch(bullingPayWithCardRejected(error.message)) )
  }
}