import axios from 'axios'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints/partnership'
import * as constants from '../constants/partnership'

import { login } from './auth'

// Headers
import APIHeaders from '../api/headers-old'
const Headers = new APIHeaders()

// Actions creators:
export const partnershipProviderPending = makeActionCreator(constants.PARTNERSHIP_PROVIDER_CREATION_PENDING)
export const partnershipProviderSuccess = makeActionCreator(constants.PARTNERSHIP_PROVIDER_CREATION_SUCCESS, 'data')
export const partnershipProviderFailed = makeActionCreator(constants.PARTNERSHIP_PROVIDER_CREATION_FAILED, 'error')
export const partnershipProviderCreateNew = makeActionCreator(constants.PARTNERSHIP_PROVIDER_CREATE_NEW)

export const postPartnershipProvider = (
  e,
  token
) => {
  const url = urlGenerator(endpoints.PARTNERSHIP_PROVIDER_BASE)
  const headers = token ? Headers.getWithAuthorization(token) : Headers.generateApplicationJsonHeaders()
  const body = e
  return dispatch => {
    dispatch(partnershipProviderPending())
    axios.post(url, JSON.stringify(body), { headers })
      .then( response => {
        dispatch(partnershipProviderSuccess(response.data))
        dispatch(login({ email: e.email, password: e.password }))
      })
      .catch( error => {
        if ( error.response && error.response.data ) {
          dispatch(partnershipProviderFailed(error.response.data))
        }
      })
  }
}
