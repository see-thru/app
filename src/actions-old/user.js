// import axios from 'axios'
// import axiosWithAuth from '../api/axios-with-auth'
// import urlGenerator from '../api/url-generator'
// import makeActionCreator from '../utils/make-action-creator'
// import * as endpoints from '../constants/endpoints/auth'
// import * as constants from '../constants/auth'
// import * as providersEndpoints from '../constants/endpoints/provider'

// import { getCreditCardList } from './billing'

// // Generators:
// import GeneratorDataStructureEditProfile from '../utils/generator-data-structure/edit-profile'

// // Headers
// import APIHeaders from '../api/headers-old'
// const Headers = new APIHeaders()

// // Actions creators:
// export const userPending = makeActionCreator(constants.AUTH_LOADING)
// export const userRejected = makeActionCreator(constants.USER_REJECTED, 'error')
// export const userGetSuccessFully = makeActionCreator(constants.SET_USER, 'data')
// export const userUpdateSuccessFully = makeActionCreator(constants.USER_UPDATED_SUCCESSFULLY)
// export const userPhotoPending = makeActionCreator(constants.USER_PHOTO_PENDING)
// export const userPhotoGetRejected = makeActionCreator(constants.USER_PHOTO_GET_REJECTED, 'error')
// export const userPhotoGetSuccessfully = makeActionCreator(constants.USER_PHOTO_GET_SUCCESSFULLY, 'data')
// export const userPhotoPostRejected = makeActionCreator(constants.USER_PHOTO_POST_REJECTED, 'error')
// export const userPhotoPostSuccessfully = makeActionCreator(constants.USER_PHOTO_POST_SUCCESSFULLY, 'data')
// export const userPhotoDeleteRejected = makeActionCreator(constants.USER_PHOTO_DELETE_REJECTED, 'error')
// export const userPhotoDeleteSuccessfully = makeActionCreator(constants.USER_PHOTO_DELETE_SUCCESSFULLY, 'message')
// export const userHealthCareServicesRejected = makeActionCreator(constants.USER_HEALTHCARE_SERVICES_REJECTED, 'error')
// export const userHealthCareServicesSuccessfully = makeActionCreator(constants.USER_HEALTHCARE_SERVICES_SUCESSFULLY, 'data')


// // async actions 
// export const createUser = ( 
//   e, 
//   role, 
//   token 
// ) => {
//   const {
//     dob,
//     email,
//     lastName,
//     firstName
//   } = e
//   const url = urlGenerator(endpoints.USER) 
//   const body = {
//     dob,
//     role,
//     email,
//     lastName,
//     firstName
//   }
//   const headers = Headers.getWithAuthorization(token)
//   return dispatch => {
//     dispatch(userPending())
//     axios.post(url, JSON.stringify(body), { headers } )
//     .then( response => {
//       dispatch(getUser(token))
//       // dispatch(userCreateSuccessFully(response)) 
//     })
//     .catch( error => console.error(error) )
//   }
// }


// export const getUser = token => {
//   const url = urlGenerator(endpoints.USER) 
//   const headers = Headers.getWithAuthorization(token)
//   return dispatch => {
//     dispatch(userPending())
//     axiosWithAuth.get(url, { headers })
//       .then( response => {
//         response.data.user.token = token
//         dispatch(getCreditCardList(token))
//         dispatch(userGetSuccessFully(response.data.user))
//       })
//       .catch( error => dispatch(userRejected(error.message)) )
//   }
// }

// export const editProfile = ( 
//   e, 
//   token 
// ) => {
//   const url = urlGenerator(endpoints.USER) 
//   const body = GeneratorDataStructureEditProfile(e)
//   const headers = Headers.getWithAuthorization(token)
//   return dispatch => {
//     dispatch(userPending())
//     axiosWithAuth.post(url, JSON.stringify(body), { headers })
//       .then( response => {
//         dispatch(getUser(token)) 
//         dispatch(userUpdateSuccessFully())
//       })
//       .catch( error => dispatch(userRejected(error.message)) )
//   }
// }

// export const getMyPhoto = token => {
//   const url = urlGenerator(endpoints.USER_PHOTO) 
//   const headers = Headers.getWithAuthorization(token)
//   return dispatch => {
//     dispatch(userPhotoPending())
//     axiosWithAuth.get(url, { headers })
//       .then( response => dispatch(userPhotoGetSuccessfully(response.data)) )
//       .catch( error => dispatch(userPhotoGetRejected(error.message)) )
//   }  
// }

// export const getPhoto = userId => {
//   const url = `${ urlGenerator(endpoints.USER_PHOTO) }/${ userId }`
//   return dispatch => {
//     dispatch(userPhotoPending())
//     axios.get(url)
//       .then( response => dispatch(userPhotoGetSuccessfully(response.data)) )
//       .catch( error => dispatch(userPhotoGetRejected(error.message)) )
//   }  
// }

// export const editMyPhoto = (
//   file,
//   crop,
//   token
// ) => {
//   let body = new FormData()
//   body.append('photo', file)
//   body.append('crop', JSON.stringify(crop))  
//   const url = urlGenerator(endpoints.USER_PHOTO) 
//   const headers = Headers.getWithAuthorization(token)
//   Object.assign(headers, {
//     "Content-Type": "application/x-www-form-urlencoded",
//     "Cache-Control": "no-cache"
//   })
//   return dispatch => {
//     dispatch(userPhotoPending())
//     axiosWithAuth.post(url, body, { headers })
//       .then( response => {
//         dispatch(getUser(token))
//         dispatch(userPhotoPostSuccessfully(response.data)) 
//       })
//       .catch( error => dispatch(userPhotoPostRejected(error.message)) )
//   }  
// }

// export const deleteMyPhoto = token => {
//   const url = urlGenerator(endpoints.USER_PHOTO) 
//   const headers = Headers.getWithAuthorization(token)
//   return dispatch => {
//     dispatch(userPhotoPending())
//     axiosWithAuth.delete(url, { headers })
//       .then( response => {
//         dispatch(getUser(token))
//         dispatch(userPhotoDeleteSuccessfully(response)) 
//       })
//       .catch( error => dispatch(userPhotoDeleteRejected(error.message)) )
//   }  
// }

// export const editHealthCareServices = (
//   file,
//   token
// ) => {
//   const url = urlGenerator(providersEndpoints.PROVIDER_HEALTHCARE_SERVICES) 
//   const headers = Headers.getWithAuthorization(token)

//   const formData = new FormData();
//   formData.append("file", file)

//   Object.assign(headers, {
//     "Content-Type": "multipart/form-data"
//   })
//   return dispatch => {
//     dispatch(userPending())
//     axiosWithAuth.post(url, formData, { headers })
//       .then( response => {
//         dispatch(userHealthCareServicesSuccessfully(response.data.success)) 
//         dispatch(getUser(token))
//       })
//       .catch( error => dispatch(userHealthCareServicesRejected(error.response.data.error)) )
//   }  
// }