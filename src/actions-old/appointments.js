import axios from 'axios'
import urlGenerator from '../api/url-generator'
import axiosWithAuth from '../api/axios-with-auth'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints/appointments'
import * as authEndpoints from '../constants/endpoints/auth'
import * as constants from '../constants/appointments'

// Headers
import APIHeaders from '../api/headers-old'
const Headers = new APIHeaders()

// Actions creators:
export const appointmentsPending = makeActionCreator(constants.APPOINTMENTS_LOADING)
export const appointmentsRejected = makeActionCreator(constants.APPOINTMENTS_FAILED, 'error')
export const appointmentsSuccessFully = makeActionCreator(constants.SET_APPOINTMENTS, 'data')
export const bookAnAppointmentSuccessFully = makeActionCreator(constants.BOOK_AN_APPOINTMENT_SUCCESSFULLY, 'data')
export const checkInPending = makeActionCreator(constants.CHECK_IN_LOADING)
export const checkInRejected = makeActionCreator(constants.CHECK_IN_FAILED, 'error')
export const checkInSuccessFully = makeActionCreator(constants.CHECK_IN_DONE, 'data')
export const patientPhotoPending = makeActionCreator(constants.PATIENT_PHOTO_PENDING)
export const patientPhotoRejected = makeActionCreator(constants.PATIENT_PHOTO_REJECTED)
export const patientPhotoSuccessFully = makeActionCreator(constants.PATIENT_PHOTO_SUCCESSFULLY, 'patientId', 'url') 
export const getBookingPending = makeActionCreator(constants.GET_BOOKING_PENDING)
export const getBookingRejected = makeActionCreator(constants.GET_BOOKING_REJECTED, 'error')
export const getBookingSuccessFully = makeActionCreator(constants.GET_BOOKING_SUCCESSFULLY, 'data')

export const getBooking = (
  token,
  bookingId
) => {
  const url =  `${ urlGenerator(endpoints.GET_BOOKING) }/${ bookingId }` 
  const headers = Headers.getWithAuthorization(token)
  return dispatch => {
    dispatch(getBookingPending())
    axiosWithAuth.get(url, { headers } )
      .then( response => dispatch(getBookingSuccessFully(response.data)) )
      .catch( error => dispatch(getBookingRejected(error.message)) )
  }
}

export const BookAnAppointment = (
  e,
  token
) => {
  const url =  urlGenerator(endpoints.PATIENT_APPOINTMENTS_LIST) 
  const body = e
  const headers = Headers.getWithAuthorization(token)
  return dispatch => {
    dispatch(appointmentsPending())
    axios.post(url, JSON.stringify(body), { headers } )
      .then( response => dispatch(bookAnAppointmentSuccessFully(response)) )
      .catch( error => dispatch(appointmentsRejected(error.message)) )
  }
}

export const getAppointments = ( 
  token, 
  type = 'provider'
) => {
  let url = urlGenerator(endpoints.PROVIDER_APPOINTMENTS_LIST) 
  if ( type === 'patient' ) {
    url = urlGenerator(endpoints.PATIENT_APPOINTMENTS_LIST) 
  }
  const headers = Headers.getWithAuthorization(token)
  return dispatch => {
    dispatch(appointmentsPending())
    axiosWithAuth.get(url, { headers })
      .then( response => dispatch(appointmentsSuccessFully(response.data)) )
      .catch( error => dispatch(appointmentsRejected(error.message)) )
  }
}

export const checkInBooking = (
  token,
  urlParameter
) => {
  const url = `${ urlGenerator(endpoints.CHECK_IN) }/${ urlParameter }`
  const headers = Headers.getWithAuthorization(token)
  return dispatch => {
    dispatch(checkInPending())
    axios.post(url, {}, { headers })
      .then( response => dispatch(checkInSuccessFully(response.data)) )
      .catch( error => dispatch(checkInRejected(error.message)) )
  }
}

export const providerCheckInBooking = (
  code,
  token,
  appointmentId
) => {
  const url = urlGenerator(endpoints.PROVIDER_CHECK_IN_BOOKING)
  const headers = Headers.getWithAuthorization(token)
  const body = { checkInCode: code }
  return dispatch => {
    dispatch(checkInPending())
    axiosWithAuth.post(url, JSON.stringify(body), { headers })
      .then( response => {
        dispatch(checkInSuccessFully(response.data))
        dispatch(getBooking(token, appointmentId))
      })
      .catch( error => dispatch(checkInRejected(error.message)) )
  }
}


export const getPatientPhoto = id => {
  const url = `${ urlGenerator(authEndpoints.USER_PHOTO) }/${ id }` 
  return dispatch => {
    dispatch(patientPhotoPending())
    axios.get(url)
      .then( response => dispatch(patientPhotoSuccessFully(id, response.data)) )
      .catch( error => dispatch(patientPhotoRejected()) )
  }  
}
