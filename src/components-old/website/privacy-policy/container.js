import React, { Component } from 'react'

import PrivacyPolicy from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

export class PrivacyPolicyContainer extends Component {
  render () {
    return <PrivacyPolicy />
  }
}

export default HasPermissionHOC(
  PrivacyPolicyContainer, 
  isFeatureEnabled(window.location.href).pages.privacyPolicy
)