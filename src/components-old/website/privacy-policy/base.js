import React from 'react'
import ReactMarkdown from 'react-markdown'

import {
  Date,
  Title,
  WrapperTitle,
  WrapperContent
} from '../../generic/pages'
import PrivacyPolicyData from '../../../content/privacy-policy'
import { StyledContainerGrid } from '../../generic/grid/index'

const PrivacyPolicy = () => (
  <StyledContainerGrid
    gridGap="0 100px"
    grid-template-rows="1"
    templateColumns="100%">
    <WrapperTitle>
      <Title>{ PrivacyPolicyData.title }</Title>
      <Date>{ PrivacyPolicyData.date }</Date>
    </WrapperTitle>
    <WrapperContent>
      <ReactMarkdown source={ PrivacyPolicyData.content } />
      <div id="information-security">
        <ReactMarkdown source={ PrivacyPolicyData.informationSecutiry } />
      </div>
      <div id="personally-identifiable-info">
        <ReactMarkdown source={ PrivacyPolicyData.personallyIdentifiableInformation } />
      </div>
      <div id="protected-health-info">
        <ReactMarkdown source={ PrivacyPolicyData.protectedHealthInformation } />
      </div>  
      <div id="health-care-provider">
        <ReactMarkdown source={ PrivacyPolicyData.healthcareProvider } />
      </div>     
      <div id="account-and-profile-info">
        <ReactMarkdown source={ PrivacyPolicyData.accountAndProfileInformation } />
      </div>   
      <div id="mailing-list-info">
        <ReactMarkdown source={ PrivacyPolicyData.mailingListInformation } />
      </div>     
      <div id="account-activity">
        <ReactMarkdown source={ PrivacyPolicyData.accountActivity } />
      </div>       
      <div id="location-information">
        <ReactMarkdown source={ PrivacyPolicyData.locationInformation } />
      </div>       
      <div id="ip-address-and-related-data">
        <ReactMarkdown source={ PrivacyPolicyData.IPAddressAndRelatedData } />
      </div>  
      <div id="cookies">
        <ReactMarkdown source={ PrivacyPolicyData.cookies } />
      </div>      
      <div id="advertising-networks-and-personalized-advertising">
        <ReactMarkdown source={ PrivacyPolicyData.advertisingNetworksAndPersonalizedAdvertising } />
      </div>    
      <div id="analytics">
        <ReactMarkdown source={ PrivacyPolicyData.analytics } />
      </div>       
      <div id="how-we-use-and-share-collected-info">
        <ReactMarkdown source={ PrivacyPolicyData.howWeUseAndShareCollectedInfo } />
      </div>                    
      <div id="sharing-collected-info-with-healthcare-providers">
        <ReactMarkdown source={ PrivacyPolicyData.sharingCollectedInfoWithHealthcareProviders } />
      </div>
      <div id="purpose-collected">
        <ReactMarkdown source={ PrivacyPolicyData.purposeCollected } />
      </div>
      <div id="text-messages">
        <ReactMarkdown source={ PrivacyPolicyData.textMessages } />
      </div>      
      <div id="text-messages-from-healthcare">
        <ReactMarkdown source={ PrivacyPolicyData.textMessagesFromHealthcare } />
      </div>            
      <div id="communication-with-you">
        <ReactMarkdown source={ PrivacyPolicyData.communicationWithYou } />
      </div>      
      <div id="service-evaluation">
        <ReactMarkdown source={ PrivacyPolicyData.serviceEvaluation } />
      </div>     
      <div id="security-and-protection">
        <ReactMarkdown source={ PrivacyPolicyData.securityAndProtection } />
      </div>      
      <div id="sharing-collected-info-with-our-suppliers">
        <ReactMarkdown source={ PrivacyPolicyData.sharingCollectedInfoWithOurSuppliers } />
      </div>  
      <div id="aggregate-data">
        <ReactMarkdown source={ PrivacyPolicyData.aggregateData } />
      </div>
      <div id="business-transactions">
        <ReactMarkdown source={ PrivacyPolicyData.businessTransactions } />
      </div>
      <div id="access-by-children">
        <ReactMarkdown source={ PrivacyPolicyData.accessByChildren } />
      </div>
      <div id="access-from-outside-the-usa">
        <ReactMarkdown source={ PrivacyPolicyData.accessFromOutsideTheUnitedStates } />
      </div>      
      <div id="how-to-access-your-personally">
        <ReactMarkdown source={ PrivacyPolicyData.howToAccessAccessYourPersonally } />
      </div>     
      <div id="contact-us">
        <ReactMarkdown source={ PrivacyPolicyData.contactUs } />
      </div>                        
    </WrapperContent>
  </StyledContainerGrid>
)

export default PrivacyPolicy