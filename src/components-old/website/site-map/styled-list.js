import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'

const SiteMapList = styled.ul`
  margin: 0;
  ${
    Media.small`
      font-size: 1rem;
      padding-left: 30px;
    `
  }
  li {
    margin: 4px 0;
  }
  a {
    color: ${ colors.successButton };
    text-decoration: none;
    &:hover {
      color: ${ colors.successButton };
    }
  }
`

export default SiteMapList