import React from 'react'
import { HashLink as Link } from 'react-router-hash-link'

import {
  Title,
  WrapperTitle,
  WrapperContent
} from '../../generic/pages'
import SiteMapList from './styled-list'
import SiteMapTitle from './styled-title'
import StyledSiteMapContainer from './styled-container.js'
import { StyledContainerGrid } from '../../generic/grid/index'

const SiteMap = () => (
  <StyledContainerGrid
    gridGap="0 100px"
    grid-template-rows="1"
    templateColumns="100%">
    <StyledSiteMapContainer>
      <WrapperTitle>
        <Title>Site Map</Title>
      </WrapperTitle>
      <WrapperContent>
        <SiteMapTitle>Home Page</SiteMapTitle>
        <SiteMapList>
          <li><Link to="/#how-we-do-it">How We Do It</Link></li>
          <li><Link to="/#everybody-wins-with-seethru">Everybody Wins with SeeThru</Link></li>
          <li><Link to="/#the-seethru-platform">The SeeThru Platform</Link></li>
        </SiteMapList>
        <SiteMapTitle>Work With Us</SiteMapTitle>
        <SiteMapList>
          <li><Link to="/working-with-us#culture-is-key">Culture is Key</Link></li>
          <li><Link to="/working-with-us#healthcare-providers">Healthcare Providers</Link></li>
          <li><Link to="/working-with-us#ambassadors-program">Ambassadors Program</Link></li>
          <li><Link to="/working-with-us#compensation">Compensation</Link></li>
        </SiteMapList>  
        <SiteMapTitle>Why SeeThru</SiteMapTitle>
        <SiteMapList>
          <li><Link to="/why-seethru#americans-are-spending-more">Americans are Spending More on Healthcare than Ever</Link></li>
          <li><Link to="/why-seethru#high-medical-costs">High Medical Costs &copy; Surprise Bills Plague Americans</Link></li>
          <li><Link to="/why-seethru#annual-out-of-pocket-healthcare-1">Annual Out-of-Pocket Healthcare Expenditure In The U.S.</Link></li>
          <li><Link to="/why-seethru#patient-payments-are-rising">Patient Payments Are Rising &copy; Many Use High Deductible Health Plans.</Link></li>
          <li><Link to="/why-seethru#annual-healthcare-revenue-cycle-management-expenditure-in-the-us">Annual Healthcare Revenue Cycle Management Expenditure in the U.S..</Link></li>
          <li><Link to="/why-seethru#patients-want">Patients Want a Modern, Transparent &copy; Consumer Friendly Experience</Link></li>
        </SiteMapList>        
        <SiteMapTitle>About Us</SiteMapTitle>
        <SiteMapList>
          <li><Link to="/about-us#mission">Mission</Link></li>
          <li><Link to="/about-us#culture">Culture</Link></li>
          <li><Link to="/about-us#our-team">Our Team</Link></li>
        </SiteMapList>     
        <SiteMapTitle>FAQ</SiteMapTitle> 
        <SiteMapList>
          <li><Link to="/faq#question1">Will information or data about my practice be sent to third parties or sold to anyone?</Link></li>
          <li><Link to="/faq#question2">How will I get paid if I use SeeThru?</Link></li>
          <li><Link to="/faq#question3">Is your website and platform HIPAA compliant?</Link></li>
          <li><Link to="/faq#question4">Can I screen patients before an initial appointment?</Link></li>
          <li><Link to="/faq#question5">What if a patient doesn't show up for his or her appointment?</Link></li>
          <li><Link to="/faq#question6">What if we need to change prices for our services?</Link></li>
          <li><Link to="/faq#question7">If a patient needs additional tests or procedures during a visit, how will the patient pay for them?</Link></li>
          <li><Link to="/faq#question8">Are payments secure?</Link></li>
          <li><Link to="/faq#question9">How will patients know to choose me as a provider?</Link></li>
        </SiteMapList>
        <SiteMapTitle>Terms of Service</SiteMapTitle> 
        <SiteMapList>
          <li><Link to="/terms#seethru-services">SeeThru Services.</Link></li>
          <li><Link to="/terms#user-responsibilities">User Responsibilities.</Link></li>
          <li><Link to="/terms#website-content">Website Content.</Link></li>
          <li><Link to="/terms#fees-and-billing">Fees And Billing.</Link></li>
          <li><Link to="/terms#limitation-of-liability-for-use">Limitation of Liability for Use.</Link></li>
          <li><Link to="/terms#the-parties-acknowledge">The parties acknowledge and agree that SeeThru...</Link></li>
          <li><Link to="/terms#representations-warranties">Representations, Warranties and Covenants of User.</Link></li>
          <li><Link to="/terms#indemnification">Indemnification</Link></li>
          <li><Link to="/terms#user-will-defend">User will defend, indemnify and hold harmless SeeThru...</Link></li>
          <li><Link to="/terms#other-representations-and-warranties">Other Representations and Warranties; Disclaimer of Warranties.</Link></li>
          <li><Link to="/terms#limitation-of-liability">Limitation of Liability.</Link></li>
          <li><Link to="/terms#trademarks">Trademarks; Publicity.</Link></li>
          <li><Link to="/terms#intellectual-property">Intellectual Property.</Link></li>
          <li><Link to="/terms#confidentiality">Confidentiality/HIPAA Compliance.</Link></li>
          <li><Link to="/terms#termination">Term; Termination.</Link></li>
          <li><Link to="/terms#miscellaneous">Miscellaneous</Link></li>
        </SiteMapList>
        <SiteMapTitle>Privacy Policy</SiteMapTitle> 
        <SiteMapList>
          <li><Link to="/privacy-policy#information-security">Information Security</Link></li>
          <li><Link to="/privacy-policy#personally-identifiable-info">Personally Identifiable Information</Link></li>
          <li><Link to="/privacy-policy#protected-health-info">Protected Health Information</Link></li>
          <li><Link to="/privacy-policy#health-care-provider">Health Care Provider Appointment Information</Link></li>
          <li><Link to="/privacy-policy#account-and-profile-info">Account and Profile Information</Link></li>
          <li><Link to="/privacy-policy#mailing-list-info">Mailing List Information</Link></li>
          <li><Link to="/privacy-policy#account-activity">Account Activity.</Link></li>
          <li><Link to="/privacy-policy#cookies">Cookies.</Link></li>
          <li><Link to="/privacy-policy#analytics">Analytics.</Link></li>
          <li><Link to="/privacy-policy#how-we-use-and-share-collected-info">How We Use and Share Collected Information.</Link></li>
          <li><Link to="/privacy-policy#sharing-collected-info-with-healthcare-providers">Sharing Collected Information with Healthcare Providers.</Link></li>
          <li><Link to="/privacy-policy#purpose-collected">Purpose Collected.</Link></li>
          <li><Link to="/privacy-policy#text-messages">Text Messages from SeeThru.</Link></li>
          <li><Link to="/privacy-policy#text-messages-from-healthcare">Text Messages from Healthcare Providers Using the Service.</Link></li>
          <li><Link to="/privacy-policy#communication-with-you">Communication With You.</Link></li>
          <li><Link to="/privacy-policy#service-evaluation">Service Evaluation and Improvement.</Link></li>
          <li><Link to="/privacy-policy#security-and-protection">Security and Protection of Rights.</Link></li>
          <li><Link to="/privacy-policy#sharing-collected-info-with-our-suppliers">Sharing Collected Information with Our Suppliers.</Link></li>
          <li><Link to="/privacy-policy#aggregate-data">Aggregate Data.</Link></li>
          <li><Link to="/privacy-policy#business-transactions">Business Transactions.</Link></li>
          <li><Link to="/privacy-policy#access-by-children">Access by Children.</Link></li>
          <li><Link to="/privacy-policy#access-from-outside-the-usa">Access from Outside the United States.</Link></li>
          <li><Link to="/privacy-policy#how-to-access-your-personally">How to Access Your Personally Identifiable Information</Link></li>
          <li><Link to="/privacy-policy#contact-us">Contact Us</Link></li>
        </SiteMapList>
      </WrapperContent>
    </StyledSiteMapContainer>
  </StyledContainerGrid>
)

export default SiteMap