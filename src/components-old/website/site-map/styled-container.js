import styled from 'styled-components'

const StyledSiteMapContainer = styled.div`
  margin: auto;
  max-width: 470px;
`

export default StyledSiteMapContainer