import React, { Component } from 'react'

import SiteMap from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

export class SiteMapContainer extends Component {
  render () {
    return <SiteMap />
  }
}

export default HasPermissionHOC(
  SiteMapContainer, 
  isFeatureEnabled(window.location.href).pages.siteMap
)