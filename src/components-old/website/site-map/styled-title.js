import styled from 'styled-components'

import Media from '../../../utils/media'
import { 
  fonts,
  colors 
} from '../../../utils/settings'

const SiteMapTitle = styled.p`
  color: ${ colors.text };
  font-size: 1.12rem;
  font-weight: ${ fonts.weightBold };
  text-transform: uppercase;
  ${
    Media.small`
      font-size: .9rem;
    `
  }
`

export default SiteMapTitle