import React from 'react'

import {
  Title,
  WrapperTitle,
  WrapperContent
} from '../../generic/pages'
import Culture from './culture'
import Providers from './providers'
import Ambassadors from './ambassadors'
import { StyledContainerGrid } from '../../generic/grid/index'

const WorkingWithUs = () => (
  <StyledContainerGrid
    gridGap="0 100px"
    grid-template-rows="1"
    templateColumns="100%">
    <WrapperTitle withBottomBorder="no">
      <Title>Work With Us</Title>
    </WrapperTitle>
    <WrapperContent>
      <div id="culture-is-key"><Culture /></div>
      <div id="healthcare-providers"><Providers /></div>
      <div id="ambassadors-program"><Ambassadors /></div>
    </WrapperContent>
  </StyledContainerGrid>
)

export default WorkingWithUs