import React from 'react'

import {
  BoxInfoTitle,
  BoxInfoImage,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'

const Strategic = () => (
  <BoxInfoWrapper>
    <BoxInfoImage 
      src="/images/example-circle.png"
      position="left top" />
    <BoxInfoContent>
      <BoxInfoTitle>Strategic Partnership</BoxInfoTitle>
      <p><strong>The healthcare ecosystem is evolving. Thanks to SeeThru, waiting three to four billing cycles to receive payments for healthcare services is becoming an issue of the past.</strong></p>
      <p>We welcome licensed healthcare practitioners who are open to acquiring new patients. Patients can view your availability, book appointments and pay for services before walking through your door. There is zero up front cost to you.</p>
      <p>It's time to let go of unreasonably low insurance reimbursement rates. You work hard and deserve to get paid by charging amounts you want, based upon the time you spend with patients and the services you provide. We make it easier for you to do your job without worrying about inefficient scheduling and billing practices. With SeeThru, you are in control.</p>
    </BoxInfoContent>
  </BoxInfoWrapper>
)

export default Strategic