import React from 'react'

import {
  Title,
  WrapperTitle,
  WrapperContent
} from '../../../generic/pages'
import JobOffer from './job-offer/base'

const JobOpennings = () => (
  <div>
    <WrapperTitle>
      <Title>Job Opennings</Title>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor.</p>
    </WrapperTitle>
    <WrapperContent>
      <JobOffer
        type="Full Time"
        title="Frontend Developer"
        place="Remote"
        description="Fixie tote bag ethnic keytar. Neutra vinyl American Apparel kale chips tofu art party, cardigan raw denim quinoa. Cray paleo tattooed, Truffaut skateboard street art PBR jean shorts Shoreditch farm-to-table Austin lo-fi Odd Future occupy. Chia semiotics skateboard, Schlitz messenger bag master cleanse High Life occupy vegan polaroid tote bag leggings." />
      <JobOffer
        type="Part Time"
        title="Node JS Developer"
        place="New York"
        description="Fixie tote bag ethnic keytar. Neutra vinyl American Apparel kale chips tofu art party, cardigan raw denim quinoa. Cray paleo tattooed, Truffaut skateboard street art PBR jean shorts Shoreditch farm-to-table Austin lo-fi Odd Future occupy. Chia semiotics skateboard, Schlitz messenger bag master cleanse High Life occupy vegan polaroid tote bag leggings." />
      <JobOffer
        type="Full Time"
        title="UX Designer"
        place="Remote"
        description="Fixie tote bag ethnic keytar. Neutra vinyl American Apparel kale chips tofu art party, cardigan raw denim quinoa. Cray paleo tattooed, Truffaut skateboard street art PBR jean shorts Shoreditch farm-to-table Austin lo-fi Odd Future occupy. Chia semiotics skateboard, Schlitz messenger bag master cleanse High Life occupy vegan polaroid tote bag leggings." />        
    </WrapperContent>
  </div>
)

export default JobOpennings