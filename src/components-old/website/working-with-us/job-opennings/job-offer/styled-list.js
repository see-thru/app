import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../../utils/settings'

const StyledList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  li {
    color: ${ colors.successButton };
    display: inline-block;
    font-size: 1.12rem;
    font-weight: ${ fonts.weightBold };
    &:last-of-type {
      margin-left: 5px;
    }
  }
`

export default StyledList