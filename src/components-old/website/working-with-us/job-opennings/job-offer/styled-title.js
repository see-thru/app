import styled from 'styled-components'

const StyledTitle = styled.h5`
  font-size: 1.32rem;
  font-weight: 400;
  margin-bottom: 4px;
  margin-top: 0;
`

export default StyledTitle