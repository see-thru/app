import styled from 'styled-components'

import { colors } from '../../../../../utils/settings'

const StyledJobOffer = styled.div`
  border-bottom: 1px solid ${ colors.border }
  margin: 10px 0;
  padding: 10px;
`

export default StyledJobOffer