import React from 'react'
import PropTypes from 'prop-types'

import StyledList from './styled-list'
import StyledTitle from './styled-title'
import StyledJobOffer from './styled'

const JobOffer = ({
  type,
  title,
  place,
  description
}) => (
  <StyledJobOffer>
    <StyledTitle>{ title }</StyledTitle>
    <StyledList>
      <li>{ place } - </li> 
      <li>{ type }</li>
    </StyledList>
    <p>{ description }</p>
  </StyledJobOffer>
)

JobOffer.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  place: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
}

export default JobOffer