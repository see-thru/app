import styled from 'styled-components'

import { fonts, colors } from '../../../utils/settings'
import { StyledButtonCustom } from '../../generic/button/styled'

const StyledButton = styled(StyledButtonCustom)`
  color: white !important;
  display: inline-block;
  font-size: 1.3rem;
  font-weight: ${ fonts.weightBold };
  margin: 10px 0;
  padding: 14px 30px;
  transition: .2s background-color;
  &:hover {
    background-color: ${ colors.bgMissionBox };
  }
`

export default StyledButton