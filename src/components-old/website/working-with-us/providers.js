import React from 'react'
import { Link } from 'react-router-dom'
import ReactMarkdown from 'react-markdown'

import StyledButton from './styled-button'
import StyledWorkWithUsContent from './styled-work-with-us-content'
import StyledAboutUsContainerGrid from '../about-us/styled-container'
import { HealthcareProvidersData } from '../../../content/working-with-us'

const Providers = () => (
  <StyledWorkWithUsContent>
    <StyledAboutUsContainerGrid
      gridGap="0 20px"
      templateRows="auto"
      templateColumns="1fr">  
      <div>
        <h4>{ HealthcareProvidersData.title }</h4>
        <ReactMarkdown source={ HealthcareProvidersData.content } />
        <StyledButton
          to={ HealthcareProvidersData.link.value }
          tag={ Link }>
          {  HealthcareProvidersData.link.title }
        </StyledButton>        
      </div>
    </StyledAboutUsContainerGrid>  
  </StyledWorkWithUsContent>
)

export default Providers