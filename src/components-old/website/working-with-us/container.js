import React, { Component } from 'react'

import WorkingWithUs from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

export class WorkingWithUsContainer extends Component {
  render () {
    return <WorkingWithUs />
  }
}

export default HasPermissionHOC(
  WorkingWithUsContainer, 
  isFeatureEnabled(window.location.href).pages.workWithUs
)