import React from 'react'
import ReactMarkdown from 'react-markdown'

import { CultureData } from '../../../content/about-us'
import StyledWorkWithUsContent from './styled-work-with-us-content'
import StyledAboutUsContainerGrid from '../about-us/styled-container'

const Culture = () => (
  <StyledWorkWithUsContent>
    <StyledAboutUsContainerGrid
      gridGap="0 20px"
      templateRows="auto"
      templateColumns="repeat(2, 1fr)">  
      <div>
        <h4>{ CultureData.title }</h4>
        <ReactMarkdown source={ CultureData.content } />
      </div>
      <img src="/images/work-with-us/graphic9.png" alt=""/>      
    </StyledAboutUsContainerGrid>  
  </StyledWorkWithUsContent>
)

export default Culture