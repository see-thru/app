import React from 'react'
import ReactMarkdown from 'react-markdown'

import StyledButton from './styled-button'
import { AmbassadorsData } from '../../../content/working-with-us'
import { CompensationData } from '../../../content/working-with-us'
import StyledWorkWithUsContent from './styled-work-with-us-content'
import StyledAboutUsContainerGrid from '../about-us/styled-container'

const Ambassadors = () => (
  <StyledWorkWithUsContent>
    <StyledAboutUsContainerGrid
      gridGap="0 20px"
      templateRows="auto"
      templateColumns="1fr">  
      <div>
        <h4>{ AmbassadorsData.title }</h4>
        <ReactMarkdown source={ AmbassadorsData.content } />    
        <div id="compensation">
          <ReactMarkdown source={ CompensationData.content } />
          <StyledButton
            tag='a'
            href='mailto:team@seethru.healthcare?subject=Ambassador Program'
            target="_blank">
            { CompensationData.link.title }
          </StyledButton>        
        </div>
      </div>
    </StyledAboutUsContainerGrid>  
  </StyledWorkWithUsContent>
)

export default Ambassadors