import styled from 'styled-components'

import StyledMainContent from '../about-us/styled-main-content'
import { colors } from '../../../utils/settings';
import Media from '../../../utils/media';

const StyledWorkWithUsContent = styled(StyledMainContent)`
  border-bottom: 1px solid ${ colors.border };
  ${
    Media.small`
      margin: 0 0 20px;
    `
  }
  ul {
    padding-left: 0;
  }
  li {
    display: inline-block;
    font-size: 1.3rem;
    margin-right: 1%;
    position: relative;
    padding-left: 18px;
    vertical-align: top;
    width: 49%;
    ${
      Media.medium`
        font-size: 1.1rem;
        display: block;
        margin-right: 0;
        width: 100%;
      `
    }
    &:before {
      background-color: ${ colors.borderMissionBox };
      border-radius: 100%;
      content: '';
      display: inline-block;
      height: 10px;
      left: 0;
      margin-right: 10px;
      top: 8px;
      position: absolute;
      width: 10px;
    }
  }
`

export default StyledWorkWithUsContent