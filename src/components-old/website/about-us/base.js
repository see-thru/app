import React, { Fragment } from 'react'

import {
  Title,
  WrapperTitle,
  WrapperContent
} from '../../generic/pages'
import Team from './team/base'
import Advisors from './advisors/base'
import MainContent from './main-content'
import { StyledContainerGrid } from '../../generic/grid/index'
import MissionAndCultureContent from './mission-and-culture-content'

const AboutUs = () => (
  <Fragment>
    <StyledContainerGrid
      gridGap="0 100px"
      grid-template-rows="1"
      templateColumns="100%">
      <MainContent />
    </StyledContainerGrid>
    <MissionAndCultureContent />
    <StyledContainerGrid
      gridGap="0 100px"
      grid-template-rows="1"
      templateColumns="100%">
      <div id="our-team">
        <WrapperTitle withBottomBorder="no">
          <Title>The SeeThru Team</Title>
          <p>We are a decentralized team of passionate and talented professionals.</p>
        </WrapperTitle>   
        <WrapperContent>
          <Team />
        </WrapperContent>
        <WrapperTitle withBottomBorder="no">
          <Title>Advisors</Title>
        </WrapperTitle>   
        <WrapperContent>
          <Advisors />
        </WrapperContent>      
      </div>
    </StyledContainerGrid>    
  </Fragment>
)

export default AboutUs