import styled from 'styled-components'

const StyledMissionAndCultureContent = styled.div`
  background: url('/images/about-us/building-bg.png') no-repeat bottom #f3f3f3;
  background-size: cover;
  padding: 70px 0;
  & > div {
    margin: 0;
  }
`

export default StyledMissionAndCultureContent