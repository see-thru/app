import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../../utils/settings'

const StyledMember = styled.div`
  text-align: center;
  padding: 10px;
  img {
    border-radius: 100%;
    height: auto;
    max-width: 200px;
    width: 100%;
  }
  h3 {
    color: ${ colors.textTitle };
    line-height: 1.2;
    font-size: 1.5rem;
    font-weight: ${ fonts.weightBold };
    margin-bottom: 5px;
  }
  p {
    font-size: 1.25rem;
    margin: 0 0 10px;
  }
`

export default StyledMember