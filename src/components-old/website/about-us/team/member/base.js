import React from 'react'
import PropTypes from 'prop-types'

import StyledMember from './styled'

const Member = ({
  photo,
  fullname,
  position
}) => (
  <StyledMember>
    <img
      alt={ fullname } 
      src={ photo } />
    <h3>{ fullname }</h3>
    <p>{ position }</p>
  </StyledMember>
)

Member.propTypes = {
  photo: PropTypes.string.isRequired,
  fullname: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired
}

export default Member