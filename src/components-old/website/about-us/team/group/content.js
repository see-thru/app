import React from 'react'
import PropTypes from 'prop-types'
 
const GroupContent = ({ children }) => (
  <div>{ children }</div>
)

GroupContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object
  ])
}

export default GroupContent