import React from 'react'
import PropTypes from 'prop-types'

const GroupWrapper = ({ children }) => (
  <div>{ children }</div>
)

GroupWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object
  ])
}

export default GroupWrapper