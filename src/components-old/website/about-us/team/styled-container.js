import styled from 'styled-components'

import Media from '../../../../utils/media'
import { StyledContainerGrid } from '../../../generic/grid/index'

const StyledContainerGridTeam = styled(StyledContainerGrid)`
  ${ 
    Media.medium`
      grid-template-columns: repeat(3, 1fr)
    ` 
  }
  ${ 
    Media.small`
      grid-template-columns: repeat(1, 1fr)
    `
  }
`

export default StyledContainerGridTeam