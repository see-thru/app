import React from 'react'

import {
  GroupWrapper,
  GroupContent
} from './group'
import Member from './member/base'
import StyledContainerGridTeam from './styled-container'
import { team as teamCatalog } from "../../../../catalogs"

const Team = () => (
  <GroupWrapper>
    <GroupContent>
      <StyledContainerGridTeam
        gridGap="10px"
        templateRows="auto"
        templateColumns="repeat(5, 1fr)">
        { teamCatalog.map( (member, index) => {
          return (
            <Member 
              key={ index }
              photo={ member.avatar }
              fullname={ member.name }
              position={ member.position } 
              linkedin={ member.linkedin } />
          )
        }) }                         
      </StyledContainerGridTeam>    
    </GroupContent>
  </GroupWrapper>  
)

export default Team