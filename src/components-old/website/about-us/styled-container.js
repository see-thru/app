import styled from 'styled-components'

import Media from '../../../utils/media'
import { StyledContainerGrid } from '../../generic/grid/index'

const StyledAboutUsContainerGrid = styled(StyledContainerGrid)`
  ${ Media.small`
    grid-gap: 0;
    grid-template-columns: repeat(1, 1fr);
    padding: 0;
  ` }
`

export default StyledAboutUsContainerGrid