import React from 'react'
import ReactMarkdown from 'react-markdown'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import { CultureData } from '../../../content/about-us'

const Culture = () => (
  <BoxInfoWrapper 
    type="aboutUs"
    section="culture">
    <BoxInfoContent>
      <BoxInfoTitle>{ CultureData.title }</BoxInfoTitle>
      <ReactMarkdown source={ CultureData.content } />
      <br />
    </BoxInfoContent>
  </BoxInfoWrapper>
)

export default Culture