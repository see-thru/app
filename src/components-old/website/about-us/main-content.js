import React from 'react'

import StyledMainContent from './styled-main-content'
import StyledAboutUsContainerGrid from './styled-container'

const MainContent = () => (
  <StyledMainContent>
    <StyledAboutUsContainerGrid
      gridGap="0 20px"
      templateRows="auto"
      templateColumns="repeat(2, 1fr)">
      <div>
        <h4>About Us</h4>    
        <p>SeeThru is a blockchain-enabled platform that prioritizes price transparency in order to change the world of healthcare payments. Our ecosystem brings patients the sorely needed value-based consumer experience found in every other service industry.</p>
        <p>SeeThru utilizes smart contracts for direct pricing with providers. In doing so, SeeThru removes third parties, which drives down patient costs, increases provider revenue, and improves access to care around the world.</p>
      </div>
      <img src="/images/about-us/graphic8.png" alt=""/>
    </StyledAboutUsContainerGrid>
  </StyledMainContent>  
)

export default MainContent