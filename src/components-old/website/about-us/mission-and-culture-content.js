import React from 'react'

import Mission from './mission'
import Culture from './culture'
import { WrapperContent } from '../../generic/pages'
import StyledAboutUsContainerGrid from './styled-container'
import StyledMissionAndCultureContent from './styled-mission-and-culture-content'

const MissionAndCultureContent = () => (
  <StyledMissionAndCultureContent>
    <WrapperContent>
      <StyledAboutUsContainerGrid
        gridGap="0 20px"
        templateRows="auto"
        templateColumns="1fr 2fr">
        <div id="mission"><Mission /></div>
        <div id="culture"><Culture /></div>
      </StyledAboutUsContainerGrid>
    </WrapperContent>
  </StyledMissionAndCultureContent>
)

export default MissionAndCultureContent