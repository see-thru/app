import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'

const StyledMainContent = styled.div`
  margin: 30px 0;
  h4 {
    color: ${ colors.textTitle };
    font-size: 3rem;
    line-height: 1;
    margin: 0;
    ${ Media.small`
      font-size: 2rem;
      text-align: center;
    ` }
  }
  p {
    font-size: 1.375rem;
    margin-bottom: 50px;
    ${ Media.small`
      font-size: 1.1rem;
      margin-bottom: 30px;
    ` }
  }
  img { 
    max-width: 540px;
    width: 100%;
  }
` 

export default StyledMainContent