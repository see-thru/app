import React from 'react'
import ReactMarkdown from 'react-markdown'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import { MissionData } from '../../../content/about-us'

const Mission = () => (
  <BoxInfoWrapper 
    type="aboutUs"
    section="mission">
    <BoxInfoContent>
      <BoxInfoTitle>{ MissionData.title }</BoxInfoTitle>
      <ReactMarkdown source={ MissionData.content } />
    </BoxInfoContent>
  </BoxInfoWrapper>
)

export default Mission