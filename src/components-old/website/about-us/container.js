import React, { Component } from 'react'

import AboutUs from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

export class AboutUsContainer extends Component {
  render () {
    return <AboutUs />
  }
}

export default HasPermissionHOC(
  AboutUsContainer, 
  isFeatureEnabled(window.location.href).pages.aboutUs
)