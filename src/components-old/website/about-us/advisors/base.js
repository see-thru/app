import React from 'react'

import {
  GroupWrapper,
  GroupContent
} from '../team/group'
import Member from '../team/member/base'
import StyledContainerGridTeam from '../team/styled-container'
import { advisors as advisorsCatalog } from "../../../../catalogs"

const Advisors = () => (
  <GroupWrapper>
    <GroupContent>
      <StyledContainerGridTeam
        gridGap="20px"
        templateRows="auto"
        templateColumns="repeat(5, 1fr)">
        { advisorsCatalog.map( (advisor, index) => {
          return (
            <Member 
              key={ index }
              photo={ advisor.avatar }
              fullname={ advisor.name }
              position=""
              linkedin={ advisor.linkedin } />    
          )
        }) }                             
      </StyledContainerGridTeam>    
    </GroupContent>
  </GroupWrapper>
)

export default Advisors