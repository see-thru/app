import React from 'react'

import Item from './item/base'
import {
  Title,
  WrapperTitle,
  WrapperContent
} from '../../generic/pages'
import FAQData from '../../../content/faq'
import { StyledContainerGrid } from '../../generic/grid/index'

const FAQ = () => (
  <StyledContainerGrid
    gridGap="0 100px"
    grid-template-rows="1"
    templateColumns="100%">
    <WrapperTitle>
      <Title>{ FAQData.title }</Title>
    </WrapperTitle>
    <WrapperContent>
      { FAQData.questions.map( (elem, index) => {
        return (
          <div 
            id={ `question${ index + 1 }` }
            key={ index }>
            <Item 
              answer={ elem.answer }
              question={ elem.question } />
          </div>
        )
      }) }
    </WrapperContent>
  </StyledContainerGrid>
)

export default FAQ