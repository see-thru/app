import React, { Component } from 'react'

import FAQ from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

export class FAQContainer extends Component {
  render () {
    return <FAQ />
  } 
}

export default HasPermissionHOC(
  FAQContainer, 
  isFeatureEnabled(window.location.href).pages.faq
)