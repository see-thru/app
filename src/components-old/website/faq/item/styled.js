import styled from 'styled-components'
import PropTypes from 'prop-types'

import { colors } from '../../../../utils/settings'
 
const StyledItem = styled.div`
  border-bottom: 1px solid ${ colors.border };
  padding: 0 10px 10px;
  h4 {
    color: ${ colors.faqTitle };
    cursor: pointer;
    margin-bottom: 10px !important;
  } 
  p {
    font-size: 1.18rem;
    height: auto;
    margin: 0;
    max-height: ${ props => props.isOpen ? '999px' : '0' };
    transition: max-height 0.3s ease-in;
    overflow: hidden;
    visibility: ${ props => props.isOpen ? 'visible' : 'hidden' };
  }
`

StyledItem.propTypes = {
  isOpen: PropTypes.bool
}

export default StyledItem