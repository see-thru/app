import React, { Component } from 'react'
import PropTypes from 'prop-types'

import StyledItem from './styled'

class Item extends Component {
  constructor () {
    super() 
    this.state = ({ isOpen: false })
  }

  toggle = () => {
    const { isOpen } = this.state
    this.setState({ isOpen: !isOpen })
  }

  render () {
    const { isOpen } = this.state
    const { 
      answer,
      question
    } = this.props
    return (
      <StyledItem isOpen={ isOpen }>
        <h4 onClick={ this.toggle }>
          { question }
        </h4>
        <p>{ answer }</p>
      </StyledItem>
    )
  }
}

Item.propTypes = {
  answer: PropTypes.string.isRequired,
  question: PropTypes.string.isRequired
}

export default Item