import React from 'react'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import StyledWrapper from './styled-wrapper'
import StyledReactMarkdown from './styled-react-markdown'
import { PatientsWantData } from '../../../content/why-seethru'
import StyledWrapperSecondaryImage from './styled-wrapper-secondary-image'

const PatientsWant = () => (
  <StyledWrapper alignTo="right">
    <BoxInfoWrapper id="patients-want">
      <BoxInfoContent>
        <BoxInfoTitle>{ PatientsWantData.title }</BoxInfoTitle>
        <StyledReactMarkdown source={ PatientsWantData.content } />
        <StyledWrapperSecondaryImage alignTo="left">
          <img src="/images/why-seethru/graphic15.png" alt="" />
        </StyledWrapperSecondaryImage>
      </BoxInfoContent>
    </BoxInfoWrapper>
  </StyledWrapper>
)

export default PatientsWant