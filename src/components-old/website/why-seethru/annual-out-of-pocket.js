import React from 'react'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import StyledWrapper from './styled-wrapper'
import StyledReactMarkdown from './styled-react-markdown'
import { AnnualOutOfPocketData } from '../../../content/why-seethru'
import StyledWrapperSecondaryImage from './styled-wrapper-secondary-image'

const AnnualOutOfPocket = () => (
  <StyledWrapper>
    <BoxInfoWrapper id="annual-out-of-pocket-healthcare-1">
      <BoxInfoContent>
        <BoxInfoTitle>{ AnnualOutOfPocketData.title }</BoxInfoTitle>
        <StyledReactMarkdown source={ AnnualOutOfPocketData.content } />
        <StyledWrapperSecondaryImage>
          <img src="/images/why-seethru/graphic12.png" alt="" />
        </StyledWrapperSecondaryImage>
      </BoxInfoContent>
    </BoxInfoWrapper>
  </StyledWrapper>
)

export default AnnualOutOfPocket