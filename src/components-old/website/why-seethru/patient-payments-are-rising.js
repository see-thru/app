import React from 'react'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import StyledWrapper from './styled-wrapper'
import StyledReactMarkdown from './styled-react-markdown'
import StyledWrapperSecondaryImage from './styled-wrapper-secondary-image'
import { PatientPaymentsAreRisingData } from '../../../content/why-seethru'

const PatientPaymentsAreRising = () => (
  <StyledWrapper alignTo="right">
    <BoxInfoWrapper id="patient-payments-are-rising">
      <BoxInfoContent>
        <BoxInfoTitle>{ PatientPaymentsAreRisingData.title }</BoxInfoTitle>
        <StyledReactMarkdown source={ PatientPaymentsAreRisingData.content } />
        <StyledWrapperSecondaryImage alignTo="left">
          <img src="/images/why-seethru/graphic13.png" alt="" />
        </StyledWrapperSecondaryImage>
      </BoxInfoContent>
    </BoxInfoWrapper>
  </StyledWrapper>
)

export default PatientPaymentsAreRising