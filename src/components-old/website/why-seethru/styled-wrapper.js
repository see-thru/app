import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledWrapper = styled.div`
  div {
    min-height: auto;
  }
  & > div {
    margin: 20px 0;
    ${ Media.medium`
      float: initial;
    ` }
  }
  img {
    width: 100%;
  }
`

export default StyledWrapper