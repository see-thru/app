import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../utils/media'

const StyledWrapperSecondaryImage = styled.div`
  display: inline-block;
  float: ${ props => props.alignTo === 'left' ? 'left' : 'right' };
  position: relative;
  width: 40%;
  ${ Media.small`
    display: block;
    text-align: center;
    width: 100%;
  ` }  
  img {
    left: ${ props => props.alignTo === 'left' ? '40px' : '-60px' };
    display: inline-block;
    max-width: 430px;
    position: relative;
    ${ Media.small`
      left: 0;
      margin-top: 20px;
      max-width: 300px;
      position: relative;
    ` }      
  }
`

StyledWrapperSecondaryImage.defaultProps = {
  alignTo: 'right'
}

StyledWrapperSecondaryImage.propTypes = {
  alignTo: PropTypes.oneOf(['left', 'right'])
}


export default StyledWrapperSecondaryImage