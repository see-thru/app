import React from 'react'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import StyledWrapper from './styled-wrapper'
import StyledReactMarkdown from './styled-react-markdown'
import { HighMedicalCostsData } from '../../../content/why-seethru/'
import StyledWrapperSecondaryImage from './styled-wrapper-secondary-image'

const HighMedicalCosts = () => (
  <StyledWrapper>
    <BoxInfoWrapper id="high-medical-costs">
      <BoxInfoContent>
        <BoxInfoTitle>{ HighMedicalCostsData.title }</BoxInfoTitle>
        <StyledReactMarkdown source={ HighMedicalCostsData.content } />
        <StyledWrapperSecondaryImage alignTo="left">
          <img src="/images/why-seethru/graphic11.png" alt="" />
        </StyledWrapperSecondaryImage>
      </BoxInfoContent>
    </BoxInfoWrapper>
  </StyledWrapper>
)

export default HighMedicalCosts