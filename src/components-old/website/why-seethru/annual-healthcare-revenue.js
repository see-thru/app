import React from 'react'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import StyledWrapper from './styled-wrapper'
import StyledReactMarkdown from './styled-react-markdown'
import StyledWrapperSecondaryImage from './styled-wrapper-secondary-image'
import { AnnualHealthcareRevenueData } from '../../../content/why-seethru'

const AnnualHealthcareRevenue = () => (
  <StyledWrapper>
    <BoxInfoWrapper id="annual-healthcare-revenue-cycle-management-expenditure-in-the-us">
      <BoxInfoContent>
        <BoxInfoTitle>{ AnnualHealthcareRevenueData.title }</BoxInfoTitle>
        <StyledReactMarkdown source={ AnnualHealthcareRevenueData.content } />
        <StyledWrapperSecondaryImage>
          <img src="/images/why-seethru/graphic14.png" alt="" />
        </StyledWrapperSecondaryImage>
      </BoxInfoContent>
    </BoxInfoWrapper>
  </StyledWrapper>
)

export default AnnualHealthcareRevenue