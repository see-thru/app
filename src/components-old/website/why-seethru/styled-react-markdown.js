import styled from 'styled-components'
import ReactMarkdown from 'react-markdown'
import Media from '../../../utils/media';

const StyledReactMarkdown = styled(ReactMarkdown)`
  display: inline-block;
  vertical-align: top;
  width: 60%;
  ${ Media.small`
    display: block;
    width: 100%;
  ` }
`

export default StyledReactMarkdown