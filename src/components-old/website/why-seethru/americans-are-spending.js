import React from 'react'

import {
  BoxInfoTitle,
  BoxInfoWrapper,
  BoxInfoContent
} from '../../generic/box-info'
import StyledWrapper from './styled-wrapper'
import StyledReactMarkdown from './styled-react-markdown'
import StyledWrapperSecondaryImage from './styled-wrapper-secondary-image'
import { AmericansAreSpendingData } from '../../../content/why-seethru'

const AmericansAreSpending = () => (
  <StyledWrapper>
    <BoxInfoWrapper id="americans-are-spending-more">
      <BoxInfoContent>
        <BoxInfoTitle>{ AmericansAreSpendingData.title }</BoxInfoTitle>
        <StyledReactMarkdown source={ AmericansAreSpendingData.content } />
        <StyledWrapperSecondaryImage>
          <img src="/images/why-seethru/graphic10.png" alt="" />
        </StyledWrapperSecondaryImage>
      </BoxInfoContent>
    </BoxInfoWrapper>
  </StyledWrapper>
)

export default AmericansAreSpending