import React, { Component } from 'react'

import WhySeeThru from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

export class WhySeeThruContainer extends Component {
  render () {
    return <WhySeeThru />
  }
}

export default HasPermissionHOC(
  WhySeeThruContainer, 
  isFeatureEnabled(window.location.href).pages.whySeethru
)