import React from 'react'

import {
  Title,
  WrapperTitle,
  WrapperContent
} from '../../generic/pages'
import PatientsWant from './patients-want'
import HighMedicalCosts from './high-medical-costs'
import AnnualOutOfPocket from './annual-out-of-pocket'
import AmericansAreSpending from './americans-are-spending'
import AnnualHealthcareRevenue from './annual-healthcare-revenue'
import { StyledContainerGrid } from '../../generic/grid/index'
import PatientPaymentsAreRising from './patient-payments-are-rising'

const WhySeeThru = () => (
  <StyledContainerGrid
    gridGap="0 100px"
    grid-template-rows="1"
    templateColumns="100%">
    <WrapperTitle>
      <Title>Why SeeThru</Title>
    </WrapperTitle>
    <WrapperContent>
      <AmericansAreSpending />
      <HighMedicalCosts />
      <AnnualOutOfPocket />
      <PatientPaymentsAreRising />
      <AnnualHealthcareRevenue />
      <PatientsWant />
    </WrapperContent>
  </StyledContainerGrid>
)

export default WhySeeThru