import React, { Component } from 'react'

import TermsOfService from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

export class TermsOfServiceContainer extends Component {
  render () {
    return <TermsOfService />
  }
}

export default HasPermissionHOC(
  TermsOfServiceContainer, 
  isFeatureEnabled(window.location.href).pages.termsOfService
)