import React from 'react'
import ReactMarkdown from 'react-markdown'

import {
  Date,
  Title,
  WrapperTitle,
  WrapperContent
} from '../../generic/pages'
import TermsData from '../../../content/terms'
import { StyledContainerGrid } from '../../generic/grid/index'

const TermsOfService = () => (
  <StyledContainerGrid
    gridGap="0 100px"
    grid-template-rows="1"
    templateColumns="100%">
    <WrapperTitle>
      <Title>{ TermsData.title }</Title>
      <Date>{ TermsData.date }</Date>
    </WrapperTitle>
    <WrapperContent>
      <ReactMarkdown source={ TermsData.content } />
      <div id="seethru-services">
        <ReactMarkdown source={ TermsData.services } />
      </div>
      <div id="user-responsibilities">
        <ReactMarkdown source={ TermsData.userResponsibilities } />
      </div>
      <div id="website-content">
        <ReactMarkdown source={ TermsData.websiteContent } />
      </div>     
      <div id="fees-and-billing">
        <ReactMarkdown source={ TermsData.feesAndBilling } />
      </div>   
      <div id="limitation-of-liability-for-use">
        <ReactMarkdown source={ TermsData.limitationOfLiabilityForUse } />
      </div>      
      <div id="the-parties-acknowledge">
        <ReactMarkdown source={ TermsData.thePartiesAcknowledge } />
      </div>          
      <div id="representations-warranties">
        <ReactMarkdown source={ TermsData.representationsWarrantiesAndCovenants } />
      </div>        
      <div id="indemnification">
        <ReactMarkdown source={ TermsData.indemnification } />
      </div>    
      <div id="user-will-defend">
        <ReactMarkdown source={ TermsData.userWillDefend } />
      </div>     
      <div id="other-representations-and-warranties">
        <ReactMarkdown source={ TermsData.otherRepresentationsAndWarranties } />
      </div>  
      <div id="limitation-of-liability">
        <ReactMarkdown source={ TermsData.limitationOfLiability } />
      </div>     
      <div id="trademarks">
        <ReactMarkdown source={ TermsData.trademarks } />
      </div>   
      <div id="intellectual-property">
        <ReactMarkdown source={ TermsData.intellectualProperty } />
      </div>   
      <div id="confidentiality">
        <ReactMarkdown source={ TermsData.confidentiality } />
      </div> 
      <div id="termination">
        <ReactMarkdown source={ TermsData.termination } />
      </div>   
      <div id="miscellaneous">
        <ReactMarkdown source={ TermsData.miscellaneous } />
      </div>                                                                  
      <p>© 2018, SeeThru Inc. All rights reserved.</p>
    </WrapperContent>
  </StyledContainerGrid>
)

export default TermsOfService