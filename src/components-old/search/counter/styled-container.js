import styled from 'styled-components'

import { StyledContainerGrid } from '../../generic/grid'
import Media from '../../../utils/media';

const StyledCounterContainer = styled(StyledContainerGrid)`
  ${ 
    Media.small`
      padding: 0;
    ` 
  }
`

export default StyledCounterContainer