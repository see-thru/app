import React from 'react'
import PropTypes from 'prop-types'

import StyledCounter from './styled'
import StyledCounterContainer from './styled-container'

const Counter = ({ value }) => (
  <StyledCounterContainer
    gridGap="0"
    templateRows="auto"
    templateColumns="1fr">
    <StyledCounter>
      { `${ value } Providers from you` }
    </StyledCounter>
  </StyledCounterContainer>
)

Counter.defaultProps = {
  value: 0
}

Counter.propTypes = {
  value: PropTypes.number
}

export default Counter