import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledCounter = styled.div`
  background-color: ${ colors.bgCounter };
  color: white;
  text-transform: uppercase;
  text-align: center;
  padding: 20px 10px;
`

export default StyledCounter