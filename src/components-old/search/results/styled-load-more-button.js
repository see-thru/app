import styled from 'styled-components'

import { StyledButtonSuccess } from '../../generic/button/styled' 

const StyledLoadMoreButton = styled(StyledButtonSuccess)`
  display: block;
  text-transform: uppercase;
  width: 100%
`

export default StyledLoadMoreButton