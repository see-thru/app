import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Provider from './provider/container'
import StyledLoadMoreButton from './styled-load-more-button'
import StyledResultsContainer from './styled-container'

export const ResultsContainer = ({
  data: { results, total },
  location,
  onClickLoadMoreButton,
  hoveredMarker,
  onProviderHover,
}) => {
  const resultsIsAnArray = results && Array.isArray(results)
  return (
    <StyledResultsContainer>
      { 
        resultsIsAnArray &&
        results.map( (e, index) => {
          return (
            <Provider
              key={ index }
              active = { hoveredMarker && e.providerDisplayId === hoveredMarker.provider.id ? true : false }
              item={ e }
              location={ location } 
              onHover={ onProviderHover }/>
          )
        }) 
      }
      {
        resultsIsAnArray &&
        results.length < total ?
        <StyledLoadMoreButton onClick={ onClickLoadMoreButton.bind(this, results) }>	
          Load More	
        </StyledLoadMoreButton>      
        : 
        <StyledLoadMoreButton>	
          No more results
        </StyledLoadMoreButton>              
     }
    </StyledResultsContainer> 
  )
}

ResultsContainer.defaultProps = {
  data: {}
}

ResultsContainer.propTypes = {
  data: PropTypes.object,
  location: PropTypes.object,
  onClickLoadMoreButton: PropTypes.func.isRequired,
  hoveredMarker: PropTypes.object
}

const mapStateToProps = (state) => ({
  data: state.SearchReducer.data,
  hoveredMarker: state.SearchReducer.hoveredMarker
})

export default connect(
  mapStateToProps
)( ResultsContainer )