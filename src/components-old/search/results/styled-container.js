import styled from 'styled-components'

const StyledResultsContainer = styled.div`
  margin-bottom: 20px;
`

export default StyledResultsContainer