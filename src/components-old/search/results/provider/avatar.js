import React from 'react'
import PropTypes from 'prop-types'

import StyledImage from './styled-avatar'
import GenerateRandomProfilePicture from '../../../../utils/generate-random-profile-picture'

const Avatar = ({
  name,
  picture
}) => {
  return (
    <div>
      <StyledImage
        src={ picture ? picture : GenerateRandomProfilePicture() }
        alt={ `profile picture of ${ name }` }
        width="80"
        height="80" />
    </div>
  )
}

Avatar.propTypes = {
  name: PropTypes.string.isRequired,
  picture: PropTypes.string
}

export default Avatar