import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../utils/settings'

const StyledAmount = styled.span`
  color: ${ colors.successButton };
  font-size: 1.12rem;
  font-weight: ${ fonts.weightBold };
`

export default StyledAmount