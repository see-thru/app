import styled from 'styled-components'
import PropTypes from 'prop-types'

import { colors } from '../../../../utils/settings'

const StyledProvider = styled.div`
  background-color: ${ props => props.active ? colors.bgFields : 'white' };
  border-bottom: 1px solid ${ colors.border };
  display: flex;
  margin: 10px 0;
  padding: 10px;
  &:hover {
    background-color: ${ colors.bgFields };
  }
`

StyledProvider.propTypes = {
  active: PropTypes.bool
}

export default StyledProvider