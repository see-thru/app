import React from 'react'
import PropTypes from 'prop-types'

import Info from './info/base'
import Avatar from './avatar'
import StyledProvider from './styled-provider'

const Provider = ({
  item,
  location,
  active,
  onHover
}) => {    
  const fullName = `${ item.provider.firstName } ${ item.provider.lastName }`
  const seethruIndex = item.provider.seethruIndex ? item.provider.seethruIndex : '-'
  return (
    <StyledProvider 
      active={ active }
      onMouseOver={ function(){ onHover(item) } }>
      <Avatar 
        name={ fullName }
        picture={ item.provider.photo } /> 
      <Info
        name={ fullName }
        displayId={ item.providerDisplayId }
        distance="1.3 miles"
        location={ location }
        specialty={ item.provider.specialty }
        seethruIndex={ seethruIndex }
        hasAvailableTime={ item.hasAvailableTime } />
    </StyledProvider>
  )
}

Provider.propTypes = {
  item: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    provider: PropTypes.shape({
      photo: PropTypes.string,
      lastName: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      specialty: PropTypes.shape({
        primary: PropTypes.string
      })
    }).isRequired,
    providerDisplayId: PropTypes.string.isRequired,
    distance: PropTypes.string,
    seethruIndex: PropTypes.string,
    hasAvailableTime: PropTypes.bool
  }).isRequired,
  location: PropTypes.object
}

export default Provider