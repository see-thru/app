import styled from 'styled-components'

import { colors } from '../../../../../utils/settings'

const StyledNotAvailableTag = styled.span`
  background-color: ${ colors.lightBorder };
  border-radius: 2px;
  color: ${ colors.textDate };
  display: inline-block;
  font-size: .8rem;
  margin-top: 2px;
  padding: 2px 6px; 
`

export default StyledNotAvailableTag