import styled from 'styled-components'

const StyledSeeThruIndexImg = styled.img`
  margin-right: 5px;
`

export default StyledSeeThruIndexImg