import React from 'react'
import qs from 'qs'
import PropTypes from 'prop-types'

import StyledInfo from './styled'
import StyledData from './styled-data'
import StyledTitle from './styled-title'
import StyledSeeThruIndexImg from './styled-seethru-index-img'
import StyledNotAvailableTag from './styled-not-available-tag'

const Info = ({
  name,
  distance,
  location,
  displayId,
  specialty,
  seethruIndex,
  hasAvailableTime
}) => {
  const parsed = qs.parse(location.search)
  return (
    <StyledInfo>
      <StyledTitle to={ `provider/${ displayId }${ parsed.what ? `/${ parsed.what }` : '/_' }` }>
        { name }
      </StyledTitle>
      <StyledData>
        <StyledSeeThruIndexImg 
          src="/images/seethru-isotype.png" 
          width="12"
          height="12" />
        { seethruIndex } | { specialty.primary }
      </StyledData>
      { !hasAvailableTime ? 
        <StyledNotAvailableTag>
          Not Available 
          { parsed.from !== 'undefined' && parsed.from ? ` from: ${ parsed.from }` : undefined }
          { parsed.to !== 'undefined' && parsed.to ? ` to: ${ parsed.to }` : undefined }
        </StyledNotAvailableTag> 
        : undefined 
      }      
    </StyledInfo>
  )
}

Info.defaultProps = {
  specialty: {
    primary: ''
  }
}

Info.propTypes = {
  name: PropTypes.string.isRequired,
  distance: PropTypes.string,
  displayId: PropTypes.string.isRequired,
  specialty: PropTypes.shape({
    primary: PropTypes.string
  }),
  seethruIndex: PropTypes.string,
  hasAvailableTime: PropTypes.bool
}

export default Info