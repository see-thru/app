import styled from 'styled-components'
import { Link } from 'react-router-dom' 

import Media from '../../../../../utils/media'
import { 
  fonts,
  colors 
} from '../../../../../utils/settings'

const StyledTitle = styled(Link)`
  color: ${ colors.textTitle };
  font-size: 1.1rem;
  font-weight: ${ fonts.weightBold };
  margin: 5px 0;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
  ${
    Media.medium`
      font-size: 1rem;
    `
  }
  ${
    Media.small`
      font-size: .9rem;
    `
  }
`

export default StyledTitle