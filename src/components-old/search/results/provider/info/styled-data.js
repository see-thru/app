import styled from 'styled-components'

import { colors } from '../../../../../utils/settings'

const StyledDistance = styled.h3`
  color: ${ colors.textDate };
  font-size: .9rem;
  font-weight: 400;
  margin: 5px 0;
`

export default StyledDistance