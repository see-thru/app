import styled from 'styled-components'

const StyledInfo = styled.div`
  padding: 0 20px;
`

export default StyledInfo