import React from 'react'
import PropTypes from 'prop-types'

import StyledPrice from './styled-price'
import StyledAmount from './styled-price-amount'

const Price = ({
  amount
}) => (
  <StyledPrice>
    <StyledAmount>${ amount }</StyledAmount>
  </StyledPrice>
)

Price.propTypes = {
  amount: PropTypes.number.isRequired
}

export default Price