import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledImage = styled.img`
  border-radius: 100%;
  height: 80px;
  width: 80px;
  ${
    Media.small`
      height: 60px;
      width: 60px;
    `
  }
`

export default StyledImage