import styled from 'styled-components'

const StyledPrice = styled.div`
  align-self: center;
  margin-left: auto;
  padding: 0 20px;
`

export default StyledPrice