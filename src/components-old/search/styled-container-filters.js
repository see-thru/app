import styled from 'styled-components'

import { StyledContainerGrid } from '../generic/grid/index'

const StyledContainerFilters = styled(StyledContainerGrid)`
  padding-left: 0;
  padding-right: 0;
`

export default StyledContainerFilters