import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../utils/settings'
import { MaxWidth } from '../../../utils/media';

const StyledInfoWindow = styled.div`
  img, 
  h3 {
    display: inline-block;
    vertical-align: middle;
  }
  h3 {
    color: ${ colors.textTitle };
    font-size: .9rem;
    margin: 4px 8px 0;
  } 
  a {
    background-color: ${ colors.successButton };
    border-radius: 2px;
    color: white;
    display: block;
    font-size: .8rem;
    font-weight: ${ fonts.weightBold };
    margin-top: 10px;
    padding: 4px 6px;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    width: 100px;
  }
  ${ MaxWidth(600)`
    text-align: center;
  `}
`

export default StyledInfoWindow