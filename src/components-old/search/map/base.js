import React from 'react'
import PropTypes from 'prop-types'
import {
  Map, 
  Marker,
  InfoWindow,
  GoogleApiWrapper
} from 'google-maps-react'

import StyledInfoWindow from './styled-info-window'
import generateLocationsObject from '../../../utils/generate-locations-object'
import GenerateRandomProfilePicture from '../../../utils/generate-random-profile-picture'

export const MapWrapper = ({
  google,
  results,
  onDragend,
  onMapReady,
  onMouseoverMarker,
  latFilter,
  lngFilter,
  whatFilter,
  activeMarker,
  onMapClicked,
  dataProvider,
  initialCenter,
  onMarkerClick,
  selectedPlace,
  fromProviderPage,
  showingInfoWindow,
  activeProvider
}) => {
  let locations, location
  locations = generateLocationsObject(results)
  if ( fromProviderPage && dataProvider.office ) {
    locations = generateLocationsObject([dataProvider])
  }
  if ( latFilter && lngFilter ) {
    location = { lat: latFilter, lng: lngFilter }
  }
  let center  
  if ( activeProvider && activeProvider.office && activeProvider.office.location ) {
    center = { lat: activeProvider.office.location.lat, lng: activeProvider.office.location.lng }
  }
  return (
    <Map 
    zoom={ 10 }
    center={ activeProvider ? center : location }
    google={ google }
    onReady={ onMapReady }
    onClick={ onMapClicked }
    onDragend={ onDragend }
    initialCenter={ initialCenter }>
      { locations && locations.map( (location, index) => {
        if ( activeProvider && location.provider.id === activeProvider.providerDisplayId  ) console.log(activeProvider)
        return (
          <Marker
            id={ location.provider.id }
            key={ index }
            icon={{
              url: activeProvider && location.provider.id === activeProvider.providerDisplayId ? "/images/map/marker-active.png" : "/images/map/marker.png",
              anchor: new google.maps.Point(20,20),
              scaledSize: new google.maps.Size(25,30)
            }} 
            onClick={ onMarkerClick }
            position={ location }
            onMouseover={ (marker, map) => onMouseoverMarker(marker, map, location) } />
        )
      }) }
      { fromProviderPage ? 
        undefined : 
        <InfoWindow
          marker={ activeMarker }
          visible={ showingInfoWindow }>
          <StyledInfoWindow>
            <img
              alt=""
              src={ selectedPlace.photo ? selectedPlace.photo : GenerateRandomProfilePicture() }
              width="30"
              height="30" />
            <h3>{ selectedPlace.name }</h3>
            <a href={ `/provider/${ selectedPlace.id }${ whatFilter ? `/${ whatFilter }` : '/_' }` }>
              View Profile
            </a>
          </StyledInfoWindow>
        </InfoWindow>      
      }
    </Map>
  )
}

MapWrapper.defaultProps = {
  results: []
}

MapWrapper.propTypes = {
  results: PropTypes.array,
  onDragend: PropTypes.func.isRequired,
  onMapReady: PropTypes.func.isRequired,
  latFilter: PropTypes.string,
  lngFilter: PropTypes.string,
  whatFilter: PropTypes.string,
  dataProvider: PropTypes.object,
  initialCenter: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired
  }),
  selectedPlace: PropTypes.object,
  fromProviderPage: PropTypes.bool
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyDWgOvd8bl0exL0LKfc7r6vQPqvqOT4hi0'
})(MapWrapper)