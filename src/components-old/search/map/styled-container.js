import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledMapContainer = styled.div`
  display: ${ props => props.isMobile === 'true' ? 'none' : 'block' };
  height: ${ props => props.height };
  max-height: ${ props => props.maxHeight };
  margin-top: 20px;
  position: relative;
  ${ Media.small`
    display: ${ props => props.isMobile === 'true' ? 'block' : 'none' };
    display: ${ props => props.fromProviderPage ? 'block' : undefined };
    margin-top: 0;
  ` }
`

export default StyledMapContainer