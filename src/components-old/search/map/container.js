import React, { Component } from 'react'
import qs from 'qs'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { hoveredMarkerChanged } from '../../../actions-old/search'

import MapWrapper from './base'
import StyledMapContainer from './styled-container'

export class MapContainer extends Component {
  constructor () {
    super() 
    this.state = ({
      initialCenter: {
        lat: 40.7128,
        lng: -74.0060
      },
      activeMarker: {},
      selectedPlace: {},      
      showingInfoWindow: false
    })
    this.onMarkerClick = this.onMarkerClick.bind(this)
    this.onMapClicked = this.onMapClicked.bind(this) 
    this.onDragend = this.onDragend.bind(this)
    this.onMapReady = this.onMapReady.bind(this)
    this.onMouseoverMarker = this.onMouseoverMarker.bind(this)
  }

  onMarkerClick (props, marker, e) {
    this.setState({
      selectedPlace: props.position.provider,
      activeMarker: marker,
      showingInfoWindow: true
    })
  }
  
  onMapClicked () {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  }

  onMouseoverMarker (marker, map, hoveredMarker) {    
    const { hoveredMarkerChanged } = this.props
    hoveredMarkerChanged(hoveredMarker)    
  }

  onDragend ( mapProps, map ) {
    const { history } = this.props
    const parsed = qs.parse(history.location.search)
    const to = parsed.to ? parsed.to : ''
    const from = parsed.from ? parsed.from : ''
    const what = parsed.what ? parsed.what : ''
    const bounds = map.getBounds();
    const lat = map.center.lat()
    const lng = map.center.lng()
    let distance = '30miles'
    if (bounds) {
      const ne = bounds.getNorthEast();
      // radius = radius of the earth in statute miles
      const radius = 3963.0;  
      // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
      const lat1 = lat / 57.2958; 
      const lng1 = lng / 57.2958;
      const lat2 = ne.lat() / 57.2958;
      const lng2 = ne.lng() / 57.2958;
      // distance = circle radius from center to Northeast corner of bounds
      const viewportDistance = radius * Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lng2 - lng1));
      distance = `${viewportDistance}miles`
    }
    history.push(`/search?&what=${ what }&from=${ from }&to=${ to }&lat=${ lat }&lng=${ lng }&dis=${distance}`)	
  }  

  onMapReady = (mapProps, map) => {
    const that = this;
    mapProps.google.maps.event.addListener(map, 'zoom_changed', function() {
        that.onDragend(mapProps, map)  
    });
    window.onresize = () => {
      const currCenter = map.getCenter()
      mapProps.google.maps.event.trigger(map, 'resize')
      map.setCenter(currCenter)
      this.onDragend(mapProps, map)
    };
  };

  render () {
    const {
      data : { results },
      height,
      location,
      isMobile,
      maxHeight,
      dataProvider,
      fromProviderPage,
      activeProvider
    } = this.props
    const parsed = qs.parse(location.search)
    return (
      <StyledMapContainer
        height={ height } 
        isMobile={ isMobile }
        maxHeight={ maxHeight }
        fromProviderPage={ fromProviderPage }>
        <MapWrapper 
          { ...this.state }
          results={ results }
          onDragend={ this.onDragend }
          onMapReady={ this.onMapReady }
          onMouseoverMarker={ this.onMouseoverMarker }
          latFilter={ parsed.lat }
          lngFilter={ parsed.lng }
          whatFilter={ parsed.what }
          dataProvider={ dataProvider }
          onMapClicked={ this.onMapClicked }
          onMarkerClick={ this.onMarkerClick }
          fromProviderPage={ fromProviderPage }
          activeProvider={ activeProvider } />
      </StyledMapContainer>      
    )
  }
}

MapContainer.defaultProps = {
  data: {},
  height: '100%',
  location: {},
  maxHeight: '900px',
  dataProvider: {},
  fromProviderPage: false,
  activeProvider: {}
}

MapContainer.propTypes = {
  data: PropTypes.object,
  height: PropTypes.string,
  isMobile: PropTypes.string,
  location: PropTypes.object,
  maxHeight: PropTypes.string,
  dataProvider: PropTypes.object,
  fromProviderPage: PropTypes.bool,
  activeProvider: PropTypes.object,
  hoveredMarkerChanged: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  data: state.SearchReducer.data,
  dataProvider: state.ProviderReducer.data,
  activeProvider: state.SearchReducer.activeProvider
})

export default connect(
  mapStateToProps,
  {
    hoveredMarkerChanged
  }
)( MapContainer )