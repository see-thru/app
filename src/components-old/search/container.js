import React, { Component } from 'react'
import { connect } from 'react-redux'
import qs from 'qs'
import PropTypes from 'prop-types'

import { 
  searchProviders, 
  activeProviderChanged 
} from '../../actions-old/search'

import Search from './base'
import isFeatureEnabled from '../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../has-permission'
import generateDataForSearch from '../../utils/generator-data-structure/search-providers'

export class SearchContainer extends Component {
  
  constructor(props){
    super(props)
    this.onProviderHover = this.onProviderHover.bind(this)
  }

  componentWillMount () {
    const { 
      location, 
      searchProviders
    } = this.props
    const parsed = qs.parse(location.search)
    const data = generateDataForSearch(parsed.to, parsed.from, parsed.what, parsed.lat, parsed.lng, parsed.dis)
    searchProviders(data)
  }

  componentWillReceiveProps (nextProps) {
    const { 
      location,
      searchProviders
    } = nextProps
    if ( location !== this.props.location ) {
      const parsed = qs.parse(location.search)
      const data = generateDataForSearch(parsed.to, parsed.from, parsed.what, parsed.lat, parsed.lng, parsed.dis)
      searchProviders(data)
    }
  }

  onClickLoadMoreButton = (resultsShown) => {
    const { 
      location, 
      searchProviders
    } = this.props
    const parsed = qs.parse(location.search)
    const data = generateDataForSearch(parsed.to, parsed.from, parsed.what, parsed.lat, parsed.lng, parsed.dis)
    searchProviders(data, resultsShown.length)
  }

  onProviderHover = activeProvider => {
    const { activeProviderChanged } = this.props
    activeProviderChanged(activeProvider)      
  }

  render () {
    return (
      <Search
        onClickLoadMoreButton={ this.onClickLoadMoreButton }      
        onProviderHover = { this.onProviderHover }  
        { ...this.props } />
    )
  }
}

SearchContainer.defaultProps = {
  data: {},
  history: {},
  location: {},
  activeProvider: null
}

SearchContainer.propTypes = {
  data: PropTypes.object,
  loading: PropTypes.bool,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  searchProviders: PropTypes.func.isRequired,
  activeProviderChanged: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  data: state.SearchReducer.data,
  loading : state.SearchReducer.loading
})

export default HasPermissionHOC(
  connect(
    mapStateToProps,
    { 
      searchProviders, 
      activeProviderChanged
    }
  )( SearchContainer ), 
  isFeatureEnabled(window.location.href).pages.searchProviders
)