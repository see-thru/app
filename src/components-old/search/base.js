import React from 'react'
import PropTypes from 'prop-types'

import Loader from '../generic/loader/base'
import Counter from './counter/base'
import MapContainer from './map/container'
import StyledSearch from './styled'
import ResultsContainer from './results/container'
import StyledContainerSearchPage from './styled-container'
import { 
  FilterWhat,
  FilterWhen,
  FiltersWrapper 
} from '../generic/filters'

const Search = ({
  data,
  loading,
  history,
  location,
  onClickLoadMoreButton,
  onProviderHover
}) => (
  <StyledSearch>
    { loading ? <Loader /> : undefined }
    <FiltersWrapper>
      <FilterWhat history={ history } />
      <FilterWhen history={ history } />
    </FiltersWrapper>
    <MapContainer 
      height="40vh"
      isMobile="true"
      location={ location } />     
    <Counter value={ data.total } />
    <StyledContainerSearchPage
      gridGap="50px 10px"
      templateRows="auto"
      templateColumns="repeat(2, 1fr)">
      <ResultsContainer 
        location={ location }        
        onClickLoadMoreButton={ onClickLoadMoreButton }
        onProviderHover = { onProviderHover } />
      <MapContainer 
        height="800px"
        history={ history }
        location={ location } />
    </StyledContainerSearchPage>
  </StyledSearch>
)

Search.defaultProps = {
  data: {}
}

Search.propTypes = {
  data: PropTypes.object,
  history: PropTypes.object,
  location: PropTypes.object,
  onClickLoadMoreButton: PropTypes.func.isRequired
}

export default Search