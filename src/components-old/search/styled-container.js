import styled from 'styled-components'

import Media from '../../utils/media'
import { StyledContainerGrid } from '../generic/grid/index'

const StyledContainerSearchPage = styled(StyledContainerGrid)`
  ${ Media.small`grid-template-columns: repeat(1, 1fr)` }
`

export default StyledContainerSearchPage