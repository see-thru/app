import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-modal'

import SignUpContent from './content'
import { register, toggleSignUpModal, toggleSignInModal } from '../../../actions-old/auth'

export class SignUpModalContainer extends Component {
  constructor () {
    super()
    this.onSubmit = this.onSubmit.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.showSignInModal = this.showSignInModal.bind(this)
  }

  onSubmit (e) {
    console.log('submitting signup', e)
    this.props.register(e)
  }

  closeModal () {
    this.props.toggleSignUpModal(false)
  }

  showSignInModal () {
    this.closeModal()
    this.props.toggleSignInModal(true)
  }

  render () {
    const { isOpen, errors } = this.props
    
    return (
      <Modal
          className="Modal__Bootstrap modal-dialog modal-dialog-centered"
          closeTimeoutMS={150}
          errors={ errors }
          isOpen={ isOpen }
          onRequestClose={this.closeModal}
          showSignInModal={ this.showSignInModal }
          style={ { overlay: { zIndex: 100 } } }
        >
          <SignUpContent
            errors={ errors }
            onSubmit={ this.onSubmit }
            showSignInModal={ this.showSignInModal } />
        </Modal>
    )
  }
}

SignUpModalContainer.propTypes = {
  isOpen: PropTypes.bool,
  errors: PropTypes.object,
  register: PropTypes.func.isRequired,
  toggleSignUpModal: PropTypes.func.isRequired,
  toggleSignInModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isOpen : state.AuthReducer.signUpModalIsOpen,
  errors : state.AuthReducer.errors
})

export default connect(
  mapStateToProps,
  { 
    register,
    toggleSignUpModal,
    toggleSignInModal
  }
)( SignUpModalContainer )