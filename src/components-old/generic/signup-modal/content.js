import React from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'
import Form from './form'
import { ErrorMessage } from '../../generic/form'
import { SubmitButton } from '../elements/buttons'

const ModalContent = styled.div`
  background-color: #00aeee;
  color: white;
  .modal-body {
    padding: 24px;
    font-size: 14px;
    .modal-content-text {
      margin-bottom: 20px;
    }
    p {
      margin: 0;
      line-height: 19px;
      font-weight: 600;
    }
    form {
      .form-text {
        color: #eee !important;
      }
    }
  }
`

const SignUpHeader = styled.div`
  display: flex;
  justify-content: space-between;
  h1 {
    flex: 1;
    font-size: 45px;
    font-weight: 800;
  }
  img {
    width: 130px;
    height: 40px;
    object-fit: contain;
  }
`

const ModalFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 25px;
`

const ModalLink = styled.button`
  background: transparent;
  border: 0;
  border-bottom: 2px solid white;
  color: white;
  padding: 0 2px;
  &:hover {
    border-color: #a6e3ff;
    color: #a6e3ff;
  }
`

const SignUpContent = ({
  errors,
  onSubmit,
  showSignInModal
}) => (
  <ModalContent className="modal-content">
    <div className="modal-body">
      <SignUpHeader>
        <h1>Sign Up</h1>
        <img 
          src="/images/header-logo.png" 
          alt="Seethru Logo"
          height="40" />
      </SignUpHeader>
      <div className="modal-content-text">
        <p>Not feeling well? Need a doctor?</p>
        <p>Find guaranteed quality and prices for your care.</p>
      </div>
      { errors.signup && <ErrorMessage>{ errors.signup }</ErrorMessage> }
      <Form onSubmit={ onSubmit } />
      <ModalFooter>
        <div>
          <p>Already registered? <ModalLink onClick={showSignInModal}>Log in</ModalLink></p>
        </div>
        <SubmitButton type="submit" form="signUpForm">Sign Up</SubmitButton>
      </ModalFooter>
    </div>
  </ModalContent>
)

SignUpContent.propTypes = {
  errors: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  showSignInModal: PropTypes.func.isRequired
}

export default SignUpContent