import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'
import { date, email, length, required, confirmation } from 'redux-form-validators'

import { Input, FormWrapper } from '../form/index'

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper id="signUpForm"
    onSubmit={ handleSubmit( onSubmit ) }>
    <Field
      title="Email address"
      name="email"
      component={ Input }
      type="email"
      validate={ [
        email(),
        length({ max: 255 }),
        required()
      ] }
    />
    <Field
      title="First name"
      name="firstName"
      component={ Input }
      validate={ [
        required()
      ] }
    />
    <Field
      title="Last name"
      name="lastName"
      component={ Input }
      validate={ [
        required()
      ] }
    />    
    <Field
      title="Password"
      name="password"
      placeholder="Password (min. 6 characters)"
      component={ Input }
      type="password"
      validate={ [
        length({ minimum: 6 }),
        required()
      ] }
    />
    <Field
      title="Confirm Password"
      name="confirm"
      component={ Input }
      type="password"
      validate={ [
        length({ minimum: 6 }),
        required(),
        confirmation({ field: 'password' })
      ] }
    />
    <Field
      title="Birthday"
      name="dob"
      placeholder="MM/DD/YYYY"
      component={ Input }
      validate={ [
        date({ format: 'mm/dd/yyyy' }),
        required(),
      ] }
    />
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form: 'signUpForm'
})( Form )