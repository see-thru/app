import styled from 'styled-components'
import { colors, fonts } from '../../../utils/settings'

export const SubmitButton = styled.button`
  background-color: ${ colors.darkButton }; 
  border: none;
  border-radius: 4px;
  color: white;
  font-size: 13px;
  font-weight: ${ fonts.weightBold };
  transition: .35s background-color;
  padding: 8px 32px;
  line-height: 20px;
  margin: 0 10px;
  &:hover {
    background-color: ${ colors.bgMissionBox };
    color: white;
  }
`