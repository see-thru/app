import styled from 'styled-components'
import PropTypes from 'prop-types'

import StyledButton from './styled-button'
import { colors } from '../../../utils/settings'

const StyledButtonCustom = styled(StyledButton)`
  background-color: ${ props => props.bg };
  color: white;
`

StyledButtonCustom.defaultProps = {
  bg: colors.darkButton
}

StyledButtonCustom.propTypes = {
  bg: PropTypes.string
}

export default StyledButtonCustom