import styled from 'styled-components'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { createElement as e } from 'react'

const StyledDefaultButton = styled(({tag, children, ...props}) => e(tag, props, children))`
  background-color: transparent;
  border: 0;
  border-radius: 0;
  text-decoration: none;
  &:focus {
    outline: 0;
  }
`

StyledDefaultButton.defaultProps = {
 tag: 'button'
}

StyledDefaultButton.propTypes = {
  tag: PropTypes.oneOf(['button', 'a', Link])
}

export default StyledDefaultButton