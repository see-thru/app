import styled from 'styled-components'

import { colors } from '../../../utils/settings'
import StyledButton from './styled-button'

const StyledButtonSuccess = styled(StyledButton)`
  background-color: ${ colors.successButton };
  color: white;
`

export default StyledButtonSuccess