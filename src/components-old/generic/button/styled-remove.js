import styled from 'styled-components'

import { colors } from '../../../utils/settings'
import StyledButton from './styled-button'

const StyledButtonRemove = styled(StyledButton)`
  background-color: ${ colors.alert };
  color: white;
`

export default StyledButtonRemove