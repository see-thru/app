import styled from 'styled-components'

import StyledDefaultButton from './styled-default'

const StyledButton = styled(StyledDefaultButton)`
  border-radius: 2px;
  padding: 14px;
`

export default StyledButton