import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'

const StyledTitle = styled.h2`
  color: ${ colors.textTitle };
  font-size: 6rem;
  line-height: 1;
  margin: 0;
  text-align: center;
  ${
    Media.medium`
      font-size: 3rem;
    `
  }
  ${
    Media.small`
      font-size: 2rem;
    `
  }
`

export default StyledTitle