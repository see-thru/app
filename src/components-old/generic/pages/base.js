import React from 'react'
import PropTypes from 'prop-types'

import StyledWrapperTitle from './styled'

const WrapperTitle = ({ 
  children,
  withBottomBorder
}) => (
  <StyledWrapperTitle withBottomBorder={ withBottomBorder }>
    { children }
  </StyledWrapperTitle>
)

WrapperTitle.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object,
  ]),
  withBottomBorder: PropTypes.string
}

export default WrapperTitle