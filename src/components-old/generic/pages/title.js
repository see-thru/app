import React from 'react'
import PropTypes from 'prop-types'

import StyledTitle from './styled-title'

const Title = ({ children }) => (
  <StyledTitle>
    { children }
  </StyledTitle>
)

Title.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object,
  ])
}

export default Title