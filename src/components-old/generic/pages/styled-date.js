import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledDate = styled.p`
  background-color: ${ colors.bgDate };
  font-size: .9rem;
  margin: 20px auto 0;
  max-width: 300px;
  text-align: center;
  padding: 10px 0;
  width: 100%;
`

export default StyledDate