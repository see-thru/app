import Date from './date'
import Title from './title'
import WrapperTitle from './base'
import WrapperContent from './content'

export {
  Date,
  Title,
  WrapperTitle,
  WrapperContent
}