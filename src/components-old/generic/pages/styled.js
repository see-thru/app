import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'

const StyledWrapperTitle = styled.div`
  border-bottom: 1px solid;
  border-bottom-color: ${ props => props.withBottomBorder === 'no' ? 'transparent' : colors.border };
  margin: 60px 0 40px;
  padding: 0 10px 40px;
  ${
    Media.medium`
      margin: 40px 0;
    `
  }
  p {
    font-size: 1.5rem;
    text-align: center;
  } 
`

StyledWrapperTitle.propTypes = {
  withBottomBorder: PropTypes.string
}

export default StyledWrapperTitle