import React from 'react'
import PropTypes from 'prop-types'

import StyledWrapperContent from './styled-content'

const WrapperContent = ({ children }) => (
  <StyledWrapperContent>
    { children }
  </StyledWrapperContent>
)

WrapperContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object,
  ])
}

export default WrapperContent