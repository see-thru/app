import React from 'react'
import PropTypes from 'prop-types'

import StyledDate from './styled-date'

const Date = ({ children }) => (
  <StyledDate>
    Last Update: { children }
  </StyledDate>
)

Date.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object,
  ])
}

export default Date