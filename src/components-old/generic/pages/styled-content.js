import styled from 'styled-components'

import Media from '../../../utils/media'
import { fonts, colors } from '../../../utils/settings'

const StyledWrapperContent = styled.div`
  margin-bottom: 80px;
  padding: 0 10px;
  ${
    Media.medium`
      margin-bottom: 40px;
    `
  }
  a {
    color: ${ colors.successButton };
    text-decoration: none;
  }
  h4, 
  strong {
    font-weight: ${ fonts.weightBold };
  }
  h4 {
    font-size: 1.25rem;
    margin: 20px 0;
    ${
      Media.medium`
        font-size: 1rem;
      `
    }
  }
  p {
    ${
      Media.medium`
        font-size: .9rem;
      `
    }
  }
`

export default StyledWrapperContent