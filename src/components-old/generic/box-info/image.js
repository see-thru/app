import React from 'react'
import PropTypes from 'prop-types'

import StyledBoxInfoImage from './styled-image'

const BoxInfoImage = ({
  src,
  position
}) => (
  <StyledBoxInfoImage 
    src={ src }
    position={ position } />
)

BoxInfoImage.propTypes = {
  src: PropTypes.string.isRequired,
  position: PropTypes.oneOf(['left top', 'right top', 'right bottom'])
}

export default BoxInfoImage