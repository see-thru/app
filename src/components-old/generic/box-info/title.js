import React from 'react'
import PropTypes from 'prop-types'

import StyledBoxInfoTitle from './styled-title'

const BoxInfoTitle = ({ children }) => (
  <StyledBoxInfoTitle>
    { children }
  </StyledBoxInfoTitle>
)

BoxInfoTitle.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object
  ])
}

export default BoxInfoTitle