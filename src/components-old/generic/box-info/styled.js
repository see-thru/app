import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'

const StyledBoxInfoWrapper = styled.div`
  position: relative;
  ${ props => props.type === 'aboutUs' ? `
      border-radius: 8px;
      margin: 10px auto;   
      & > div {
        background-color: transparent;
      }
    ` : `
      margin: 200px auto 0;
    ` 
  }
  ${ props => props.section === 'mission' && `
    background-color: ${ colors.bgMissionBox };
    border: 0;
    color: white;
    h4 {
      color: white;
      margin: 0 0 30px;
      &:after {
        content: '';
        display: block;
        height: 8px;
        background: ${ colors.borderMissionBox };
        width: 120px;
        margin: 10px auto 0;
      }
    }
    p {
      font-size: 1.3rem;
      text-align: center;
    }
  ` }
  ${ props => props.section === 'culture' && `
    border: 0;
    color: ${ colors.bgHowWeDoIt }
    h4 {
      margin: 0;
      text-align: left;
    }
    p, 
    ul {
      font-size: 1.3rem;
    }  
    ul {
      list-style: none;
      padding-left: 0;
    }
    li {
      display: inline-block;
      margin-bottom: 3px;
      margin-right: 1%;
      margin-top: 3px;
      vertical-align: top;
      width: 49%;      
      &:before {
        background-color: ${ colors.successButton };
        border-radius: 100%;
        content: '';
        display: inline-block;
        height: 10px;
        margin-right: 10px;
        vertical-align: middle;
        width: 10px;
      }
    }  
  ` }
  ${
    Media.medium`
      margin-top: 20px;
      width: 100%;
    `
  }
  h4 {
    font-size: 2rem;
    ${
      Media.small`
        font-size: 1.6rem;
      `
    }
  }
  strong {
    font-size: 1.21rem;
    font-weight: 700;
    ${
      Media.medium`
        font-size: 1rem;
      `
    }
  }
  p {
    margin-bottom: 0;
  }
  p,
  ul {
    ${
      Media.medium`
        font-size: .9rem;
      `
    }
  }
  li {
    ${ Media.medium`
      display: block; 
      margin-right: 0; 
      width: 100%;
    `}
  }
`

export default StyledBoxInfoWrapper