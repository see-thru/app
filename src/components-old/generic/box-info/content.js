import React from 'react'
import PropTypes from 'prop-types'

import StyledBoxInfoContent from './styled-content'

const BoxInfoContent = ({ children }) => (
  <StyledBoxInfoContent>{ children }</StyledBoxInfoContent>
) 

BoxInfoContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object
  ])
}

export default BoxInfoContent