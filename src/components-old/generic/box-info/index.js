import BoxInfoTitle from './title'
import BoxInfoImage from './image'
import BoxInfoWrapper from './base'
import BoxInfoContent from './content'

export {
  BoxInfoTitle,
  BoxInfoImage,
  BoxInfoWrapper,
  BoxInfoContent
}