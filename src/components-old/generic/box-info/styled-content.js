import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'

const StyledBoxInfoContent = styled.div`
  background-color: white;
  padding: 50px;
  ${
    Media.medium`
      min-height: 280px;
      padding: 20px;
    `
  }
  ${
    Media.small`
      clear: both;
      min-height: auto;
    `
  }
  img {
    &[alt='main-image'] {
      border: 1px solid ${ colors.text };
      border-radius: 10px;
      max-width: 650px;
    }
  } 
`

export default StyledBoxInfoContent