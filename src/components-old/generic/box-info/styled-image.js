import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../utils/media'

const StyledBoxInfoImage = styled.img`
  position: absolute;
  z-index: 1;
  ${
    Media.medium`
      bottom: auto;
      height: auto;
      left: auto;
      margin: 40px 20px 20px;
      position: relative;
      right: auto;
      top: auto;
      width: 220px;
      max-width: 100%;
      z-index: 3;
      ${ props => {
        if ( props.position === 'left top' ) { 
          return `float: left`
        }
        if ( props.position === 'right top' || props.position === 'right bottom' ) { 
          return `float: right`
        }
      }}
    `
  }
  ${
    Media.small`
      float: none;
      margin: 20px 0 0 20px;
    `
  }
  ${ props => {
    if ( props.position === 'left top' ) { 
      return `
        height: 346px;
        right: 80%;
        top: -160px;
        width: 346px;
      `
    }
    if ( props.position === 'right top' ) { 
      return `
        height: 346px;
        left: 70%;
        top: -160px;
        width: 346px;
      `
    }
    if ( props.position === 'right bottom' ) { 
      return `
        bottom: -40px;
        height: 160px;
        left: 70%;
        width: 340px;
      `
    }
  }}
`

StyledBoxInfoImage.propTypes = {
  position: PropTypes.oneOf(['left top', 'right top', 'right bottom'])
}

export default StyledBoxInfoImage