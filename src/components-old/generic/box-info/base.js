import React from 'react'
import PropTypes from 'prop-types'

import StyledBoxInfoWrapper from './styled'

const BoxInfoWrapper = ({ 
  id, 
  type,
  section,
  children 
}) => (
  <StyledBoxInfoWrapper 
    id={ id }
    type={ type }
    section={ section }>
    { children }
  </StyledBoxInfoWrapper>
)

BoxInfoWrapper.defaultProps = {
  id: '',
  type: '',
  section: ''
}

BoxInfoWrapper.propTypes = {
  id: PropTypes.string,
  type: PropTypes.string,
  section: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object
  ])
}

export default BoxInfoWrapper