import styled from 'styled-components'

const StyledTitle = styled.h4`
  text-align: center;
  margin-bottom: 40px;
`

export default StyledTitle