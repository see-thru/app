import React from 'react'
import PropTypes from 'prop-types'

import StyledIcon from './styled'

const Icon = (
  props 
) => (
  <StyledIcon 
    { ...props }
    className={ props.name }>
    <use xlinkHref={ `#${ props.name }` }></use>
  </StyledIcon>
)

Icon.defaultProps = {
  fill: 'black',
  display: 'inline-block'
}

Icon.propTypes = {
  fill: PropTypes.string,
  name: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  display: PropTypes.string
}

export default Icon