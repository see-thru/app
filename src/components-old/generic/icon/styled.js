import styled from 'styled-components'

const StyledIcon = styled.svg`
  display: { props => props.display };
  fill: { props => props.fill };
  height: { props => props.height };
  width: { props => props.width };
`

export default StyledIcon