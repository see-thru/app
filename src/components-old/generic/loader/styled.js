import styled from 'styled-components'

const StyledLoader = styled.div`
  align-items: center;
  background-color: rgba(255,255,255,.8);
  display: flex;
  left: 0;
  height: 100%;
  justify-content: center;
  top: 0;
  position: fixed;
  width: 100%;
  z-index: 11;
`

export default StyledLoader