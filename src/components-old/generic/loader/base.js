import React from 'react'

import Icon from '../icon/base'
import StyledLoader from './styled'

const Loader = () => (
  <StyledLoader>
    <Icon 
      name="loader" 
      width="34" 
      height="40" />
  </StyledLoader>
)

export default Loader