import React from 'react'
import PropTypes from 'prop-types'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../modal/index'
import Icon from '../icon/base'
import Form from './form'

const SubscribeProviderModal = ({
  isOpen,
  onSubmit,
  closeModal
}) => (
  <ModalContainer
    width="600px"
    height="auto"
    isOpen={ isOpen }
    closeModal={ closeModal }>
    <StyledModalHeader>
      <StyledModalHeaderTitle>Register provider</StyledModalHeaderTitle>
      <StyledModalHeaderCloseButton onClick={ closeModal }>
        <Icon
          fill="#ccc"
          width="16"
          name="cancel"
          height="16" />
      </StyledModalHeaderCloseButton>
    </StyledModalHeader>
    <StyledModalContent>
      <Form onSubmit={ onSubmit } />
    </StyledModalContent>
  </ModalContainer>
)

SubscribeProviderModal.propTypes = {
  isOpen: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired
}

export default SubscribeProviderModal