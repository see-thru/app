import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { createProvider } from '../../../actions-old/provider'
import { toggleSubscribeProviderModal } from '../../../actions-old/mailchimp'

import SubscribeProviderModal from './base'
import generateDataForCreateToProvider from '../../../utils/generator-data-structure/create-to-provider'

export class SubscribeProviderModalContainer extends Component {
  onSubmit = (e) => {
    const { createProvider } = this.props
    const data = generateDataForCreateToProvider(e)
    createProvider(data)
  }

  closeModal = () => {
    const { toggleSubscribeProviderModal } = this.props
    toggleSubscribeProviderModal(false)
  }

  render () {
    const { isOpen } = this.props
    return (
      <SubscribeProviderModal 
        isOpen={ isOpen }
        onSubmit={ this.onSubmit }
        closeModal={ this.closeModal } />
    )
  }
}

SubscribeProviderModalContainer.propTypes = {
  isOpen: PropTypes.bool,
  createProvider: PropTypes.func.isRequired,
  toggleSubscribeProviderModal: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  isOpen : state.MailchimpReducer.subscribeProviderModalIsOpen
})

export default connect(
  mapStateToProps,
  { 
    createProvider,
    toggleSubscribeProviderModal
  }
)( SubscribeProviderModalContainer )