import React from 'react'
import PropTypes from 'prop-types'
import {
  Field,
  reduxForm
} from 'redux-form'
import {
  email,
  required
} from 'redux-form-validators'

import {
  Input,
  FormWrapper
} from '../form/index'

import { StyledButtonSuccess } from '../button/styled'

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper
    onSubmit={ handleSubmit( onSubmit ) }>
    <Field
      title="First Name"
      name="firstName"
            component={ Input }
      validate={[
        required()
      ]}
    />
    <Field
      title="Last Name"
      name="lastName"
            component={ Input }
      validate={[
        required()
      ]}
    />
    <Field
      title="Zip Code"
      name="zip"
      component={ Input }
      validate={[
        required()
      ]}
    />
    <Field
      title="Email Address"
      name="email"
            component={ Input }
      type="email"
      validate={ [
        email(),
        required()
      ] }
    />
    <Field
      title="Phone"
      name="phone"
            component={ Input }
    />
    <StyledButtonSuccess
      type="submit">
      Register
    </StyledButtonSuccess>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'providerForm'
})( Form )