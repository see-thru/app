import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../modal/index'
import Icon from '../icon/base'
import Form from './form'
import { ErrorMessage } from '../../generic/form'
import StyledMessageSuccess from '../subscribe-general-modal/status-messages/styled-message-success'

const ForgotPasswordModal = ({
  isOpen,
  errors,
  onSubmit,
  closeModal,
  messagePasswordResetEmail,
  showForgotPasswordModal
}) => (
  <ModalContainer    
    width="600px"
    height="auto"
    isOpen={ isOpen }
    closeModal={ closeModal }>
    <StyledModalHeader>
      <StyledModalHeaderTitle>Forgot Password</StyledModalHeaderTitle>
      <StyledModalHeaderCloseButton onClick={ closeModal }>
        <Icon 
          fill="#ccc"
          width="16"
          name="cancel"
          height="16" />
      </StyledModalHeaderCloseButton>
    </StyledModalHeader>
    <StyledModalContent>
      <Fragment>
        { messagePasswordResetEmail && messagePasswordResetEmail.successMessage ? <StyledMessageSuccess>{ messagePasswordResetEmail.successMessage }</StyledMessageSuccess> : undefined }
        { errors.sendPasswordEmail ? <ErrorMessage>{ errors.sendPasswordEmail }</ErrorMessage> : undefined }
        <Form onSubmit={ onSubmit } />
          Back To
          <button onClick={ showForgotPasswordModal }>
            Log in
          </button>
      </Fragment>
    </StyledModalContent>
  </ModalContainer>
)

ForgotPasswordModal.propTypes = {
  isOpen: PropTypes.bool,
  errors: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  messagePasswordResetEmail: PropTypes.shape({
    successMessage: PropTypes.string
  }).isRequired,
  showForgotPasswordModal: PropTypes.func.isRequired
}

export default ForgotPasswordModal