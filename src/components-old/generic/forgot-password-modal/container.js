import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { 
  toggleSignInModal,
  sendPasswordResetEmail,
  toggleForgotPasswordModal 
} from '../../../actions-old/auth'

import ForgotPasswordModal from './base'

export class ForgotPasswordModalContainer extends Component {
  constructor () {
    super()
    this.onSubmit = this.onSubmit.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.showForgotPasswordModal = this.showForgotPasswordModal.bind(this)
  }

  onSubmit (e) {
    const { sendPasswordResetEmail } = this.props
    sendPasswordResetEmail(e.email)
  }

  closeModal () {
    const { toggleForgotPasswordModal } = this.props
    toggleForgotPasswordModal(false)
  }

  showForgotPasswordModal () {
    const { toggleSignInModal } = this.props
    this.closeModal()
    toggleSignInModal(true)
  }

  render () {
    const { 
      isOpen,
      errors,
      messagePasswordResetEmail
    } = this.props
    return (
      <ForgotPasswordModal 
        isOpen={ isOpen }
        errors={ errors }
        onSubmit={ this.onSubmit }
        closeModal={ this.closeModal }
        messagePasswordResetEmail={ messagePasswordResetEmail }
        showForgotPasswordModal={ this.showForgotPasswordModal } />
    )
  }
}

ForgotPasswordModalContainer.propTypes = {
  isOpen: PropTypes.bool,
  errors: PropTypes.object,
  toggleSignInModal: PropTypes.func.isRequired,
  sendPasswordResetEmail: PropTypes.func.isRequired,
  messagePasswordResetEmail: PropTypes.shape({
    successMessage: PropTypes.string
  }).isRequired,
  toggleForgotPasswordModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isOpen : state.AuthReducer.resetModalIsOpen,
  errors : state.AuthReducer.errors,
  messagePasswordResetEmail: state.AuthReducer.sendPasswordEmail
})

export default connect(
  mapStateToProps,
  { 
    toggleSignInModal,
    sendPasswordResetEmail,
    toggleForgotPasswordModal
  }
)( ForgotPasswordModalContainer )