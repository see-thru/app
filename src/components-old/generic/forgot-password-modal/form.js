import React from 'react'
import PropTypes from 'prop-types'
import {
  Field,
  reduxForm
} from 'redux-form'
import {
  email,
  length,
  required,
} from 'redux-form-validators'

import {
  Input,
  FormWrapper
} from '../form/index'

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper
    onSubmit={ handleSubmit( onSubmit ) }>
    <Field
      title="Email Address"
      name="email"
      component={ Input }
      type="email"
      validate={ [
        email(),
        length({ max: 255 }),
        required()
      ] }
    />
    <button type="submit">
      Send reset link
    </button>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'forgetPasswordForm'
})( Form )