import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledGridItem = styled.div`
  align-self: ${ props => props.alignSelf };
  grid-row: ${ props => props.gridRow };
  grid-area: ${ props => props.gridArea };
  grid-column: ${ props => props.gridColumn };
  justify-self: ${ props => props.justifySelf };
`;

StyledGridItem.propTypes = {
  alignSelf: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([
      'auto',
      'stretch',
      'center',
      'flex-start',
      'flex-end',
      'baseline',
      'initial',
      'inherit'
    ])
  ]),
  gridRow: PropTypes.string,
  gridArea: PropTypes.string,
  gridColumn: PropTypes.string,
  justifySelf: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([
      'stretch', 
      'center', 
      'start', 
      'end'
    ])
  ])
}

export default StyledGridItem