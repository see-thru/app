import PropTypes from 'prop-types'
import styled from 'styled-components'

import { sizes } from '../../../utils/media'

const StyledContainer = styled.div`
  justify-content: ${ props => props.justifyContent };
  margin: 0 auto;
  max-width: ${ sizes.giant }px;
  padding-left: 20px;
  padding-right: 20px;
  width: 100%;
`;

StyledContainer.propTypes = {
  justifyContent: PropTypes.string
}

export default StyledContainer
