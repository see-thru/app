import PropTypes from 'prop-types'
import styled from 'styled-components'

import StyledContainer from './styled-container'

const StyledContainerGrid = styled(StyledContainer)`
  display: grid;
  grid-gap: ${ props => props.gridGap };
  grid-template-rows: ${ props => props.templateRows };
  grid-template-areas: ${ props => props.templateAreas };
  grid-template-columns: ${ props => props.templateColumns };
  justify-items: ${ props => props.justifyItems };
`;

StyledContainerGrid.propTypes = {
  gridGap: PropTypes.string,
  templateRows: PropTypes.string,
  templateAreas: PropTypes.string,
  templateColumns: PropTypes.string,
  justifyItems: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([
      'stretch', 
      'center', 
      'start', 
      'end'
    ])
  ])
}

export default StyledContainerGrid