import React, { Component } from 'react'
import qs from 'qs'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { toggleResetPasswordModal } from '../../../actions-old/auth'

import ResetPasswordModal from './base'

export class ResetPasswordModalContainer extends Component {
  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    const parsed = qs.parse(props.location.search) 
    if ( parsed.resetPassword ) {
      this.openModal()
    }
  }

  onSubmit (e) {
    const { location } = this.props
    const parsed = qs.parse(location.search)
    const token = parsed.token
    console.log('token', token)
    console.log(e)
  }

  closeModal () {
    const { 
      history,
      toggleResetPasswordModal 
    } = this.props
    toggleResetPasswordModal(false)
    history.push('/')
  }

  openModal () {
    const { toggleResetPasswordModal } = this.props
    toggleResetPasswordModal(true)
  }

  render () {
    const { 
      isOpen,
      errors
    } = this.props
    return (
      <ResetPasswordModal 
        isOpen={ isOpen }
        errors={ errors }
        onSubmit={ this.onSubmit }
        closeModal={ this.closeModal } />
    )
  }
}

ResetPasswordModalContainer.propTypes = {
  isOpen: PropTypes.bool,
  errors: PropTypes.object,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  location: PropTypes.object,
  toggleResetPasswordModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isOpen : state.AuthReducer.resetPasswordModalIsOpen,
  errors : state.AuthReducer.errors
})

export default connect(
  mapStateToProps,
  { 
    toggleResetPasswordModal
  }
)( ResetPasswordModalContainer )