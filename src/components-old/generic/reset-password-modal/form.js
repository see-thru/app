import React from 'react'
import PropTypes from 'prop-types'
import {
  Field,
  reduxForm
} from 'redux-form'
import {
  length,
  required,
  confirmation
} from 'redux-form-validators'

import {
  Input,
  FormWrapper
} from '../form/index'

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper
    onSubmit={ handleSubmit( onSubmit ) }>
    <Field
      title="Password"
      name="password"
      placeholder="minimum 6 characters"
      component={ Input }
      type="password"
      validate={ [
        length({ minimum: 6 }),
        required()
      ] }
    />
    <Field
      title="Confirm Password"
      name="confirm"
      placeholder=""
      component={ Input }
      type="password"
      validate={ [
        length({ minimum: 6 }),
        required(),
        confirmation({ field: 'password' })
      ] }
    />
    <button type="submit">
      Send reset link
    </button>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'forgetPasswordForm'
})( Form )