import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../modal/index'
import Icon from '../icon/base'
import Form from './form'
import { ErrorMessage } from '../../generic/form'

const ResetPasswordModal = ({
  isOpen,
  errors,
  onSubmit,
  closeModal
}) => (
  <ModalContainer    
    width="600px"
    height="auto"
    isOpen={ isOpen }
    closeModal={ closeModal }>
    <StyledModalHeader>
      <StyledModalHeaderTitle>Reset Password</StyledModalHeaderTitle>
      <StyledModalHeaderCloseButton onClick={ closeModal }>
        <Icon 
          fill="#ccc"
          width="16"
          name="cancel"
          height="16" />
      </StyledModalHeaderCloseButton>
    </StyledModalHeader>
    <StyledModalContent>
      <Fragment>
        { errors.resetPassword ? <ErrorMessage>{ errors.resetPassword }</ErrorMessage> : undefined }
        <Form onSubmit={ onSubmit } />
      </Fragment>
    </StyledModalContent>
  </ModalContainer>
)

ResetPasswordModal.propTypes = {
  isOpen: PropTypes.bool,
  errors: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired
}

export default ResetPasswordModal