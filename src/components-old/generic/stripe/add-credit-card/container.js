import React from 'react'
import {
  Elements,
  StripeProvider
} from 'react-stripe-elements'
import PropTypes from 'prop-types'

import { SplitForm } from './form'
import { STRIPE_API_KEY } from '../../../../constants/stripe'

const StripeAddCreditCardContainer = ({ onSubmit }) => (
  <StripeProvider apiKey={ STRIPE_API_KEY }>
    <Elements>
      <SplitForm onSubmit={ onSubmit } />
    </Elements>
  </StripeProvider>  
)

StripeAddCreditCardContainer.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

export default StripeAddCreditCardContainer