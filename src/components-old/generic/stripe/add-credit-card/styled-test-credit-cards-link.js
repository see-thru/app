import styled from 'styled-components'

import { colors, fonts } from '../../../../utils/settings'

const StyledTestCreditCardsLink = styled.a`
  display: block;
  color: ${ colors.placeholderFields };
  clear: both;
  font-size: .9rem;
  font-weight: ${ fonts.weightBold };
  text-decoration: none;
  text-align: right;
`

export default StyledTestCreditCardsLink