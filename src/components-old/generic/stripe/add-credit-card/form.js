import React from 'react'

import {
  injectStripe,  
  CardCVCElement,
  CardNumberElement,
  CardExpiryElement,
  PostalCodeElement
} from 'react-stripe-elements'

import { 
  Label,
  FieldWrapper,
  ErrorMessage
} from '../../form'
import isFeatureEnabled from '../../../../utils/permission/is-feature-enabled'
import { StyledButtonSuccess } from '../../button/styled'
import StyledAddCreditCardForm from './styled'
import StyledTestCreditCardsLink from './styled-test-credit-cards-link'

const handleBlur = () => {
  console.log('[blur]');
};
const handleFocus = () => {
  console.log('[focus]');
};
const handleReady = () => {
  console.log('[ready]');
};

const createOptions = (fontSize: string, padding: ?string) => {
  return {
    style: {
      base: {
        fontSize,
        color: '#333',
        letterSpacing: '0.025em',
        '::placeholder': {
          color: '#999',
        },
        ...(padding ? {padding} : {}),
      },
      invalid: {
        color: '#bd2a2a',
      },
    },
  };
};

class _SplitForm extends React.Component<InjectedProps & {fontSize: string}> {
  constructor () {
    super() 
    this.state = ({
      cardCvc: null,
      cardNumber: null,
      cardExpiry: null,
      postalCode: null
    })
  }

  handleChange = (change) => {
    let error = undefined
    if ( change.error && change.error.message ) error = change.error.message
    switch (change.elementType) {
      case 'cardNumber':
        this.setState({ cardNumber: error })
        break;
      case 'cardExpiry':
        this.setState({ cardExpiry: error })
        break;
      case 'cardCvc':
        this.setState({ cardCvc: error })
        break;
      case 'postalCode':
        this.setState({ postalCode: error })
        break;
      default:
        break;
    }
  }

  handleSubmit = ev => {
    const { onSubmit } = this.props
    ev.preventDefault();
    const name = document.getElementById("card-holder-name").value;
    if (this.props.stripe) {
      this.props.stripe
        .createToken({ name })
        .then( response => {
          console.log('[token]', response)
          if ( response.token ) {
            onSubmit( 
              response.token.id, 
              response.token.card.last4, 
              response.token.card.name,
              response.token.card.exp_month,
              response.token.card.exp_year
            )
          }
        });
    } else {
      console.log("Stripe.js hasn't loaded yet.");
    }
  }

  render() {
    const { 
      cardCvc,
      cardNumber,
      cardExpiry,
      postalCode
    } = this.state
    return (
      <StyledAddCreditCardForm>
      <form onSubmit={this.handleSubmit}>
        <FieldWrapper>
          <Label for="name">Name</Label>
          <input 
            id="card-holder-name" 
            name="name" 
            type="text" 
            placeholder="The name as it appears on the card"
            required />          
        </FieldWrapper>
        <FieldWrapper>
          <Label>Card Number</Label>
          <CardNumberElement
            onChange={ this.handleChange }
            onBlur={handleBlur}
            onFocus={handleFocus}
            onReady={handleReady}
            {...createOptions(this.props.fontSize)} />
          { cardNumber ? <ErrorMessage>{ cardNumber }</ErrorMessage> : undefined }
        </FieldWrapper>
        { isFeatureEnabled(window.location.href).feature.testCreditCardsList ? 
          <StyledTestCreditCardsLink 
            rel="noopener noreferrer"
            href="https://stripe.com/docs/testing#cards" 
            target="_blank">
            Test credit cards
          </StyledTestCreditCardsLink>
          : undefined }
        <FieldWrapper>
          <Label>Expiration date</Label>
          <CardExpiryElement
            onBlur={handleBlur}
            onChange={this.handleChange}
            onFocus={handleFocus}
            onReady={handleReady}
            {...createOptions(this.props.fontSize)}
          />
          { cardExpiry ? <ErrorMessage>{ cardExpiry }</ErrorMessage> : undefined }
        </FieldWrapper>
        <FieldWrapper>
          <Label>CVC</Label>
          <CardCVCElement
            onBlur={handleBlur}
            onChange={this.handleChange}
            onFocus={handleFocus}
            onReady={handleReady}
            {...createOptions(this.props.fontSize)}
          />
          { cardCvc ? <ErrorMessage>{ cardCvc }</ErrorMessage> : undefined }
        </FieldWrapper>
        <FieldWrapper>
          <Label>Postal Code</Label>
          <PostalCodeElement
            onBlur={handleBlur}
            onChange={this.handleChange}
            onFocus={handleFocus}
            onReady={handleReady}
            {...createOptions(this.props.fontSize)}
          />
          { postalCode ? <ErrorMessage>{ postalCode }</ErrorMessage> : undefined }
        </FieldWrapper>
        <StyledButtonSuccess type="submit">
          Save
        </StyledButtonSuccess>
      </form>
      </StyledAddCreditCardForm>
    );
  }
}

export const SplitForm = injectStripe(_SplitForm);