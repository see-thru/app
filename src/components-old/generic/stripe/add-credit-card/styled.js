import styled from 'styled-components'
import { colors } from '../../../../utils/settings';

const StyledAddCreditCardForm = styled.div`
  input,
  .StripeElement {
    display: block;
    margin: 0;
    padding: 16px 14px;
    font-size: 1em;
    border: 0;
    outline: 0;
    border-radius: 2px;
    background: ${ colors.bgFields };
    width: 100%;
  }
`

export default StyledAddCreditCardForm