import styled from 'styled-components'

const StyledModalContentTitle = styled.p`
  font-size: 1.2rem;
  margin-top: 0;
`

export default StyledModalContentTitle