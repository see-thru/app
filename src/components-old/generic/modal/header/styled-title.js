import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../utils/settings'

const StyledModalHeaderTitle = styled.h4`
  color: ${ colors.successButton };
  font-size: 1.12rem;
  font-weight: ${ fonts.weightBold };
  margin: 0;
  text-transform: uppercase;
  padding: 20px 50px 15px 20px;
`

export default StyledModalHeaderTitle