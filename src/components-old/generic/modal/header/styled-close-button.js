import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledModalHeaderCloseButton = styled.button`
  background-color: transparent;
  border: 0;
  cursor: pointer;
  right: 18px;
  top: 20px;
  position: absolute;
  ${ Media.small`
    right: 6px;
  ` }
`

export default StyledModalHeaderCloseButton