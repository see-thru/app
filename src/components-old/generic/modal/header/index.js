import StyledModalHeader from './styled-base'
import StyledModalHeaderTitle from './styled-title'
import StyledModalHeaderCloseButton from './styled-close-button'

export {
  StyledModalHeader,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
}