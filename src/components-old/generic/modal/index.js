import ModalContainer from './container'
import {
  StyledModalHeader,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from './header/index'
import StyledModalContent from './styled-content'
import StyledModalContentTitle from './styled-content-title'

export {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalContentTitle,
  StyledModalHeaderCloseButton
}