import styled from 'styled-components'

const StyledModalContainer = styled.div`
  align-items: ${ props => props.alignItems };
  background-color: rgba(0,0,0,.7);
  content: '';
  cursor: pointer;
  display: flex;
  height: 100%;
  justify-content: ${ props => props.justifyContent };
  left: 0;
  position: fixed;
  top: 0;
  overflow: auto;
  width: 100%;
  z-index: 10;
`

export default StyledModalContainer