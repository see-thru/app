import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledModal = styled.div`
  background-color: white;
  border-radius: 4px;
  box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
  cursor: auto;
  height: ${ props => props.height };
  margin: 10px;
  min-height: ${ props => props.minHeight };
  max-width: calc(100vw - 20px);
  overflow-y: auto;
  width: ${ props => props.width };
  ${ Media.small`
    max-height: calc(100vh - 20px);
  ` }
`

export default StyledModal