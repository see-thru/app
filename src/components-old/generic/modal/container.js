import React from 'react'
import PropTypes from 'prop-types'

import Modal from './base'
import StyledModalContainer from './styled-container'

const ModalContainer = ({
  width,
  height,
  isOpen,
  children,
  minHeight,
  closeModal,
  alignItems,
  justifyContent
}) => {
  if ( !isOpen ) return null
  return (
    <StyledModalContainer
      alignItems={ alignItems }
      justifyContent={ justifyContent }>
      <Modal 
        width={ width }
        height={ height }  
        minHeight={ minHeight }
        closeModal={ closeModal }>
        { children }
      </Modal>
    </StyledModalContainer>
  )
} 

ModalContainer.defaultProps = {
  alignItems: 'flex-start',
  justifyContent: 'center'
}

ModalContainer.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  isOpen: PropTypes.bool,
  children: PropTypes.node.isRequired,
  minHeight: PropTypes.string,
  closeModal: PropTypes.func.isRequired,
  alignItems: PropTypes.string,
  justifyContent: PropTypes.string
}

export default ModalContainer