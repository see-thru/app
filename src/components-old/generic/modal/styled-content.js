import styled from 'styled-components'

const StyledModalContent = styled.div`
  padding: 0 20px 20px;
`

export default StyledModalContent