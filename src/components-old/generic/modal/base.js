import React, { Component } from 'react'
import PropTypes from 'prop-types'
import onClickOutside from "react-onclickoutside"

import StyledModal from './styled-base'

export class Modal extends Component {
  handleClickOutside (evt) {
    const { 
      closeModal 
    } = this.props
    closeModal()
  }

  render () {
    const {
      width,
      height,
      children,
      minHeight
    } = this.props
    return (
      <StyledModal
        width={ width }
        height={ height }
        minHeight={ minHeight }>
        { children }
      </StyledModal>
    )
  }
}

Modal.defaultProps = {
  width: 'calc(100vw - 40px)',
  height: 'calc(100vh - 40px)',
  minHeight: 'auto'
}

Modal.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  children: PropTypes.node.isRequired,
  minHeight: PropTypes.string,
  closeModal: PropTypes.func.isRequired
}

export default onClickOutside(Modal)