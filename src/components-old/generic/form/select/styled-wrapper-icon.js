import styled from 'styled-components'

const StyledIconWrapper = styled.div`
  right: 15px;
  top: 16px;
  position: absolute;
`

export default StyledIconWrapper