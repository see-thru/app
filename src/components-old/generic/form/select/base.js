import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../icon/base'
import Label from '../label/base'
import FieldWrapper from '../wrappers/field/base'
import ErrorMessage from '../error-message/base'
import StyledSelect from './styled'
import StyledIconWrapper from './styled-wrapper-icon'
import StyledSelectWrapper from './styled-wrapper'

const Select = ({
  meta,
  name,
  input,
  title,
  options,
  hasLabel
}) => (
  <FieldWrapper>
    { hasLabel ?
      <Label
        htmlFor={ name }>
        { title }
      </Label>
      : undefined
    }
    <StyledSelectWrapper>
      <StyledSelect
        id={ name }
        { ...input }>
        { options.map( (option, index) => {
          return (
            <option
              value={ option.value }
              key={ index }>
              { option.name }
            </option>
          )
        }) }
      </StyledSelect>
      <StyledIconWrapper>
        <Icon 
          name="select-arrow"
          width="10px" 
          height="6px" />
      </StyledIconWrapper>
    </StyledSelectWrapper>
    { meta.touched && meta.error ?
      <ErrorMessage>
        { meta.error }
      </ErrorMessage>
      : undefined
    }
  </FieldWrapper>
)

Select.defaultProps = {
  hasLabel: true
}

Select.propTypes = {
  meta: PropTypes.object.isRequired,
  name: PropTypes.string,
  input: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  })).isRequired,
  hasLabel: PropTypes.bool
}

export default Select