import styled from 'styled-components'

import Media from '../../../../utils/media'
import { colors } from '../../../../utils/settings'

const StyledSelect = styled.select`
  appearance: none;
  background-color: ${ colors.bgFields };
  border: 0;
  border-radius: 2;
  color: ${ colors.text };
  font-size: 1rem;
  height: 55px;
  padding: 0 35px 0 15px;
  width: 100%;
  ${ Media.small`font-size: 1rem` }
  & option:first {
    color: #ddd;
  }
`

export default StyledSelect