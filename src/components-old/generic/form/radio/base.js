import React     from 'react'
import PropTypes from 'prop-types'

import Label from '../label/base'
import StyledRadio from './styled'
import FieldWrapper from '../wrappers/field/base'
import ErrorMessage from '../error-message/base'
import StyledWrapperCheckbox from '../checkbox/styled-wrapper'

const Radio = ({
  meta,
  input,
  title,
  hasLabel
}) => (
  <FieldWrapper>
    <StyledWrapperCheckbox>
      <input
        id={ `${ input.name }[${ title }]` }
        type="radio"
        checked={ input.value }
        { ...input }
      />
      <StyledRadio></StyledRadio>
      { hasLabel &&
        <Label
          htmlFor={ `${ input.name }[${ title }]` }>
          { title }
        </Label>
      }
    </StyledWrapperCheckbox>
    { meta.touched && meta.error ?
      <ErrorMessage>
        { meta.error }
      </ErrorMessage>
      : undefined
    }
  </FieldWrapper>
)

Radio.defaultProps = {
  hasLabel: true
}

Radio.propTypes = {
  meta: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  hasLabel: PropTypes.bool
}

export default Radio