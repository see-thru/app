import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import StyledCheckboxAndRadio from '../styled-checkbox-and-radio'

const StyledRadio = styled(StyledCheckboxAndRadio)`
  border: 1px solid  ${ colors.border };
  border-radius: 100%;
`

export default StyledRadio