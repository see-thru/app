import React from 'react'

import PropTypes from 'prop-types'

import Label from '../label/base'
import FieldWrapper from '../wrappers/field/base'
import ErrorMessage from '../error-message/base'
import StyledInputDatePicker from './styled'

const InputDatePicker = ({
  meta,
  title,
  input,
  hasLabel,
}) => (
  <FieldWrapper>
    { hasLabel ?
      <Label
        htmlFor={ input.name }>
        { title }
      </Label>
      : undefined 
    }
    <StyledInputDatePicker
      id={ input.name }
      { ...input } />
    { meta.touched && meta.error ?
      <ErrorMessage>
        { meta.error }
      </ErrorMessage>
      : undefined
    }
  </FieldWrapper>
)

InputDatePicker.defaultProps = {
  type: 'text',
  hasLabel: true,
  readOnly: false
}

InputDatePicker.propTypes = {
  meta: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  hasLabel: PropTypes.bool
}

export default InputDatePicker
