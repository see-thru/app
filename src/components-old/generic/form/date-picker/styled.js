import styled from 'styled-components'
import DatePicker from 'react-date-picker'

import { colors } from '../../../../utils/settings'

const StyledInputDatePicker = styled(DatePicker)`
  background-color: ${ colors.bgFields };
  border: 0;
  border-radius: 2px;
  color: ${ colors.text };
  height: 55px;
  width: 100%;
  .react-date-picker__button {
    border: 0;
    padding: 0 15px;
    width: 100%;
  }
  .react-calendar {
    bottom: 55px;
    position: absolute;
  }
  & * {
    outline: 0;
  }
`

export default StyledInputDatePicker