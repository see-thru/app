import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { fonts, colors } from '../../../../utils/settings'

const StyledError = styled.span`
  color: ${ colors.alert };
  display: block;
  font-size: .8rem;
  font-weight: ${ fonts.weightBold };
  margin: 5px 0;
`

const ErrorMessage = ({ children }) => (
  <StyledError>
    { children }
  </StyledError>
)

ErrorMessage.propTypes = {
  children: PropTypes.node.isRequired
}

export default ErrorMessage
