import styled from 'styled-components'

const StyledFileName = styled.span`
  display: block;
  margin-bottom: 0px;
  font-size: .9rem;
  color: #999;
`

export default StyledFileName
