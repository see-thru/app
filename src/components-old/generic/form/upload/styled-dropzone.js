import styled from 'styled-components'
import Dropzone from 'react-dropzone'

import { colors } from '../../../../utils/settings'

const StyledDropzone = styled(Dropzone)`
  background-color: white;
  border: 1px solid ${ colors.border };
  color: #999;
  cursor: pointer;
  font-size: .9rem;
  margin: 20px 0;
  text-align: center;
  padding: 40px 20px 20px;
`

export default StyledDropzone