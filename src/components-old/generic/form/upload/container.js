import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import Icon from '../../icon/base'
import StyledList from './styled-list'
import ErrorMessage from '../error-message/base'
import StyledDropzone from './styled-dropzone'
import StyledFileName from './styled-file-name'
import StyledUploadContainer from './styled'

const UploadContainer = ({
  meta: {
    touched,
    error
  },
  name,
  input,
  accept,
  multiple,
  placeholder
}) => (
  <StyledUploadContainer>
    <StyledDropzone
      name={ name }
      accept={ accept }
      onDrop={ ( filesToUpload, e ) => input.onChange(filesToUpload) }           
      multiple={ multiple }
      className="dropzone-input">
      <Fragment>
        <Icon 
          name="upload" 
          width="60" 
          height="50px" />
        <p>{ placeholder }</p>
      </Fragment>
    </StyledDropzone>
    { touched && error ? 
      <ErrorMessage>{error}</ErrorMessage> 
      : undefined
    }
    { input.value && Array.isArray(input.value) && (
      <StyledList>
        { input.value.map( (file, i) => {
          return (
            <li key={ i }>
              <StyledFileName>
                { file.name ? file.name : `File #${ i }` }
              </StyledFileName>
            </li>
          )
        } )}
      </StyledList>
    ) }
  </StyledUploadContainer>
)

UploadContainer.defaultProps = {
  accept: 'image/*',
  multiple: true,
  placeholder: 'This dropzone accepts only images. Try dropping some here, or click to select files to upload.'
}

UploadContainer.propTypes = {
  name: PropTypes.string,
  input: PropTypes.object.isRequired,
  multiple: PropTypes.bool,
  placeholder: PropTypes.string
}

export default UploadContainer