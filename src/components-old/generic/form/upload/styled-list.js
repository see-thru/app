import styled from 'styled-components'

const StyledList = styled.ul`
  list-style: none;
  padding: 0;
`

export default StyledList