import styled from 'styled-components'

const StyledUploadContainer = styled.div`
  clear: both;
`

export default StyledUploadContainer