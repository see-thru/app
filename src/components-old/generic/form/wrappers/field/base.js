import React from 'react'
import PropTypes from 'prop-types'

import StyledFieldWrapper from './styled'

const FieldWrapper = ({ 
  children 
}) => (
  <StyledFieldWrapper>
    { children }
  </StyledFieldWrapper>
)

FieldWrapper.propTypes = {
  children: PropTypes.node.isRequired
}

export default FieldWrapper
