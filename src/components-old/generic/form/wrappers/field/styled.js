import styled from 'styled-components'

const StyledFieldWrapper = styled.div`
  clear: both;
  float: left;
  margin: 10px 0;
  width: 100%;
`

export default StyledFieldWrapper