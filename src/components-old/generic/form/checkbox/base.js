import React     from 'react'
import PropTypes from 'prop-types'

import Icon from '../../icon/base'
import Label from '../label/base'
import FieldWrapper from '../wrappers/field/base'
import ErrorMessage from '../error-message/base'
import StyledCheckbox from './styled'
import StyledWrapperCheckbox from './styled-wrapper'

const Checkbox = ({
  meta,
  input,
  title,
  hasLabel
}) => (
  <FieldWrapper>
    <StyledWrapperCheckbox>
      <input
        id={ input.name }
        type="checkbox"
        checked={ input.value }
        { ...input }
      />
      <StyledCheckbox>
        <Icon 
          name="checkbox"
          width="15px"
          height="11px" />
      </StyledCheckbox>
      { hasLabel &&
        <Label
          htmlFor={ input.name }>
          { title }
        </Label>
      }
    </StyledWrapperCheckbox>
    { meta.touched && meta.error ?
      <ErrorMessage>
        { meta.error }
      </ErrorMessage>
      : undefined
    }
  </FieldWrapper>
)

Checkbox.defaultProps = {
  hasLabel: true
}

Checkbox.propTypes = {
  meta: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  hasLabel: PropTypes.bool
}

export default Checkbox