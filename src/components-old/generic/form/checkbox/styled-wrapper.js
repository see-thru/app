import styled from 'styled-components'
import { colors } from '../../../../utils/settings';

const StyledWrapperCheckbox = styled.div`
  min-height: 30px;
  position: relative;
  & > * { 
    display: inline-block;
    vertical-align: middle;
  }
  label {
    cursor: pointer;
    margin-bottom: 0;
    padding-left: 40px;
    position: relative;
    z-index: 2;
  }
  input {
    left: 0;
    height: 1px;
    position: absolute;
    visibility: hidden;
    width: 1px;
    &:checked ~ div {
      background-color: ${ colors.darkLink }
      & svg {
        transform: scale(1);
      }
    }
  }
`

export default StyledWrapperCheckbox