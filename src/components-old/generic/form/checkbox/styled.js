import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import StyledCheckboxAndRadio from '../styled-checkbox-and-radio'

const StyledCheckbox = styled(StyledCheckboxAndRadio)`
  background-color: ${ colors.bgFields };
  border-radius: 2px;
  svg {
    left: 7px;
    position: relative;
    top: 3px;
    transform: scale(0);
    transition: .6s transform; 
  }
`

export default StyledCheckbox