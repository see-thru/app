import styled from 'styled-components'

const StyledCheckboxAndRadio = styled.div`
  display: block;
  height: 30px;
  left: 0;
  position: absolute;
  transition: .3s background-color;
  top: 0;
  width: 30px;
  z-index: 1;
`

export default StyledCheckboxAndRadio