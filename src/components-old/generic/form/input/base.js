import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledInput = styled.input`
  font-size: 14px;
  ::placeholder {
    color: ${ colors.placeholderFields };
  }
`

const Input = ({
  meta,
  type,
  title,
  input,
  hasLabel,
  placeholder
}) => (
  <div className="form-group">
    { hasLabel && 
      <label for="exampleInputEmail1">Email address</label>
    }
    <StyledInput
      title={ title }
      type={ type } 
      className="form-control"
      placeholder={ placeholder || title }
      { ...input } />
    { meta.touched && meta.error &&
      <small className="form-text text-danger">{ title } { meta.error }</small>
    }
  </div>
)

Input.defaultProps = {
  type: 'text',
  hasLabel: false
}

Input.propTypes = {
  type: PropTypes.string,
  meta: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  hasLabel: PropTypes.bool,
  readOnly: PropTypes.bool,
  placeholder: PropTypes.string,
  IsLargeField: PropTypes.bool
}

export default Input
