import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledTextarea = styled.textarea`
  appearance: none;
  background-color: ${ colors.bgFields };
  border: 0;
  border-radius: 2px;
  color: ${ colors.text };
  font-size: 1rem;
  height: 300px; 
  padding: 10px;
  width: 100%;
`

export default StyledTextarea