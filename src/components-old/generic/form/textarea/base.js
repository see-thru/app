import React from 'react'
import PropTypes from 'prop-types'

import Label from '../label/base'
import StyledTextarea from './styled'
import FieldWrapper from '../wrappers/field/base'
import ErrorMessage from '../error-message/base'

const Textarea = ({
  meta,
  name,
  title,
  input,
  hasLabel,
  readOnly
}) => {
  return (
    <FieldWrapper>
      { hasLabel ?
        <Label
          htmlFor={ name }>
          { title }
        </Label>
        : undefined 
      }
      <StyledTextarea
        id={ name }
        readOnly={ readOnly }
        { ...input }
      />
      { meta.touched && meta.error ?
        <ErrorMessage>
          { meta.error }
        </ErrorMessage>
        : undefined
      }
    </FieldWrapper>
  )
}

Textarea.defaultProps = {
  hasLabel: true,
  readOnly: false
}

Textarea.propTypes = {
  name: PropTypes.string,
  meta: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  hasLabel: PropTypes.bool,
  readOnly: PropTypes.bool
}

export default Textarea
