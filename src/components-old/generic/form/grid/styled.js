import styled from 'styled-components'

import Media from '../../../../utils/media'
import { StyledContainerGrid } from '../../grid'

const StyledFormContainerGrid = styled(StyledContainerGrid)`
  padding: 0;
  ${ Media.medium`grid-template-columns: repeat(2, 1fr)` }
  ${ Media.small`grid-template-columns: repeat(1, 1fr)` }
`

export default StyledFormContainerGrid