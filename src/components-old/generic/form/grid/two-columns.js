import React from 'react'
import PropTypes from 'prop-types'

import StyledFormContainerGrid from './styled'

const TwoColumns = ({ children }) => (
  <StyledFormContainerGrid
    gridGap="0 10px"
    templateRows="auto"
    templateColumns="repeat(2, 1fr)">
    { children }
  </StyledFormContainerGrid>
)

TwoColumns.propTypes = {
  children: PropTypes.array.isRequired
}

export default TwoColumns