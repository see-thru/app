import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { GoogleApiWrapper } from 'google-maps-react'

import Label from '../label/base'
import FieldWrapper from '../wrappers/field/base'
import ErrorMessage from '../error-message/base'
import StyledInputGeoSuggest from './styled'

export class InputGeoSuggest extends Component {
  onSuggestSelect = value => {
    const { input } = this.props
    if ( value && value.description && value.location ) {
      const location = {
        lat: value.location.lat,
        lng: value.location.lng
      }
      input.onChange({
        location,
        description: value.description
      })
    } 
  }

  render () {
    const {
      meta,
      title,
      input,
      hasLabel      
    } = this.props
    return (
      <FieldWrapper>
        { hasLabel ?
          <Label
            htmlFor={ input.name }>
            { title }
          </Label>
          : undefined 
        }
        <StyledInputGeoSuggest
          id={ input.name }
          type="text"
          country={ ['us'] }
          placeholder="Zip code or City"
          { ...input }
          ignoreEnter
          autoComplete="off"
          onSuggestSelect={ value => this.onSuggestSelect(value) } />
        { meta.touched && meta.error ?
          <ErrorMessage>
            { meta.error }
          </ErrorMessage>
          : undefined
        }
      </FieldWrapper>    
    )
  }
}

InputGeoSuggest.defaultProps = {
  type: 'text',
  hasLabel: true,
  readOnly: false
}

InputGeoSuggest.propTypes = {
  meta: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  hasLabel: PropTypes.bool
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyDWgOvd8bl0exL0LKfc7r6vQPqvqOT4hi0'
})(InputGeoSuggest)