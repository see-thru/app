import styled from 'styled-components'
import Geosuggest from 'react-geosuggest'

import { colors } from '../../../../utils/settings'
import { MaxWidth } from '../../../../utils/media'

const StyledInputGeoSuggest = styled(Geosuggest)`
  position: relative;
  input {
    background-color: ${ colors.bgFields };
    border: 0;
    border-radius: 2px;
    color: ${ colors.text };
    font-size: 1rem;
    height: 55px;
    padding: 0 15px;
    width: 100%;
  }
  & * {
    outline: 0;
  }
  .geosuggest__suggests {
    color: ${ colors.text };
    font-size: 0.8rem;
    list-style: none;
    margin: 0;
    padding: 0;
    position: absolute;
    width: 300px;
    border-radius: 5px;
    border-top-left-radius: 0;
    z-index: 1;
    background-color: #f1f1f1;
    ${ MaxWidth(350)`
      width: 100%;
    `}
    &--hidden {
      display: none;
    }
  }

  .geosuggest__item {
    background-color: transparent;
    border-bottom: 1px solid #dedede;
    color: ${ colors.darkBlue };
    cursor: pointer;
    padding: 10px 15px;
    text-align: left;
    transition: .2s background-color;
    &:hover,
    &--active {
      background-color: ${ colors.darkBlue};
    }    
  }
`

export default StyledInputGeoSuggest