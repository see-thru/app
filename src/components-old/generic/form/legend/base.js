import React from 'react'
import PropTypes from 'prop-types'

import StyledLegend from './styled'

const Legend = ({ 
  children 
}) => (
  <StyledLegend>
    { children }
  </StyledLegend>
)

Legend.propTypes = {
  children: PropTypes.node.isRequired
}

export default Legend