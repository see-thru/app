import styled from 'styled-components'

import { colors } from '../../../../utils/settings' 

const StyledLegend = styled.legend`
  border-bottom: 1px solid ${ colors.text };
  clear: both;
  float: left;
  font-size: 1.2rem;
  margin: 20px 0;
  width: 100%;
`

export default StyledLegend