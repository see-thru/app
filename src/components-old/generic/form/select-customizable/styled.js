import styled from 'styled-components'
import Select from 'react-select'

import { colors } from '../../../../utils/settings'

const StyledSelectCustomizable = styled(Select)`
  .Select-control {
    background-color: ${ colors.bgFields };
    border: 0;
    border-radius: 2;
    font-size: 1rem;
    height: 55px;
  }
  .Select-placeholder {
    line-height: 55px;
  }
  .Select-value {
    padding: 4px;
  }
  .Select-input {
    height: 55px;
    line-height: 55px;
  }
  .Select-placeholder {
    color: #666;
  } 
  &.Select--multi .Select-value {
    background-color: ${ colors.lightHighlight };
    color: white;
    margin-left: 10px;
    margin-top: 10px;
  }
  &.Select--single .Select-value {
    background-color: ${ colors.bgFields };
    line-height: 50px !important;
    padding-left: 15px !important;
  }
  &.is-focused .Select-control {
    background-color: ${ colors.bgFields } !important;
  }
`

export default StyledSelectCustomizable
