import React from 'react'
import PropTypes from 'prop-types'
import 'react-select/dist/react-select.css'

import Label from '../label/base'
import FieldWrapper from '../wrappers/field/base'
import ErrorMessage from '../error-message/base'
import StyledSelectCustomizable from './styled'

const SelectCustomizable = ({
  meta,
  name,
  multi,
  input,
  title,
  options,
  hasLabel,
  placeholder
}) => (
  <FieldWrapper>
    { hasLabel ?
      <Label
        htmlFor={ name }>
        { title }
      </Label>
      : undefined
    }
    <StyledSelectCustomizable
      { ...input }
      id={ name }
      name={ name }
      multi={ multi }
      value={ input.value }
      options={ options }
      placeholder={ placeholder }
      onBlur={ () => input.onBlur(input.value) } />   
    { meta.touched && meta.error ?
      <ErrorMessage>
        { meta.error }
      </ErrorMessage>
      : undefined
    }     
  </FieldWrapper>
)

SelectCustomizable.defaultProps = {
  multi: false,
  hasLabel: true,
  placeholder: 'Select ...'
}

SelectCustomizable.propTypes = {
  meta: PropTypes.object.isRequired,
  name: PropTypes.string,
  multi: PropTypes.bool,
  placeholder: PropTypes.string,
  input: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  })).isRequired,
  hasLabel: PropTypes.bool
}


export default SelectCustomizable
