import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledSocialNetworks = styled.div`
  text-align: right;
  ${
    Media.medium`
      text-align: center;
    `
  }
`

export default StyledSocialNetworks