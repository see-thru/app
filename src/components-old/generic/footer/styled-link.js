import styled from 'styled-components'

const SIZE_ICONS = '36px'

const Link = styled.a`
  border-radius: 100%;
  height: ${ SIZE_ICONS };
  display: inline-block;
  margin: 0 5px;
  text-align: center;
  padding: 6px 0;
  vertical-align: top;
  width: ${ SIZE_ICONS };
`

export default Link