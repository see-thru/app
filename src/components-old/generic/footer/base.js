import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { toggleSignUpModal } from '../../../actions-old/auth'
import { toggleSubscribeGeneralModal } from '../../../actions-old/mailchimp'

import Copyright from './copyright'
import StyledFooter from './styled'
import SocialNetworks from './social-networks'
import LinksOfInterest from './links-of-interest/base'
import StyledInfoMessage from './styled-info-message'
import { StyledGridItem } from '../grid/index'
import StyledFooterContainer from './styled-container'
import StyledGridItemEnrollProvider from './styled-grid-item-enroll-provider'
import StyledGridItemSocialNetworks from './styled-grid-item-social-networks'

export const Footer = ({
  toggleSignUpModal,
  toggleSubscribeGeneralModal
}) => (
  <StyledFooter>
    <StyledInfoMessage>
      In the event of a medical emergency, dial 9-1-1 or visit your closest emergency room immediately.
    </StyledInfoMessage>
    <StyledFooterContainer
      gridGap="10px"
      templateRows="auto"
      templateColumns="repeat(3, 1fr)">
      <StyledGridItemEnrollProvider>
        <Link to="/partnership">
          Provider Signup
        </Link>
        <button onClick={ toggleSignUpModal.bind(this, true) }>
          Patient Signup
        </button>
        <button onClick={ toggleSubscribeGeneralModal.bind(this, true) }>
          Newsletter
        </button>
      </StyledGridItemEnrollProvider>      
      <StyledGridItem>
        <LinksOfInterest />
      </StyledGridItem>
      <StyledGridItemSocialNetworks 
        alignSelf="end"
        justifySelf="end">
        <SocialNetworks />
        <Copyright />
      </StyledGridItemSocialNetworks>
    </StyledFooterContainer>
  </StyledFooter>
)

Footer.propTypes = {
  toggleSignUpModal: PropTypes.func.isRequired,
  toggleSubscribeGeneralModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({})

export default connect(
  mapStateToProps,
  {
    toggleSignUpModal,
    toggleSubscribeGeneralModal
  }
)( Footer )
