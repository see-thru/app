import styled from 'styled-components'

import Media from '../../../../utils/media'
import { colors } from '../../../../utils/settings'

const StyledLinksOfInterest = styled.div`
  margin-left: 5px;
  ${
    Media.medium`
      margin: 0 auto 20px;
      max-width: 400px;
      padding: 0 20px;
    `
  }
  ${
    Media.small`
      text-align: center;
    `
  }
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
  li {
    display: inline-block;
    margin: 2px 1%;
    vertical-align: top;
    width: 31%;
    ${
      Media.medium`
        margin: 2px 2%;
        width: 46%;
      `
    }
  }
  a {
    color: white;
    text-decoration: none;
    font-size: .9rem;
    transition: .2s color;
    &:hover {
      color: ${ colors.lightBorder };
    }
  }
`

export default StyledLinksOfInterest