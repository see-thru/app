import React from 'react'
import { Link } from 'react-router-dom'

import StyledLinksOfInterest from './styled'

const LinksOfInterest = () => (
  <StyledLinksOfInterest>
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/working-with-us">Work With Us</Link>
      </li>
      <li>
        <Link to="/terms">Terms of Service</Link>
      </li>
      <li>
        <Link to="/about-us">About Us</Link>
      </li>
      <li>
        <Link to="/faq">FAQ</Link>
      </li>
      <li>
        <Link to="/privacy-policy">Privacy Policy</Link>
      </li>
      <li>
        <Link to="/why-seethru">Why SeeThru</Link>
      </li>
      <li>
        <a 
          rel="noopener noreferrer"
          href="https://blog.seethru.healthcare/"
          target="_blank">
          Blog
        </a>
      </li>
      <li>
        <Link to="/site-map">Site Map</Link>
      </li>
    </ul>
  </StyledLinksOfInterest>
)

export default LinksOfInterest