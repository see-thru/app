import React from 'react'

import Icon from '../icon/base'
import StyledLink from './styled-link'
import StyledSocialNetworks from './styled-social-networks'

const FILL_ICONS = '#FFF'

const SocialNetworks = () => {
  return (
    <StyledSocialNetworks>
      <StyledLink 
        href="https://twitter.com/SeeThruHealth" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="twitter"
          width="30px"
          height="30px" />
      </StyledLink>
      <StyledLink 
        href="https://www.facebook.com/SeeThruHealth/" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="facebook"
          width="30px"
          height="30px" />
      </StyledLink>
      <StyledLink 
        href="https://www.linkedin.com/company/seethru/" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="linkedin" 
          width="30px"
          height="30px" />
      </StyledLink>
      <StyledLink
        href="mailto:team@seethru.heatlhcare" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="email"
          width="30px"
          height="30px" />
      </StyledLink>
    </StyledSocialNetworks>
  )
}

export default SocialNetworks