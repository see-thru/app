import React from 'react'

import StyledMessage from './styled-message'

const Copyright = () => (
  <StyledMessage>
    Copyright © 2018 - SeeThru
  </StyledMessage>
)

export default Copyright;