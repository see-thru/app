import styled from 'styled-components'

import Media from '../../../utils/media'
import { StyledGridItem } from '../grid/index'

const StyledGridItemSocialNetworks = styled(StyledGridItem)`
  ${
    Media.medium`
      align-self: auto;
      justify-self: auto;
    `
  }
`

export default StyledGridItemSocialNetworks