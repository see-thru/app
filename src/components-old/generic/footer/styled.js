import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledFooter = styled.footer`
  background-color: ${ colors.bgFooter };
  color: white;
  padding-bottom: 40px;
`;

export default StyledFooter