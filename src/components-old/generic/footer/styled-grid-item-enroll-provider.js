import styled from 'styled-components'

import Media from '../../../utils/media'
import { fonts } from '../../../utils/settings'
import { StyledGridItem } from '../grid/index'

const StyledGridItemEnrollProvider = styled(StyledGridItem)`
  ${
    Media.medium`
      text-align: center;
    `
  }
  p {
    margin: 0 0 10px;
    font-size: 1.125rem;
    font-weight: ${ fonts.weightBold };
  }
  a,
  button {
    color: white;
    display: block;
    font-size: .9rem;
    margin: 8px 0;
    padding: 0;
    text-align: left;
    text-decoration: none;
    ${
      Media.medium`
        display: inline-block;
        margin: 2px 10px;
        text-align: center;
      `
    }
  }
`

export default StyledGridItemEnrollProvider