import styled from 'styled-components'

import { colors } from '../../../utils/settings'
import Media from '../../../utils/media';

const StyledMessage = styled.p`
  color: ${ colors.textFooter };
  font-size: 1rem;
  margin-top: 10px;
  text-align: right;
  ${
    Media.medium`
      text-align: center;
    `
  }
`

export default StyledMessage