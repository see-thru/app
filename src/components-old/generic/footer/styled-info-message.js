import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors, fonts } from '../../../utils/settings'

const StyledInfoMessage = styled.p`
  background: ${ colors.bgLightFooter };
  border-top: 1px solid ${ colors.borderLightFooter };
  color: ${ colors.homeBoxCreates };
  font-size: 1.18rem;
  font-weight: ${ fonts.weightBold };
  margin: 0 0 20px;
  text-align: center;
  padding: 20px;
  ${ Media.medium`
    font-size: 1rem;
  ` }
`

export default StyledInfoMessage