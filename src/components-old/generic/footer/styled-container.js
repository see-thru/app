import styled from 'styled-components'

import Media from '../../../utils/media'
import { StyledContainerGrid } from '../grid/index'

const StyledFooterContainer = styled(StyledContainerGrid)`
  ${
    Media.small`
      grid-template-columns: 1fr;
    `
  }
`

export default StyledFooterContainer