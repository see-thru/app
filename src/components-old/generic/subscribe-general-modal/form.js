import React from 'react'
import PropTypes from 'prop-types'
import {
  Field,
  reduxForm
} from 'redux-form'
import {
  email,
  length,
  required
} from 'redux-form-validators'

import {
  Input,
  Checkbox,
  TwoColumns,
  FormWrapper
} from '../form/index'

import { StyledButtonSuccess } from '../button/styled'

const Form = ({
  onSubmit,
  onValidated,
  handleSubmit
}) => (
  <FormWrapper
    onSubmit={ handleSubmit( onSubmit.bind(this, onValidated)) }>
    <TwoColumns>
      <Field
        title="First Name"
        name="FNAME"
        component={ Input }
        validate={[
          required()
        ]}
      />
      <Field
        title="Last Name"
        name="LNAME"
        component={ Input }
        validate={[
          required()
        ]}
      />
    </TwoColumns>
    <Field
      title="Email Address"
      name="EMAIL"
      component={ Input }
      type="email"
      validate={ [
        email(),
        length({ max: 255 }),
        required()
      ] }
    />
    <Field
      title="Zip Code"
      name="ZIP"
      component={ Input }
      validate={[
        required()
      ]}
    />
    <Field
      title="I want to shop for transparent healthcare"
      name="SHOPFORHEALTHCARE"
      component={ Checkbox }
      type="checkbox"
    />
    <Field
      title="I am a provider"
      name="ISPROVIDER"
      component={ Checkbox }
      type="checkbox"
    />
    <Field
      title="I'm interested in investing in SeeThru"
      name="ISINVESTOR"
      component={ Checkbox }
      type="checkbox"
    />       
    <StyledButtonSuccess 
      type="submit">
      Subscribe
    </StyledButtonSuccess>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'mailchimpForm'
})( Form )