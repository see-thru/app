import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import MailchimpSubscribe from "react-mailchimp-subscribe"

import { MAILCHIMP_URL_SUBSCRIBE_GENERAL } from '../../../constants/mailchimp-config'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalContentTitle,
  StyledModalHeaderCloseButton
} from '../modal/index'
import Form from './form'
import Icon from '../icon/base'
import StatusMessages from './status-messages/base'

const SubscribeGeneralModal = ({
  isOpen,
  onSubmit,
  closeModal
}) => (
  <ModalContainer    
    width="600px"
    height="auto"
    isOpen={ isOpen }
    closeModal={ closeModal }>
    <StyledModalHeader>
      <StyledModalHeaderTitle>Keep me posted about SeeThru</StyledModalHeaderTitle>
      <StyledModalHeaderCloseButton onClick={ closeModal }>
        <Icon 
          fill="#ccc"
          width="16"
          name="cancel"
          height="16" />
      </StyledModalHeaderCloseButton>
    </StyledModalHeader>
    <StyledModalContent>
      <StyledModalContentTitle>
        Thanks for your interest! We are hard at work building the marketplace. Sign up and be the first to know when we are live!
      </StyledModalContentTitle>
      <MailchimpSubscribe
        url={ MAILCHIMP_URL_SUBSCRIBE_GENERAL }
        render={({ 
          status, 
          message,
          subscribe
        }) => (
          <Fragment>
            <Form 
              onSubmit={ onSubmit }  
              onValidated={ formData => subscribe(formData) } />
            <StatusMessages 
              status={ status }
              message={ message } />
          </Fragment>
        )}
      />
      
    </StyledModalContent>
  </ModalContainer>
)

SubscribeGeneralModal.propTypes = {
  isOpen: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired
}

export default SubscribeGeneralModal