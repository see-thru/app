import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import StyledMessage from './styled-message'

const StyledMessageSuccess = styled(StyledMessage)`
  color: ${ colors.successButton }
`

export default StyledMessageSuccess