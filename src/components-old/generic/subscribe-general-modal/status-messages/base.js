import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import StyledMessageError from './styled-message-error'
import StyledMessageLoading from './styled-message-loading'
import StyledMessageSuccess from './styled-message-success'

const StatusMessages = ({
  status, 
  message
}) => (
  <Fragment>
    { status === "sending" ?
      <StyledMessageLoading>Sending...</StyledMessageLoading>
      : undefined
    }
    { status === "error" ?
      <StyledMessageError dangerouslySetInnerHTML={ { __html: message } } /> 
      : undefined
    }
    { status === "success" ? 
      <StyledMessageSuccess>Thanks! We’ll be in touch shortly!</StyledMessageSuccess>
      : undefined
    }    
  </Fragment>
)

StatusMessages.propTypes = {
  status: PropTypes.string,
  message: PropTypes.string
}

export default StatusMessages