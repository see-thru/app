import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import StyledMessage from './styled-message'

const StyledMessageError = styled(StyledMessage)`
  color: ${ colors.alert }
`

export default StyledMessageError