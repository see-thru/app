import styled from 'styled-components'

const StyledMessage = styled.div`
  margin-top: 10px;
  padding: 10px;
`

export default StyledMessage