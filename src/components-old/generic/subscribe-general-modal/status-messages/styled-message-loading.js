import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import StyledMessage from './styled-message'

const StyledMessageLoading = styled(StyledMessage)`
  color: ${ colors.link }
`

export default StyledMessageLoading