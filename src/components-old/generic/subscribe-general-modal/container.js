import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { 
  toggleSubscribeGeneralModal
} from '../../../actions-old/mailchimp'

import SubscribeGeneralModal from './base'

import {
  FIELD_NAME_IS_PROVIDER,
  FIELD_NAME_SEND_WHITEPAPER,
  FIELD_NAME_SHOP_FOR_HEALTHCARE
} from '../../../constants/mailchimp'

export class SubscribeGeneralModalContainer extends Component {
  constructor () {
    super()
    this.onSubmit = this.onSubmit.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  onSubmit (onValidated, e) {
    const {
      EMAIL,
      FNAME,
      ISPROVIDER,
      SENDWHITEPAPER,
      SHOPFORHEALTHCARE
    } = e
    onValidated({
      EMAIL,
      FNAME,
      [FIELD_NAME_IS_PROVIDER]: ISPROVIDER || undefined,
      [FIELD_NAME_SEND_WHITEPAPER]: SENDWHITEPAPER || undefined,
      [FIELD_NAME_SHOP_FOR_HEALTHCARE]: SHOPFORHEALTHCARE || undefined
    })
  }

  closeModal () {
    const {
      toggleSubscribeGeneralModal
    } = this.props
    toggleSubscribeGeneralModal(false)
  }

  render () {
    const { 
      isOpen 
    } = this.props
    return (
      <SubscribeGeneralModal
        isOpen={ isOpen }
        onSubmit={ this.onSubmit }
        closeModal={ this.closeModal } />
    )
  }
}

SubscribeGeneralModalContainer.propTypes = {
  isOpen: PropTypes.bool,
  toggleSubscribeGeneralModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isOpen : state.MailchimpReducer.subscribeGeneralModalIsOpen
})

export default connect(
  mapStateToProps,
  { toggleSubscribeGeneralModal }
)( SubscribeGeneralModalContainer )