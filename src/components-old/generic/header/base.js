import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Logo from './logo'
import NavContainer from './nav/container'

const StyledHeader = styled.header`
  background-color: white;
  box-shadow: 0 2px 2px rgba(0,0,0,.2);
  height: 60px;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 2;
`

const StyledContainer = styled.div`
  height: inherit;
  margin: auto;
  max-width: 1240px;
  padding: 0 15px;
`

const Header = ({
  history
}) => (
  <StyledHeader>
    <StyledContainer>
      <Logo />
      <NavContainer history={ history } />
    </StyledContainer>
  </StyledHeader>
)

Header.propTypes = {
  history: PropTypes.object.isRequired
}

export default Header