import React, { Component } from 'react'
import PropTypes from 'prop-types'
import BodyClassName from 'react-body-classname'
import onClickOutside from 'react-onclickoutside'
import styled from 'styled-components'

import HomeNav from './home'
import MenuButton from './button/base'

const StyledNavContainer = styled.div`
  display: inline-block;
  float: right;
  height: inherit;
`

export class NavContainer extends Component {
  constructor () {
    super() 
    this.state = ({ isOpened : false })
  }

  toggle = () => {
    const { isOpened } = this.state 
    this.setState({ isOpened : !isOpened })
  }

  handleClickOutside = (evt) => {
    this.setState({ isOpened: false })
  }
  
  render () {
    const { isOpened } = this.state
    const { history } = this.props
    return (
      <StyledNavContainer>
        { isOpened ? <BodyClassName className="shadow-block" /> : undefined }
        <MenuButton 
          isActived={ isOpened }
          toggle={ this.toggle } />
        <HomeNav 
          toggle={ this.toggle }
          history={ history }
          isOpened={ isOpened } />
      </StyledNavContainer>
    )
  }
}

NavContainer.propTypes = {
  history: PropTypes.object.isRequired
}

export default onClickOutside(NavContainer)