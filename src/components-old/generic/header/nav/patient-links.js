import React, { Fragment } from 'react'

import { StyledLinkRouter } from './styled-links'

const PatientLinks = () => (
  <Fragment>
    <StyledLinkRouter 
      to="/user/edit-photo"
      mobile="true">
      Edit Photo
    </StyledLinkRouter>
    <StyledLinkRouter  
      to="/user/payment-info/methods"
      mobile="true">
      Payment Information
    </StyledLinkRouter> 
    <StyledLinkRouter to="/patient/booking/appointments">
      Appointments
    </StyledLinkRouter>
  </Fragment>
)

export default PatientLinks