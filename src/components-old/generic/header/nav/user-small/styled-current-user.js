import styled from 'styled-components'

const StyledCurrentUser = styled.div`
  img {
    margin-left: 20px;
  }
  img, p {
    display: inline-block;
  }
`

export default StyledCurrentUser