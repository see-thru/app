import styled from 'styled-components'

import Media from '../../../../../utils/media'
import { colors } from '../../../../../utils/settings'

const StyledSmallUserNav = styled.div`
  background-color: ${ colors.bgUserNav };
  color: white;
  font-size: 3rem;
  display: none;
  height: 160px;
  text-align: left;
  position: relative;
  ${
    Media.small`
      display: block;
    `
  }
  & > div {
    background-color: inherit;
    top: 0;
    a,
    button {
      display: inline-block;
      margin: 0 1%;
      padding-bottom: 0;
      width: 48%;
    }
    button {
      ${
        Media.small`
          padding-right: 40px;
          text-align: right;
        `
      }
    }
  }
  a {
    clear: both;
  }
  p {
    margin-bottom: 0;
  }
` 

export default StyledSmallUserNav