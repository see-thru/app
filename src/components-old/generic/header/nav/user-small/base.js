import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Icon from '../../../icon/base'
import StyledCurrentUser from './styled-current-user'
import StyledSmallUserNav from './styled'
import StyledMockAvatar from '../user/styled-avatar'
import StyledDropdownList from '../user/styled-dropdown-list'
import StyledDropdownLink from '../user/styled-dropdown-link'
import StyledDropdownTitle from '../user/styled-dropdown-title'
import StyledDropdownButton from '../user/styled-dropdown-button'

export const SmallUserNav = ({
  user,
  logout
}) => (
  <StyledSmallUserNav>
    <StyledDropdownList>
      <StyledCurrentUser>
        <StyledMockAvatar src={ user.photo } />
        <StyledDropdownTitle>
          { `${ user.firstName } ${ user.lastName }` }
          <span>({ user.role })</span>
        </StyledDropdownTitle>
      </StyledCurrentUser>
      <StyledDropdownLink to="/user/edit-profile">
        Edit profile
      </StyledDropdownLink>
      <StyledDropdownButton onClick={ logout }>
        Logout
        <Icon 
          fill="white"
          name="logout" 
          width="12" 
          height="12" />
      </StyledDropdownButton>
    </StyledDropdownList>
  </StyledSmallUserNav>
)

SmallUserNav.propTypes = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user
})

export default connect(
  mapStateToProps,
  {}
)( SmallUserNav )