import React, { Component } from 'react'
import PropTypes from 'prop-types'

import SmallUserNav from './base'

export class SmallUserNavContainer extends Component {
  render () {
    return (
      <SmallUserNav { ...this.props } />
    )
  }
} 

SmallUserNavContainer.propTypes = {
  logout: PropTypes.func.isRequired
}

export default SmallUserNavContainer