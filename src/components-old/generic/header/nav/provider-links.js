import React, { Fragment } from 'react'

import { StyledLinkRouter } from './styled-links'

const ProviderLinks = () => (
  <Fragment>
    <StyledLinkRouter 
      to="/user/provider/edit-provider-profile"
      mobile="true">
      Edit Profile
    </StyledLinkRouter>   
    <StyledLinkRouter 
      to="/user/edit-photo"
      mobile="true">
      Edit Photo
    </StyledLinkRouter>   
    <StyledLinkRouter 	
      to="/user/provider/update-schedules"	
      mobile="true">	
      Update Schedules	
    </StyledLinkRouter>   
    <StyledLinkRouter
      to="/user/provider/edit-healthcare-services"
      mobile="true">
      Edit Healthcare Services
    </StyledLinkRouter>    
    <StyledLinkRouter to="/provider/booking/appointments/list">
      Appointments
    </StyledLinkRouter>    
  </Fragment>
)

export default ProviderLinks