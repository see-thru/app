import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { toggleSubscribeGeneralModal, toggleSubscribeProviderModal } from '../../../../actions-old/mailchimp'
import { logout, toggleSignInModal, toggleSignUpModal } from '../../../../actions-old/auth'

import StyledNav from './wrapper'
import PatientLinks from './patient-links'
import ProviderLinks from './provider-links'
import UserNavContainer from './user/container'
import SmallUserNavContainer from './user-small/container'
import {  StyledButton, StyledLinkRouter, StyledButtonBlue } from './styled-links'

export const HomeNav = ({
  user,
  logout,
  toggle,
  history,
  isLogged,
  isOpened,
  toggleSignUpModal,	  
  toggleSignInModal,
  toggleSubscribeProviderModal
}) => (
  <StyledNav 
    onClick={ toggle }
    isOpened={ isOpened }>
    { isLogged ? 
      <Fragment>
        <SmallUserNavContainer logout={ logout.bind(this, history) } />
        { user.role === 'provider' ? <ProviderLinks /> : undefined }
        { user.role === 'patient' ? <PatientLinks /> : undefined }
        <UserNavContainer logout={ logout.bind(this, history) } />
      </Fragment>
      :
      <Fragment>         
        <StyledLinkRouter to='/about-us'>	
          About Us	
        </StyledLinkRouter>  
        <StyledLinkRouter to='/why-seethru'>	
          Why SeeThru	
        </StyledLinkRouter>          
        <StyledButton
          onClick={ toggleSubscribeProviderModal.bind(this, true) }	      
          type="register">	
          Provider Signup	
        </StyledButton>
        <StyledButton
          onClick={ toggleSignUpModal.bind(this, true) }
          type="register">
          Patient Signup
        </StyledButton>
        <StyledButtonBlue
          onClick={ toggleSignInModal.bind(this, true) }
          type="signin">
          Sign in
        </StyledButtonBlue>
      </Fragment>
    }
  </StyledNav>
)

HomeNav.propTypes = {
  user: PropTypes.shape({
    role: PropTypes.oneOf(['patient', 'provider'])
  }),
  toggle: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  isOpened: PropTypes.bool, 
  isLogged: PropTypes.bool,
  toggleSubscribeProviderModal: PropTypes.func.isRequired,
  toggleSubscribeGeneralModal: PropTypes.func.isRequired,
  toggleSignInModal: PropTypes.func.isRequired,
  toggleSignUpModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  isLogged: state.AuthReducer.isLogged
})

export default connect(
  mapStateToProps,
  { 
    logout,
    toggleSubscribeProviderModal,
    toggleSubscribeGeneralModal,
    toggleSignInModal,
    toggleSignUpModal
  }
)( HomeNav )
