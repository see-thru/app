import { Link } from 'react-router-dom'
import styled, { css } from 'styled-components'

import Media from '../../../../utils/media'
import { colors, fonts } from '../../../../utils/settings'
import GetLinkColorForNav from '../../../../utils/get-link-color-for-nav'

const styles = css`
  border: 0;
  color: ${ ({ type }) => GetLinkColorForNav(type) }
  cursor: pointer;
  display: inline-block;
  line-height: 20px;
  margin: 0 10px;
  padding: 20px 5px;
  transition: .2s color;
  text-decoration: none;
  &:hover {
    color: ${ colors.navBarHoverLink };
  }
  &:focus {
    outline: 0;
  }
  ${ Media.medium`
    font-size: .9rem;
    margin: 0 7px;
  ` }
  ${
    Media.small`
      border-bottom: 1px solid #eee;
      display: block;
      font-size: .9rem;
      margin: 0;
      padding: 13px 20px;
      width: 100%;
      &:hover {
        background-color: ${ colors.lightBlue };
        color: white;
      }
    ` 
  }
  ${ props => props.mobile === 'true' ?  `display: none;` : undefined }
`

export const StyledButton = styled.button`
  ${ styles }
  background-color: transparent;
  border: ${ props => props.isAvatar === 'yes' ? '0' : `1px solid #9c9c9c` };
  border-radius: 4px;
  color: ${ colors.textLight };
  font-size: 13px;
  font-weight: ${ fonts.weightBold };
  padding: ${ props => props.isAvatar === 'yes' ? '10px 0' : '8px 32px' };
  transition: .3s color, .3s border-color;
  &:hover {
    border-color: ${ colors.navBarHoverLink };
  }
  ${ Media.medium`
    padding: 8px 20px;
  ` }
  ${ Media.small`
    border: 0;
    border-bottom: 1px solid #eee;
    border-radius: 0;
    padding: 13px 20px;
    &:hover {
      border-color: inherit;
    }
  `}
`
export const StyledButtonBlue = styled.button`
  ${ styles }
  background-color: ${ colors.darkButton }; 
  border-radius: 4px;
  color: white;
  font-size: 13px;
  font-weight: ${ fonts.weightBold };
  transition: .35s background-color;
  padding: 8px 32px;
  &:hover {
    background-color: ${ colors.bgMissionBox };
    color: white;
  }
  ${ Media.medium`
    padding: 8px 17px;
  `}
  ${ Media.small`
    background-color: transparent;
    border: 0;
    border-radius: 0;
    color: ${ ({ type }) => GetLinkColorForNav(type) }
    padding: 13px 20px;
    &:hover { 
      background-color: ${ colors.lightBlue };
      color: white;
    }
  `}
  `
  
export const StyledLink = styled.a`${ styles }`
export const StyledLinkRouter = styled(Link)`${ styles }`