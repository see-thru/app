import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../../utils/media'

const StyledNav = styled.nav`
  ${
    Media.small`
      background-color: white;
      border-top: 2px solid #eee;
      left: 0;
      height: ${ props => props.isOpened ? 'auto' : '0' };
      text-align: center;
      top: 60px;
      overflow: hidden;
      position: absolute;
      width: 100%;
      visibility: ${ props => props.isOpened ? 'visible' : 'hidden' };
      z-index: 2;
    ` 
  }
  ${ Media.small`
    a, button {
      transition: .2s transform;
    }
    a {
      &:nth-child(1) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-10px)' };
      }
      &:nth-child(2) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-20px)' };
      }
      &:nth-child(3) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-30px)' };
      }
      &:nth-child(4) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-40px)' };
      }    
      &:nth-child(5) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-50px)' };
      }
    }
    button {
      transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-40px)' };
    }  
  ` }
`

StyledNav.propTypes = {
  isOpened: PropTypes.bool
}

export default StyledNav