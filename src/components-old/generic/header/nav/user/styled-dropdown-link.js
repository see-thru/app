import styled from 'styled-components'
import { Link } from 'react-router-dom'

import Media from '../../../../../utils/media'
import { colors } from '../../../../../utils/settings'
import StyledDropdownAction from './styled-dropdown-action'

const StyledDropdownLink = styled(Link)`
  color: ${ colors.text };
  text-decoration: none;
  text-align: center;
  ${
    Media.small`
    color: white;
    `
  }
  ${ StyledDropdownAction }
`

export default StyledDropdownLink