import styled from 'styled-components'

const StyledDropdownTitle = styled.p`
  font-size: .9rem;
  margin: 0;
  padding: 0 20px 10px;
  text-transform: capitalize;
  span {
    display: block;
    font-size: .75rem;
  }
`

export default StyledDropdownTitle