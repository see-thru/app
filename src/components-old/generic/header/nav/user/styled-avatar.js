import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../../../utils/media'

const StyledMockAvatar = styled.img`
  background-color: #eee;
  border-radius: 100%;
  display: block;
  height: 46px;
  width: 46px;
  ${
    Media.medium`
      height: 30px;
      width: 30px;
    `
  }
  ${
    Media.small`
      height: 40px;
      top: 6px;
      position: relative;
      width: 40px;    
    `
  }
`

StyledMockAvatar.defaultProps = {
  src: '/images/example-avatar.png'
}

StyledMockAvatar.propTypes = {
  src: PropTypes.string
}

export default StyledMockAvatar