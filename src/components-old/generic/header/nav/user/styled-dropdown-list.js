import styled from 'styled-components'

import Media from '../../../../../utils/media'
import { colors } from '../../../../../utils/settings'

const StyledDropdownList = styled.div`
  background-color: white;
  border: 1px solid ${ colors.border };
  right: 0;
  top: 56px;
  transform: translateY(10px);
  padding: 10px 0 0;
  position: absolute;
  width: 240px;
  z-index: 2;
  ${
    Media.medium`
      top: 39px;
    `
  }
  ${
    Media.small`
      border: 0;
      border-radius: 0;
      left: 0;
      margin-top: 0;
      right: auto;
      top: 100%;
      padding: 0;
      width: 100%;
    `
  }
  & > svg {
    bottom: 100%;
    right: 25px;
    position: absolute;
    ${
      Media.medium`
        right: 18px;
      `
    }
  }
`

export default StyledDropdownList