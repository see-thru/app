import styled from 'styled-components'

import { colors } from '../../../../../utils/settings'
import { StyledButton } from '../styled-links'
import StyledDropdownAction from './styled-dropdown-action'
import Media from '../../../../../utils/media';

const StyledDropdownButton = styled(StyledButton)`
  ${ StyledDropdownAction }
  background-color: transparent;
  border-top: 0;
  color: ${ colors.text };
  font-weight: 400;
  position: relative;
  ${
    Media.small`
      color: white;
    `
  }
  svg {
    right: 20px;
    top: 12px;
    position: absolute;
  }
`

export default StyledDropdownButton