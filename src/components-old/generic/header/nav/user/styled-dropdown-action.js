import { colors } from '../../../../../utils/settings'

const StyledDropdownAction = `
  border-bottom: 1px solid ${ colors.border };
  display: block;
  font-size: .9rem;
  margin: 0;
  text-align: left;
  padding: 10px 20px;
  width: 100%;
  &:last-of-type {
    border-bottom: 0;
  }
  &:hover {
    text-decoration: underline;
  }
`

export default StyledDropdownAction