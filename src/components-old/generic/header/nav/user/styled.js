import styled from 'styled-components'

import Media from '../../../../../utils/media'

const StyledUserNav = styled.div`
  display: inline-block;
  position: relative;
  vertical-align: middle;
  ${
    Media.small`
      display: none;
    `
  }
` 

export default StyledUserNav