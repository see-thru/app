import React, { Component } from 'react'
import PropTypes from 'prop-types'
import onClickOutside from "react-onclickoutside"

import UserNav from './base'

export class UserNavContainer extends Component {
  constructor () {
    super()
    this.state = ({ isOpen : false })
    this.toggle = this.toggle.bind(this)
    this.closeDropdown = this.closeDropdown.bind(this)
  }

  toggle () {
    const {
      isOpen
    } = this.state
    this.setState({ isOpen : !isOpen })
  }

  closeDropdown () {
    this.setState({ isOpen : false })
  }

  handleClickOutside (evt) {
    this.closeDropdown()
  }

  render () {
    const { isOpen } = this.state
    const { logout } = this.props
    return (
      <UserNav 
        isOpen={ isOpen }
        toggle={ this.toggle }
        logout={ logout }
        closeDropdown={ this.closeDropdown } />
    )
  }
} 

UserNavContainer.propTypes = {
  logout: PropTypes.func.isRequired
}

export default onClickOutside(UserNavContainer)