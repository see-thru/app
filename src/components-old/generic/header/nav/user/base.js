import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Icon from '../../../icon/base'
import StyledUserNav from './styled'
import StyledMockAvatar from './styled-avatar'
import { StyledButton } from '../styled-links'
import StyledDropdownList from './styled-dropdown-list'
import StyledDropdownLink from './styled-dropdown-link'
import StyledDropdownTitle from './styled-dropdown-title'
import StyledDropdownButton from './styled-dropdown-button'

export const UserNav = ({
  user,
  isOpen,
  toggle,
  logout,
  closeDropdown
}) => (
  <StyledUserNav>
    <StyledButton
      isAvatar='yes'
      onClick={ toggle }>
      <StyledMockAvatar src={ user.photo && user.photo.length >= 1 ? user.photo : undefined } />
    </StyledButton>
    { isOpen ?
      <StyledDropdownList onClick={ closeDropdown }>
        <Icon 
          name="triangle" 
          width="14"
          height="7" />
        <StyledDropdownTitle>
          { `${ user.firstName } ${ user.lastName }`}
          <span>({ user.role })</span>
        </StyledDropdownTitle>
        <StyledDropdownLink
          to="/user/edit-profile">
          Edit profile
        </StyledDropdownLink>
        <StyledDropdownLink
          to="/user/edit-photo">
          Edit photo
        </StyledDropdownLink>
        { user.role === 'patient' ? 
          <StyledDropdownLink
            to="/user/payment-info/methods">
            Payment information
          </StyledDropdownLink>
          : undefined }
        { user.role === 'provider' ? 
          <Fragment>
            <StyledDropdownLink
              to="/user/provider/edit-provider-profile">
              Edit Provider Profile
            </StyledDropdownLink>            
            <StyledDropdownLink	
              to="/user/provider/update-schedules">	
              Update Schedules	
            </StyledDropdownLink>	            
            <StyledDropdownLink
              to="/user/provider/edit-healthcare-services">
              Edit Healthcare Services
            </StyledDropdownLink>
          </Fragment>
          : undefined }          
        <StyledDropdownButton
          onClick={ logout }>
          Logout
          <Icon 
            fill="#999"
            name="logout" 
            width="12" 
            height="12" />
        </StyledDropdownButton>
      </StyledDropdownList>
      : undefined 
    }
  </StyledUserNav>
)

UserNav.propTypes = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  closeDropdown: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user
})

export default connect(
  mapStateToProps,
  {}
)( UserNav )
