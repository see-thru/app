import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../icon/base'
import StyledButton from './styled'

const MenuButton = ({
  toggle,
  isActived
}) => (
  <StyledButton
    isActived={ isActived }
    onClick={ toggle.bind(this) }>
    Menu
    <Icon 
      name="menu-arrow" 
      width="6px" 
      height="3px" />
  </StyledButton>
)

MenuButton.propTypes = {
  toggle: PropTypes.func.isRequired,
  isActived: PropTypes.bool
}

export default MenuButton