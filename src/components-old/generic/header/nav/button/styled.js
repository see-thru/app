import styled from 'styled-components'

import Media from '../../../../../utils/media'
import { fonts } from '../../../../../utils/settings'

const StyledButton = styled.button`
  color: #565656;
  cursor: pointer;
  display: none;
  font-size: 14px;
  font-weight: ${ fonts.weightBold };
  text-transform: uppercase;
  padding: 20px 10px;
  line-height: 20px;
  ${ 
    Media.small`
      display: block;
    `
  }
  &:focus {
    outline: 0;
  }
  svg {
    margin-left: 6px;
    position: relative;
    transform:  ${ props => props.isActived ? 'rotate(180deg)' : 'rotate(0)' };
    transition: .3s transform;
    bottom: 4px;
  }
`

export default StyledButton