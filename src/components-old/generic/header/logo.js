import React from 'react'

import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Media from '../../../utils/media'

const StyledLogo = styled.div`
  display: inline-block;
  margin-left: 50px;
  ${ Media.medium`
    margin-left: 0;
  `}
`

const StyledLogoLink = styled(Link)`
  display: block;
  padding-top: 10px;
  ${ Media.medium`
    padding-top: 15px;
    img {
      height: 30px;
    }
  `}
`

const Logo = () => (
  <StyledLogo>
    <StyledLogoLink to="/">
      <img 
        src="/images/header-logo.png" 
        alt="Seethru Logo"
        height="40" />
    </StyledLogoLink>
  </StyledLogo>
)

export default Logo