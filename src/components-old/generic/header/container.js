import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Header from './base'

class HeaderContainer extends Component {
  render () {
    const { history } = this.props
    return <Header history={ history } />
  }
}

HeaderContainer.propTypes = {
  history: PropTypes.object.isRequired
}

export default HeaderContainer