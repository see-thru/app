import React, { Component, Fragment } from 'react'
import qs from 'qs'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-modal'

import { login, toggleSignInModal, toggleSignUpModal, toggleForgotPasswordModal } from '../../../actions-old/auth'
import Loader from '../loader/base'
import SignInContent from './content'

export class SignInModal extends Component {
  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.showSignUpModal = this.showSignUpModal.bind(this)
    this.showForgotPasswordModal = this.showForgotPasswordModal.bind(this)
    const parsed = qs.parse(props.location.search) 
    let updatedMessage = ''
    if ( parsed.passwordUpdated ) {
      this.openModal()
      updatedMessage = 'Password updated successfully! Please, Sign in!'
    }
    this.state = { updatedMessage }
  }

  onSubmit (e) {
    const { login, history } = this.props
    console.log(e)
    login(e).then( response => {
      if ( response.code !== 'auth/wrong-password' ) 
        history.push('/')
        this.setState({ updatedMessage: null })
    })
  }

  closeModal () {
    const { history, toggleSignInModal } = this.props
    toggleSignInModal(false)
    this.setState({ updatedMessage: null })
    history.push('/')
  }

  openModal () { // what is this?
    this.props.toggleSignInModal(true)
  }

  showSignUpModal () {
    this.closeModal() // this is done automatically by the action, right?
    this.props.toggleSignUpModal(true)
  }

  showForgotPasswordModal () {
    this.closeModal() // this is done automatically by the action
    this.props.toggleForgotPasswordModal(true)
  }

  render () {
    const { updatedMessage } = this.state
    const { errors, isOpen, loading } = this.props
    
    return (
      <Fragment>
        { loading ? <Loader /> : undefined }
        <Modal
          className="Modal__Bootstrap modal-dialog modal-dialog-centered"
          closeTimeoutMS={100}
          isOpen={ isOpen }
          onRequestClose={this.closeModal}
          style={ { overlay: { zIndex: 100 } } }
          contentLabel="Sign in" >
          <SignInContent
            errors={ errors }
            onSubmit={ this.onSubmit }
            updatedMessage={ updatedMessage }
            showSignUpModal={ this.showSignUpModal }
            showForgotPasswordModal={ this.showForgotPasswordModal } />
        </Modal>
      </Fragment>
    )
  }
}

SignInModal.propTypes = {
  login: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  errors: PropTypes.object,
  location: PropTypes.object.isRequired,
  toggleSignInModal: PropTypes.func.isRequired,
  toggleSignUpModal: PropTypes.func.isRequired,
  toggleForgotPasswordModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  errors: state.AuthReducer.errors,
  isOpen: state.AuthReducer.signInModalIsOpen,
  loading: state.AuthReducer.loading
})

export default connect(
  mapStateToProps,
  { 
    login,
    toggleSignInModal, 
    toggleSignUpModal,
    toggleForgotPasswordModal
  }
)( SignInModal )