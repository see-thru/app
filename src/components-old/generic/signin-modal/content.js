import React from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'
import Form from './form'
import { ErrorMessage } from '../../generic/form'
import { SubmitButton } from '../elements/buttons'

const ModalContent = styled.div`
  background-color: #00aeee;
  color: white;
  .modal-body {
    padding: 24px;
    font-size: 14px;
    .modal-content-text {
      margin-bottom: 20px;
    }
    p {
      margin: 0;
      line-height: 19px;
      font-weight: 600;
    }
    form {
      .form-text {
        color: #eee !important;
      }
    }
  }
`

const SignInHeader = styled.div`
  display: flex;
  justify-content: space-between;
  h1 {
    flex: 1;
    font-size: 45px;
    font-weight: 800;
  }
  img {
    width: 130px;
    height: 40px;
    object-fit: contain;
  }
`

const ModalFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 25px;
`

const ModalLink = styled.button`
  background: transparent;
  border: 0;
  border-bottom: 2px solid white;
  color: white;
  padding: 0 2px;
  &:hover {
    border-color: #a6e3ff;
    color: #a6e3ff;
  }
`

const SignInContent = ({
  errors,
  onSubmit,
  showSignUpModal,
  showForgotPasswordModal
}) => (
  <ModalContent className="modal-content">
    <div className="modal-body">
      <SignInHeader>
        <h1>Sign In</h1>
        <img 
          src="/images/header-logo.png" 
          alt="Seethru Logo"
          height="40" />
      </SignInHeader>
      <div className="modal-content-text">
        <p>Not feeling well? Need a doctor?</p>
        <p>Find guaranteed quality and prices for your care.</p>
      </div>
      { errors.signin ? <ErrorMessage>{ errors.signin }</ErrorMessage> : undefined }
      <Form onSubmit={ onSubmit } />
      <ModalFooter>
        <div>
          <p>Not yet registered? <ModalLink onClick={showSignUpModal}>Sign up.</ModalLink></p>
          <ModalLink onClick={showForgotPasswordModal}>Forgot password?</ModalLink>
        </div>
        <SubmitButton type="submit" form="signInForm">Sign In</SubmitButton>
      </ModalFooter>
    </div>
  </ModalContent>
)

SignInContent.propTypes = {
  errors: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  showSignUpModal: PropTypes.func.isRequired,
  showForgotPasswordModal: PropTypes.func.isRequired
}

export default SignInContent