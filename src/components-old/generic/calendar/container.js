import React, { Component } from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'

import StyledCalendar from './styled'

import IconPrev from './icon-prev'
import IconNext from './icon-next'
import { GetAvailablesHours } from '../../../utils/generator-data-structure/choose-appointment-time/'

class CalendarContainer extends Component {
  constructor () {
    super()
    this.state = ({ date: new Date() })
  }

  onChange = date => {
    const { onSubmit } = this.props
    this.setState({ date })
    if ( onSubmit ) onSubmit(date)
  }

  daysAreDisabled = value => {
    const { 
      daySelected,
      dateSelected,
      daysDisabled,
      unavailabilities,
      availabilitiesWithFormat
    } = this.props
    let isDisabled = false 
    daysDisabled.map( day => {
      if ( value.date.getDay() === day ) {
        isDisabled = true 
      } else {
        const areThereAvailableHours = GetAvailablesHours(availabilitiesWithFormat.weekdays, daySelected, unavailabilities, dateSelected)
        if ( moment(value.date).format('MM/DD/YYYY') === moment(dateSelected).format('MM/DD/YYYY') && areThereAvailableHours.length === 0 ) {
          isDisabled = true
        }
      }
      return day
    })
    return isDisabled
  }

  render () {
    const { 
      minDate,
      selectRange 
    } = this.props
    return (
      <StyledCalendar
        minDate={ minDate ? minDate : undefined }
        onChange={ this.onChange }
        prevLabel={ <IconPrev /> } 
        nextLabel={ <IconNext /> }
        selectRange={ selectRange }
        calendarType="ISO 8601"
        tileDisabled={ this.daysAreDisabled }
        formatShortWeekday={ value => ['S', 'M', 'T', 'W', 'T', 'F', 'S'][value.getDay()] } />
    )
  }
}

CalendarContainer.defaultProps = {
  selectRange: false,
  daysDisabled: []
}

CalendarContainer.propTypes = {
  minDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date)
  ]),
  onSubmit: PropTypes.func,
  selectRange: PropTypes.bool,
  daysDisabled: PropTypes.array
}

export default CalendarContainer