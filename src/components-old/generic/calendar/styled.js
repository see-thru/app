import styled from 'styled-components'
import Calendar from 'react-calendar'

import { colors } from '../../../utils/settings'

const StyledCalendar = styled(Calendar)`
  &.react-calendar {
    border-bottom: 1px solid ${ colors.border };
    border-left: 0;
    border-right: 0;
    border-top: 0;
    padding-bottom: 20px;
    width: 100%;
    button {
      margin: 4px 0;
    }
  }
  .react-calendar__tile--active {
    background-color: ${ colors.bgCalendarSelectDay };
    &:disabled {
      color: ${ colors.text };
    }
  }
  .react-calendar__month-view__days__day--weekend {
    color: #D9D9D9;
  }
`

export default StyledCalendar