import React from 'react'

import Icon from '../icon/base'

const IconPrev = () => (
  <Icon 
    name="calendar-prev"
    width="32"
    height="32" /> 
)

export default IconPrev