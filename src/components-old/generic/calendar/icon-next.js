import React from 'react'

import Icon from '../icon/base'

const IconNext = () => (
  <Icon 
    name="calendar-next"
    width="32"
    height="32" /> 
)

export default IconNext