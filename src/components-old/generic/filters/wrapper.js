import React from 'react'
import PropTypes from 'prop-types'

import StyledFilterContainerGrid from './styled-grid-container'

const FiltersWrapper = ({ children }) => (
  <StyledFilterContainerGrid
    gridGap="0"
    templateRows="auto"
    templateColumns="1fr">
    { children }
  </StyledFilterContainerGrid>
)

FiltersWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.object
  ]).isRequired
}

export default FiltersWrapper