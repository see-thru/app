import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import Media from '../../../../utils/media';

const StyledFilterCloseButton = styled.button`
  color: ${ colors.textTitle };
  left: 10px;
  font-size: .9rem;
  text-transform: uppercase;
  top: 0;
  position: absolute;
  ${
    Media.small`
      left: 0;
    `
  }
`

export default StyledFilterCloseButton