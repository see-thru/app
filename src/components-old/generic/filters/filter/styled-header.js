import styled from 'styled-components'

const StyledFilterHeader = styled.div`
  margin: 20px 0;
  position: relative;
`

export default StyledFilterHeader