import styled from 'styled-components'

const StyledFiltersComponent = styled.div`
  background-color: white;
  height: 100%;
  left: 0;
  top: 0;
  overflow-x: auto;
  position: fixed;
  width: 100%;
  z-index: 4;
`

export default StyledFiltersComponent