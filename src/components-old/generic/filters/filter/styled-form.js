import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledForm = styled.div`
  form {
    padding: 0 10px;
    position: relative;
    & > div {
      margin: 0;
    }
  }
  input {
    background-color: white;
    border: 1px solid ${ colors.border };
    padding-left: 30px;
    outline: none;
  }
  label {
    margin: 0;
  }
  svg {
    left: 25px;
    top: 23px;
    position: absolute;
  }
`

export default StyledForm