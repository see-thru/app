import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledFilterResultsItem = styled.button`
  border-bottom: 1px solid ${ colors.border };
  color: ${ colors.link };
  display: block;
  font-size: .9rem;
  padding: 10px;
  text-align: left;
  transform: translateX(0);
  transition: .3s transform;
  width: 100%;
  &:hover {
    transform: translateX(5px);
  }
`

export default StyledFilterResultsItem