import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledFilterTitle = styled.h4`
  color: ${ colors.successButton };
  font-size: .9rem;
  font-weight: 400;
  margin: 0;
  text-align: center;
  text-transform: uppercase;
`

export default StyledFilterTitle