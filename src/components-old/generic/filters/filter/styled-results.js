import styled from 'styled-components'

const StyledFilterResults = styled.div`
  clear: both;
  float: left;
  margin: 20px 0 0;
  padding: 0 20px;
  width: 100%;
`

export default StyledFilterResults