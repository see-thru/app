import React, { Component } from 'react'
import qs from 'qs'
import moment from 'moment'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { searchProviders } from '../../../../actions-old/search'

import Button from './button/base'
import SelectToWhen from './select-to/base'
import StyledFilterWhen from './styled'
import GetFormattedValueOfWhenFilter from '../../../../utils/get-formatted-value-of-when-filter'

export class FilterWhen extends Component {
  constructor () {
    super()
    this.state = ({ isOpen: false })
  }

  openComponent = () => {
    this.setState({ isOpen: true })
  }

  closeComponent = () => {
    this.setState({ isOpen: false })
  }

  onSubmit = dates => {
    const { 
      history,
      searchProviders
    } = this.props
    this.closeComponent()
    let data = {
      when: {
        to: moment(dates[1]).format('DD-MM-YY'),
        from: moment(dates[0]).format('DD-MM-YY')
      }
    }   
    const parsed = qs.parse(history.location.search)
    let what = parsed.what
    if ( what ) {
      data.what = what
    }
    searchProviders(data)
    what = what ? `&what=${ what }` : ''
    const from = data.when.from ? `&from=${ data.when.from }` : ''
    const to = data.when.to ? `&to=${ data.when.to }` : ''
    history.push(`/search?${ what }${ from }${ to }`)
  }

  render () {
    const { isOpen } = this.state
    const { history } = this.props
    const parsed = qs.parse(history.location.search)
    return (
      <StyledFilterWhen>
        <Button
         value={ GetFormattedValueOfWhenFilter(parsed.to, parsed.from) }
         openComponent={ this.openComponent } />
        { isOpen ? 
          <SelectToWhen
            onSubmit={ this.onSubmit } 
            closeComponent={ this.closeComponent } />
          : undefined }
      </StyledFilterWhen>
    )
  }
}

FilterWhen.propTypes = {
  history: PropTypes.shape({
    location: PropTypes.shape({
      search: PropTypes.string
    }).isRequired
  }).isRequired
}

const mapStateToProps = (state) => ({})

export default connect(
  mapStateToProps,
  { searchProviders }
)( FilterWhen )