import React from 'react'
import PropTypes from 'prop-types'

import {
  StyledFilter,
  StyledFilterTitle,
  StyledFilterHeader,
  StyledFilterCloseButton
} from '../../filter'
import CalendarContainer from '../../../calendar/container'

const SelectToWhen = ({
  minDate,
  onSubmit,
  closeComponent
}) => (
  <StyledFilter>
    <StyledFilterHeader>
      <StyledFilterCloseButton onClick={ closeComponent }>
        cancel
      </StyledFilterCloseButton>
      <StyledFilterTitle>When</StyledFilterTitle>
    </StyledFilterHeader>
    <CalendarContainer
      minDate={ minDate }
      onSubmit={ onSubmit } 
      selectRange={ true } />
  </StyledFilter> 
)

SelectToWhen.defaultProps = {
  minDate: new Date()
}

SelectToWhen.propTypes = {
  minDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date)
  ]),  
  onSubmit: PropTypes.func.isRequired,
  closeComponent: PropTypes.func.isRequired
}

export default SelectToWhen