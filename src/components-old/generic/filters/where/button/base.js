import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../icon/base'
import StyledValue from '../../what/button/styled-value'
import StyledButton from '../../what/button/styled'

const Button = ({
  value,
  openComponent
}) => (
  <StyledButton 
    type="where"
    onClick={ openComponent }>
    <Icon 
      name="search-location" 
      width="14"
      height="19" />
    <StyledValue>{ value }</StyledValue>
  </StyledButton>
)

Button.propTypes = {
  value: PropTypes.string, 
  openComponent: PropTypes.func.isRequired
}

export default Button