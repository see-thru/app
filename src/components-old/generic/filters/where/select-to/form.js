import React from 'react'
import PropTypes from 'prop-types'
import {
  Field,
  reduxForm
} from 'redux-form'

import {
  Input,
  FormWrapper
} from '../../../form/index'
import StyledForm from '../../filter/styled-form'
import Icon from '../../../icon/base';

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <StyledForm>
    <FormWrapper onSubmit={ handleSubmit( onSubmit ) }>
      <Field
        title=""
        name="where"
        type="search"
        placeholder="Where're you searching?"
        component={ Input }
      />
      <Icon 
        name="search-lens-2" 
        width="10" 
        height="10" />      
    </FormWrapper>
  </StyledForm>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'filter-where'
})( Form )