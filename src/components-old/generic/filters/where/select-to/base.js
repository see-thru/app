import React from 'react'
import PropTypes from 'prop-types'

import Form from './form'
import {
  StyledFilter,
  StyledFilterTitle,
  StyledFilterHeader,
  StyledFilterResults,
  StyledFilterCloseButton,
  StyledFilterResultsItem
} from '../../filter'

const SelectToWhere = ({
  closeComponent
}) => (
  <StyledFilter>
    <StyledFilterHeader>
      <StyledFilterCloseButton onClick={ closeComponent }>
        cancel
      </StyledFilterCloseButton>
      <StyledFilterTitle>Where</StyledFilterTitle>
    </StyledFilterHeader>
    <Form onSubmit={ e => console.log(e) } />
    <StyledFilterResults>
      <StyledFilterResultsItem onClick={ () => console.log('New York') }>
        New York
      </StyledFilterResultsItem>
      <StyledFilterResultsItem onClick={ () => console.log('Philadelphia') }>
        Philadelphia
      </StyledFilterResultsItem>
      <StyledFilterResultsItem onClick={ () => console.log('Boston') }>
        Boston
      </StyledFilterResultsItem>
    </StyledFilterResults>
  </StyledFilter> 
)

SelectToWhere.propTypes = {
  closeComponent: PropTypes.func.isRequired
}

export default SelectToWhere