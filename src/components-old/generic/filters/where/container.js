import React, { Component } from 'react'

import Button from './button/base'
import SelectToWhere from './select-to/base'
import StyledFilterWhere from './styled'

class FilterWhere extends Component {
  constructor () {
    super()
    this.state = ({ isOpen: false })
  }

  openComponent = () => {
    this.setState({ isOpen: true })
  }

  closeComponent = () => {
    this.setState({ isOpen: false })
  }

  render () {
    const { isOpen } = this.state
    return (
      <StyledFilterWhere>
        <Button
         value="New York"
         openComponent={ this.openComponent } />
        { isOpen ? 
          <SelectToWhere closeComponent={ this.closeComponent } />
          : undefined }
      </StyledFilterWhere>
    )
  }
}

export default FilterWhere