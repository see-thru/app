import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'
import { StyledContainerGrid } from '../grid'

const StyledFilterContainerGrid = styled(StyledContainerGrid)`
  margin-bottom: 20px;
  margin-top: 20px;
  ${
    Media.small`
      background-color: ${ colors.bgFilterWhat };
      margin: 0;
      padding: 0;
    `
  }
`

export default StyledFilterContainerGrid