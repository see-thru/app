import React from 'react'
import PropTypes from 'prop-types'
import {
  Field,
  reduxForm
} from 'redux-form'

import Icon from '../../../icon/base';
import {
  Input,
  FormWrapper
} from '../../../form/index'
import StyledForm from '../../filter/styled-form'
import StyledSubmitButton from '../../styled-submit-button'

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <StyledForm>
    <FormWrapper onSubmit={ handleSubmit( onSubmit ) }>
      <Field
        title=""
        name="what"
        type="search"
        placeholder="What’s going on?"
        component={ Input }
      />
      <Icon 
        name="search-lens-2" 
        width="10" 
        height="10" />  
      <StyledSubmitButton type="submit">
        Submit
      </StyledSubmitButton>    
    </FormWrapper>
  </StyledForm>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'filterWhat'
})( Form )