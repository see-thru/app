import React from 'react'
import PropTypes from 'prop-types'

import Form from './form'
import {
  StyledFilter,
  StyledFilterTitle,
  StyledFilterHeader,
  StyledFilterCloseButton
} from '../../filter'

const SelectToWhat = ({
  onSubmit,
  closeComponent
}) => (
  <StyledFilter>
    <StyledFilterHeader>
      <StyledFilterCloseButton onClick={ closeComponent }>
        cancel
      </StyledFilterCloseButton>
      <StyledFilterTitle>What</StyledFilterTitle>
    </StyledFilterHeader>
    <Form onSubmit={ onSubmit } />
  </StyledFilter> 
)

SelectToWhat.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  closeComponent: PropTypes.func.isRequired
}

export default SelectToWhat