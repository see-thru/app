import styled from 'styled-components'
import PropTypes from 'prop-types'

import { colors } from '../../../../../utils/settings'

const StyledButton = styled.button`
  ${ props => props.type === 'what' ? `
    background-color: ${ colors.bgFilterWhat };
  ` : undefined }
  ${ props => props.type === 'where' ? `
    background-color: ${ colors.bgFilterDark };
  ` : undefined }
  color: white;
  display: block;
  text-align: left;
  outline: none;
  padding: 12px;
  width: 100%;
  svg {
    display: inline-block;
    vertical-align: middle;
  }
`

StyledButton.defaultProps = {
  type: 'what'
}

StyledButton.propTypes = {
  type: PropTypes.string
}

export default StyledButton