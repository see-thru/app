import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../icon/base'
import StyledValue from './styled-value'
import StyledButton from './styled'

const Button = ({
  value,
  openComponent
}) => (
  <StyledButton onClick={ openComponent }>
    <Icon 
      name="search-lens" 
      width="21"
      height="21" />
    <StyledValue>{ value }</StyledValue>
  </StyledButton>
)

Button.propTypes = {
  value: PropTypes.string, 
  openComponent: PropTypes.func.isRequired
}

export default Button