import styled from 'styled-components'

const StyledValue = styled.span`
  display: inline-block;
  margin-left: 10px;
  vertical-align: middle;
`

export default StyledValue