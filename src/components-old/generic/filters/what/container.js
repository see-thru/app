import React, { Component } from 'react'
import qs from 'qs'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { searchProviders } from '../../../../actions-old/search'

import Button from './button/base'
import SelectToWhat from './select-to/base'
import StyledFilterWhat from './styled'

export class FilterWhat extends Component {
  constructor () {
    super()
    this.state = ({ isOpen: false })
  }

  openComponent = () => {
    this.setState({ isOpen: true })
  }

  closeComponent = () => {
    this.setState({ isOpen: false })
  }

  onSubmit = e => {
    const { 
      history,
      searchProviders
    } = this.props
    this.closeComponent()
    const parsed = qs.parse(history.location.search)
    let to = parsed.to
    let from = parsed.from
    if ( to && from ) {
      e.when = {
        to, from
      }
    }
    searchProviders(e)
    const what = e.what ? `&what=${ e.what }` : ''
    from = from ? `&from=${ from }` : ''
    to = to ? `&to=${ to }` : ''
    history.push(`/search?${ what }${ from }${ to }`)
  }

  render () {
    const { isOpen } = this.state
    const { history } = this.props
    const parsed = qs.parse(history.location.search)
    return (
      <StyledFilterWhat>
        <Button
         value={ parsed.what ? parsed.what : 'What’s going on?' }
         openComponent={ this.openComponent } />
        { isOpen ? 
          <SelectToWhat 
            onSubmit={ this.onSubmit }
            closeComponent={ this.closeComponent } />
          : undefined }
      </StyledFilterWhat>
    )
  }
}

FilterWhat.propTypes = {
  history: PropTypes.shape({
    location: PropTypes.shape({
      search: PropTypes.string
    }).isRequired
  }).isRequired
}

const mapStateToProps = (state) => ({})

export default connect(
  mapStateToProps,
  { searchProviders }
)( FilterWhat )