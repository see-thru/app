import styled from 'styled-components'

import { colors, fonts } from '../../../utils/settings'

const StyledSubmitButton = styled.button`
  background: ${ colors.successButton };
  border-radius: 2px;
  color: white;
  font-size: .8rem;
  font-weight: ${ fonts.weightBold };
  right: 20px;
  text-transform: uppercase;
  top: 15px;
  padding: 4px 8px;
  position: absolute;
`

export default StyledSubmitButton