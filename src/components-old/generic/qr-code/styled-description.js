import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledQRCodeDescription = styled.p`
  display: block;
  margin-top: 5px;
  small {
    color: ${ colors.textLight };
  }
`

export default StyledQRCodeDescription