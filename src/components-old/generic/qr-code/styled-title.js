import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledQRCodeTitle = styled.h3`
  color: ${ colors.successButton };
  font-size: 1rem;
  margin-top: 0;
  text-align: center;
  text-transform: uppercase;
`

export default StyledQRCodeTitle