import React from 'react'

import Status from './status/'
import Patient from './patient/'

const VisitStartedContainer = ( props ) => (
  <div>
    <Status { ...props } />
    <Patient 
      patient={ props.booking && props.booking.patient ? props.booking.patient : {} }
      totalPrice={ props.booking && props.booking.cost ? `$${ props.booking.cost }` : '$0' } />
  </div>
)

export default VisitStartedContainer