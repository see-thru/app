import styled from 'styled-components'

import { colors } from '../../../../../../utils/settings'

const StyledInfo = styled.div`
  margin: 20px 10px;
  h3, 
  img {
    display: inline-block;
    vertical-align: middle;
  }
  h3 {
    color: ${ colors.successButton };
    font-size: 1rem;
    margin: 0;
  }
  img {
    margin-right: 10px;
  }
  ul {
    list-style: none;
    margin: 10px 0;
    padding: 0;
  }
  li {
    font-size: .9rem;
    margin: 5px 0;
  }
  span {
    color: ${ colors.textLight };
  }
`

export default StyledInfo