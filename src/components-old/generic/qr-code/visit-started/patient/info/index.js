import React from 'react'
import PropTypes from 'prop-types'

import StyledInfo from './styled'

const Info = ({
  sex,
  email,
  photo,
  birthday,
  fullName,
  languages
}) => {
  let languagesFormatted = ''
  languages.map( language => languagesFormatted += ` ${ language.value }` )
  return (
    <StyledInfo>
      <img 
        alt={ fullName }
        src={ photo }
        width="40"
        height="40" />
      <h3>{ fullName }</h3>
      <ul>
        <li><span>Sex:</span> { sex }</li>
        <li><span>Birthday:</span> { birthday }</li>
        <li><span>Email:</span> { email }</li>
        <li><span>Language:</span> { languagesFormatted }</li>
      </ul>
    </StyledInfo>
  )
}

Info.defaultProps = {
  photo: '/images/example-avatar.png'
}

Info.propTypes = {
  sex: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  photo: PropTypes.string,
  birthday: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  languages: PropTypes.array.isRequired
}

export default Info