import React from 'react'
import PropTypes from 'prop-types'

import Info from './info/'

const Patient = ({ 
  patient,
  totalPrice
}) => (
  <div>
    <Info 
      sex={ patient.sex }
      email={ patient.email }
      birthday={ patient.dob }
      fullName={ `${ patient.firstName } ${ patient.lastName }` }
      languages={ patient.languages ? patient.languages : [] } />
  </div>
)

Patient.propTypes = {
  patient: PropTypes.shape({
    sex: PropTypes.oneOf(['male', 'female']),
    email: PropTypes.string,
    birthday: PropTypes.string,
    lastName: PropTypes.string,
    firstName: PropTypes.string,
    languages: PropTypes.array
  }),
  totalPrice: PropTypes.string
}

export default Patient