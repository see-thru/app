import React from 'react'
import PropTypes from 'prop-types'
import {
  Field,
  reduxForm
} from 'redux-form'
import { required } from 'redux-form-validators'

import {
  Textarea,
  FormWrapper
} from '../../../../form/'
import StyledMessage from './styled-message'

const Form = ({
  onSubmit,
  handleSubmit,
  amountToCharge
}) => (
  <FormWrapper onSubmit={ handleSubmit( onSubmit ) }>
    <Field
      title="Visit Notes"
      name="visit-notes"
      component={ Textarea }
      validate={ [
        required()
      ] }
    />
    <StyledMessage>
      Please complete the patient notes to receive your payment { amountToCharge }
    </StyledMessage>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  amountToCharge: PropTypes.string.isRequired
}

export default reduxForm({
  form : 'visitNotesForm'
})( Form )