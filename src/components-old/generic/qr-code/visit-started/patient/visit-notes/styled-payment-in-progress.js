import styled from 'styled-components'

const StyledPaymentInProgress = styled.div`
  margin-top: 10px;
  p,
  h4 {
    margin: 0;
  }
  p {
    font-size: .9rem;
  }  
  svg {
    bottom: 3px;
    margin-right: 6px;
    position: relative;
    vertical-align: middle;
  }
`

export default StyledPaymentInProgress