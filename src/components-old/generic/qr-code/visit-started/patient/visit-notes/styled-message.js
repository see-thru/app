import styled from 'styled-components'

import { colors } from '../../../../../../utils/settings'

const StyledMessage = styled.p`
  clear: both;
  color: ${ colors.textTitle };
  font-size: .9rem;
  font-weight: 700;
  margin: 10px 0;
`

export default StyledMessage