import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Form from './form'
import StyledVisitNotes from './styled'
import PaymentInProgress from './payment-in-progress'

class VisitNotes extends Component {
  constructor () {
    super()
    this.state = ({
      isVisitNotesFormSended: false
    })
  }

  onSubmit = e => {
    console.log(e)
    this.setState({ isVisitNotesFormSended: true })
  }

  render () {
    const { isVisitNotesFormSended } = this.state
    return (
      <StyledVisitNotes>
        { isVisitNotesFormSended ?
          <PaymentInProgress />
          : 
          <Form
            { ...this.props } 
            onSubmit={ this.onSubmit } />
        }
      </StyledVisitNotes>
    )
  }
}

VisitNotes.propTypes = {
  amountToCharge: PropTypes.string.isRequired
}

export default VisitNotes