import React from 'react'

import Icon from '../../../../icon/base'
import StyledPaymentInProgress from './styled-payment-in-progress'

const PaymentInProgress = () => (
  <StyledPaymentInProgress>
    <h4>
      <Icon
        name="check"
        width="24"
        height="24" />
      Thanks!      
    </h4>
    <p>Your payment in progress.</p>
  </StyledPaymentInProgress>
)

export default PaymentInProgress