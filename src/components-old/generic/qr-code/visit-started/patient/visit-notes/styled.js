import styled from 'styled-components'

import { colors } from '../../../../../../utils/settings'

const StyledVisitNotes = styled.div`
  border-top: 1px solid ${ colors.border };
  margin-top: 10px;
  padding: 10px 10px 0;
`

export default StyledVisitNotes