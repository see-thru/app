import React from 'react'

import StyledPatientRecord from './styled'

const PatientRecord = () => (
  <StyledPatientRecord>
    <h4>Recent Visit</h4>
    <span>June 22</span>
    <p>All the patient biometrics looks regular. The blood exams looks good, except for the colesterol of litle higher than limit: 210.</p>
    <button>Patient Record</button>
  </StyledPatientRecord>
)

export default PatientRecord