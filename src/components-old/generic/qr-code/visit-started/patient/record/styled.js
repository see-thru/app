import styled from 'styled-components'
import { colors } from '../../../../../../utils/settings';

const StyledPatientRecord = styled.div`
  margin: 20px 10px;
  h4 {
    font-size: 1rem;
    margin: 0;
  }
  span {
    background: ${ colors.lightBorder };
    border-radius: 2px;
    color: ${ colors.placeholderFields };
    display: inline-block;
    font-size: .9rem;
    font-weight: 700;
    margin-top: 10px;
    padding: 4px 8px;
  }
  p {
    font-size: .9rem;
    margin-top: 5px;
  }
  button {
    padding: 0;
    font-size: .9rem;
    font-weight: 700;
    color: ${ colors.successButton };
  }
`

export default StyledPatientRecord