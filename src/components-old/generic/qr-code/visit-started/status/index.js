import React from 'react'
import moment from 'moment-timezone'
import PropTypes from 'prop-types'

import Item from './item/'
import StyledStatus from './styled'

const Status = ({ booking }) => {
  moment.tz.setDefault('EST')
  return (
    <StyledStatus>
      <Item
        hour={ moment.utc(booking.checkInTime).format('LT') }
        message={ `Visit Started with ${ booking.patient.firstName } ${ booking.patient.lastName }` }
        isCompleted />   
      <Item
        hour=''
        message='Visit time 15 minutes'
        isCompleted />           
    </StyledStatus>
  )
}

Status.propTypes = {
  booking: PropTypes.shape({
    patient: PropTypes.shape({
      lastName: PropTypes.string,
      firstName: PropTypes.string
    }),
    checkInTime: PropTypes.number
  })
}

export default Status