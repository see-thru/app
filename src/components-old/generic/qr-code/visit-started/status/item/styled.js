import styled from 'styled-components'

import Media from '../../../../../../utils/media'
import { colors } from '../../../../../../utils/settings';

const StyledItem = styled.div`
  border-bottom: 1px solid ${ colors.bgLightFooter };
  font-size: .9rem;
  margin-bottom: 10px;
  padding: 10px;
  &:last-of-type {
    border-bottom-color: transparent;
    padding-bottom: 0;
  }
  ${
    Media.small`
      font-size: .85rem;
    `
  }
  p,
  span {
    display: inline-block;
    vertical-align: middle;
  }
  p {
    margin: 0 0 0 20px;
    padding-left: 28px;
    position: relative;
    ${
      Media.small`
        display: block;
        margin: 5px 0;
      `
    }
  }
  svg {
    left: 0;
    top: -2px;
    position: absolute;
  }
  span {
    font-weight: 700;
  }
`

export default StyledItem