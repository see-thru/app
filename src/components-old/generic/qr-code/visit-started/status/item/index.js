import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../../../generic/icon/base'
import StyledItem from './styled'

const Item = ({
  hour,
  message,
  isCompleted,
}) => (
  <StyledItem>
    <span>{ hour }</span>
    <p>
      { isCompleted ? 
        <Icon 
          name="check"
          width="24"
          height="24" />
        : undefined }
      { message }
    </p>
  </StyledItem>
)

Item.propTypes = {
  hour: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  isCompleted: PropTypes.bool
}

export default Item