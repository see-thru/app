import styled from 'styled-components'

import Media from '../../../../../utils/media'
import { colors } from '../../../../../utils/settings'

const StyledStatus = styled.div`
  background-color: ${ colors.bgFooter };
  border-radius: 2px;
  color: white;
  padding: 10px 20px; 
  ${
    Media.small`
      padding: 10px;
    `
  }
`

export default StyledStatus