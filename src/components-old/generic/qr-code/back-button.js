import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import StyledButtonBackToEdit from '../../user/button-back-to-edit/styled'

const BackButton = ({ to }) => (
  <StyledButtonBackToEdit>
    <Link to={ to }>
      Back to Appointments
    </Link>
  </StyledButtonBackToEdit>
)

BackButton.propTypes = {
  to: PropTypes.string.isRequired
}

export default BackButton