import React from 'react'
import PropTypes from 'prop-types'

import Sunday from './days/sunday'
import Friday from './days/friday'
import Monday from './days/monday'
import Tuesday from './days/tuesday'
import Thursday from './days/thursday'
import Saturday from './days/saturday'
import Wednesday from './days/wednesday'
import StyledSchedulesContainerGrid from './styled-container'

const Schedules = ({ 
  hours,
  deleteHour,
  selectToHour,
  HourIsSelected
}) => (
  <StyledSchedulesContainerGrid
    gridGap="10px"
    templateRows="auto"
    templateColumns="repeat(7, 1fr)">
    <Monday
      hours={ hours.mon }
      deleteHour={ deleteHour }
      selectToHour={ selectToHour }
      HourIsSelected={ HourIsSelected } />
    <Tuesday
      hours={ hours.tue }
      deleteHour={ deleteHour }
      selectToHour={ selectToHour }
      HourIsSelected={ HourIsSelected } />
    <Wednesday
      hours={ hours.wed }
      deleteHour={ deleteHour }
      selectToHour={ selectToHour }
      HourIsSelected={ HourIsSelected } />
    <Thursday
      hours={ hours.thu }
      deleteHour={ deleteHour }
      selectToHour={ selectToHour }
      HourIsSelected={ HourIsSelected } />
    <Friday
      hours={ hours.fri }
      deleteHour={ deleteHour }
      selectToHour={ selectToHour }
      HourIsSelected={ HourIsSelected } />      
    <Saturday
      hours={ hours.sat }
      deleteHour={ deleteHour }
      selectToHour={ selectToHour }
      HourIsSelected={ HourIsSelected } /> 
    <Sunday
      hours={ hours.sun }
      deleteHour={ deleteHour }
      selectToHour={ selectToHour }
      HourIsSelected={ HourIsSelected } />            
  </StyledSchedulesContainerGrid>
)

Schedules.propTypes = {
  hours: PropTypes.shape({
    mon: PropTypes.array,
    tue: PropTypes.array,
    wed: PropTypes.array,
    thu: PropTypes.array,
    fri: PropTypes.array,
    sat: PropTypes.array,
    sun: PropTypes.array
  }),
  deleteHour: PropTypes.func.isRequired,
  selectToHour: PropTypes.func.isRequired,
  HourIsSelected: PropTypes.func.isRequired
}

export default Schedules