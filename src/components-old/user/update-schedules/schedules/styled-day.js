import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledDay = styled.div`
  color: ${ colors.schedulesDay };
  font-size: .8rem;
  text-align: center;
  text-transform: uppercase;
`

export default StyledDay