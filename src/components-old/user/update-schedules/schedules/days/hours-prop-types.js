import PropTypes from 'prop-types'

const HoursPropTypes = PropTypes.shape({
  hour: PropTypes.number.isRequired
})

export default HoursPropTypes