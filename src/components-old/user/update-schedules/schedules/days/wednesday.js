import React from 'react'
import PropTypes from 'prop-types'

import StyledDay from '../styled-day'
import StyledHour from '../styled-hour'
import HoursPropTypes from './hours-prop-types'
import { HoursAvailable as CatalogHours } from '../../../../../catalogs'

const Monday = ({
  hours,
  deleteHour,
  selectToHour,
  HourIsSelected
}) => (
  <div>
    <StyledDay>Wed</StyledDay>
    { CatalogHours.map( ( hour, index ) => {
      return (
        <StyledHour
          key={ index } 
          type="button"
          onClick={ HourIsSelected(hours, hour.value) === 1 ? deleteHour.bind(this, 'wed', hour.value) : selectToHour.bind(this, 'wed', hour.value) }
          isSelected={ HourIsSelected(hours, hour.value) === 1 ? 'yes' : 'no' }>
          { hour.key }
        </StyledHour>
      )
    }) }       
  </div>  
)

Monday.propTypes = {
  hours: PropTypes.arrayOf(HoursPropTypes),
  deleteHour: PropTypes.func.isRequired,
  selectToHour: PropTypes.func.isRequired,
  HourIsSelected: PropTypes.func.isRequired
}

export default Monday 