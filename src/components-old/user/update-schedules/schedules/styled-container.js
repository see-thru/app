import styled from 'styled-components'

import { StyledContainerGrid } from '../../../generic/grid'

const StyledSchedulesContainerGrid = styled(StyledContainerGrid)`
  margin: 0 0 20px;
  padding-left: 0;
  padding-right: 0;
`

export default StyledSchedulesContainerGrid