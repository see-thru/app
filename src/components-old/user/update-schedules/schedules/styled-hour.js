import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import Media from '../../../../utils/media';

const StyledHour = styled.button`
  background-color: ${ props => props.isSelected === 'yes' ? colors.bgCalendarSelectDay : colors.lightBorder };
  border-radius: 2px;
  color: ${ props => props.isSelected === 'yes' ? 'white' : colors.placeholderFields };
  font-size: .8rem;
  margin: 5px 0;
  text-transform: uppercase;
  padding: 14px 8px;
  width: 100%;
  ${
    Media.small`
      font-size: .75rem;
      padding: 4px;
    `
  }
`

export default StyledHour