import React, { Component } from 'react'

import Schedules from './base'
import AddHourByDay from '../../../../utils/schedules/add-hour-by-day'
import HourIsSelected from '../../../../utils/schedules/hours-is-selected'
import DeleteHourByDay from '../../../../utils/schedules/delete-hour-by-day'
import GeneratorInitialValuesSchedulesForm from '../../../../utils/generator-initial-values-form/schedules/'

class SchedulesContainer extends Component {
  constructor () {
    super()
    this.state = ({
      hours: {
        mon: [],
        tue: [],
        wed: [],
        thu: [],
        fri: [],
        sat: [],
        sun: [],
      }
    })
  }
 
  componentDidUpdate ( prevProps ) {
    const { loading } = this.props
    if ( ! loading && prevProps.profile !== this.props.profile ) {
      this.setState({ hours: GeneratorInitialValuesSchedulesForm(this.props.profile) })
    }
  }

  deleteHour = ( day, hour ) => {
    const { hours } = this.state
    const { change } = this.props
    switch (day) {
      case 'mon':
        DeleteHourByDay( hours.mon, hour )
        break;
      case 'tue':
        DeleteHourByDay( hours.tue, hour )
        break;
      case 'wed':
        DeleteHourByDay( hours.wed, hour )
        break;
      case 'thu':
        DeleteHourByDay( hours.thu, hour )
        break; 
      case 'fri':
        DeleteHourByDay( hours.fri, hour )
        break;  
      case 'sat':
        DeleteHourByDay( hours.sat, hour )
        break;  
      case 'sun':
        DeleteHourByDay( hours.sun, hour )
        break;                                          
      default:
        break;
    }
    change('hours', hours)
    this.setState({ hours })
  }

  selectToHour = (day, hour) => {
    const { hours } = this.state
    const { change } = this.props
    switch (day) {
      case 'mon':
        AddHourByDay( hours.mon, hour )
        break;
      case 'tue':
        AddHourByDay( hours.tue, hour )
        break;    
      case 'wed':
        AddHourByDay( hours.wed, hour )
        break;    
      case 'thu':
        AddHourByDay( hours.thu, hour )     
        break;   
      case 'fri':
        AddHourByDay( hours.fri, hour )      
        break;             
      case 'sat':
        AddHourByDay( hours.sat, hour )      
        break;  
      case 'sun':
        AddHourByDay( hours.sun, hour ) 
        break;            
      default:
        break;
    }
    change('hours', hours)
    this.setState({ hours })
  }

  render () {
    const { hours } = this.state
    return (
      <Schedules 
        hours={ hours }
        deleteHour={ this.deleteHour }
        selectToHour={ this.selectToHour }
        HourIsSelected={ HourIsSelected } />
    )
  }
}

export default SchedulesContainer