import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { 
  isDirty,
  reduxForm 
} from 'redux-form'

import { FormWrapper } from '../../generic/form/index'
import SchedulesContainer from './schedules/container'
import { StyledButtonSuccess } from '../../generic/button/styled'
import GeneratorInitialValuesSchedulesForm from '../../../utils/generator-initial-values-form/schedules/'

export let Form = ({
  change,
  isDirty,
  loading,
  profile,
  onSubmit,
  handleSubmit
}) => {
  return (
    <FormWrapper onSubmit={ handleSubmit( onSubmit ) }>
      <SchedulesContainer
        change={ change }
        loading={ loading }
        profile={ profile ? profile : {} }  />
      { isDirty ? 
        <StyledButtonSuccess type="submit">
          Save
        </StyledButtonSuccess>
        : undefined }
    </FormWrapper>
  )
}

Form.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

Form = reduxForm({
  form : 'updateSchedules',
  initialValues : {},
  enableReinitialize : true
})( Form )

const FormContainer = connect(
  state => ({
    isDirty: isDirty('updateSchedules')(state),
    profile: state.ProviderReducer.profile,
    initialValues: state.ProviderReducer.profile !== null ? GeneratorInitialValuesSchedulesForm(state.ProviderReducer.profile) : {}
  }),
)(Form)

export default FormContainer