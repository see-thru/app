import React from 'react'
import PropTypes from 'prop-types'

import Form from './form'
import Loading from '../../generic/loader/base'
import ButtonBackToEdit from '../button-back-to-edit/base'
import StyledWrapperContent from '../styled-wrapper-content'

const UpdateSchedules = ({
  loading,
  onSubmit
}) => (
  <StyledWrapperContent isVeryWide>
    { loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    <Form
      loading={ loading } 
      onSubmit={ onSubmit } />
  </StyledWrapperContent>  
)

UpdateSchedules.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired
}

export default UpdateSchedules