import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { 
  editTimeSchedule,
  getProviderProfile
} from '../../../actions-old/provider'
import generateDataForTimeSchedule from '../../../utils/generator-data-structure/time-schedule/'

import UpdateSchedules from './base'

export class UpdateSchedulesContainer extends Component {
  componentWillMount () {
    const {
      user, 
      getProviderProfile 
    } = this.props
    getProviderProfile(user.token)
  }

  onSubmit = e => {
    const {
      user, 
      editTimeSchedule 
    } = this.props
    const data = generateDataForTimeSchedule(e)
    editTimeSchedule( user.token, data )
  }

  render () {
    return (
      <UpdateSchedules 
        onSubmit={ this.onSubmit }
        { ...this.props } />
    )
  }
}

UpdateSchedulesContainer.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  loading: PropTypes.bool,
  editTimeSchedule: PropTypes.func.isRequired,
  getProviderProfile: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  loading: state.ProviderReducer.loading
})

export default connect(
  mapStateToProps, 
  { 
    editTimeSchedule,
    getProviderProfile
  }
)( UpdateSchedulesContainer )