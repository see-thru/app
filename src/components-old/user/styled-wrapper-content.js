import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../utils/media'
import { colors } from '../../utils/settings'

const StyledWrapperContent = styled.div`
  background-color: white;
  border: 1px solid ${ colors.border };
  padding: 15px 30px;
  max-width: ${ props => props.isVeryWide ? '100%' : '450px' };
  ${ 
    Media.small`
      border: 0;
      max-width: inherit;
      padding: 0 10px;
    `
  }
`

StyledWrapperContent.propTypes = {
  isVeryWide: PropTypes.bool 
}

export default StyledWrapperContent