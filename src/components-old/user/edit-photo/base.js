import React from 'react'
import PropTypes from 'prop-types'

import Form from './form'
import Loading from '../../generic/loader/base'
import CurrentPhoto from './current-photo/base'
import ButtonBackToEdit from '../button-back-to-edit/base'
import StyledWrapperContent from '../styled-wrapper-content'
import StyledSuccessMessage from '../styled-success-message'

const EditPhoto = ({
  user,
  photo,
  onSubmit,
  uploadedPhoto,
  deleteMyPhoto
}) => (
  <StyledWrapperContent>
    { photo.loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    { user && user.photo && uploadedPhoto.success ? 
      <StyledSuccessMessage>Photo uploaded!</StyledSuccessMessage> 
      : undefined 
    }
    { user && user.photo ? 
      <CurrentPhoto
        token={ user.token } 
        photo={ user.photo }
        deleteMyPhoto={ deleteMyPhoto } />
      : undefined
    }
    <Form onSubmit={ e => onSubmit(e) } />
  </StyledWrapperContent>
)

EditPhoto.defaultProps = {
  uploadedPhoto: {}
}

EditPhoto.propTypes = {
  user: PropTypes.shape({
    photo: PropTypes.string
  }).isRequired,
  photo: PropTypes.shape({
    loading: PropTypes.bool
  }).isRequired,
  onSubmit: PropTypes.func.isRequired,
  uploadedPhoto: PropTypes.object
}

export default EditPhoto
