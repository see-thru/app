import React from 'react'
import PropTypes from 'prop-types'

import StyledDeleteButton from './styled-delete-button'
import StyledCurrentPhotoImg from './styled-current-photo-img'

const CurrentPhoto = ({
  token,
  photo,
  deleteMyPhoto,
  isAllowedDeletePhoto
}) => (
  <div>
    <StyledCurrentPhotoImg 
      src={ photo } 
      alt="current" />
    { isAllowedDeletePhoto ? 
      <StyledDeleteButton onClick={ deleteMyPhoto.bind(this, token) }>
        Delete current photo
      </StyledDeleteButton>
      : undefined }
  </div>
)

CurrentPhoto.defaultProps = {
  isAllowedDeletePhoto: true
}

CurrentPhoto.propTypes = {
  token: PropTypes.string,
  photo: PropTypes.string.isRequired,
  deleteMyPhoto: PropTypes.func,
  isAllowedDeletePhoto: PropTypes.bool
}

export default CurrentPhoto