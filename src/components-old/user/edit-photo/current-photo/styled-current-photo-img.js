import styled from 'styled-components'

const StyledCurrentPhotoImg = styled.img`
  max-width: 150px;
  width: 100%;
`

export default StyledCurrentPhotoImg