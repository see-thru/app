import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledDeleteButton = styled.button`
  color: ${ colors.alert };
  display: block;
  font-size: .9rem;
  padding: 5px 0;
`

export default StyledDeleteButton