import React from 'react'
import PropTypes from 'prop-types'
import { 
  Field,
  reduxForm
} from 'redux-form'
import { required } from 'redux-form-validators'

import { 
  FormWrapper,
  UploadContainer
} from '../../generic/form/index'
import { StyledButtonSuccess } from '../../generic/button/styled'

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper
    onSubmit={ handleSubmit( (e) => onSubmit(e) ) }>
    <Field
      name="photo"
      title="Photo"
      component={ UploadContainer }
      type="file"
      accept=".jpg, .png"
      validate={ [
        required()
      ] }
    />
    <StyledButtonSuccess type="submit">
      Save
    </StyledButtonSuccess>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

export default reduxForm({
  form : 'editPhoto',
  initialValues : {}
})( Form )