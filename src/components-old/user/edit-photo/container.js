import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  getMyPhoto,
  editMyPhoto,
  deleteMyPhoto
} from '../../../actions-old/user'

import EditPhoto from './base'

export class EditPhotoContainer extends Component {
  componentWillMount () {
    const {
      user,
      getMyPhoto
    } = this.props
    getMyPhoto(user.token)
  }

  componentWillReceiveProps (nextProps) {
    const {
      user,
      getMyPhoto
    } = nextProps
    if ( user.token !== this.props.user.token ) {
      getMyPhoto(user.token)
    }
  }

  onSubmit = e => {
    const { 
      user,
      editMyPhoto 
    } = this.props
    editMyPhoto(e.photo[0], e.photo[0].crop, user.token)
  }

  render () {
    return (
      <EditPhoto 
        onSubmit={ this.onSubmit }
        { ...this.props } />
    )
  }
}

EditPhotoContainer.propTypes = {
  user: PropTypes.shape({
    photo: PropTypes.string,
    token: PropTypes.string.isRequired
  }).isRequired,
  photo: PropTypes.shape({
    loading: PropTypes.bool
  }).isRequired,
  getMyPhoto: PropTypes.func.isRequired,
  editMyPhoto: PropTypes.func.isRequired,
  deleteMyPhoto: PropTypes.func.isRequired,
  uploadedPhoto: PropTypes.object
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  photo: state.AuthReducer.photo,
  uploadedPhoto: state.AuthReducer.uploadedPhoto
})

export default connect(
  mapStateToProps, 
  { 
    getMyPhoto,
    editMyPhoto,
    deleteMyPhoto
  }
)( EditPhotoContainer )