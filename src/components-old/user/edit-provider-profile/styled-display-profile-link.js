import styled from 'styled-components'

import { fonts, colors } from '../../../utils/settings'
import { StyledDefaultButton } from '../../generic/button/styled'

const StyledDisplayProfileLink = styled(StyledDefaultButton)`
  color: ${ colors.textTitle };
  display: block;
  font-weight: ${ fonts.weightBold };
  margin-top: 20px;
  svg {
    margin-left: 10px;
  }
`

export default StyledDisplayProfileLink