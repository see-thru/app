import React from 'react'
import { Field } from 'redux-form'
import { required } from 'redux-form-validators'

import { 
  Input,
  Legend,
  FieldsetWrapper,
  SelectCustomizable
} from '../../../generic/form/index'
import { degrees as degreesCatalog } from '../../../../catalogs'

const Information = () => (
  <FieldsetWrapper>
    <Legend>Information:</Legend>
    <Field
      name="degrees"
      title="Degree/s"
      multi={ true }
      options={ degreesCatalog }
      component={ SelectCustomizable }
      validate={ [
        required()
      ] }
    />    
    <Field
      name="personalNPI"
      title="Personal NPI"
      component={ Input }
      validate={ [
        required()
      ] }
    />
    <Field
      name="practiceName"
      title="Practice Name"
      component={ Input }

    />
    <Field
      name="practiceNPI"
      title="Practice NPI"
      component={ Input }
          />
  </FieldsetWrapper>
)

export default Information