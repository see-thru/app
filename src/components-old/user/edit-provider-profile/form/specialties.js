import React from 'react'
import { Field } from 'redux-form'
import { required } from 'redux-form-validators'

import { 
  Legend,
  FieldsetWrapper,
  SelectCustomizable
} from '../../../generic/form/index'
import { 
  specialities as specialitiesCatalog,
  subSpecialities as subSpecialitiesCatalog 
} from '../../../../catalogs'

const Specialties = () => (
  <FieldsetWrapper>
    <Legend>Specialties:</Legend>
    <Field
      name="primary"
      title="Primary"
      options={ specialitiesCatalog }
      component={ SelectCustomizable }
      validate={ [
        required()
      ] }
    />  
    <Field
      name="secondary"
      title="Secondary"
      multi={ true }
      options={ subSpecialitiesCatalog }
      component={ SelectCustomizable }
    />        
  </FieldsetWrapper>
)

export default Specialties