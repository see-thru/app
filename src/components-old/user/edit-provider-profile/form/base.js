import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'

import Bio from './bio'
import Location from './location'
import Information from './information'
import Specialties from './specialties'
import MedicalTraining from './medical-training'
import { FormWrapper } from '../../../generic/form/index'
import { StyledButtonSuccess } from '../../../generic/button/styled'

import GeneratorInitialValuesEditProviderProfileForm from '../../../../utils/generator-initial-values-form/edit-provider-profile'

export let Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper onSubmit={ handleSubmit( onSubmit ) }>
    <Information />
    <Location />
    <MedicalTraining />
    <Specialties />
    <Bio />
    <StyledButtonSuccess type="submit">
      Save
    </StyledButtonSuccess>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

Form = reduxForm({
  form : 'editProviderProfile',
  initialValues : {},
  enableReinitialize : true
})( Form )

const FormContainer = connect(
  state => ({
    initialValues: state.ProviderReducer.profile !== null && state.ProviderReducer.profile.data ? GeneratorInitialValuesEditProviderProfileForm(state.ProviderReducer.profile.data, state.AuthReducer.user) : {}
  }),
)(Form)

export default FormContainer