import React from 'react'
import { Field } from 'redux-form'

import { 
  Input,
  Legend,
  FieldsetWrapper
} from '../../../generic/form/index'

const Location = () => (
  <FieldsetWrapper>
    <Legend>Location:</Legend>
    <Field
      name="street"
      title="Street"
      component={ Input }
    />
    <Field
      name="unit"
      title="Unit"
      component={ Input }
    />
    <Field
      name="city"
      title="City"
      component={ Input }
    />
    <Field
      name="state"
      title="State"
      component={ Input }

    />
    <Field
      name="zip"
      title="Zip"
      component={ Input }
    />
  </FieldsetWrapper>
)

export default Location