import React from 'react'
import { Field } from 'redux-form'

import { 
  Legend,
  Textarea,
  FieldsetWrapper
} from '../../../generic/form/index'

const Bio = () => (
  <FieldsetWrapper>
    <Legend>Biogragraphy:</Legend>
    <Field
      name="bio"
      title=""
      component={ Textarea }
    />
  </FieldsetWrapper>
)

export default Bio