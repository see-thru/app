import React from 'react'
import { Field } from 'redux-form'

import getYearsCatalog from '../../../../utils/select/generate-years'

import { 
  Input,
  Legend,
  FieldsetWrapper,
  SelectCustomizable
} from '../../../generic/form/index'

const MedicalTraining = () => (
  <FieldsetWrapper>
    <Legend>Medical Training:</Legend>
    <Field
      name="medicalSchool"
      title="Medical School"
      component={ Input }
          />
    <Field
      name="medicalSchoolYear"
      title="Year"
      options={ getYearsCatalog(1980, 2017, true) }
      component={ SelectCustomizable }
    />  
    <Field
      name="residency"
      title="Residency"
      component={ Input }
          />
    <Field
      name="residencyYear"
      title="Year"
      options={ getYearsCatalog(1980, 2017, true) }
      component={ SelectCustomizable }
    />  
    <Field
      name="fellowship"
      title="Fellowship"
      component={ Input }
          />
    <Field
      name="fellowshipYear"
      title="Year"
      options={ getYearsCatalog(1980, 2017, true) }
      component={ SelectCustomizable }
    />      
  </FieldsetWrapper>
)

export default MedicalTraining