import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { 
  updateProvider,
  getProviderProfile 
} from '../../../actions-old/provider'
import generateDataForUpdateToProvider from '../../../utils/generator-data-structure/update-to-provider'

import EditProviderProfile from './base'

export class EditProviderProfileContainer extends Component {
  componentWillMount () {
    const { 
      user,
      getProviderProfile 
    } = this.props
    getProviderProfile(user.token)
  }

  onSubmit = e => {
    const {
      user, 
      updateProvider
    } = this.props
    let data = generateDataForUpdateToProvider(e)
    updateProvider(data, user.token, user.providerDisplayId)    
  }

  render () {
    const { 
      profile,
      loading,
      providerData
    } = this.props
    return (
      <EditProviderProfile 
        loading={ loading }
        onSubmit={ this.onSubmit }
        isUpdated={ providerData.success }
        providerDisplayId={ profile.data.providerDisplayId } />
    )
  }
}

EditProviderProfileContainer.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  loading: PropTypes.bool,
  providerData: PropTypes.shape({
    success: PropTypes.bool
  }),
  profile: PropTypes.shape({
    providerDisplayId: PropTypes.string
  }),
  updateProvider: PropTypes.func.isRequired,
  getProviderProfile: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  profile: state.ProviderReducer.profile,
  loading: state.ProviderReducer.loading,
  providerData: state.ProviderReducer.data
})

export default connect(
  mapStateToProps, 
  { 
    updateProvider,
    getProviderProfile 
  }
)( EditProviderProfileContainer )