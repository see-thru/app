import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import Icon from '../../generic/icon/base'
import Form from './form/base'
import Loading from '../../generic/loader/base'
import ButtonBackToEdit from '../button-back-to-edit/base'
import StyledWrapperContent from '../styled-wrapper-content'
import StyledSuccessMessage from '../styled-success-message'
import StyledDisplayProfileLink from './styled-display-profile-link'

const EditProfile = ({
  loading,
  onSubmit,
  isUpdated,
  providerDisplayId
}) => (
  <StyledWrapperContent>
    { loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    { isUpdated ? 
      <StyledSuccessMessage>Your provider profile was updated successfully!</StyledSuccessMessage> 
      : undefined }
    <Form onSubmit={ onSubmit } />
    <StyledDisplayProfileLink 
      to={ `/provider/${ providerDisplayId }/_` }
      tag={ Link } >
      Display My Provider Profile
      <Icon 
        name="date-next-arrow"
        width="40" 
        height="7" />
    </StyledDisplayProfileLink>
  </StyledWrapperContent>
)

EditProfile.prototypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  isUpdated: PropTypes.bool,
  providerDisplayId: PropTypes.string.isRequired
}

export default EditProfile