import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../utils/media'

const StyledNav = styled.div`
  display: ${ props => props.type === 'mobile' ? 'none' : 'block' }
  ${
    Media.small`
      display: ${ props => props.type === 'mobile' ? 'block' : 'none' }
    `
  }
`

StyledNav.defaultProps = {
  type: 'desktop'
}

StyledNav.propTypes = {
  type: PropTypes.string
}

export default StyledNav