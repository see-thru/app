import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { 
  updateProvider,
  getProviderProfile 
} from '../../../actions-old/provider'
import generateDataForQuestions from '../../../utils/generator-data-structure/questions'

import ResearchQuestions from './base'

export class ResearchQuestionsContainer extends Component {
  componentWillMount () {
    const { 
      user,
      getProviderProfile 
    } = this.props
    getProviderProfile(user.providerDisplayId, user.token)
  }

  componentWillReceiveProps (nextProps) {
    const { 
      user,
      getProviderProfile 
    } = nextProps
    if ( user.token !== this.props.user.token ) {
      getProviderProfile(user.providerDisplayId, user.token)
    }
  }

  onSubmit = e => {
    const {
      user, 
      updateProvider
    } = this.props
    let data = generateDataForQuestions(e)
    updateProvider(data, user.token, user.providerDisplayId)    
  }

  render () {
    const { loading } = this.props
    return (
      <ResearchQuestions 
        loading={ loading }
        onSubmit={ this.onSubmit } />
    )
  }
}

ResearchQuestionsContainer.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired,
    providerDisplayId: PropTypes.string.isRequired
  }),
  loading: PropTypes.bool,
  updateProvider: PropTypes.func.isRequired,
  getProviderProfile: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  loading: state.ProviderReducer.loading
})

export default connect(
  mapStateToProps, 
  { 
    updateProvider,
    getProviderProfile 
  }
)( ResearchQuestionsContainer )