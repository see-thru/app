import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { required } from 'redux-form-validators'
import { 
  Field,
  reduxForm
} from 'redux-form'

import {
  questions as QuestionsCatalog
} from '../../../catalogs'

import { 
  Input,
  FormWrapper
} from '../../generic/form/index'
import { StyledButtonSuccess } from '../../generic/button/styled'

import GeneratorInitialValuesQuestionsForm from '../../../utils/generator-initial-values-form/questions'

export let Form = ({
  month,
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper onSubmit={ handleSubmit( (e) => onSubmit(e) ) }>
    { QuestionsCatalog.map( question => {
      return (
        <Field
          key={ question.key }
          name={ question.key }
          title={ question.text }
          component={ Input }
          validate={ [
            required()
          ] }
        />
      )
    }) }
    <StyledButtonSuccess type="submit">
      Save
    </StyledButtonSuccess>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

Form = reduxForm({
  form : 'editResearchQuestions',
  initialValues : {},
  enableReinitialize : true
})( Form )

const FormContainer = connect(
  state => ({
    initialValues: state.ProviderReducer.profile !== null && state.ProviderReducer.profile.data ? GeneratorInitialValuesQuestionsForm(state.ProviderReducer.profile.data) : {}
  }),
)(Form)

export default FormContainer