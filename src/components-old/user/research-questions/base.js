import React from 'react'
import PropTypes from 'prop-types'

import Form from './form'
import Loading from '../../generic/loader/base'
import ButtonBackToEdit from '../button-back-to-edit/base'
import StyledWrapperContent from '../styled-wrapper-content'

const ResearchQuestions = ({
  loading,
  onSubmit
}) => (
  <StyledWrapperContent>
    { loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    <Form onSubmit={ onSubmit } />
  </StyledWrapperContent>
)

ResearchQuestions.prototypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired
}

export default ResearchQuestions