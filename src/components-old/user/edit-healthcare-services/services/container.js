import React from 'react'
import PropTypes from 'prop-types'

import Service from './service/base'
import StyledBundles from '../bundles/styled'

const Services = ({ services }) => (
  <StyledBundles>
    { services.map( (service, index) => {
      return (
        <Service 
          key={ index }
          service={ service } />
      )
    }) }
  </StyledBundles>
)

Services.defaultProps = {
  services: []
}

Services.propTypes = {
  services: PropTypes.array 
}

export default Services