import React from 'react'
import PropTypes from 'prop-types'

import StyledName from '../../bundles/bundle/styled-name'
import StyledCost from '../../bundles/bundle/styled-cost'
import StyledBundle from '../../bundles/bundle/styled'
import StyledHeader from '../../bundles/bundle/styled-header'
import StyledEstimatedTime from '../../bundles/bundle/styled-estimated-time'

const Service = ({ service }) => (
  <StyledBundle>
    <StyledName>{ service.name }</StyledName>
    <StyledHeader>
      <StyledEstimatedTime>Estimated Time: <span>{ `${ service.time } minutes` }</span></StyledEstimatedTime>
      <StyledCost>Cost: <span>{ `$${ service.cost }` }</span></StyledCost>
    </StyledHeader>
  </StyledBundle>
)

Service.propTypes = {
  service: PropTypes.shape({
    name: PropTypes.string,
    cost: PropTypes.string,
    time: PropTypes.string
  }).isRequired
}

export default Service