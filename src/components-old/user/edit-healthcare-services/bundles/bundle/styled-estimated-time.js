import styled from 'styled-components'

const StyledEstimatedTime = styled.p`
  margin: 0;
`

export default StyledEstimatedTime