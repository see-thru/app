import styled from 'styled-components'

const StyledBundleName = styled.h3`
  font-weight: 400;
  margin: 0 0 5px;
`

export default StyledBundleName