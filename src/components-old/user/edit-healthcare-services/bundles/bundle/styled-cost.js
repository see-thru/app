import styled from 'styled-components'

const StyledCost = styled.p`
  margin: 0;
  right: 0;
  top: 0;
  position: absolute;
`

export default StyledCost