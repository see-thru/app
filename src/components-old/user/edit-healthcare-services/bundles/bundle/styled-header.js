import styled from 'styled-components'

import { colors } from '../../../../../utils/settings'

const StyledHeader = styled.div`
  margin-bottom: 20px;
  padding-right: 100px;
  position: relative;
  p {
    color: ${ colors.textLight };
    font-size: .9rem;
  }
  span {
    color: ${ colors.text };
  }
`

export default StyledHeader