import React from 'react'
import PropTypes from 'prop-types'

import StyledName from './styled-name'
import StyledCost from './styled-cost'
import StyledList from '../../../../provider/show/medical-services/bundle/styled-list'
import StyledBundle from './styled'
import StyledHeader from './styled-header'
import StyledEstimatedTime from './styled-estimated-time'

const Bundle = ({ bundle }) => (
  <StyledBundle>
    <StyledName>{ bundle.bundle }</StyledName>
    <StyledHeader>
      <StyledEstimatedTime>Estimated Time: <span>{ `${ bundle.time } minutes` }</span></StyledEstimatedTime>
      <StyledCost>Cost: <span>{ `$${ bundle.cost }` }</span></StyledCost>
    </StyledHeader>
    <StyledBundle>
      <StyledList>
        { bundle.services.map( ( service, index ) => {
          return (
            <li key={ index }>
              { service }
            </li>
          )
        }) }
      </StyledList>
    </StyledBundle>
  </StyledBundle>
)

Bundle.defaultProps = {
  bundle: {
    services: []
  }
}

Bundle.propTypes = {
  bundle: PropTypes.shape({
    cost: PropTypes.string,
    time: PropTypes.string,
    bundle: PropTypes.string,
    services: PropTypes.array
  }).isRequired
}

export default Bundle