import React from 'react'
import PropTypes from 'prop-types'

import Bundle from './bundle/base'
import StyledBundles from './styled'

const Bundles = ({ bundles }) => (
  <StyledBundles>
    { bundles.map( (bundle, index) => {
      return (
        <Bundle 
          key={ index }
          bundle={ bundle } />
      )
    }) }
  </StyledBundles>
)

Bundles.defaultProps = {
  bundles: []
}

Bundles.propTypes = {
  bundles: PropTypes.array 
}

export default Bundles