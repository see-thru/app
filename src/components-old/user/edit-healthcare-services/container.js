import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { editHealthCareServices } from '../../../actions-old/user'

import HealthcareServices from './base'

export class HealthcareServicesContainer extends Component {
  onSubmit = e => {
    const {
      user, 
      editHealthCareServices 
    } = this.props
    editHealthCareServices(e.chargeMaster[0], user.token)
  }

  render () {
    const { 
      user,
      loading,
      healthcareServices
    } = this.props
    return (
      <HealthcareServices 
        error={ healthcareServices && healthcareServices.error && healthcareServices.error }
        loading={ loading }
        onSubmit={ this.onSubmit }
        servicesAndBundles={ user.servicesAndBundles } />
    )
  }
}

HealthcareServicesContainer.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired,
    servicesAndBundles: PropTypes.shape({
      bundles: PropTypes.array.isRequired,
      services: PropTypes.array.isRequired
    })
  }),
  loading: PropTypes.bool,
  healthcareServices: PropTypes.object.isRequired,
  editHealthCareServices: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  loading: state.AuthReducer.loading,
  healthcareServices: state.AuthReducer.healthcareServices
})

export default connect(
  mapStateToProps, 
  { editHealthCareServices }
)( HealthcareServicesContainer )