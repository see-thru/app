import React from 'react'

import StyledDeleteBundles from './styled'
import StyledDeleteAllButton from './styled-delete-all-button'

const DeleteBundles = () => (
  <StyledDeleteBundles>
    <p>Deleting or changing this bundle will affect your prices. Patients who already  booked and payed for an appointment to this bundle will have to be treated according the price agreed upon the booking.</p>
    <StyledDeleteAllButton onClick={ () => console.log('delete bundles!!') }>
      Delete All
    </StyledDeleteAllButton>
  </StyledDeleteBundles>
)

export default DeleteBundles