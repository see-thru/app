import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledDeleteBundles = styled.div`
  margin: 60px 0 0;
  p {
    color: ${ colors.textLight };
    font-size: .85rem;
    margin-bottom: 10px;
  }
`

export default StyledDeleteBundles