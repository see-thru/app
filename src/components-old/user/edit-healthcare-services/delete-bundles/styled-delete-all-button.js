import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledDeleteAllButton = styled.button`
  color: ${ colors.alert };
  font-size: .9rem;
  text-transform: uppercase;
  padding: 0;
`

export default StyledDeleteAllButton