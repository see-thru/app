import React from 'react'
import PropTypes from 'prop-types'

import Form from './form/base'
import Bundles from './bundles/container'
import Loading from '../../generic/loader/base'
import Services from './services/container'
import ButtonBackToEdit from '../button-back-to-edit/base'
import StyledErrorMessage from '../styled-error-message'
import StyledWrapperContent from '../styled-wrapper-content'

const HealthcareServices = ({
  error,
  loading,
  onSubmit,
  deleteBundles,
  servicesAndBundles
}) => (
  <StyledWrapperContent isVeryWide>
    { loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    <Bundles bundles={ servicesAndBundles.bundles } />
    <Services services={ servicesAndBundles.services } />
    { error ? <StyledErrorMessage>{ error }</StyledErrorMessage> : undefined }
    <Form 
      onSubmit={ onSubmit } 
      errorMessage={ error } />
  </StyledWrapperContent>
)

HealthcareServices.defaultProps = {
  servicesAndBundles: {
    bundles: [],
    services: []
  }
}

HealthcareServices.prototypes = {
  error: PropTypes.string,
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  servicesAndBundles: PropTypes.shape({
    bundles: PropTypes.array.isRequired,
    services: PropTypes.array.isRequired
  })
}

export default HealthcareServices