import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'

import { FormWrapper } from '../../../generic/form/index'
import UploadChargeMaster from './upload-charge-master/base'
import StyledDownloadCSVLink from './styled-download-csv-link'
import { StyledButtonSuccess } from '../../../generic/button/styled'

export let Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper onSubmit={ handleSubmit( onSubmit ) }>
    <UploadChargeMaster />
    <StyledButtonSuccess type="submit">
      CSV Upload
    </StyledButtonSuccess>
    <StyledDownloadCSVLink href="https://storage.cloud.google.com/seethru-dev.appspot.com/healthcareService/healthcareService-template.csv">
      Download CSV template
    </StyledDownloadCSVLink>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

Form = reduxForm({
  form : 'editHealthcareServices',
  initialValues : {},
  enableReinitialize : true
})( Form )

const FormContainer = connect(
  state => ({
    initialValues: {}
  }),
)(Form)

export default FormContainer