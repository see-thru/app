import React from 'react'
import { Field } from 'redux-form'
import { required } from 'redux-form-validators'

import { 
  FieldsetWrapper,
  UploadContainer
} from '../../../../generic/form/index'

import StyledFieldWrapper from './styled-field'

const UploadChargeMaster = () => (
  <FieldsetWrapper>
    <StyledFieldWrapper>
      <Field
        name="chargeMaster"
        type="file"
        accept=".csv"
        title="Upload"
        component={ UploadContainer }
        placeholder="This dropzone accepts only .csv files. Try dropping some here, or click to select files to upload."
        validate={ [
          required()
        ] }
      />
    </StyledFieldWrapper>
  </FieldsetWrapper>
)

export default UploadChargeMaster