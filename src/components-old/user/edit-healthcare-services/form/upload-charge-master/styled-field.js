import styled from 'styled-components'

const StyledFieldWrapper = styled.div`
  max-width: 450px;
`

export default StyledFieldWrapper