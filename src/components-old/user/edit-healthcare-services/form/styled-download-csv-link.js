import styled from 'styled-components'

import Media from '../../../../utils/media'
import { colors } from '../../../../utils/settings'

const StyledDownloadCSVLink = styled.a`
  color: ${ colors.successButton };
  display: inline-block;
  margin-left: 20px;
  &:hover {
    text-decoration: none;
  }
  ${
    Media.small`
      display: block;
      margin-left: 0;
      margin-top: 10px;
    `
  }
`

export default StyledDownloadCSVLink