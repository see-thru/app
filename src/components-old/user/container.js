import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import PrivateRoute from '../private-router'
import Navigation from './nav'
import NavigationMobile from './nav-mobile'
import isFeatureEnabled from '../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../has-permission'
import EditPhotoContainer from './edit-photo/container'
import PaymentInfoContainer from './payment-info/container'
import EditProfileContainer from './edit-profile/container'
import StyledContainerEditPages from './styled-container'
import UpdateSchedulesContainer from './update-schedules/container'
import ResearchQuestionsContainer from './research-questions/container'
import HealthcareServicesContainer from './edit-healthcare-services/container'
import EditProviderProfileContainer from './edit-provider-profile/container'

export const UserContainer = ({
  match,
  isLogged
}) => (
  <StyledContainerEditPages
    gridGap="50px 10px"
    templateRows="auto"
    templateColumns="140px 1fr">
    <PrivateRoute 
      path={ match.path } 
      component={ Navigation }
      isLogged={ isLogged } />
    <PrivateRoute 
      path={ `${ match.path }/edit` }
      component={ NavigationMobile }
      isLogged={ isLogged } />
    <PrivateRoute 
      exact
      path={ `${ match.path }/edit-profile` }
      component={ EditProfileContainer }
      isLogged={ isLogged } />
    <PrivateRoute 
      exact
      path={ `${ match.path }/edit-photo` }
      component={ EditPhotoContainer }
      isLogged={ isLogged } />
    <PrivateRoute
      path={ `${ match.path }/payment-info` }
      component={ PaymentInfoContainer }
      isLogged={ isLogged } />      
    <PrivateRoute 
      exact 
      path={ `${ match.path }/provider/update-schedules` }
      component={ UpdateSchedulesContainer }
      isLogged={ isLogged } />      
    <PrivateRoute 
      exact 
      path={ `${ match.path }/provider/edit-provider-profile` }
      component={ EditProviderProfileContainer }
      isLogged={ isLogged } />
    <PrivateRoute 
      exact
      path={ `${ match.path }/provider/edit-healthcare-services` }
      component={ HealthcareServicesContainer }
      isLogged={ isLogged } />
    <PrivateRoute 
      exact 
      path={ `${ match.path }/provider/research-questions` }
      component={ ResearchQuestionsContainer }
      isLogged={ isLogged } />
  </StyledContainerEditPages>
)

const mapStateToProps = (state) => ({
  isLogged: state.AuthReducer.isLogged
})

UserContainer.propTypes = {
  match: PropTypes.object.isRequired,
  isLogged: PropTypes.bool
}

export default HasPermissionHOC( 
  connect(
    mapStateToProps,
    {}
  )( UserContainer ),   
  isFeatureEnabled(window.location.href).pages.editProfile
)