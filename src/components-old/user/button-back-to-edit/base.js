import React from 'react'
import { Link } from 'react-router-dom'

import StyledButtonBackToEdit from './styled'

const ButtonBackToEdit = () => (
  <StyledButtonBackToEdit>
    <Link to="/user/edit">
      Back to Edit Page
    </Link>
  </StyledButtonBackToEdit>
)

export default ButtonBackToEdit