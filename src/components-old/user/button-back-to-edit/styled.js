import styled from 'styled-components'

import Media from '../../../utils/media'
import { colors } from '../../../utils/settings'

const StyledButtonBackToEdit = styled.div`
  display: none;
  margin-bottom: 20px;
  text-transform: uppercase;
  ${
    Media.small`
      display: block;
    `
  }
  a {
    color: ${ colors.textTitle }
    font-size: .85rem;
    text-decoration: none;
    position: relative;
  }
`

export default StyledButtonBackToEdit