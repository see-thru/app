import React, { Fragment } from 'react'
import { connect } from 'react-redux'

import StyledNav from './styled-nav'
import StyledNavLink from './styled-nav-link'

export const NavigationMobile = ({
  user
}) => (
  <StyledNav type="mobile">
    <StyledNavLink 
      to={ `/user/edit-profile` }
      type="mobile">
      Edit Profile
    </StyledNavLink>
    <StyledNavLink 
      to={ `/user/edit-photo` }
      type="mobile">
      Edit Photo
    </StyledNavLink>   
    { user && user.role === 'patient' ?
      <Fragment>
        <StyledNavLink 
          to={ `/user/payment-info/methods` }
          type="mobile">
          Payment Info
        </StyledNavLink>
      </Fragment>
      : undefined
    }
    { user && user.role === 'provider' ?  
      <Fragment>
        <StyledNavLink 
          to="/user/provider/edit-provider-profile"
          type="mobile">
          Edit Provider
        </StyledNavLink>
        <StyledNavLink 
          to="/user/provider/edit-healthcare-services"
          type="mobile">
          Edit Healthcare Services
        </StyledNavLink>            
        <StyledNavLink	
          to="/user/provider/update-schedules"
          type="mobile">	
          Update Schedules	
        </StyledNavLink>	           
      </Fragment>
      : undefined }
  </StyledNav>
)  

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user
})

export default connect(
  mapStateToProps,
  {}
)( NavigationMobile )