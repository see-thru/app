import React from 'react'
import PropTypes from 'prop-types'

import Navigation from '../navigation/'
import InvoicesList from './invoices-list'
import StyledWrapper from '../styled-wrapper'
import ButtonBackToEdit from '../../button-back-to-edit/base'

const Invoices = ({ invoices }) => (
  <StyledWrapper>
    <ButtonBackToEdit />
    <Navigation section="invoices" />
    <InvoicesList invoices={ invoices } />
  </StyledWrapper>
)

Invoices.defaultProps = {
  invoices: []
}

Invoices.propTypes = {
  invoices: PropTypes.array
}

export default Invoices 