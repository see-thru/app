import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../generic/icon/base'
import StyledInvoice from './styled-invoice'

const InvoicesList = ({ invoices }) => (
  <div>
    { invoices.map( (invoice, index) => {
      return (
        <StyledInvoice key={ index }>
          <Icon 
            name="invoice"
            width="25" 
            height="30" />        
          <p>
            <span>Date And Time</span>
            { invoice.date } at { invoice.time }
          </p>
          <p>
            <span>Amount</span>
            { invoice.amount }
          </p>          
          <p>
            <span>Status</span>
            { invoice.status }
          </p>          
          <a href={ `/path/to/invoice/${ invoice.id }` }>
            Download
          </a>
        </StyledInvoice>
      )
    }) }
  </div>
)

InvoicesList.propTypes = {
  invoices: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    date: PropTypes.string,
    time: PropTypes.string,
    status: PropTypes.string,
    amount: PropTypes.string,
  })).isRequired
}

export default InvoicesList