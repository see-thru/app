import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import Media from '../../../../utils/media';

const StyledInvoice = styled.div`
  border-bottom: 1px solid ${ colors.border };
  font-size: .9rem;
  margin: 20px 20px;
  position: relative;
  padding: 0px 40px 10px;
  &:last-of-type {
    border-bottom: 0;
  }
  svg {
    left: 0;
    top: 0;
    position: absolute;
  }
  a {
    bottom: 20px;
    color: ${ colors.successButton };
    right: 20px;
    text-decoration: none;
    text-transform: uppercase;
    position: absolute;
    ${
      Media.small`
        bottom: 0;
        right: 0;
        position: relative;
      `
    }
  }
  span {
    color: ${ colors.textLight };
    display: block;
    text-transform: uppercase;
  }
`

export default StyledInvoice