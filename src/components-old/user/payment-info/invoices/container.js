import React, { Component } from 'react'

import Invoices from './base' 

class InvoicesContainer extends Component {
  render () {
    return (
      <Invoices invoices={[
        { id: 1, date: 'September 28th', time: '18:00', status: 'pending', amount: '224,32' },
        { id: 2, date: 'August 15th', time: '13:00', status: 'paid', amount: '114,00' }
      ]} />
    )
  }
}

export default InvoicesContainer