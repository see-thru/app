import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledMessage = styled.div`
  color: ${ colors.textLight };
  font-size: .9rem;
  margin: 10px 0;
  padding: 10px 20px;
`

export default StyledMessage