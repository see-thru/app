import styled from 'styled-components'

import { colors } from '../../../utils/settings'
import Media from '../../../utils/media';

const StyledWrapper = styled.div`
  border: 1px solid ${ colors.border }
  ${
    Media.small`
      border: 0;
    `
  }
`

export default StyledWrapper