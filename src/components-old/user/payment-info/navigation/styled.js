import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledNavigation = styled.div`
  background-color: ${ colors.lightBorder };
`

export default StyledNavigation