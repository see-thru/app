import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../../utils/media'
import { colors } from '../../../../utils/settings'
import { StyledButton } from '../../../generic/button/styled'

const StyledLink = styled(StyledButton)`
  color: ${ props => props.activated === 'true' ? 'black' : colors.textDate };
  display: inline-block;
  font-size: .9rem;
  padding: 20px 10px;
  text-align: center;
  width: 50%;
  ${
    Media.small`
      padding: 10px 5px;
    `
  }
`

StyledLink.propTypes = {
  activated: PropTypes.oneOf(['true', 'false'])
}

export default StyledLink