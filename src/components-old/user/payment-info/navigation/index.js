import React from 'react'
import { Link } from 'react-router-dom'

import StyledLink from './styled-link'
import StyledNavigation from './styled'

const Navigation = ({ section }) => (
  <StyledNavigation>
    <StyledLink
      to="/user/payment-info/methods"
      tag={ Link }
      activated={ section === 'methods' ? 'true' : 'false' }>
      Payment Methods
    </StyledLink>
  </StyledNavigation>
)

export default Navigation