import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledCard = styled.div`
  border-bottom: 1px solid ${ colors.border };
  margin: 10px 0;
  padding: 0 20px;
  position: relative;
  span {
    vertical-align: middle;
  }
  svg {
    left: 20px;
    top: 0;
    position: absolute;
  }
  button {
    color: ${ colors.textLight };
    right: 20px;
    text-decoration: none;
    top: 0;
    position: absolute;
  }
  p {
    color: ${ colors.textTitle };
    font-size: .9rem;
    padding-left: 35px;  
    padding-right: 30px;
  }
`

export default StyledCard