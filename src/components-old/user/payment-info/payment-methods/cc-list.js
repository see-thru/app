import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../generic/icon/base'
import StyledCard from './styled-card'
import StyledMessage from '../styled-message'

const CCList = ({ 
  openModal,
  creditCards 
}) => (
  <div>
    { creditCards && creditCards.cards && creditCards.cards.length === 0 ?  
      <StyledMessage>There are not credit cards.</StyledMessage>
      : undefined 
    }
    { creditCards && creditCards.cards && creditCards.cards.map( (card, index) => {
      return (
        <StyledCard key={ index }>
          <p>
            <Icon 
              name="credit-card"
              width="30" 
              height="20"  />
            <span>{ `Ending in ${ card.lastFourDigits }` }</span>
          </p>
          <button onClick={ openModal.bind(this, card.cardId) }>
            Delete
          </button>
        </StyledCard>
      )
    }) }
  </div>
)

CCList.propTypes = {
  openModal: PropTypes.func.isRequired,
  creditCards: PropTypes.shape({
    cards: PropTypes.array.isRequired
  })
}

export default CCList