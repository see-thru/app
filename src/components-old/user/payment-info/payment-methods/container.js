import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import PaymentMethods from './base' 
import { 
  deleteCreditCard,
  getCreditCardList 
} from '../../../../actions-old/billing'

export class PaymentMethodsContainer extends Component {
  constructor () {
    super()
    this.state = ({
      cardIdSelected: undefined,
      isOpenConfirmDeleteModal: false
    })
  }

  componentWillMount () {
    const { 
      user,
      getCreditCardList 
    } = this.props
    getCreditCardList(user.token)
  }

  openModal = cardIdSelected => {
    this.setState({ 
      cardIdSelected,
      isOpenConfirmDeleteModal: true,
    })
  }

  closeModal = () => {
    this.setState({ 
      cardIdSelected: undefined,
      isOpenConfirmDeleteModal: false 
    })
  }  

  deleteCC = () => {
    const { cardIdSelected } = this.state
    const { 
      user,
      deleteCreditCard 
    } = this.props
    deleteCreditCard(cardIdSelected, user.token)
    this.closeModal()
  }

  render () {
    const { isOpenConfirmDeleteModal } = this.state
    const { 
      data,
      loading 
    } = this.props
    return (
      <PaymentMethods 
        isOpen={ isOpenConfirmDeleteModal }
        loading={ loading }
        deleteCC={ this.deleteCC }
        openModal={ this.openModal }
        closeModal={ this.closeModal }
        creditCards={ data } />
    )
  }
}

PaymentMethodsContainer.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  data: PropTypes.shape({
    cards: PropTypes.array.isRequired
  }),
  loading: PropTypes.bool,
  deleteCreditCard: PropTypes.func.isRequired,
  getCreditCardList: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  data: state.BillingReducer.data,
  loading: state.BillingReducer.loading
})

export default connect(
  mapStateToProps, 
  { 
    deleteCreditCard,
    getCreditCardList 
  }
)( PaymentMethodsContainer )