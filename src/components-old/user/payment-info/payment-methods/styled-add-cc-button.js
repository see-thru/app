import styled from 'styled-components'

import { StyledButtonSuccess } from '../../../generic/button/styled'

const StyledAddCCButton = styled(StyledButtonSuccess)`
  display: inline-block;
  margin: 0 10px 10px;
  padding: 4px 8px;
`

export default StyledAddCCButton