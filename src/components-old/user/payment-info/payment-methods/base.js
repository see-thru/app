import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import CCList from './cc-list'
import Loading from '../../../generic/loader/base'
import Navigation from '../navigation/'
import StyledWrapper from '../styled-wrapper'
import ButtonBackToEdit from '../../button-back-to-edit/base'
import StyledAddCCButton from './styled-add-cc-button'
import ConfirmDeleteModal from './confirm-delete-modal'

const PaymentMethods = ({ 
  isOpen,
  loading,
  deleteCC,
  openModal,
  closeModal,
  creditCards 
}) => (
  <StyledWrapper>
    { loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    <Navigation section="methods" />  
    <CCList 
      openModal={ openModal }  
      creditCards={ creditCards } />
    <ConfirmDeleteModal
      isOpen={ isOpen }
      deleteCC={ deleteCC }
      closeModal={ closeModal } />
    <StyledAddCCButton 
      to="/user/payment-info/add-credit-card" 
      tag={ Link }>
      Add Credit Card
    </StyledAddCCButton>
  </StyledWrapper>
)

PaymentMethods.propTypes = {
  isOpen: PropTypes.bool,
  deleteCC: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  creditCards: PropTypes.object,
}

export default PaymentMethods 