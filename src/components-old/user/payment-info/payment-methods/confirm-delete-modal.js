import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../generic/icon/base'
import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../../../generic/modal/index'
import { StyledButtonSuccess } from '../../../generic/button/styled'

const ConfirmDeleteModal = ({
  isOpen,
  deleteCC,
  closeModal
}) => (
  <ModalContainer    
    width="600px"
    height="auto"
    isOpen={ isOpen }
    closeModal={ closeModal }>
    <StyledModalHeader>
      <StyledModalHeaderTitle>Confirm Delete</StyledModalHeaderTitle>
      <StyledModalHeaderCloseButton onClick={ closeModal }>
        <Icon 
          fill="#ccc"
          width="16"
          name="cancel"
          height="16" />
      </StyledModalHeaderCloseButton>
    </StyledModalHeader>
    <StyledModalContent>
      <Fragment>
        <p>Do you want to remove this credit card?</p>
        <StyledButtonSuccess onClick={ deleteCC }>
          Yes
        </StyledButtonSuccess>
      </Fragment>
    </StyledModalContent>
  </ModalContainer>
)

ConfirmDeleteModal.propTypes = {
  isOpen: PropTypes.bool,
  deleteCC: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired
}

export default ConfirmDeleteModal