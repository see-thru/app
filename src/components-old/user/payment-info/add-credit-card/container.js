import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getFormValues } from 'redux-form'

import AddCreditCard from './base'
import { saveCreditCard } from '../../../../actions-old/billing'
import generateDataForCreditCard from '../../../../utils/generator-data-structure/credit-card'

export class AddCreditCardContainer extends Component {
  onSubmit = ( token, last4, name, expMonth, expYear ) => {
    const { 
      user,
      history,
      saveCreditCard 
    } = this.props
    const data = generateDataForCreditCard(token, last4, name, expMonth, expYear)
    saveCreditCard(data, user.token, history)
  }

  render () {
    const { 
      error,
      loading, 
      successMessage
    } = this.props
    return (
      <AddCreditCard 
        error={ error }
        loading={ loading }
        onSubmit={ this.onSubmit }
        successMessage={ successMessage } />
    )
  }
}

AddCreditCardContainer.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  error: PropTypes.string,
  loading: PropTypes.bool,
  successMessage: PropTypes.shape({
    success: PropTypes.bool
  }),
  saveCreditCard: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  error: state.BillingReducer.errorPostBilling,
  loading: state.BillingReducer.loading,
  formValues: getFormValues('addCreditCardForm')(state),
  successMessage: state.BillingReducer.successMessagePostBilling,
})

export default connect(
  mapStateToProps, 
  { saveCreditCard }
)( AddCreditCardContainer )