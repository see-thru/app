import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { colors } from '../../../../utils/settings'

const StyledLinkSeeYourCCList = styled(Link)`
  color: ${ colors.link };
  display: inline-block;
  margin: 20px 0 10px;
  text-decoration: none;
`

export default StyledLinkSeeYourCCList