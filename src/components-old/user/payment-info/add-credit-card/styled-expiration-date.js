import styled from 'styled-components'

const StyledExpirationDate = styled.div`
  clear: both;
  & > div:first-of-type {
    margin-right: 10px;
  }
`

export default StyledExpirationDate