import React from 'react'
import PropTypes from 'prop-types'

import Loading from '../../../generic/loader/base'
import ButtonBackToEdit from '../../button-back-to-edit/base'
import StyledErrorMessage from '../../styled-error-message'
import StyledWrapperContent from '../../styled-wrapper-content'
import StyledSuccessMessage from '../../styled-success-message'
import StyledLinkSeeYourCCList from './styled-link-see-your-cc-list'
import StripeAddCreditCardContainer from '../../../generic/stripe/add-credit-card/container'

const AddCreditCard = ({
  error,
  loading,
  onSubmit,
  successMessage
}) => (
  <StyledWrapperContent>
    { loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    { successMessage.success ? 
      <StyledSuccessMessage>The credit card was added successfully!</StyledSuccessMessage> 
      : undefined 
    }
    { error ? 
      <StyledErrorMessage>{ error }</StyledErrorMessage>
      : undefined 
    }
    <StripeAddCreditCardContainer onSubmit={ onSubmit } />
    <StyledLinkSeeYourCCList to="/user/payment-info/methods">
      See your credit card list
    </StyledLinkSeeYourCCList>
  </StyledWrapperContent>
)

AddCreditCard.prototypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  successMessage: PropTypes.shape({
    success: PropTypes.bool
  })
}

export default AddCreditCard