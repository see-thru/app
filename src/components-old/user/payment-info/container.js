import React, { Fragment } from 'react'
import { Route } from 'react-router-dom'

import AddCreditCardContainer from './add-credit-card/container'
import PaymentMethodsContainer from './payment-methods/container'

const PaymentInfoContainer = ({ match }) => (
  <Fragment>
    <Route
      path={ `/user/payment-info/add-credit-card` } 
      component={ AddCreditCardContainer } />
    <Route 
      path={ `/user/payment-info/methods` } 
      component={ PaymentMethodsContainer } />           
  </Fragment>
)

export default PaymentInfoContainer