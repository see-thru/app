import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../utils/settings'

const StyledErrorMessage = styled.p`
  color: ${ colors.alert }
  font-size: .9rem;
  font-weight: ${ fonts.weightBold };
`

export default StyledErrorMessage