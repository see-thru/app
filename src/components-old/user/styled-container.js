import styled from 'styled-components'

import Media from '../../utils/media'
import { StyledContainerGrid } from '../generic/grid/index'

const StyledContainerEditPages = styled(StyledContainerGrid)`
  margin-bottom: 40px;
  margin-top: 40px;
  ${ Media.small`grid-template-columns: 1fr` }
`

export default StyledContainerEditPages