import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../utils/settings'

const StyledSuccessMessage = styled.p`
  color: ${ colors.darkLink }
  font-size: .9rem;
  font-weight: ${ fonts.weightBold };
`

export default StyledSuccessMessage