import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledSelectYear = styled.div`
  display: inline-block;
  width: 100px;
  ${ Media.small`
    display: block;
    margin: 0;
    width: 100%;
  `}
`

export default StyledSelectYear