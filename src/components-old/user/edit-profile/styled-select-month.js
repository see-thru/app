import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledSelectMonth = styled.div`
  display: inline-block;
  width: 30%;
  ${ Media.small`
    display: block;
    margin: 0;
    width: 100%;
  `}
`

export default StyledSelectMonth