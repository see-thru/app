import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getFormValues } from 'redux-form'

import { editProfile } from '../../../actions-old/user'

import EditProfile from './base'

export class EditProfileContainer extends Component {
  onSubmit = e => {
    const { 
      user,
      editProfile
    } = this.props
    editProfile(e, user.token)
  }

  render () {
    const { 
      loading, 
      formValues,
      successMessage
    } = this.props
    const month = formValues && formValues.dobMonth ? formValues.dobMonth : undefined
    return (
      <EditProfile 
        month={ month }
        loading={ loading }
        onSubmit={ this.onSubmit }
        successMessage={ successMessage } />
    )
  }
}

EditProfileContainer.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  loading: PropTypes.bool,
  editProfile: PropTypes.func.isRequired,
  successMessage: PropTypes.string
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  loading: state.AuthReducer.loading,
  formValues: getFormValues('editProfile')(state),
  successMessage: state.AuthReducer.updatedProfileMessage
})

export default connect(
  mapStateToProps, 
  { editProfile }
)( EditProfileContainer )