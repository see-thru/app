import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledSelectDay = styled.div`
  display: inline-block;
  margin: 0 10px;
  width: 80px;
  ${ Media.small`
    display: block;
    margin: 0;
    width: 100%;
  `}
`

export default StyledSelectDay