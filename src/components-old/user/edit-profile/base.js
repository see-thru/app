import React from 'react'
import PropTypes from 'prop-types'

import Loading from '../../generic/loader/base'
import FormContainer from './form'
import ButtonBackToEdit from '../button-back-to-edit/base'
import StyledWrapperContent from '../styled-wrapper-content'
import StyledSuccessMessage from '../styled-success-message'

const EditProfile = ({
  month,
  loading,
  onSubmit,
  successMessage
}) => (
  <StyledWrapperContent>
    { loading ? <Loading /> : undefined }
    <ButtonBackToEdit />
    { successMessage ? 
      <StyledSuccessMessage>
        { successMessage }
      </StyledSuccessMessage> 
      : undefined 
    }
    <FormContainer
      month={ month } 
      onSubmit={ onSubmit } />
  </StyledWrapperContent>
)

EditProfile.prototypes = {
  month: PropTypes.string,
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  successMessage: PropTypes.string
}

export default EditProfile