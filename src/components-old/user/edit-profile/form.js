import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { 
  Field,
  reduxForm
} from 'redux-form'
import {
  email,
  length,
  required
} from 'redux-form-validators'

import { 
  gender as GenderCatalog,
  months as MonthsCatalog,
  languages as LanguagesCatalog,
  currencies as CurrenciesCatalog,
} from '../../../catalogs'

import { 
  Input,
  Select,
  FormWrapper,
  SelectCustomizable
} from '../../generic/form/index'
import StyledSelectDay from './styled-select-day'	
import getYearsCatalog from '../../../utils/select/generate-years'	
import StyledSelectYear from './styled-select-year'	
import StyledSelectMonth from './styled-select-month'	
import getDaysCatalogByMonth from '../../../utils/select/generate-days'
import { StyledButtonSuccess } from '../../generic/button/styled'

import GeneratorInitialValuesEditProfileForm from '../../../utils/generator-initial-values-form/edit-profile'

export let Form = ({
  month,
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper onSubmit={ handleSubmit( (e) => onSubmit(e) ) }>
    <Field
      name="email"
      type="email"
      title="Email Address"
      readOnly={ true }
      component={ Input }
            validate={ [
        email(),
        required()
      ] }
    />    
    <Field
      name="firstName"
      title="First Name"
      component={ Input }
            validate={ [
        required()
      ] }
    />
    <Field
      name="lastName"
      title="Last Name"
      component={ Input }
            validate={ [
        required()
      ] }
    />
    <Field
      name="sex"
      title="Gender"
      component={ Select }
      options={ GenderCatalog }
    />
    <div>	    
      <StyledSelectMonth>	      
        <Field
          name="dobMonth"
          title="Date of Birth"
          component={ Select }
          options={ MonthsCatalog }	 
          validate={ [	   
            required()	
          ] }	
        />	
      </StyledSelectMonth>
      <StyledSelectDay>	
        <Field	
          name="dobDay"	
          title=""	
          hasLabel={ false }	
          component={ Select }	
          options={ getDaysCatalogByMonth(Number(month - 1)) }	
          validate={ [	
            required()	
          ] }	
        /> 	
      </StyledSelectDay>  	
      <StyledSelectYear>	
        <Field	
          name="dobYear"	
          title=""	
          hasLabel={ false }	
          component={ Select }	
          options={ getYearsCatalog() }	
          validate={ [	
            required()	
          ] }	
        />  	
      </StyledSelectYear>      
    </div>  
    <Field
      name="phone"
      title="Phone Number"
      component={ Input }
            validate={ [
        required()
      ] }
    />    
    <Field
      name="currency"
      title="Preferred Currency"
      options={ CurrenciesCatalog }
      component={ Select }
      validate={ [
        required()
      ] }
    />
    <Field
      name="languages"
      title="Languages"
      multi={ true }
      options={ LanguagesCatalog }
      component={ SelectCustomizable }
      validate={ [
        length({ max: 5, message: 'You can select maximun 5 languages' }),
        required()
      ] }
    />
    <StyledButtonSuccess type="submit">
      Save
    </StyledButtonSuccess>
  </FormWrapper>
)

Form.defaultProps = {	
  month: '0'	
}

Form.propTypes = {
  month: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

Form = reduxForm({
  form : 'editProfile',
  initialValues : {},
  enableReinitialize : true
})( Form )

const FormContainer = connect(
  state => ({
    initialValues: state.AuthReducer.user !== null ? GeneratorInitialValuesEditProfileForm(state.AuthReducer.user) : {}
  }),
)(Form)

export default FormContainer