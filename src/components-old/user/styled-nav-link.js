import styled from 'styled-components'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import { colors } from '../../utils/settings'

const StyledNavLink = styled(NavLink)`
  color: ${ colors.text }
  border: 1px solid ${ colors.lightBorder };
  display: block;
  text-decoration: none;
  text-align: center;
  margin-bottom: 10px;
  padding: 10px;
  &.active {
    background-color: ${ colors.lightHighlight };
    border-color: transparent;
    color: white;
  }
  ${ props => props.type === 'mobile' ? `
      background-color: ${ colors.successButton };
      color: white;
    `
    : undefined }  
`

StyledNavLink.defaultProps = {
  type: 'desktop'
}

StyledNavLink.propTypes = {
  type: PropTypes.string
}

export default StyledNavLink