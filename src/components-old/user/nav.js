import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import StyledNav from './styled-nav'
import StyledNavLink from './styled-nav-link'

export const Navigation = ({
  user,
  match
}) => (
  <StyledNav>
    <StyledNavLink to={ `${ match.path }/edit-profile` }>
      Edit Profile
    </StyledNavLink>
    <StyledNavLink to={ `${ match.path }/edit-photo` }>
      Edit Photo
    </StyledNavLink>
    { user && user.role === 'patient' ? 
      <Fragment>
        <StyledNavLink to={ `${ match.path }/payment-info/methods` }>
          Payment Info
        </StyledNavLink>
      </Fragment>
      : undefined  
    }
    { user && user.role === 'provider' ?
      <Fragment>
        <StyledNavLink to={ `${ match.path }/provider/edit-provider-profile` }>
          Edit Provider
        </StyledNavLink>
        <StyledNavLink to={ `${ match.path }/provider/edit-healthcare-services` }>
          Edit Healthcare Services
        </StyledNavLink>
        <StyledNavLink to={ `${ match.path }/provider/update-schedules` }>	
          Update Schedules	
        </StyledNavLink>	        
      </Fragment>      
      : undefined 
    }
  </StyledNav>
)  

Navigation.propTypes = {
  user: PropTypes.object,
  match: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user
})

export default connect(
  mapStateToProps,
  {}
)( Navigation )