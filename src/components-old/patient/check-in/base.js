import React from 'react'
import moment from 'moment'
import QRCode from 'qrcode.react'
import PropTypes from 'prop-types'

import Item from '../../generic/qr-code/visit-started/status/item/'
import Container from '../appointments/container'
import StyledStatus from '../../generic/qr-code/visit-started/status/styled'
import StyledWrapperCheckin from './styled-wrapper-checkin'
import StyledButtonBackToEdit from '../../generic/qr-code/back-button'
import StyledQRCodeDescription from '../../generic/qr-code/styled-description'

const CheckIn = ({ 
  checkIn,
  booking,
  fullName
}) => {
  return (
    <Container
      gridGap="20px 100px"
      grid-template-rows="1"
      templateColumns="100%">
      <StyledButtonBackToEdit to="/patient/booking/appointments" />
      <StyledWrapperCheckin>
        { checkIn.data && checkIn.data.checkInCode ? 
          <QRCode
            value={ checkIn.data.checkInCode }
            size={ 260 }
            level={ "L" }
            bgColor={ "#ffffff" }
            fgColor={ booking.data && booking.data.status === 'checked-in' ? '#999' : '#1b4575' } /> 
          : undefined }
        <StyledQRCodeDescription>
          Show this QR Code When you arrive for your appointment. <br /> 
          <small>Or directly copy & paste the code: { checkIn.data && checkIn.data.checkInCode }</small>
        </StyledQRCodeDescription>             
      </StyledWrapperCheckin>
      { booking.data && booking.data.status === 'checked-in' ?
        <StyledStatus>
          <Item
            hour={ moment.utc(booking.data.checkInTime).format('LT') }
            message={ `Visit Started with ${ fullName }` }
            isCompleted />
        </StyledStatus>
        : undefined }    
    </Container>
  )
}


CheckIn.propTypes = {
  checkIn: PropTypes.shape({
    data: PropTypes.object,
    loading: PropTypes.bool
  }).isRequired,
  booking: PropTypes.shape({
    status: PropTypes.string
  }),
  fullName: PropTypes.string
}

export default CheckIn