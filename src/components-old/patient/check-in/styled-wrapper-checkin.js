import styled from 'styled-components'

import Media from '../../../utils/media'

const StyledWrapperCheckin = styled.div`
  margin: 0 auto;
  max-width: 400px;
  canvas {
    display: block;
    margin: 0 0 20px;
    max-width: 260px;
    width: 96%;
    ${
      Media.small`
        margin: 0 0 20px;
      `
    }
  }
`

export default StyledWrapperCheckin