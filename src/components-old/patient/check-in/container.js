import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  getBooking,
  checkInBooking 
} from '../../../actions-old/appointments'

import CheckIn from './base'

export class CheckInContainer extends Component {
  componentWillMount () {
    const {
      user,
      match,
      getBooking,
      checkInBooking
    } = this.props
    checkInBooking(
      user.token, 
      match.params.appointmentId
    )
    getBooking(
      user.token,
      match.params.appointmentId
    )
  }
  
  render () {
    const { 
      user,
      match,
      booking,
      getBooking
    } = this.props
    if ( booking.data && booking.data.status !== 'checked-in' ) {
      setTimeout(() => {
        getBooking(
          user.token,
          match.params.appointmentId
        )
      }, 6000);
    }
    let fullName
    if ( booking.data && booking.data.provider ) {
      fullName = `${ booking.data.provider.firstName } ${ booking.data.provider.lastName }`
    } 
    return (
      <CheckIn 
        fullName={ fullName }
        { ...this.props } />
    )
  }
}

CheckInContainer.propTypes = {
  user: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  checkIn: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  checkInBooking: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  checkIn: state.AppointmentsReducer.checkIn,
  booking: state.AppointmentsReducer.booking
})

export default connect(
  mapStateToProps,
  { 
    getBooking,
    checkInBooking 
  }
)( CheckInContainer )