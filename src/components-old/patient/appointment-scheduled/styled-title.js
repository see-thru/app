import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledTitle = styled.h3`
  color: ${ colors.textTitle };
  font-size: 1.1rem;
  font-weight: 400;
  margin-top: 40px;
  text-align: center;
  svg {
    margin-right: 5px;
  }
`

export default StyledTitle