import styled from 'styled-components'

const StyledImg = styled.img`
  display: block;
  height: auto;
  margin: 20px auto;
  max-width: 100%;
`

export default StyledImg