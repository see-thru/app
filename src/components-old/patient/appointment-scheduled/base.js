import React from 'react'

import Icon from '../../generic/icon/base'
import Provider from './provider/base'
import StyledImg from './styled-img'
import StyledTitle from './styled-title'
import AddToMyCalendar from './add-to-my-calendar/base'
import { StyledContainerGrid } from '../../generic/grid'

const AppointmentScheduled = () => (
  <StyledContainerGrid
    gridGap="0 10px"
    templateRows="auto"
    templateColumns="1fr">
    <div>
      <StyledTitle>
        <Icon  
          name="checkbox"
          width="15"
          height="11" />
        Appointment Scheduled!
      </StyledTitle>
      <StyledImg src="/images/appointment-scheduled.jpg" alt="" />
      <AddToMyCalendar 
        day="15" 
        month="July"
        event={ {
          title: 'Sample SeeThru Event',
          description: 'This is the sample description',
          location: 'New York',
          startTime: '2018-10-16T14:15:00-04:00',
          endTime: '2018-10-16T15:45:00-04:00'
        } } />
      <Provider 
        name="Dr Crystal Coleman"
        title="Primary Care Doctor"
        avatar="/images/example-avatar.png"
        location="195 Montague Street Brooklyn, NY" />
    </div>
  </StyledContainerGrid>
)

export default AppointmentScheduled