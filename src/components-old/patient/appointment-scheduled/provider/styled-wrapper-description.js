import styled from 'styled-components'

import Media from '../../../../utils/media'
import { 
  fonts,
  colors 
} from '../../../../utils/settings'

const StyledWrapperDescription = styled.div`
  padding-left: 100px;
  ${
    Media.small`
      padding-left: 50px;
    `
  }
  h2 {
    color: ${ colors.textTitle };
    font-size: 1.2rem;
    font-weight: ${ fonts.weightBold };
    margin: 0;
    ${
      Media.small`
        font-size: 1rem;
      `
    }    
  }
  h4 {
    color: ${ colors.textSubtitle };
    font-size: .9rem;
    font-weight: 400;
    margin: 0 0 6px;
    ${
      Media.small`
        font-size: .8rem;
      `
    }      
  }
  p {
    color: ${ colors.schedulesDay };
    font-size: .9rem;
    margin: 0;
    ${
      Media.small`
        font-size: .8rem;
      `
    }          
  }
`

export default StyledWrapperDescription