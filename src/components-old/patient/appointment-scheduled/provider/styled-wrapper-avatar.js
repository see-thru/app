import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledWrapperAvatar = styled.div`
  left: 0;
  top: 0;
  position: absolute;
  img {
    height: 80px;
    width: 80px;
    ${
      Media.small`
        height: 40px;
        width: 40px;
      `
    }
  }
`

export default StyledWrapperAvatar