import React from 'react'
import PropTypes from 'prop-types'

import StyledProvider from './styled'
import StyledWrapperAvatar from './styled-wrapper-avatar'
import StyledWrapperDescription from './styled-wrapper-description'

const Provider = ({
  name,
  title,
  avatar,
  location
}) => (
  <StyledProvider>
    <StyledWrapperAvatar>
      <img 
        src={ avatar } 
        alt={ name } />
    </StyledWrapperAvatar>
    <StyledWrapperDescription>
      <h2>{ name }</h2>
      <h4>{ title }</h4>
      <p>{ location }</p>
    </StyledWrapperDescription>
  </StyledProvider>
)

Provider.defualtProps = {
  avatar: '/images/example-avatar.png'
}

Provider.propTypes = {
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  location: PropTypes.string.isRequired,
}

export default Provider
