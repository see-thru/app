import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledProvider = styled.div`
  margin: 40px auto 80px;
  max-width: 340px;
  position: relative;
  width: 100%;
  ${
    Media.small`
      margin-left: 0;
      margin-right: 0;
    `
  }
`

export default StyledProvider