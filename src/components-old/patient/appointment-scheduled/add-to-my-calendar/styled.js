import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import Media from '../../../../utils/media';

const StyledAddToMyCalendar = styled.div`
  border: 1px solid ${ colors.border };
  border-radius: 2px;
  margin: 0 auto;
  padding: 0 0 20px;
  text-align: center;
  width: 200px;
  ${ 
    Media.small`
      margin-left: 0;
      margin-right: 0;
    `
  }
  &:before {
    background-color: ${ colors.alert };
    content: '';
    display: block;
    height: 30px;
    margin-bottom: 20px;
    position: relative;
    width: 100%;
  }
  p {
    display: block;
    font-size: 1.1rem;
    font-weight: 400;
    margin: 0;
    text-transform: uppercase;
    &:last-of-type {
      font-size: 3rem;
      line-height: 1;
    }
  }
  .react-add-to-calendar {
    &__wrapper {
      color: ${ colors.textLight };
      cursor: pointer;
      font-size: .9rem;
      margin-top: 10px;
    }
    &__dropdown {
      ul {
        list-style: none;
        margin: 10px 0 0;
        padding: 0;
      }
      li {
        margin: 5px 0;
      }
      a {
        border-radius: 2px;
        color: ${ colors.text };
        text-decoration: none;
        padding: 4px 8px;
        &:hover {
          background-color: ${ colors.border };
          color: ${ colors.textSubtitle };
        }
      }
    }
  }
`

export default StyledAddToMyCalendar