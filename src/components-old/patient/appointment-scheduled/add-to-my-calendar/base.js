import React from 'react'
import PropTypes from 'prop-types'
import AddToCalendar from 'react-add-to-calendar'

import StyledAddToMyCalendar from './styled'

const AddToMyCalendar = ({
  day,
  month, 
  event
}) => (
  <StyledAddToMyCalendar>
    <p>{ month }</p>
    <p>{ day }</p>
    <AddToCalendar event={ event } />
  </StyledAddToMyCalendar>
)

AddToMyCalendar.propTypes = {
  day: PropTypes.string.isRequired,
  month: PropTypes.string.isRequired,
  event: PropTypes.shape({
    title: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    startTime: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  })
}

export default AddToMyCalendar