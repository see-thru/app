import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import StyledWrapper from './styled-wrapper'

const StyledWrapperWithJustifyContent = styled(StyledWrapper)`
  border-bottom: 1px solid ${ colors.border };
  justify-content: space-between;
  margin: 20px 0;
  padding-bottom: 20px;
`

export default StyledWrapperWithJustifyContent