import styled from 'styled-components'

const StyledSection = styled.div`
  margin: 0 10px;
`

export default StyledSection