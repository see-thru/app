import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../utils/settings'

const StyledName = styled.h4`
  color: ${ colors.darkLink };
  font-size: 1.12rem;
  font-weight: ${ fonts.weightBold };
  margin: 0 0 10px;
`

export default StyledName