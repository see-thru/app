import styled from 'styled-components'
import PropTypes from 'prop-types'

import { colors } from '../../../../utils/settings'

const StyledItem = styled.p`
  color: ${ props => props.isLighter === 'true' ? colors.bgCounter : colors.darkLink };
  font-size: .9rem;
  line-height: 1.7;
  margin: 0;
`

StyledItem.propTypes = {
  isLighter: PropTypes.oneOf(['true', 'false'])
}

export default StyledItem