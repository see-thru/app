import styled from 'styled-components'

import { StyledButtonSuccess } from '../../../generic/button/styled'

const StyledLink = styled( StyledButtonSuccess)`
  display: inline-block;
  margin-top: 10px;
`

export default StyledLink