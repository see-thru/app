import React from 'react'
import moment from 'moment-timezone'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import StyledItem from './styled-item'
import StyledName from './styled-name'
import StyledLink from './styled-link'
import StyledSection from './styled-section'

const Info = ({
  id,
  type,
  name,
  date,
  address,
  isCheckInAvaialable
}) => {
  let toLink = `appointments/${ id }/check-in`
  if ( type === 'provider' ) toLink = `/provider/booking/appointments/${ id }/check-in`
  return (
    <StyledSection>
      <StyledName>{ name }</StyledName>
      <StyledItem>{ `${ moment.utc(date).format('MMMM Do') } at ${ moment.utc(date).format('ha') }` }</StyledItem>
      <StyledItem isLighter="true">{ address }</StyledItem>
      { isCheckInAvaialable ? 
        <StyledLink 
          to={ toLink }
          tag={ Link }>
          Check-in
        </StyledLink>          
        : undefined }
    </StyledSection>
  )
}

Info.defaultProps = {
  timezone: 'EST'
}

Info.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  address: PropTypes.string,
  timezone: PropTypes.oneOf(['EST', 'Europe/Madrid']),
  isCheckInAvaialable: PropTypes.bool,
}

export default Info