import styled from 'styled-components'

const StyledImage = styled.img`
  border-radius: 100%;
  height: 60px;
  max-width: 60px;
  width: 60px;
`

export default StyledImage