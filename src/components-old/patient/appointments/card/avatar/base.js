import React from 'react'
import PropTypes from 'prop-types'

import StyledSection from '../styled-section'
import StyledImage from './styled-image'

const Avatar = ({ 
  name,
  photo
}) => (
  <StyledSection>
    <StyledImage
      src={ photo ? photo : '/images/example-avatar.png' }
      alt={ name }
      width="80"
      height="80" />
  </StyledSection>
)

Avatar.propTypes = {
  name: PropTypes.string,
  photo: PropTypes.string
}

export default Avatar