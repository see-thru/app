import React from 'react'
import PropTypes from 'prop-types'

import Info from './info'
import Avatar from './avatar/base'
import StyledWrapper from './styled-wrapper'

const Content = ({ 
  type,
  item,
  isCheckInAvaialable
}) => {
  let data
  if ( type === 'patient' ) data = item.provider
  if ( type === 'provider' ) data = item.patient
  const fullName = data ? `${ data.firstName } ${ data.lastName }` : undefined
  return (
    <StyledWrapper>
      <Avatar 
        name={ data ? data.firstName : undefined } 
        photo={ data ? data.photo : undefined } />
      <Info 
        id={ item.id }
        type={ type }
        name={ fullName }
        date={ item.date }
        address={ item && item.provider && item.provider.address ? item.provider.address : undefined } 
        isCheckInAvaialable={ isCheckInAvaialable } />
    </StyledWrapper>
  )
}

Content.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object.isRequired,
  isCheckInAvaialable: PropTypes.bool,
}

export default Content