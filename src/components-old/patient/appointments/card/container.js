import React from 'react'
import PropTypes from 'prop-types'

import Content from './content'
import StyledWrapperWithJustifyContent from './styled-wrapper-with-justify-content'

const Card = ( props ) => (
  <StyledWrapperWithJustifyContent>
    <Content { ...props } />
  </StyledWrapperWithJustifyContent>
)

Card.propTypes = {
  type: PropTypes.string,
  item: PropTypes.shape({
    date: PropTypes.string.isRequired,
    provider: PropTypes.shape({
      photo: PropTypes.string,
      address: PropTypes.string,
      lastName: PropTypes.string,
      firstName: PropTypes.string
    }),
    patient: PropTypes.shape({
      photo: PropTypes.string,
      lastName: PropTypes.string,
      firstName: PropTypes.string
    }),
    isCheckInAvaialable: PropTypes.bool
  }).isRequired,
}

export default Card