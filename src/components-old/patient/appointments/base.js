import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { getAppointments } from '../../../actions-old/appointments'

import Loader from '../../generic/loader/base'
import TodayList from './today'
import MyPastList from './my-past'
import UpcomingList from './upcoming'
import StyledContainer from './container'
import getAppointmentsByType from '../../../utils/get-appointments-by-type'

export class AppointmentsList extends Component {
  componentWillMount () {
    const {
      user,
      getAppointments
    } = this.props
    const type = this.whatIsType()
    getAppointments(user.token, type)
  }

  componentWillReceiveProps (nextProps) {
    const {
      user,
      getAppointments
    } = nextProps
    const type = this.whatIsType()
    if ( user.token !== this.props.user.token ) {
      getAppointments(user.token, type)
    }
  }

  whatIsType = () => {
    const { match } = this.props
    let type = 'patient'
    if ( match.path === '/provider/booking/appointments/list' ) type = 'provider'
    return type
  }

  render () {
    const {
      data,
      loading
    } = this.props
    const results = data && data.results ? data.results : []
    return (
      <StyledContainer
        gridGap="50px 100px"
        grid-template-rows="1"
        templateColumns="100%">
        { loading ? <Loader /> : undefined } 
        <TodayList 
          type={ this.whatIsType() }
          data={ getAppointmentsByType(results, 'today') } />
        <UpcomingList 
          type={ this.whatIsType() }
          data={ getAppointmentsByType(results) } />
        <MyPastList 
          type={ this.whatIsType() }
          data={ getAppointmentsByType(results, 'past') } />
      </StyledContainer>      
    )
  }
}

AppointmentsList.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }).isRequired,
  data: PropTypes.shape({
    count: PropTypes.number,
    results: PropTypes.array.isRequired
  }).isRequired,
  match: PropTypes.object.isRequired,
  getAppointments: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  data: state.AppointmentsReducer.data,
  loading: state.AppointmentsReducer.loading,
})

export default connect(
  mapStateToProps,
  { getAppointments }
)( AppointmentsList )