import React from 'react'
import PropTypes from 'prop-types'

import Card from './card/container'
import Header from './header'

const MyPastList = ({ data }) => (
  <div>
    <Header>Past</Header>
    { data.map( (x, index) => {
      return (
        <Card 
          key={ index }
          item={ x } />
      )
    }) }
    { data.length === 0 ? <p>There are no past visits</p> : undefined }
  </div>
) 

MyPastList.defaultProps = {
  data: []
}

MyPastList.propTypes = {
  data: PropTypes.array.isRequired
}


export default MyPastList