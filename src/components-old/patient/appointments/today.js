import React from 'react'
import PropTypes from 'prop-types'

import Card from './card/container'
import Header from './header'

const TodayList = ({ 
  type,
  data 
}) => (
  <div>
    <Header>Today</Header>
    { data.map( (x, index) => {
      return (
        <Card 
          key={ index }
          item={ x }
          type={ type }
          isCheckInAvaialable={ true } />
      )
    }) }
    { data.length === 0 ? <p>There are no today visits</p> : undefined }
  </div>
) 

TodayList.defaultProps = {
  data: []
}

TodayList.propTypes = {
  type: PropTypes.oneOf(['patient', 'provider']),
  data: PropTypes.array.isRequired
}


export default TodayList