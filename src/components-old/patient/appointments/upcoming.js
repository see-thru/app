import React from 'react'
import PropTypes from 'prop-types'

import Card from './card/container'
import Header from './header'

const UpcomingList = ({ data }) => (
  <div>
    <Header>Upcoming</Header>
    { data.map( (x, index) => {
      return (
        <Card 
          key={ index }
          item={ x } />
      )
    }) }
    { data.length === 0 ? <p>There are no upcoming appointments</p> : undefined }
  </div>
)

UpcomingList.defaultProps = {
  data: []
}

UpcomingList.propTypes = {
  data: PropTypes.array.isRequired
}

export default UpcomingList