import styled from 'styled-components'

import { StyledContainerGrid } from '../../generic/grid/index'

const StyledContainer = styled(StyledContainerGrid)`
  margin: 60px auto;
`

export default StyledContainer