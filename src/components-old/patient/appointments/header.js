import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const Header = styled.h3`
  color: ${ colors.successButton };
  font-size: 1rem;
  font-weight: 500;
  margin: 0 0 20px;
  text-transform: uppercase;
`

export default Header