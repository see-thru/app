import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import PrivateRoute from '../private-router'
import AppointmentsList from './appointments/base'
import CheckInContainer from './check-in/container'
import AppointmentScheduledContainer from './appointment-scheduled/container'

export const PatientContainer = ({
  match,
  isLogged
}) => (
  <Fragment>
    <PrivateRoute 
      exact
      path={ `${ match.path }/booking/appointments` }
      component={ AppointmentsList }
      isLogged={ isLogged } />
    <PrivateRoute 
      exact
      path={ `${ match.path }/booking/appointments/:appointmentId/check-in` }
      component={ CheckInContainer }
      isLogged={ isLogged } />
    <PrivateRoute 
      path={ `${ match.path }/appointment-scheduled` }
      component={ AppointmentScheduledContainer }
      isLogged={ isLogged } />
  </Fragment>
)

const mapStateToProps = (state) => ({
  isLogged: state.AuthReducer.isLogged
})

PatientContainer.propTypes = {
  match: PropTypes.object.isRequired,
  isLogged: PropTypes.bool
}

export default connect(
  mapStateToProps,
  {}
)( PatientContainer ) 