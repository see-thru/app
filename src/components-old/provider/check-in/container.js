import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { 
  getBooking,
  providerCheckInBooking 
} from '../../../actions-old/appointments'

import CheckIn from './base'

export class CheckInContainer extends Component {
  constructor () {
    super()
    this.state = {
      delay: 300,
      result: 'No result',
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.handleScan = this.handleScan.bind(this)
  }

  componentWillMount () {
    const {
      user,
      match, 
      getBooking
    } = this.props
    getBooking(
      user.token,
      match.params.appointmentId
    )
  }

  handleScan (data) {
    const { 
      user,
      match,
      providerCheckInBooking
    } = this.props
    if (data) {
      this.setState({ result: data })
      providerCheckInBooking(
        data,
        user.token,
        match.params.appointmentId
      )
    }
  }

  onSubmit (e) {
    const {
      code
    } = e
    const { 
      user,
      match,
      providerCheckInBooking
    } = this.props
    providerCheckInBooking(
      code,
      user.token,
      match.params.appointmentId
    )
  }

  handleError (err) {
    console.error(err)
  }

  render () {
    const {
      delay,
      result
    } = this.state
    return (
      <CheckIn
        delay={ delay }
        result={ result }
        onSubmit={ this.onSubmit }
        handleScan={ this.handleScan }
        handleError={ this.handleError } 
        { ...this.props } />
    )
  }
}

CheckInContainer.propTypes = {
  user: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  checkIn: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  providerCheckInBooking: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  checkIn: state.AppointmentsReducer.checkIn,
  booking: state.AppointmentsReducer.booking
})

export default connect(
  mapStateToProps,
  { 
    getBooking,
    providerCheckInBooking 
  }
)( CheckInContainer )