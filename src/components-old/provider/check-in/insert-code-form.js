import React from 'react'
import PropTypes from 'prop-types'
import { required } from 'redux-form-validators'
import { 
  Field,
  reduxForm 
} from 'redux-form'

import { 
  Input,
  FormWrapper 
} from '../../generic/form/index'
import StyledInsertCodeForm from './styled-insert-code-form'
import { StyledButtonSuccess } from '../../generic/button/styled'

export const InsertCodeForm = ({
  onSubmit,
  handleSubmit
}) => (
  <StyledInsertCodeForm>
    <FormWrapper onSubmit={ handleSubmit( onSubmit ) }>
      <Field
        name="code"
        title="Or directly you can write the code here:"
        component={ Input }
                validate={[
          required()
        ]}
      />
      <StyledButtonSuccess type="submit">
        Submit
      </StyledButtonSuccess>
    </FormWrapper>
  </StyledInsertCodeForm>
)

InsertCodeForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'insertCodeForm'
})( InsertCodeForm )