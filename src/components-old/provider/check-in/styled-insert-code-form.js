import styled from 'styled-components'

const StyledInsertCodeForm = styled.div`
  margin: auto;
  max-width: 400px;
  padding: 0 8px;
  width: 100%
  label {
    font-size: .9rem;
  }
`

export default StyledInsertCodeForm