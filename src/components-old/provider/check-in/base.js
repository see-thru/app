import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import QrReader from 'react-qr-reader'

import Loader from '../../generic/loader/base'
import Container from '../../patient/appointments/container'
import InsertCodeForm from './insert-code-form'
import StyledQRCodeTitle from '../../generic/qr-code/styled-title'
import VisitStartedContainer from '../../generic/qr-code/visit-started/container'
import StyledButtonBackToEdit from '../../generic/qr-code/back-button'

const CheckIn = ({
  delay,
  result,
  checkIn,
  booking,
  onSubmit,
  handleScan,
  handleError
}) => (
  <Container
    gridGap="20px 100px"
    grid-template-rows="1"
    templateColumns="100%">
    { checkIn.loading || booking.loading ? <Loader /> : undefined }
    <div>
      { booking.data && booking.data.status !== 'checked-in' ? 
        <Fragment>
          <StyledButtonBackToEdit to="/provider/booking/appointments/list" />
          <StyledQRCodeTitle>Scan the QR Code:</StyledQRCodeTitle>
          <QrReader
            delay={ delay }
            onError={ handleError }
            onScan={ handleScan }
            style={{ width: '96%', maxWidth: '400px', margin: '0 auto' }} />
          <InsertCodeForm onSubmit={ onSubmit } />
        </Fragment>
        : undefined }
      { booking.data && booking.data.status === 'checked-in' ? 
        <VisitStartedContainer booking={ booking.data } />
        : undefined }
    </div>
  </Container>
)

CheckIn.propTypes = {
  delay: PropTypes.number.isRequired,
  result: PropTypes.string,
  checkIn: PropTypes.shape({
    loading: PropTypes.bool,
    data: PropTypes.shape({
      status: PropTypes.string
    })
  }).isRequired,
  booking: PropTypes.shape({
    data: PropTypes.shape({
      status: PropTypes.string
    }),  
    loading: PropTypes.bool,
  }),
  onSubmit: PropTypes.func.isRequired,
  handleScan: PropTypes.func.isRequired,
  handleError: PropTypes.func.isRequired
}

export default CheckIn