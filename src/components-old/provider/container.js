import React from 'react'
import { Route, Switch } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import PrivateRoute from '../private-router'
import AppointmentsList from '../patient/appointments/base'
import CheckInContainer from './check-in/container'
import ProviderShowContainer from './show/container'
import NoMatchContainer from '../not-match/container'

export const ProviderContainer = ({
  match,
  isLogged
}) => (
  <Switch>
    <Route 
      exact
      path={ `${ match.path }/:providerId/:whatFilter` }
      component={ ProviderShowContainer } />
    <PrivateRoute 
      exact
      path={ `${ match.path }/booking/appointments/list` }
      component={ AppointmentsList }
      isLogged={ isLogged } />
    <PrivateRoute 
      exact
      path={ `${ match.path }/booking/appointments/:appointmentId/check-in` }
      component={ CheckInContainer }
      isLogged={ isLogged } />
    <Route component={ NoMatchContainer } />
  </Switch>
)

const mapStateToProps = (state) => ({
  isLogged: state.AuthReducer.isLogged
})

ProviderContainer.propTypes = {
  match: PropTypes.object.isRequired,
  isLogged: PropTypes.bool
}

export default connect(
  mapStateToProps,
  {}
)( ProviderContainer ) 