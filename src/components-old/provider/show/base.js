import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'

import Loader from '../../generic/loader/base'
import Header from './header/container'
import Location from './location/container'
import DateContainer from './date/container'
import StyledProvider from './styled-provider'
import Qualifications from './qualifications/container'
import ConfirmPayment from './confirm-payment/container'
import GetLastCreditCard from '../../../utils/get-last-credit-card'
import ThereAreCreditCards from '../../../utils/there-are-credit-cards'
import GetBookingTotalPrice from '../../../utils/get-booking-total-price'
import { StyledContainerGrid } from '../../generic/grid/index'
import GetAmountAvailablesDays from '../../../utils/get-amount-availables-days'
import MedicalServicesContainer from './medical-services/container'
import StyledContainerProviderPage from './styled-container'
import GetBookingTotalEstimatedTime from '../../../utils/get-booking-total-estimated-time'

const Provider = ({
  loading,
  provider,
  isLogged,
  isSelected,
  daySelected,
  dataBilling,
  dateSelected,
  hourSelected,
  getDaySelected,
  getHourSelected,
  bundlesSelected,
  servicesSelected,
  toggleSignInModal,
  providerIdDisplay,
  selectServicesOrBundles
}) => {
  let dateMessage = ''
  if ( GetAmountAvailablesDays(provider.availabilities) === 0 ) {
    dateMessage = "There aren't available times"
  } 
  let hourFormatted = new Date()
  if ( hourSelected ) hourFormatted.setHours(hourSelected)
  const lastCreditCard = GetLastCreditCard(dataBilling) ? GetLastCreditCard(dataBilling) : undefined    
  return ( 
    <StyledProvider>
      { loading ? <Loader /> : undefined }
      <StyledContainerGrid
        gridGap="20px 10px"
        templateRows="auto"
        templateColumns="repeat(1, 1fr)">
        <Header provider={ provider } />
      </StyledContainerGrid>
      <StyledContainerProviderPage
        gridGap="0 10px"
        templateRows="auto"
        templateColumns="repeat(2, 1fr)">
        <div>
          <DateContainer
            daySelected={ daySelected }
            dateSelected={ dateSelected }
            hourSelected={ hourSelected }
            availabilities={ provider.availabilities ? provider.availabilities : undefined } 
            estimatedVisit="12 minutes (Estimated Visit Time)"
            getDaySelected={ getDaySelected }
            getHourSelected={ getHourSelected } 
            unavailabilities={ provider.unavailableTimes ? provider.unavailableTimes : undefined } />
          <Location
            address={ provider.office ? provider.office.address : undefined }
            distance="1.8 miles" />
          <MedicalServicesContainer
            isSelected={ isSelected }
            servicesAndBundles={ provider.servicesAndBundles }
            selectServicesOrBundles={ selectServicesOrBundles } />
        </div>
        <Qualifications provider={ provider.provider } />
        <ConfirmPayment 
          date={ dateSelected ? moment(new Date( dateSelected )).format('MMMM Do') : dateMessage }
          time={ hourSelected ? moment( hourFormatted ).format('ha') : '' }
          isLogged={ isLogged }
          provider={ provider }
          ccNumber={ lastCreditCard ? ` ${ lastCreditCard.lastFourDigits }` : 'XXXX' }
          totalPrice={ GetBookingTotalPrice(bundlesSelected, servicesSelected) }
          dateSelected={ dateSelected }
          hasCreditCard={ ThereAreCreditCards(dataBilling) }
          availabilities={ provider.availabilities ? provider.availabilities : undefined } 
          bundlesSelected={ bundlesSelected }
          time24HoursClock={ hourSelected ? moment( hourFormatted ).format('H') : '' }
          servicesSelected={ servicesSelected }
          toggleSignInModal={ toggleSignInModal }
          providerIdDisplay={ providerIdDisplay }
          totalEstimatedTime={ GetBookingTotalEstimatedTime(bundlesSelected, servicesSelected) } />      
      </StyledContainerProviderPage>
    </StyledProvider>
  )
}

Provider.propTypes = {
  office: PropTypes.shape({
    address: PropTypes.string.isRequired,
    location: PropTypes.shape({
      lat: PropTypes.string,
      lng: PropTypes.string
    })
  }),
  loading: PropTypes.bool,
  isLogged: PropTypes.bool,
  provider: PropTypes.shape({
    provider: PropTypes.object,
    practiceName: PropTypes.string,
    displayId: PropTypes.string,
    servicesAndBundles: PropTypes.object
  }).isRequired,
  isSelected: PropTypes.func.isRequired,
  dataBilling: PropTypes.shape({
    cards: PropTypes.array
  }).isRequired,
  daySelected: PropTypes.string.isRequired,
  hourSelected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  dateSelected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
    PropTypes.object
  ]).isRequired,
  getDaySelected: PropTypes.func.isRequired,
  getHourSelected: PropTypes.func.isRequired,
  bundlesSelected: PropTypes.array.isRequired,
  servicesSelected: PropTypes.array.isRequired,
  toggleSignInModal: PropTypes.func.isRequired,
  providerIdDisplay: PropTypes.string.isRequired,
  selectServicesOrBundles: PropTypes.func.isRequired
}

export default Provider