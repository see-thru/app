import styled from 'styled-components'

const StyledProvider = styled.div`
  margin: 20px 0;
`

export default StyledProvider