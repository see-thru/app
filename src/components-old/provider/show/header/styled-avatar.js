import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledAvatar = styled.img`
  border-radius: 100%;
  height: 100px;
  width: 100px;
  ${
    Media.small`
      height: 80px;
      width: 80px;
    `
  }
`

export default StyledAvatar