import styled from 'styled-components'

const StyledHeader = styled.div`
  display: flex;
  margin: 10px 0 20px;
`

export default StyledHeader