import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../../utils/settings'

const StyledTitle = styled.h2`
  color: ${ colors.textTitle };
  font-size: 1.1rem;
  font-weight: ${ fonts.weightBold };
  margin: 5px 0 0;
`

export default StyledTitle