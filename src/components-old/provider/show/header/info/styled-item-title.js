import styled from 'styled-components'

const StyledItemTitle = styled.span`
  margin-right: 10px;
`

export default StyledItemTitle