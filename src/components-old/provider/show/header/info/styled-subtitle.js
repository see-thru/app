import styled from 'styled-components'
import { colors } from '../../../../../utils/settings';

const StyledSubtitle = styled.h3`
  color: ${ colors.textSubtitle };
  font-size: .9rem;
  font-weight: 400;
  margin: 0;
`

export default StyledSubtitle