import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import StyledInfo from './styled'
import StyledList from './styled-list'
import StyledItem from './styled-item'
import StyledTitle from './styled-title'
import StyledSubtitle from './styled-subtitle'
import StyledItemContent from './styled-item-content'
import StyledSeeThruIndexImg from '../../../../search/results/provider/info/styled-seethru-index-img'

const Info = ({ 
  name,  
  specialty,
  practiceName,
  seethruIndex
}) => (
  <StyledInfo>
    <StyledTitle>{ name }</StyledTitle>
    <StyledSubtitle>{ practiceName }</StyledSubtitle>
    { specialty.primary ? 
      <StyledList>
        <StyledItem>
          <StyledItemContent>
            Primary Specialty: { specialty.primary }
          </StyledItemContent> 
        </StyledItem>    
      </StyledList>      
      : undefined }
    { specialty.secondary && specialty.secondary.length >= 1 ? 
      <StyledList>
        <StyledItem>
          <StyledItemContent>
            Secondary: { specialty.secondary.map( (secondary, index) => {
              return (
                <Fragment key={ index }>
                  { `${ secondary } ` }
                </Fragment>
              )
            }) }
          </StyledItemContent>      
        </StyledItem>    
      </StyledList>      
      : undefined }
    <StyledList>
      <StyledItem>
        <StyledItemContent>
          <StyledSeeThruIndexImg 
          src="/images/seethru-isotype.png" 
          width="12"
          height="12" />
          { seethruIndex }
        </StyledItemContent>
      </StyledItem>
    </StyledList>
  </StyledInfo>
)

Info.defaultProps = {
  specialty: {
    secondary: []
  }
}

Info.propTypes = {
  name: PropTypes.string,
  practiceName: PropTypes.string,
  seethruIndex: PropTypes.string,
  specialty: PropTypes.shape({
    primary: PropTypes.string,
    secondary: PropTypes.array
  })
}

export default Info