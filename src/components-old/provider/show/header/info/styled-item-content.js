import styled from 'styled-components'

import { colors } from '../../../../../utils/settings'

const StyledItemContent = styled.span`
  color: ${ colors.textDate };
  font-size: .9rem;
  margin: 5px 0;
`

export default StyledItemContent