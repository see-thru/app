import React from 'react'
import PropTypes from 'prop-types'

import StyledAvatar from './styled-avatar'

const Avatar = ({ 
  url,
  name
}) => (
  <StyledAvatar
    src={ url }
    alt={ name }
    width="120"
    height="120" />
)

Avatar.defaultProps = {
  url: '/images/example-avatar.png'
}

Avatar.propTypes = {
  url: PropTypes.string,
  name: PropTypes.string
}

export default Avatar