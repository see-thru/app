import React from 'react'
import PropTypes from 'prop-types'

import Info from './info/base'
import Avatar from './avatar'
import StyledHeader from './styled-container'
import GenerateRandomProfilePicture from '../../../../utils/generate-random-profile-picture'

const Header = ({ provider }) => {
  let data = {}
  if ( provider && provider.provider ) {
    data = provider.provider
  }
  const fullName = `${ data.firstName } ${ data.lastName }`
  return (
    <StyledHeader>
      <Avatar   
        url={ data.photo ? data.photo : GenerateRandomProfilePicture() }
        name={ fullName } />
      <Info 
        name={ fullName }
        specialty={ data.specialty }
        practiceName={ data.practiceName }
        seethruIndex={ data.seethruIndex ? data.seethruIndex : '-' } />
    </StyledHeader>
  )
}

Header.propTypes = {
  provider: PropTypes.shape({
    provider: PropTypes.shape({
      photo: PropTypes.string,
      lastName: PropTypes.string,
      firstName: PropTypes.string,
      practiceName: PropTypes.string,
      specialty: PropTypes.shape({
        primary: PropTypes.string,
        secondary: PropTypes.array
      }),
      seethruIndex: PropTypes.string
    }).isRequired
  }).isRequired
}

export default Header