import React, { Component } from 'react'
import moment from 'moment'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { 
  getProvider,
  providerSuccessFully
} from '../../../actions-old/provider'
import { toggleSignInModal } from '../../../actions-old/auth'

import Provider from './base'
import isFeatureEnabled from '../../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../../has-permission'

import GetDay from '../../../utils/first-available-date/get-day'
import GetHour from '../../../utils/first-available-date/get-hour'
import GetNameDay from '../../../utils/first-available-date/get-name-day'
import GetNumberOfDays from '../../../utils/first-available-date/get-number-of-days'
import GetProviderFromSearchResults from '../../../utils/get-provider-from-search-results'
import { 
  SelectServicesFromSearchResults,
  SelectBundlesServicesFromSearchResults
} from '../../../utils/select-services-search-results/index'

export class ProviderShowContainer extends Component {
  constructor () {
    super()
    this.state = ({
      daySelected: '',
      dateSelected: '',
      hourSelected: '',
      bundlesSelected: [],
      servicesSelected: []
    })
  }

  componentWillMount () {
    const { 
      match,
      dataResults,
      getProvider,
      providerSuccessFully
    } = this.props
    const provider = GetProviderFromSearchResults(dataResults, match.params.providerId)
    if ( provider ) {
      providerSuccessFully(provider)
    } else {
      getProvider(match.params.providerId)
    }
  }

  componentWillReceiveProps ( nextProps ) {
    const { data } = nextProps
    
    console.log('componentWillReceiveProps', data, nextProps)
    if ( this.props.data !== data ) {
      if ( 
        data && 
        data.availabilities && 
        data.availabilities.weekdays &&
        data.availabilities.weekdays.length >= 1 ) {
        const currentDate = new Date()
        const nameDay = GetNameDay(currentDate.getDay())
        const firstAvailableDay = GetDay(nameDay, currentDate.getDay(), data.availabilities.weekdays)
        const firstAvailableHour = GetHour(firstAvailableDay, currentDate, data.availabilities.weekdays)
        currentDate.setHours(firstAvailableHour)        
        const days = GetNumberOfDays(currentDate.getDay(), firstAvailableDay)
        const nextDay = moment(currentDate).startOf('isoWeek').add(days, 'days')
        this.getDaySelected(nextDay)
        this.getHourSelected(firstAvailableHour)
      } 
      if ( data && data._highlight && data._highlight["services.name"] ) {
        const bundles = data.servicesAndBundles && data.servicesAndBundles.bundles ? data.servicesAndBundles.bundles : []
        const services = data.servicesAndBundles && data.servicesAndBundles.services ? data.servicesAndBundles.services : []
        const hightligh = data._highlight["services.name"][0]
        const servicesSelectedFromSearchResults = SelectServicesFromSearchResults(services, hightligh)
        this.setState({ servicesSelected: servicesSelectedFromSearchResults })
        const bundlesServicesSelectedFromSearchResults = SelectBundlesServicesFromSearchResults(bundles, hightligh)
        this.setState({ bundlesSelected: bundlesServicesSelectedFromSearchResults })
      }
    }
  }

  selectServicesOrBundles = ( type, value ) => {
    const {
      bundlesSelected,
      servicesSelected
    } = this.state
    if ( type === 'service' ) {
      if ( this.isSelected(type, value) ) {
        const index = this.getIndex(type, value)
        servicesSelected.splice(index, 1)
      } else {
        servicesSelected.push(value)
      }
      this.setState({ servicesSelected })      
    }
    if ( type === 'bundle' ) {
      if ( this.isSelected(type, value) ) {
        const index = this.getIndex(type, value)
        bundlesSelected.splice(index, 1)
      } else {
        bundlesSelected.push(value)
      }
      this.setState({ bundlesSelected })
    }
  }

  isSelected = ( type, value ) => {
    const {
      bundlesSelected,
      servicesSelected
    } = this.state
    if ( type === 'service' ) {
      const selected = servicesSelected.filter( ( service, i ) => service === value )
      if ( selected.length >= 1 ) return true
    }
    if ( type === 'bundle' ) {
      const selected = bundlesSelected.filter( ( bundle, i ) => bundle === value )
      if ( selected.length >= 1 ) return true
    }
  }

  getIndex = ( type, value ) => {
    const {
      bundlesSelected,
      servicesSelected
    } = this.state
    let index
    if ( type === 'service' ) {
      const selected = servicesSelected.filter( ( service, i ) => {
        if ( service === value ) index = i
        return service === value
      })
      if ( selected.length >= 1 ) return index
    }
    if ( type === 'bundle' ) {
      const selected = bundlesSelected.map( ( bundle, i ) => {
        if ( bundle === value ) index = i
        return bundle === value
      })
      if ( selected.length >= 1 ) return index
    }    
  }

  getDaySelected = date => {
    this.setState({
      daySelected: moment( new Date(date) ).format('dddd'), 
      dateSelected: moment( date )
    })
  } 

  getHourSelected = hour => {
    this.setState({
      hourSelected: hour
    })
  }

  render () {
    const {
      daySelected,
      dateSelected,
      hourSelected,
      bundlesSelected,
      servicesSelected
    } = this.state
    const { 
      data, 
      match,
      loading,
      isLogged,
      dataBilling,
      loadingAuth,
      loadingBilling,
      toggleSignInModal,
      loadingBookAnAppointment
    } = this.props
    return (
      <Provider
        match={ match }
        loading={ loading || loadingBookAnAppointment || loadingAuth || loadingBilling ? true : false }
        provider={ data }
        isLogged={ isLogged }
        isSelected={ this.isSelected }
        daySelected={ daySelected }
        dataBilling={ dataBilling }
        dateSelected={ dateSelected }
        hourSelected={ hourSelected }
        getDaySelected={ this.getDaySelected }
        getHourSelected={ this.getHourSelected }
        bundlesSelected={ bundlesSelected }
        servicesSelected={ servicesSelected }
        toggleSignInModal={ toggleSignInModal }
        providerIdDisplay={ match.params.providerId }
        selectServicesOrBundles={ this.selectServicesOrBundles } />
    )
  }
}

ProviderShowContainer.propTypes = {
  data: PropTypes.object,
  match: PropTypes.shape({
    params: PropTypes.shape({
      providerId: PropTypes.string.isRequired
    })
  }).isRequired,
  loading: PropTypes.bool,
  isLogged: PropTypes.bool,
  location: PropTypes.object,
  dataResults: PropTypes.object,
  dataBilling: PropTypes.object,
  getProvider: PropTypes.func.isRequired,
  loadingAuth: PropTypes.bool,
  loadingPayment: PropTypes.bool,
  loadingBilling: PropTypes.bool,
  toggleSignInModal: PropTypes.func.isRequired,
  providerSuccessFully: PropTypes.func.isRequired,
  loadingBookAnAppointment: PropTypes.bool
}

const mapStateToProps = (state) => ({
  data: state.ProviderReducer.provider,
  loading: state.ProviderReducer.loading,
  isLogged: state.AuthReducer.isLogged,
  loadingAuth: state.AuthReducer.loading,
  dataResults: state.SearchReducer.data,
  dataBilling: state.BillingReducer.data,
  loadingBilling: state.BillingReducer.loading,
  loadingBookAnAppointment: state.AppointmentsReducer.loading
})

export default HasPermissionHOC(
  connect(
    mapStateToProps,
    { 
      getProvider,
      toggleSignInModal,
      providerSuccessFully
    }
  )( ProviderShowContainer ), 
  isFeatureEnabled(window.location.href).pages.providerDetail
)