import styled from 'styled-components'

import { colors } from '../../../../../utils/settings'

const StyledCloseButton = styled.button`
  color: ${ colors.textDate };
  display: block;
  font-size: .85rem;
  margin: 20px 0;
  text-align: right;
  padding-right: 20px;
  width: 100%;
  svg {
    bottom: 2px;
    display: inline-block;
    margin-right: 6px;
    position: relative;
    vertical-align: middle;
  }
`

export default StyledCloseButton