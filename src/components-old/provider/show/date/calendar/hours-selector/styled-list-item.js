import styled from 'styled-components'
import PropTypes from 'prop-types'

import { colors } from '../../../../../../utils/settings'
import Media from '../../../../../../utils/media'

const StyledListItem = styled.li`
  background-color: ${ colors.lightBorder };
  border-radius: 10px;
  margin: 10px 20px;
  padding: 0 14px;
  width: calc(100% - 40px);
  button {
    display: block;
    text-align: left;
    padding: 14px 0;
    outline: 0;
    width: 100%;
  }
  span {
    color: transparent;
    float: right;
    font-size: .9rem;
    ${ 
      Media.small`
        display: block;
        float: none;
        margin-top: 5px;
      ` 
    }
  }
  ${ props => props.hourIsSelected === 'true' ? `
    background-color: ${ colors.bgCalendarSelectDay };
    span,
    button {
      color: white;
    }
  ` : undefined }  
`

StyledListItem.propTypes = {
  hourIsSelected: PropTypes.string,
}

export default StyledListItem