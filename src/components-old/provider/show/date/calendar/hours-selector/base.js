import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'

import StyledList from './styled-list'
import StyledTitle from './styled-title'
import StyledListItem from './styled-list-item'

const HoursSelector = ({
  selectToHour,
  hourSelected,
  availableHours,
  estimatedVisit
}) => {
  return (
    <StyledList>
      { availableHours.length >= 1 ? 
        <StyledTitle>Availables Hours:</StyledTitle> 
        : undefined }
      { availableHours.map( (hour, index) => {
        let hourFormatted = new Date()
        hourFormatted.setHours(hour)
        return (
          <StyledListItem
            key={ index }
            hourIsSelected={ hourSelected === hour ? 'true' : 'false' }>
            <button onClick={ selectToHour.bind(this, hour) }>
              { moment( hourFormatted ).format('ha') }
            </button>
          </StyledListItem>
        )
      }) }
    </StyledList>
  )
}

HoursSelector.defaultProps = {
  availableHours: []
}

HoursSelector.propTypes = {
  selectToHour: PropTypes.func.isRequired,
  hourSelected: PropTypes.number,
  availableHours: PropTypes.array,
  estimatedVisit: PropTypes.string.isRequired
}

export default HoursSelector