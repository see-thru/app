import styled from 'styled-components'

const StyledTitle = styled.h3`
  font-size: 1rem;
  font-weight: 400;
  padding: 0 20px;
`

export default StyledTitle