import React, { Component } from 'react'
import PropTypes from 'prop-types'

import HoursSelector from './base'

class HoursSelectorContainer extends Component { 
  constructor () {
    super()
    this.state = ({ hourSelected: 0 })
  }

  selectToHour = ( hour ) => {
    const {
      closeCalendar, 
      getHourSelected 
    } = this.props
    this.setState({ hourSelected: hour })
    getHourSelected(hour)
    closeCalendar()
  }

  render () {
    const { hourSelected } = this.state
    const {
      hours, 
      estimatedVisit 
    } = this.props
    return (
      <HoursSelector 
        selectToHour={ this.selectToHour }
        hourSelected={ hourSelected }
        availableHours={ hours }
        estimatedVisit={ estimatedVisit } />
    )
  }
}

HoursSelectorContainer.defaultProps = {
  hours: []
}

HoursSelectorContainer.propTypes = {
  hours: PropTypes.array,
  closeCalendar: PropTypes.func.isRequired,
  estimatedVisit: PropTypes.string.isRequired,
  getHourSelected: PropTypes.func.isRequired
}

export default HoursSelectorContainer