import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../../generic/icon/base'
import CalendarContainer from '../../../../generic/calendar/container'
import StyledCloseButton from './styled-close-button'
import StyledWrapperCalendar from './styled'
import HoursSelectorContainer from './hours-selector/container'

const Calendar = ({
  hours,
  minDate,
  daySelected,
  dateSelected,
  hourSelected,
  closeCalendar,
  getDaySelected,
  estimatedVisit,
  availabilities,
  getHourSelected,
  unavailabilities,
  availabilitiesWithFormat
}) => (
  <StyledWrapperCalendar>
    <StyledCloseButton onClick={ closeCalendar }>
      <Icon 
        name="calendar-close"
        width="22"
        height="22" />
      Close
    </StyledCloseButton>
    <CalendarContainer
      minDate={ minDate }
      onSubmit={ getDaySelected } 
      daySelected={ daySelected }
      dateSelected={ dateSelected }
      hourSelected={ hourSelected }
      daysDisabled={ availabilities }
      unavailabilities={ unavailabilities }
      availabilitiesWithFormat={ availabilitiesWithFormat } />
    <HoursSelectorContainer
      hours={ hours } 
      closeCalendar={ closeCalendar }
      getHourSelected={ getHourSelected }
      estimatedVisit={ estimatedVisit } />
  </StyledWrapperCalendar>
)

Calendar.defaultProps = {
  minDate: new Date()
}

Calendar.propTypes = {
  hours: PropTypes.array,
  minDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date)
  ]),
  closeCalendar: PropTypes.func.isRequired,
  getDaySelected: PropTypes.func.isRequired,
  estimatedVisit: PropTypes.string.isRequired,
  availabilities: PropTypes.array.isRequired,
  getHourSelected: PropTypes.func.isRequired
}

export default Calendar