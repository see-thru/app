import styled from 'styled-components'
import Media from '../../../../../utils/media';

const StyledWrapperCalendar = styled.div`
  background-color: white;
  left: 0;
  height: 100%;
  overflow-x: auto;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 3;
  ${
    Media.small`
      height: auto;
      max-height: 100%;
    `
  }
`

export default StyledWrapperCalendar