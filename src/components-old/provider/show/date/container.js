import React, { Component } from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import BodyClassName from 'react-body-classname'

import Button from './button/base'
import Calendar from './calendar/base'
import {
  GetAvailablesHours, 
  GetDisabledDaysFromAvailablesDays 
} from '../../../../utils/generator-data-structure/choose-appointment-time/'
import GetAmountAvailablesDays from '../../../../utils/get-amount-availables-days'

class DateContainer extends Component {
  constructor () {
    super()
    this.state = ({ 
      isOpened: false
    })
  }

  openCalendar = () => {
    this.setState({ isOpened: true })
  }

  closeCalendar = () => {
    this.setState({ isOpened: false })
  }  

  displayMessage = which => {
    let dateMessage = 'you must select a day'
    let timeMessage = 'you must select a time'
    const { availabilities } = this.props
    if ( GetAmountAvailablesDays(availabilities) === 0 ) {
      dateMessage = "There aren't available times"
      timeMessage = ''
    } 
    if ( which === 'date' ) return dateMessage 
    if ( which === 'time' ) return timeMessage
  } 

  render () {
    const { 
      isOpened,
    } = this.state
    const { 
      daySelected,
      dateSelected,
      hourSelected,
      estimatedVisit, 
      getDaySelected,
      availabilities,
      getHourSelected,
      unavailabilities
    } = this.props
    let hourFormatted = new Date()
    if ( hourSelected ) hourFormatted.setHours(hourSelected)
    return (
      <div>
        { isOpened ? <BodyClassName className="shadow-block" /> : undefined }
        <Button 
          date={ dateSelected ? moment(new Date( dateSelected )).format('MMMM Do') : this.displayMessage('date') } 
          time={ hourSelected ? moment( hourFormatted ).format('ha') : this.displayMessage('time') }
          openCalendar={ GetAmountAvailablesDays(availabilities) !== 0 ? this.openCalendar : () => console.log('there are not available dates') } />
        { isOpened ? 
          <Calendar 
            hours={ 
              GetAvailablesHours(
                availabilities.weekdays, 
                daySelected, 
                unavailabilities,
                dateSelected
              ) 
            }
            daySelected={ daySelected }
            dateSelected={ dateSelected }
            hourSelected={ hourSelected }
            closeCalendar={ this.closeCalendar }
            estimatedVisit={ estimatedVisit }
            getDaySelected={ getDaySelected }
            getHourSelected={ getHourSelected }
            availabilities={ GetDisabledDaysFromAvailablesDays(availabilities.weekdays) }
            unavailabilities={ unavailabilities } 
            availabilitiesWithFormat={ availabilities } /> 
          : undefined }
      </div>
    )
  }
}

DateContainer.propTypes = {
  estimatedVisit: PropTypes.string.isRequired,
  availabilities: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.shape({
      weekdays: PropTypes.arrayOf(PropTypes.shape({
        slots: PropTypes.array.isRequired,
        weekday: PropTypes.string.isRequired
      }))
    })
  ])
}

export default DateContainer