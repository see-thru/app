import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../../generic/icon/base'
import StyledButton from './styled'

const Button = ({
  date,
  time,
  openCalendar
}) => (
  <StyledButton onClick={ openCalendar }>
    <span>
      <Icon 
        name="search-time"
        width="16"
        height="16" />
      { date }
    </span>
    <span>{ time }</span>
  </StyledButton>  
)

Button.propTypes = {
  date: PropTypes.string,
  time: PropTypes.string,
  openCalendar: PropTypes.func.isRequired
}

export default Button