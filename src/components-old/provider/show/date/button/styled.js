import styled from 'styled-components'
import { colors } from '../../../../../utils/settings';

const StyledButton = styled.button`
  background-color: ${ colors.bgFilterWhat };
  border-radius: 2px;
  color: white;
  font-size: .9rem;
  margin: 10px 0;
  text-align: left;
  padding: 12px 14px;
  width: 100%;
  svg,
  span {
    vertical-align: middle;
  }
  svg {
    bottom: 2px;
    margin-right: 8px;
    position: relative;
  }
  span {
    display: inline-block;
    &:last-of-type {
      margin-left: 20px;
    }
  }
`

export default StyledButton