import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import AddToCalendar from 'react-add-to-calendar'

import StyledImg from './styled-img'
import StyledDay from './styled-day'
import StyledMonth from './styled-month'
import StyledCalendar from './styled-calendar'
import StyledCalendarTop from './styled-calendar-top'
import StyledAddToCalendar from './styled-add-to-calendar'
import StyledSeeBookedAppointmentsButton from './styled-see-booked-appointments-button'

const AppointmentScheduled = ({
  day,
  date,
  month
}) => (
  <div>
    <StyledImg 
      alt=""
      src="/images/appointment-scheduled.jpg" 
      width="auto" />
    <StyledCalendar>
      <StyledCalendarTop />
      <div>
        <StyledMonth>{ month }</StyledMonth>
        <StyledDay>{ day }</StyledDay>
      </div>
      <StyledAddToCalendar>
        <AddToCalendar event={ date }/>
      </StyledAddToCalendar>
    </StyledCalendar>
    <StyledSeeBookedAppointmentsButton tag={ Link } to="/patient/booking/appointments">
      See Booked Appointments
    </StyledSeeBookedAppointmentsButton>
  </div>
)

AppointmentScheduled.propTypes = {
  day: PropTypes.string.isRequired,
  date: PropTypes.object.isRequired,
  month: PropTypes.string.isRequired
}

export default AppointmentScheduled