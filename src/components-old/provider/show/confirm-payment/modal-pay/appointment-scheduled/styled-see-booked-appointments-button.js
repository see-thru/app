import styled from 'styled-components'

import { StyledButtonSuccess } from '../../../../../generic/button/styled' 

const StyledSeeBookedAppointmentsButton = styled(StyledButtonSuccess)`
  display: block;
  margin: 20px auto 0;
  text-align: center;
  max-width: 300px;
`

export default StyledSeeBookedAppointmentsButton