import styled from 'styled-components'

const StyledImg = styled.img`
  display: block;
  margin: 0 auto;
  width: 100%;
`

export default StyledImg