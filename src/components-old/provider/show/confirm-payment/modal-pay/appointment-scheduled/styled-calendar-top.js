import styled from 'styled-components'

import { colors } from '../../../../../../utils/settings'

const StyledCalendarTop = styled.div`
  background-color: ${ colors.bgCalendarTop };
  display: block;
  height: 40px;
  margin-bottom: 20px;
  width: 100%;
`

export default StyledCalendarTop