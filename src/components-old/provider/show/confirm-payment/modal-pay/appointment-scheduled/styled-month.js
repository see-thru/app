import styled from 'styled-components'

const StyledMonth = styled.span`
  display: block;
  font-size: .9rem;
  text-transform: uppercase;
`

export default StyledMonth