import styled from 'styled-components'

import { colors } from '../../../../../../utils/settings'

const StyledCalendar = styled.div`
  background-color: ${ colors.lightBorder };
  border: 1px solid ${ colors.border };
  border-radius: 2px;
  text-align: center;
  margin: 30px auto 0;
  width: 170px;
`

export default StyledCalendar