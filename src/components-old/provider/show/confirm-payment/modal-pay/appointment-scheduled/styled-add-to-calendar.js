import styled from 'styled-components'
import { colors } from '../../../../../../utils/settings';

const StyledAddToCalendar = styled.div`
  cursor: pointer;
  font-size: .9rem;
  padding: 20px 0;
  ul {
    list-style: none;
    padding: 0;
    margin: 10px 0 0;
  }
  a {
    display: block;
    text-decoration: none;
  }
  li > a {
    color: ${ colors.link };
    padding: 5px 0;
  }
`

export default StyledAddToCalendar