import styled from 'styled-components'

const StyledDay = styled.span`
  display: block;
  font-size: 3rem;
  line-height: 1;
`

export default StyledDay