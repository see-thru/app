import styled from 'styled-components'
import { colors } from '../../../../../../utils/settings';

const StyledLabel = styled.p`  
  font-size: .9rem;
  margin: 0;
  &:last-of-type {
    margin: 20px 0 50px;
  }
  span,
  strong {
    color: ${ colors.schedulesDay }
  }
`

export default StyledLabel