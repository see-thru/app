import React from 'react'
import PropTypes from 'prop-types'

import StyledLabel from './styled-label'
import StyledButton from './styled-button'
import StyledConfirmPay from './styled'
import ListOfSelectedServices from './list-of-selected-services/'

const ConfirmPay = ({
  date,
  time,
  onSubmit,
  location,
  totalPrice,
  bundlesSelected,
  servicesSelected,
  providerFullName
}) => (
  <StyledConfirmPay>
    <img
      alt="Seethru isotype" 
      src="/images/seethru-isotype.png" 
      height="50"
      width="50" />
    <StyledLabel>Visiting <strong>{ providerFullName }</strong></StyledLabel>
    <StyledLabel>on <strong>{ date }</strong> at <strong>{ time }</strong></StyledLabel>
    <StyledLabel>at <span>{ location }</span></StyledLabel>
    <ListOfSelectedServices
      bundles={ bundlesSelected }
      services={ servicesSelected } />        
    <StyledButton onClick={ onSubmit }>
      Pay ${ totalPrice }
    </StyledButton>
  </StyledConfirmPay>
)

ConfirmPay.propTypes = {
  date: PropTypes.string,
  time: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  location: PropTypes.string,
  totalPrice: PropTypes.number.isRequired,
  providerFullName: PropTypes.string
}

export default ConfirmPay