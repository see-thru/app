import React from 'react'
import PropTypes from 'prop-types'

import StyledListOfSelectedServices from './styled'

const ListOfSelectedServices = ({
  bundles,
  services
}) => (
  <StyledListOfSelectedServices>
    <h4>Services Selected:</h4>
    <ul>
      { services.map( (service, index) => {
        return (
          <li key={ index }>
            { service.name }
          </li>
        )
      }) }
      { bundles.map( bundle => bundle.services.map( (service, index) => {
        return (
          <li key={ index }>
            { service }
          </li>
        )
      }) ) }
    </ul>
  </StyledListOfSelectedServices>
)

ListOfSelectedServices.propTypes = {
  bundles: PropTypes.array,
  services: PropTypes.array
}

export default ListOfSelectedServices