import styled from 'styled-components'

import { colors } from '../../../../../../../utils/settings'

const StyledListOfSelectedServices = styled.div`
  margin-bottom: 40px;
  h4 {
    color: ${ colors.bgCounter };
    margin: 0 0 10px;
  }
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
  li {
    font-size: .9rem;
    margin: 5px 0;
  }
`

export default StyledListOfSelectedServices