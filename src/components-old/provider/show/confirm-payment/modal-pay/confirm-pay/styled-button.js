import styled from 'styled-components'

import { colors } from '../../../../../../utils/settings'

const StyledButton = styled.button`
  background-color: ${ colors.bgCounter };
  border-radius: 2px;
  color: white;
  display: block;
  padding: 20px;
  width: 100%;
`

export default StyledButton