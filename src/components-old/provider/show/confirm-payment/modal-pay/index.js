import React, { Component } from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../../../../generic/modal/index'
import Icon from '../../../../generic/icon/base'
import ConfirmPay from './confirm-pay/'
import { 
  GetBundlesSelected,
  GetProviderAddress,
  GetProviderFullName,
  GetServicesSelected
} from '../../../../../utils/get-provider/'
import AppointmentScheduled from './appointment-scheduled/'
import generateDataForPayWithCard from '../../../../../utils/generator-data-structure/pay-with-card/'

class ModalPay extends Component {
  constructor () {
    super()
    this.state = ({
      willShow: 'confirmPay'
    })
  }

  showTo = section => {
    this.setState({ willShow: section })
  }

  closeModal = () => {
    const { closeModalPay } = this.props
    this.showTo('confirmPay')
    closeModalPay()
  }

  onSubmit = () => {
    const {
      user,
      creditCard,
      PayWithCard,
      dateSelected,
      bundlesSelected,
      time24HoursClock,
      servicesSelected,
      providerIdDisplay
    } = this.props
    const data = generateDataForPayWithCard(
      creditCard, 
      dateSelected, 
      time24HoursClock, 
      bundlesSelected, 
      servicesSelected, 
      providerIdDisplay
    )
    PayWithCard(data, user.token)
    this.showTo('appointmentScheduled')
  }

  render () {
    const { willShow } = this.state
    const { 
      date, 
      time,
      provider,
      totalPrice,
      dateSelected,
      bundlesSelected,
      time24HoursClock,
      servicesSelected,
      isModalPayOpened,
      totalEstimatedTime
    } = this.props
    const width = willShow === 'appointmentScheduled' ? '500px' : '600px'
    const address = GetProviderAddress(provider) 
    const fullName = GetProviderFullName(provider)
    const services = GetServicesSelected(servicesSelected)
    const bundles = GetBundlesSelected(bundlesSelected)
    let fullDate, fullDateWithEstimatedTime
    if ( time24HoursClock ) fullDate = moment(new Date(dateSelected)).set({ h: time24HoursClock })
    if ( totalEstimatedTime ) fullDateWithEstimatedTime = moment(fullDate).set({ m: totalEstimatedTime })
    return (
      <ModalContainer    
        width={ width }
        height="auto"
        isOpen={ isModalPayOpened }
        closeModal={ this.closeModal }>
        <StyledModalHeader>
          <StyledModalHeaderTitle>
            { willShow === 'confirmPay' ? 'Confirm Payment' : undefined }
            { willShow === 'appointmentScheduled' ? 'Appointment Scheduled' : undefined }
          </StyledModalHeaderTitle>
          <StyledModalHeaderCloseButton onClick={ this.closeModal }>
            <Icon 
              fill="#ccc"
              width="16"
              name="cancel"
              height="16" />
          </StyledModalHeaderCloseButton>
        </StyledModalHeader>
        <StyledModalContent>
          { willShow === 'confirmPay' ?
            <ConfirmPay 
              date={ date }
              time={ time }
              onSubmit={ this.onSubmit }
              location={ address }
              totalPrice={ totalPrice }
              bundlesSelected={ bundlesSelected }
              servicesSelected={ servicesSelected }
              providerFullName={ fullName } />
            : undefined }
          { willShow === 'appointmentScheduled' ? 
            <AppointmentScheduled
              day={ moment(dateSelected).format('DD') }
              date={ {
                title: `Appointment with ${ fullName }`,
                description: `${ bundles } ${ services }`,
                location: address,
                startTime: fullDate,
                endTime: fullDateWithEstimatedTime
              } }
              month={ moment(dateSelected).format('MMMM') } />
            : undefined }
        </StyledModalContent>
      </ModalContainer>
    )
  }
}

ModalPay.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  date: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  provider: PropTypes.object,
  creditCard: PropTypes.shape({
    cardName: PropTypes.string,
    lastFourDigits: PropTypes.string,
    expirationDate: PropTypes.string
  }).isRequired,
  totalPrice: PropTypes.number.isRequired,
  dateSelected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
    PropTypes.object
  ]).isRequired,
  closeModalPay: PropTypes.func.isRequired,
  bundlesSelected: PropTypes.array.isRequired,
  time24HoursClock: PropTypes.string.isRequired,
  servicesSelected: PropTypes.array.isRequired,
  isModalPayOpened: PropTypes.bool,
  totalEstimatedTime: PropTypes.number
}

export default ModalPay