import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledBookButton = styled.button`
  background-color: ${ colors.successButton };
  border-radius: 2px;
  color: white;
  display: inline-block;
  font-size: .9rem;
  margin-bottom: 4px;
  text-decoration: none;
  padding: 2px 6px;
  outline: none;
`

export default StyledBookButton