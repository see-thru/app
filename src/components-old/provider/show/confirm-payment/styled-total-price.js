import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../utils/settings'

const StyledTotalPrice = styled.span`
  background-color: white;
  border-radius: 2px;
  color: ${ colors.bgMapDescription };
  font-weight: ${ fonts.weightBold };
  right: 15px;
  top: 12px;
  padding: 2px 6px;
  position: absolute;
`

export default StyledTotalPrice