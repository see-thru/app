import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledConfirmPayment = styled.div`
  background-color: ${ colors.bgMapDescription };
  color: white;
  cursor: pointer;
  font-size: .9rem;
  min-height: 50px;
  padding: 15px 100px 15px 15px;
  position: relative;
`

export default StyledConfirmPayment