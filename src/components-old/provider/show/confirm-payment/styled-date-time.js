import styled from 'styled-components'

const StyledDateTime = styled.span`
  display: block;
  margin-left: 4px;
`

export default StyledDateTime