import React from 'react'
import PropTypes from 'prop-types'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../../../generic/modal/index'
import Icon from '../../../generic/icon/base'
import StyledParagraph from './styled-paragraph'

const ModalCallUs = ({
  closeModalCallUs,
  isModalCallUsOpened
}) => (
  <ModalContainer    
    width="600px"
    height="auto"
    isOpen={ isModalCallUsOpened }
    closeModal={ closeModalCallUs }>
    <StyledModalHeader>
      <StyledModalHeaderTitle>Complete the Booking</StyledModalHeaderTitle>
      <StyledModalHeaderCloseButton onClick={ closeModalCallUs }>
        <Icon 
          fill="#ccc"
          width="16"
          name="cancel"
          height="16" />
      </StyledModalHeaderCloseButton>
    </StyledModalHeader>
    <StyledModalContent>
      <StyledParagraph>
        Call us at ** <a href="tel:19147196113">+1 (914) 719-6113</a> ** to book this appointment
      </StyledParagraph>
    </StyledModalContent>
  </ModalContainer>
)

ModalCallUs.propTypes = {
  closeModalCallUs: PropTypes.func.isRequired,
  isModalCallUsOpened: PropTypes.bool
}

export default ModalCallUs