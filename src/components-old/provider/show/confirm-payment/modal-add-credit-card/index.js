import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../../../../generic/modal/index'
import Icon from '../../../../generic/icon/base'
import StyledErrorMessage from '../../../../user/styled-error-message'
import generateDataForCreditCard from '../../../../../utils/generator-data-structure/credit-card'
import StripeAddCreditCardContainer from '../../../../generic/stripe/add-credit-card/container'

class ModalAddCreditCard extends Component {
  onSubmit = ( token, last4, name, expMonth, expYear ) => {
    const { 
      user,
      saveCreditCard,
      openModalCreditCardList
    } = this.props
    const data = generateDataForCreditCard(token, last4, name, expMonth, expYear)
    saveCreditCard(data, user.token, undefined, openModalCreditCardList)
  }

  render () {
    const {
      errorPostBilling,
      closeModalAddCreditCard,
      isModalAddCreditCardOpened
    } = this.props
    return (
      <ModalContainer    
        width="600px"
        height="auto"
        isOpen={ isModalAddCreditCardOpened }
        closeModal={ closeModalAddCreditCard }>
        <StyledModalHeader>
          <StyledModalHeaderTitle>Add Credit Card</StyledModalHeaderTitle>
          <StyledModalHeaderCloseButton onClick={ closeModalAddCreditCard }>
            <Icon 
              fill="#ccc"
              width="16"
              name="cancel"
              height="16" />
          </StyledModalHeaderCloseButton>
        </StyledModalHeader>
        <StyledModalContent>
          { errorPostBilling ? <StyledErrorMessage>{ errorPostBilling }</StyledErrorMessage> : undefined }
          <StripeAddCreditCardContainer onSubmit={ this.onSubmit } />
        </StyledModalContent>
      </ModalContainer>
    )
  }
}

ModalAddCreditCard.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  errorPostBilling: PropTypes.string,
  saveCreditCard: PropTypes.func.isRequired,
  closeModalAddCreditCard: PropTypes.func.isRequired,
  openModalCreditCardList: PropTypes.func.isRequired,
  isModalAddCreditCardOpened: PropTypes.bool,
}

export default ModalAddCreditCard