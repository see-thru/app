import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledAddCreditCardLink = styled.button`
  background-color: ${ colors.successButton };
  border-radius: 2px;
  color: white;
  display: inline-block;
  margin-bottom: 4px;
  text-decoration: none;
  padding: 2px 6px;
`

export default StyledAddCreditCardLink