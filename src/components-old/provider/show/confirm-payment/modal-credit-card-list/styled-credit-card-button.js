import styled from 'styled-components'
import { colors } from '../../../../../utils/settings';

const StyledCreditCardButton = styled.button`
  background-color: ${ colors.lightBorder };
  border-radius: 2px;
  color: ${ colors.textLight };
  display: block;
  font-size: .9rem;
  margin: 10px 0;
  text-align: left;
  transition: background-color .3s;
  padding: 10px;
  width: 100%;
  &:hover {
    background-color: ${ colors.border };
  }
`

export default StyledCreditCardButton