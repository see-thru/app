import React from 'react'
import PropTypes from 'prop-types'

import {
  ModalContainer,
  StyledModalHeader,
  StyledModalContent,
  StyledModalHeaderTitle,
  StyledModalHeaderCloseButton
} from '../../../../generic/modal/index'
import Icon from '../../../../generic/icon/base'
import StyledParagraph from '../styled-paragraph'
import StyledCreditCardButton from './styled-credit-card-button'
import StyledAddCreditCardLink from '../styled-add-credit-card-link'
import StyledWrapperCreditCardIcon from './styled-credit-card-icon'

const ModalCreditCardList = ({
  data,
  selectACreditCard,
  openModalAddCreditCard,
  closeModalCreditCardList,
  isModalCreditCardListOpened
}) => (
  <ModalContainer    
    width="600px"
    height="auto"
    isOpen={ isModalCreditCardListOpened }
    closeModal={ closeModalCreditCardList }>
    <StyledModalHeader>
      <StyledModalHeaderTitle>Your Credit Card List</StyledModalHeaderTitle>
      <StyledModalHeaderCloseButton onClick={ closeModalCreditCardList }>
        <Icon 
          fill="#ccc"
          width="16"
          name="cancel"
          height="16" />
      </StyledModalHeaderCloseButton>
    </StyledModalHeader>
    <StyledModalContent>
      <StyledParagraph>Select one:</StyledParagraph>
      { data && data.cards && data.cards.map( (card, index) => {
        return (
          <StyledCreditCardButton
            key={ index } 
            onClick={ selectACreditCard.bind(this, card) }>
            <StyledWrapperCreditCardIcon>
              <Icon 
                name="credit-card" 
                width="30" 
                height="20" />
            </StyledWrapperCreditCardIcon>
            Ending in { card.lastFourDigits }
          </StyledCreditCardButton>
        )
      }) }
      <StyledAddCreditCardLink onClick={ openModalAddCreditCard }>
        Add Credit Card
      </StyledAddCreditCardLink>
    </StyledModalContent>
  </ModalContainer>
)

ModalCreditCardList.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  data: PropTypes.shape({
    cards: PropTypes.array.isRequired
  }),
  selectACreditCard: PropTypes.func.isRequired,
  openModalAddCreditCard: PropTypes.func.isRequired,
  closeModalCreditCardList: PropTypes.func.isRequired,
  isModalCreditCardListOpened: PropTypes.bool
}

export default ModalCreditCardList