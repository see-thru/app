import styled from 'styled-components'

const StyledWrapperCreditCardIcon = styled.span`
  margin-right: 5px;
  vertical-align: sub;
`

export default StyledWrapperCreditCardIcon