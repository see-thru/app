import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledParagraph = styled.p`
  color: ${ colors.text }
  a {
    color: ${ colors.link };
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`

export default StyledParagraph