import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { 
  PayWithCard,
  saveCreditCard,
  getCreditCardList
} from '../../../../actions-old/billing'

import ModalPay from './modal-pay/'
import StyledDateTime from './styled-date-time'
import StyledBookButton from './styled-book-button'
import StyledTotalPrice from './styled-total-price'
import ModalAddCreditCard from './modal-add-credit-card/'
import ModalCreditCardList from './modal-credit-card-list/'
import StyledConfirmPayment from './styled'
import GetAmountAvailablesDays from '../../../../utils/get-amount-availables-days'

export class ConfirmPayment extends Component {
  constructor () {
    super()
    this.state = ({ 
      creditCard: {},
      isModalPayOpened: false,
      isModalAddCreditCardOpened: false,
      isModalCreditCardListOpened: false
    })
  }

  componentWillMount () {
    const { 
      user,
      getCreditCardList 
    } = this.props 
    if ( user && user.token ) getCreditCardList(user.token)
  }

  componentWillReceiveProps ( nextProps ) {
    const {
      user, 
      getCreditCardList,
      successMessagePostBilling
    } = nextProps
    if (successMessagePostBilling !== this.props.successMessagePostBilling ) {
      if ( user && user.token ) getCreditCardList(user.token)
    }
  }

  openModalPay = () => {
    this.setState({ isModalPayOpened: true })
  }

  openModalAddCreditCard = () => {
    this.closeModal()
    this.setState({ isModalAddCreditCardOpened: true })
  }

  openModalCreditCardList = () => {
    this.setState({ 
      isModalAddCreditCardOpened: false,
      isModalCreditCardListOpened: true 
    })
  }

  closeModal = () => {
    this.setState({ 
      isModalPayOpened: false,
      isModalAddCreditCardOpened: false,
      isModalCreditCardListOpened: false
    })
  }  

  selectACreditCard = creditCard => {
    this.setState({ creditCard })
    this.closeModal()
    this.openModalPay()
  }

  displayOnClick = () => {
    const { 
      isLogged,
      totalPrice,
      hasCreditCard,
      toggleSignInModal
    } = this.props
    let onClick
    if ( hasCreditCard ) {
      onClick = this.openModalCreditCardList
    } else {
      onClick = this.openModalAddCreditCard
    }
    if ( totalPrice === 0 ) {
      onClick = () => console.log('there are not selected services')
    }
    if ( ! isLogged ) onClick = toggleSignInModal.bind(this, true)
    return onClick
  }

  render () {
    const { 
      creditCard,
      isModalPayOpened,
      isModalAddCreditCardOpened,
      isModalCreditCardListOpened
    } = this.state
    const {
      user,
      date,
      time,
      data,
      ccNumber,
      provider,
      isLogged,
      totalPrice,
      PayWithCard,
      dateSelected,
      hasCreditCard,
      availabilities,
      saveCreditCard,
      bundlesSelected,
      errorPostBilling,
      time24HoursClock,
      servicesSelected,
      providerIdDisplay,
      totalEstimatedTime
    } = this.props
    const thereAreBundlesOrServicesSelected = bundlesSelected.length >= 1 || servicesSelected.length >= 1 ? true : false 
    return (
      <Fragment>
        <StyledConfirmPayment onClick={ GetAmountAvailablesDays(availabilities) !== 0 ? this.displayOnClick() : () => console.log('there are not available dates') }>
          { ! thereAreBundlesOrServicesSelected ?
            <StyledBookButton>
              You must select at least one service
            </StyledBookButton> 
            : undefined
          }
          { isLogged && hasCreditCard ? 
            <StyledBookButton>
              Card ending in { ccNumber }
            </StyledBookButton> 
            : '' 
          }
          <StyledDateTime>{ `${ date } ${ time }` }</StyledDateTime>
          { totalEstimatedTime > 0 ? 
            <StyledDateTime>
              Estimated Visit Time { totalEstimatedTime } min
            </StyledDateTime>
            : undefined
          }
          <StyledTotalPrice>{ `$${ totalPrice }` }</StyledTotalPrice>
        </StyledConfirmPayment>
        <ModalAddCreditCard 
          user={ user }
          saveCreditCard={ saveCreditCard }
          errorPostBilling={ errorPostBilling }
          openModalCreditCardList={ this.openModalCreditCardList }
          closeModalAddCreditCard={ this.closeModal }
          isModalAddCreditCardOpened={ isModalAddCreditCardOpened } />
        <ModalCreditCardList 
          data={ data }
          selectACreditCard={ this.selectACreditCard }
          openModalAddCreditCard={ this.openModalAddCreditCard }
          closeModalCreditCardList={ this.closeModal }
          isModalCreditCardListOpened={ isModalCreditCardListOpened } />   
        <ModalPay
          user={ user }
          date={ date }
          time={ time }
          provider={ provider }
          creditCard={ creditCard }
          totalPrice={ totalPrice }
          PayWithCard={ PayWithCard }
          dateSelected={ dateSelected }
          closeModalPay={ this.closeModal }
          bundlesSelected={ bundlesSelected }
          servicesSelected={ servicesSelected }
          isModalPayOpened={ isModalPayOpened }
          time24HoursClock={ time24HoursClock }
          providerIdDisplay={ providerIdDisplay }
          totalEstimatedTime={ totalEstimatedTime } />                    
      </Fragment>
    )
  }
}

ConfirmPayment.propTypes = {
  user: PropTypes.shape({
    token: PropTypes.string.isRequired
  }),
  data: PropTypes.shape({
    cards: PropTypes.array.isRequired
  }),
  date: PropTypes.string,
  time: PropTypes.string,
  ccNumber: PropTypes.string,
  isLogged: PropTypes.bool,
  provider: PropTypes.object.isRequired,
  totalPrice: PropTypes.number,
  PayWithCard: PropTypes.func.isRequired,
  dateSelected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
    PropTypes.object
  ]).isRequired,
  hasCreditCard: PropTypes.bool,
  availabilities: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.shape({
      weekdays: PropTypes.arrayOf(PropTypes.shape({
        slots: PropTypes.array.isRequired,
        weekday: PropTypes.string.isRequired
      }))
    })
  ]),
  saveCreditCard: PropTypes.func.isRequired,
  bundlesSelected: PropTypes.array.isRequired,
  errorPostBilling: PropTypes.string,
  time24HoursClock: PropTypes.string.isRequired,
  servicesSelected: PropTypes.array.isRequired,
  toggleSignInModal: PropTypes.func.isRequired,
  getCreditCardList: PropTypes.func.isRequired,
  providerIdDisplay: PropTypes.string.isRequired,
  totalEstimatedTime: PropTypes.number.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  data: state.BillingReducer.data,
  errorPostBilling: state.BillingReducer.errorPostBilling,
  successMessagePostBilling: state.BillingReducer.successMessagePostBilling,
})

export default connect(
  mapStateToProps, 
  { 
    PayWithCard,
    saveCreditCard,
    getCreditCardList
  }
)( ConfirmPayment )