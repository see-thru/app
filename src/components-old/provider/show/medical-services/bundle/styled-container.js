import styled from 'styled-components'

import { StyledContainerGrid } from '../../../../generic/grid'

const StyledBundleContainerGrid = styled(StyledContainerGrid)`
  padding-left: 0;
  padding-right: 0;
`

export default StyledBundleContainerGrid