import React from 'react'
import PropTypes from 'prop-types'

import StyledName from '../../../../user/edit-healthcare-services/bundles/bundle/styled-name'
import StyledList from './styled-list'
import StyledCost from '../../../../user/edit-healthcare-services/bundles/bundle/styled-cost'
import StyledTitle from '../styled-title'
import StyledHeader from '../../../../user/edit-healthcare-services/bundles/bundle/styled-header'
import StyledSelectedLabel from '../styled-selected-label'
import StyledEstimatedTime from '../../../../user/edit-healthcare-services/bundles/bundle/styled-estimated-time'
import StyledSelectedWrapper from '../styled-selected-wrapper'
import StyledBundleContainerGrid from './styled-container'

const Bundle = ({ 
  bundle,
  isSelected,
  selectServicesOrBundles 
}) => {
  const bundleIsSelected = isSelected('bundle', bundle)
  return (
    <StyledSelectedWrapper 
      onClick={ selectServicesOrBundles.bind(this, 'bundle', bundle) }
      isSelected={ bundleIsSelected ? 'true' : undefined }>
      <StyledName>{ bundle.bundle }</StyledName>
      <StyledHeader>
        <StyledEstimatedTime>Estimated Time: <span>{ bundle.time } minutes</span></StyledEstimatedTime>
        <StyledCost>Cost: <span>${ bundle.cost }</span></StyledCost>
        { bundleIsSelected ? 
          <StyledSelectedLabel>Selected</StyledSelectedLabel>
          : undefined
        }
      </StyledHeader>
      <StyledTitle>Included In Visit</StyledTitle>
      <StyledBundleContainerGrid
        gridGap="20px 10px"
        templateRows="auto"
        templateColumns="repeat(2, 1fr)">
        <div>
          <StyledList>
            { bundle.services.map( (service, index) => {
              return (
                <li key={ index }>
                  { service }
                </li>
              )
            }) }
          </StyledList>
        </div>
      </StyledBundleContainerGrid>
    </StyledSelectedWrapper>
  )
}


Bundle.defaultProps = {
  bundle: {
    services: []
  }
}

Bundle.propTypes = {
  bundle: PropTypes.shape({
    cost: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    time: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    bundle: PropTypes.string,
    services: PropTypes.array
  }).isRequired,
  isSelected: PropTypes.func.isRequired,
  selectServicesOrBundles: PropTypes.func.isRequired
}

export default Bundle