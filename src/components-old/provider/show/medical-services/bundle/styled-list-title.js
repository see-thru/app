import styled from 'styled-components'

import { fonts } from '../../../../../utils/settings'

const StyledListTitle = styled.li`
  font-weight: ${ fonts.weightBold };
  margin-bottom: 6px;
  text-transform: uppercase;
`

export default StyledListTitle