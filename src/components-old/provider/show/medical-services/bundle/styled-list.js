import styled from 'styled-components'
import PropTypes from 'prop-types'

import { colors } from '../../../../../utils/settings'

const StyledList = styled.ul`
  color: ${ colors.textDate };
  font-size: .9rem;
  list-style: none;
  margin: 0 0 20px;
  margin-left: ${ props => props.textAlign === 'centered' ? '20px' : '0' };
  padding: 0 5px;
  ${ props => props.textAlign === 'centered' ? `
    &:last-of-type {
      margin-top: 30px;
    }
  ` : undefined }
  li {
    margin: 10px 0;
  }
`

StyledList.propTypes = {
  textAlign: PropTypes.string
}

export default StyledList