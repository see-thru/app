import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledTitle = styled.h3`
  color: ${ colors.textColorTitleQualifications };
  font-size: .9rem;
  font-weight: 400;
  margin: 10px 0;
  text-transform: uppercase;
`

export default StyledTitle