import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledMedicalServicesContainer = styled.div`
  margin: 20px 0;
  max-height: 400px;
  overflow-y: scroll;
  overflow-x: hidden;
  padding: 10px;
  ${
    Media.medium`
      max-height: initial;
      overflow: hidden;
      padding: 0;
    `
  }
`

export default StyledMedicalServicesContainer