import React from 'react'
import PropTypes from 'prop-types'

import Bundle from './bundle/base'
import Service from './service/base'
import StyledMedicalServicesContainer from './styled'

const MedicalServicesContainer = ({
  isSelected,
  servicesAndBundles,
  selectServicesOrBundles 
}) => (
  <StyledMedicalServicesContainer>
    { servicesAndBundles.bundles.map( (bundle, index) => {
      return (
        <Bundle
          key={ index }
          bundle={ bundle }
          isSelected={ isSelected }
          selectServicesOrBundles={ selectServicesOrBundles } />
      )
    }) }
    { servicesAndBundles.services.map( (service, index) => {
      return (
        <Service 
          key={ index }
          service={ service }
          isSelected={ isSelected }
          selectServicesOrBundles={ selectServicesOrBundles } />
      )
    }) }
  </StyledMedicalServicesContainer>
)

MedicalServicesContainer.defaultProps = {
  servicesAndBundles: {
    bundles: [],
    services: []
  }
}

MedicalServicesContainer.propTypes = {
  servicesAndBundles: PropTypes.shape({
    bundles: PropTypes.array,
    services: PropTypes.array
  }).isRequired,
  isSelected: PropTypes.func.isRequired,
  selectServicesOrBundles: PropTypes.func.isRequired
}

export default MedicalServicesContainer