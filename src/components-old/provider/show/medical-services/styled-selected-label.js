import styled from 'styled-components'

import { 
  fonts,
  colors 
} from '../../../../utils/settings'

const StyledSelectedLabel = styled.span`
  background-color: ${ colors.link };
  border-radius: 2px;
  color: white !important;
  font-size: .8rem;
  font-weight: ${ fonts.weightBold };
  padding: 4px 10px;
`

export default StyledSelectedLabel