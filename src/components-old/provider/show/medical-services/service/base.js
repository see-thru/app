import React from 'react'
import PropTypes from 'prop-types'

import StyledName from '../../../../user/edit-healthcare-services/bundles/bundle/styled-name'
import StyledCost from '../../../../user/edit-healthcare-services/bundles/bundle/styled-cost'
import StyledHeader from '../../../../user/edit-healthcare-services/bundles/bundle/styled-header'
import StyledSelectedLabel from '../styled-selected-label'
import StyledEstimatedTime from '../../../../user/edit-healthcare-services/bundles/bundle/styled-estimated-time'
import StyledSelectedWrapper from '../styled-selected-wrapper'

const Service = ({ 
  service,
  isSelected,
  selectServicesOrBundles 
}) => {
  const serviceIsSelected = isSelected('service', service)
  return (
    <StyledSelectedWrapper 
      onClick={ selectServicesOrBundles.bind(this, 'service', service) }
      isSelected={ serviceIsSelected ? 'true' : undefined }>  
      <StyledName>{ service.name }</StyledName>
      <StyledHeader>
        <StyledEstimatedTime>Estimated Time: <span>{ service.time } minutes</span></StyledEstimatedTime>
        <StyledCost>Cost: <span>${ service.cost }</span></StyledCost>
        { serviceIsSelected ? 
          <StyledSelectedLabel>Selected</StyledSelectedLabel>
          : undefined
        }
      </StyledHeader>  
    </StyledSelectedWrapper>
  )
}

Service.propTypes = {
  service: PropTypes.shape({
    name: PropTypes.string,
    cost: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    time: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])
  }).isRequired,
  isSelected: PropTypes.func.isRequired,
  selectServicesOrBundles: PropTypes.func.isRequired
}

export default Service