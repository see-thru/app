import styled from 'styled-components'

const StyledSelectedWrapper = styled.div`
  cursor: pointer;
  ${ props => props.isSelected === 'true' ? `
    background-color: #ddeeff;
    margin-bottom: 10px;
    padding: 14px;
    & > div {
      margin-bottom: 10px;
    }
    ` : undefined }
`

export default StyledSelectedWrapper