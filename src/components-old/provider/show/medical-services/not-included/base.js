import React from 'react'

import StyledName from '../../../../user/edit-healthcare-services/bundles/bundle/styled-name'
import StyledCost from '../../../../user/edit-healthcare-services/bundles/bundle/styled-cost'
import StyledList from '../bundle/styled-list'
import StyledTitle from '../styled-title'
import StyledHeader from '../../../../user/edit-healthcare-services/bundles/bundle/styled-header'
import StyledEstimatedTime from '../../../../user/edit-healthcare-services/bundles/bundle/styled-estimated-time'

const NotIncluded = () => (
  <div>
    <StyledTitle>Not Included</StyledTitle>    
    <StyledName>Allergy Check</StyledName>
    <StyledHeader>
      <StyledEstimatedTime>Estimated Time: <span>30 minutes</span></StyledEstimatedTime>
      <StyledCost>Cost: <span>$120</span></StyledCost>
    </StyledHeader>    
    <StyledList>
      <li>Diagnosis</li>
      <li>Skin allergy test</li>
      <li>Blood exam: blood type</li>
    </StyledList>
  </div>
)

export default NotIncluded