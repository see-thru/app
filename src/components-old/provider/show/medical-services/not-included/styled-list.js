import styled from 'styled-components'
import { colors } from '../../../../../utils/settings';

const StyledList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0 5px;
  li {
    border-bottom: 1px solid ${ colors.border };
    margin: 10px 0;
    span {
      background-color: white;
      color: ${ colors.textDate };
      font-size: .8rem;
      padding: 6px 4px;
      &:last-of-type {
        bottom: 3px;
        float: right;
        position: relative;
      }
    }
  }
`

export default StyledList