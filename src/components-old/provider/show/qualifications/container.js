import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Qualifications from './base'

class QualificationsContainer extends Component {
  render () {
    return (
      <Qualifications { ...this.props } />
    )
  }
}

QualificationsContainer.defaultProps = {
  provider: {}
}

QualificationsContainer.propTypes = {
  provider: PropTypes.object.isRequired
}

export default QualificationsContainer