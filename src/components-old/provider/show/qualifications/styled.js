import styled from 'styled-components'

const StyledQualifications = styled.div`
  margin: 10px 0;
  padding: 10px 0;
`

export default StyledQualifications