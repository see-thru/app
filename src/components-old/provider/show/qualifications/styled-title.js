import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledTitle = styled.h3`
  color: ${ colors.textTitle };
  font-size: 1rem;
  font-weight: 400;
  margin: 0 0 5px;
  padding: 0 5px 5px;
`

export default StyledTitle