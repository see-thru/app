import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../generic/icon/base'
import StyledItem from './styled-item'
import StyledItemTitle from './styled-item-title'
import StyledItemContent from './styled-item-content'

class ItemMedicalTraining extends Component {
  constructor () {
    super()
    this.state = ({ isOpen: false })
  }

  toggleItem = () => {
    const { isOpen } = this.state
    this.setState({ isOpen: !isOpen })
  }

  render () {
    const { isOpen } = this.state
    const { provider } = this.props
    return (
      <StyledItem>
        <StyledItemTitle 
          onClick={ this.toggleItem }
          isActivated={ isOpen ? 'true' : 'false' }>
          Medical Training
          <Icon 
            name="qualifications-close" 
            width="8" 
            height="8" />
        </StyledItemTitle>
        { isOpen ? 
          <Fragment>  
            { provider.medicalTraining.fellowship ? 
              <StyledItemContent>
                Fellowship: { provider.medicalTraining.fellowship.name } - Year: { provider.medicalTraining.fellowship.year }
              </StyledItemContent>                 
              : undefined }
            { provider.medicalTraining.medicalSchool ? 
              <StyledItemContent>
                Medical School: { provider.medicalTraining.medicalSchool.name } - Year: { provider.medicalTraining.medicalSchool.year }
              </StyledItemContent>                
              : undefined }
            { provider.medicalTraining.residency ? 
              <StyledItemContent>
                Residency: { provider.medicalTraining.residency.name } - Year: { provider.medicalTraining.residency.year }
              </StyledItemContent>                
              : undefined }
          </Fragment>
          : undefined }
      </StyledItem> 
    )
  }
}

ItemMedicalTraining.propTypes = {
  provider: PropTypes.shape({
    medicalTraining: PropTypes.shape({
      fellowship: PropTypes.shape({
        name: PropTypes.string.isRequired,
        year: PropTypes.number.isRequired
      }),
      medicalSchool: PropTypes.shape({
        name: PropTypes.string.isRequired,
        year: PropTypes.number.isRequired
      }),
      residency: PropTypes.shape({
        name: PropTypes.string.isRequired,
        year: PropTypes.number.isRequired
      }),
    })
  }).isRequired
}

export default ItemMedicalTraining