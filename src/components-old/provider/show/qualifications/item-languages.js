import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Icon from '../../../generic/icon/base'
import StyledItem from './styled-item'
import StyledItemTitle from './styled-item-title'
import StyledItemContent from './styled-item-content'

class ItemLanguages extends Component {
  constructor () {
    super()
    this.state = ({ isOpen: false })
  }

  toggleItem = () => {
    const { isOpen } = this.state
    this.setState({ isOpen: !isOpen })
  }

  render () {
    const { isOpen } = this.state
    const { provider } = this.props
    return (
      <StyledItem>
        <StyledItemTitle 
          onClick={ this.toggleItem }
          isActivated={ isOpen ? 'true' : 'false' }>
          Languages
          <Icon 
            name="qualifications-close" 
            width="8" 
            height="8" />
        </StyledItemTitle>
        { isOpen ? 
          <StyledItemContent>
            { provider.languages.map( (language, index) => {
              return (
                <span key={ index }>
                  { `${ language } ` }
                </span>
              )
            }) }
          </StyledItemContent> 
          : undefined }
      </StyledItem> 
    )
  }
}

ItemLanguages.propTypes = {
  provider: PropTypes.shape({
    languages: PropTypes.array
  }).isRequired
}

export default ItemLanguages