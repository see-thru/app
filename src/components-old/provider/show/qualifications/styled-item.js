import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledItem = styled.div`
  border-bottom: 1px solid ${ colors.border };
  margin: 10px;
  &:last-of-type {
    border-bottom: 0;
    margin-bottom: 0;
  }
`

export default StyledItem