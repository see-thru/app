import styled from 'styled-components'
import PropTypes from 'prop-types'

import { colors } from '../../../../utils/settings'

const StyledItemTitle = styled.h4`
  color: ${ props => props.isActivated === 'true' ? colors.textColorTitleQualifications : colors.textTitle };
  border-bottom: 1px solid ${ colors.border };
  cursor: pointer;
  font-size: .8rem;
  font-weight: 400;
  margin: 0;
  padding: 10px;
  text-transform: uppercase;
  svg {
    float: right;
    position: relative;
    transform: ${ props => props.isActivated === 'true' ? 'rotate(0)' : 'rotate(44deg)' };
    transition: .3s transform;
    top: 8px;
  }
`

StyledItemTitle.propTypes = {
  isActivated: PropTypes.string
}

export default StyledItemTitle