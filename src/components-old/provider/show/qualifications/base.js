import React from 'react'
import PropTypes from 'prop-types'

import StyledTitle from './styled-title'
import StyledQualifications from './styled'

import ItemBio from './item-bio'
import ItemDegree from './item-degree'
import ItemLanguages from './item-languages'
import ItemMedicalTraining from './item-medical-training'

const Qualifications = ({
  provider
}) => {

  // This snippet need to refactor
  let areThereMedicalTraining = false
  if ( provider.medicalTraining ) {
    const { medicalTraining } = provider
    if ( medicalTraining.fellowship && Object.keys(medicalTraining.fellowship).length >= 1 ) {
      areThereMedicalTraining = true
    } 
    if ( medicalTraining.medicalSchool && Object.keys(medicalTraining.medicalSchool).length >= 1 ) {
      areThereMedicalTraining = true
    } 
    if ( medicalTraining.residency && Object.keys(medicalTraining.residency).length >= 1 ) {
      areThereMedicalTraining = true
    }
  }

  return (
    <StyledQualifications>
      <StyledTitle>Qualifications & Experience</StyledTitle>
      { provider.bio ? <ItemBio provider={ provider } /> : undefined }
      { provider.degree && provider.degree.length >= 1 ? <ItemDegree provider={ provider } /> : undefined }
      { provider.languages && provider.languages.length >= 1 ? <ItemLanguages provider={ provider } /> : undefined }
      { areThereMedicalTraining ? <ItemMedicalTraining provider={ provider } /> : undefined }            
    </StyledQualifications>
  )
}

Qualifications.defaultProps = {
  provider: {
    degree: [],
    languages: []
  }
}

Qualifications.propTypes = {
  provider: PropTypes.shape({
    bio: PropTypes.string,
    degree: PropTypes.array,
    languages: PropTypes.array,
    medicalTraining: PropTypes.shape({
      fellowship: PropTypes.shape({
        name: PropTypes.string,
        year: PropTypes.number
      }),
      medicalSchool: PropTypes.shape({
        name: PropTypes.string,
        year: PropTypes.number
      }),
      residency: PropTypes.shape({
        name: PropTypes.string,
        year: PropTypes.number
      })      
    })
  }).isRequired
}

export default Qualifications