import styled from 'styled-components'
import { colors } from '../../../../utils/settings';

const StyledItemContent = styled.p`
  color: ${ colors.textColorDescriptionQualifications };
  font-size: .9rem;
  margin: 0;
  padding: 10px;
` 

export default StyledItemContent