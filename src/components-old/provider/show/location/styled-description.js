import styled from 'styled-components'

import { colors } from '../../../../utils/settings'

const StyledDescription = styled.div`
  background-color: ${ colors.bgMapDescription };
  border-radius: 2px;
  color: white;
  font-size: .8rem;
  margin-bottom: 5px;
  padding: 10px;
  & > span {
    display: inline-block;
    margin: 0 1%;
    vertical-align: top;
    width: 48%;
    &:last-of-type {
      text-align: right;
    }
  }
`

export default StyledDescription