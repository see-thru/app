import React from 'react'
import PropTypes from 'prop-types'

import MapContainer from '../../../search/map/container'
import StyledLocation from './styled'
import StyledDescription from './styled-description'

const Location = ({
  address
}) => (
  <StyledLocation>
    <StyledDescription>
      <span>{ address }</span>
    </StyledDescription>
    <MapContainer 
      height="400px"
      fromProviderPage />
  </StyledLocation>
)

Location.propTypes = {
  address: PropTypes.string
}

export default Location