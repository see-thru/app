import styled from 'styled-components'

const StyledLocation = styled.div`
  position: relative;
  & > div:last-of-type {
    margin-top: 0;
  }
`

export default StyledLocation