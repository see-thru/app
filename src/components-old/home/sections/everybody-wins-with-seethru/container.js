import React from 'react'

import Header from '../header/base'
import StyledContainerGridBanner from '../banner/styled-container'
import StyledEverybodyWinsWithSeeThru from './styled'

const EverybodyWinsWithSeeThru = () => (
  <StyledEverybodyWinsWithSeeThru>
    <Header title="Everybody Wins With Seethru"/>
    <StyledContainerGridBanner
      gridGap="0 20px"
      templateRows="auto"
      templateColumns="repeat(2, 1fr)">
      <div>
        <h4>Meet Joe: The Patient Story</h4>
        <ul>
          <li>Avoid Wait Times</li>
          <li>Know what you’re gonna pay</li>
          <li>Get care today</li>
        </ul>
        <video 
          poster="/images/home/video-placeholder1.jpg"
          controls>
          <source src="/videos/seethru-joe.mp4" />
        </video>
      </div>
      <div>
        <h4>Meet Dr. Jill: The Provider Story</h4>
        <ul>
          <li>Happy patients</li>
          <li>Optimize your schedule</li>
          <li>Avoid billing hassles</li>
        </ul>
        <video 
          poster="/images/home/video-placeholder2.jpg"
          controls>
          <source src="/videos/jills-animated-story-v3.mp4" />
        </video>
      </div>
    </StyledContainerGridBanner>
  </StyledEverybodyWinsWithSeeThru>
)

export default EverybodyWinsWithSeeThru