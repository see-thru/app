import styled from 'styled-components'

import Media from '../../../../utils/media'
import { fonts, colors } from '../../../../utils/settings'

const StyledEverybodyWinsWithSeeThru = styled.div`
  video {
    ${
      Media.small`
        margin-bottom: 20px;
      `
    }
  }
  h4 {
    color: ${ colors.textLight };
    font-size: 1.6rem;
    font-weight: ${ fonts.weightBold };
    margin: 0 0 10px;
    text-align: center;
    ${
      Media.medium`
        font-size: 1rem;
      `
    }
  }
  ul {
    color: ${ colors.textLight };
    font-size: 1.2rem;
    list-style-position: inside;
    margin: 20px 0;
    text-align: center;
    padding: 0;
    ${
      Media.medium`
        font-size: 1rem;
      `
    }
    ${
      Media.small`
        text-align: left;
        padding: 0 10px;
      `
    }
  }
`

export default StyledEverybodyWinsWithSeeThru