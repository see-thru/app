import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledContent = styled.div`
  color: white;
  padding: 100px 10px 40px;
  ${
    Media.medium`
      padding-top: 40px;
    `
  }
  img {
    margin: auto;
    max-width: 500px;
    top: 50px;
    position: relative;
    width: 100%;
    ${
      Media.big`
        max-width: 400px;
        top: 90px;
      `
    }
    ${
      Media.medium`
        top: 90px;
      `
    }
    ${
      Media.small`
        max-width: 280px;
        top: 50px;
      `
    }
  }
`

export default StyledContent