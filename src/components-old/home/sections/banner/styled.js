import styled from 'styled-components'
import Media from '../../../../utils/media';

const StyledBanner = styled.div`
  background: url('/images/home/hero-bg.png') bottom no-repeat #00adee;
  background-size: cover;
  border-bottom: 25px solid #304658;
  position: relative;
  ${
    Media.small`
      background-image: url('/images/home/hero-bg-mobile.png');
    `
  }
`

export default StyledBanner