import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import SearchForm from './search/container'
import StyledTitle from './styled-title'
import StyledBanner from './styled'
import StyledContent from './styled-content'
import StyledSubtitle from './styled-subtitle'
import StyledContainerGridBanner from './styled-container'

export const Banner = ({ 
  history
}) => (
  <StyledBanner>
    <StyledContent>
      <StyledContainerGridBanner
        gridGap="0 20px"
        templateRows="auto"
        templateColumns="1fr 2fr">
        <div>
          <StyledTitle>Transparent <br />Healthcare</StyledTitle>
          <StyledSubtitle>Not Feeling Well? Need a Doctor? <br />Find guaranteed quality and prices for your care.</StyledSubtitle>
          <SearchForm history={ history } />
        </div>
        <img src="/images/home/hero-graphic2.png" alt="" />
      </StyledContainerGridBanner>
    </StyledContent>
  </StyledBanner>
)

Banner.propTypes = {
  history: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({})	

export default connect(	
  mapStateToProps,	
  {}	
)( Banner ) 