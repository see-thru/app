import styled from 'styled-components'

import Media from '../../../../../utils/media'
import { StyledDefaultButton } from '../../../../generic/button/styled'

const StyledButton = styled(StyledDefaultButton)`
  cursor: pointer;
  left: calc(5px + 100%);
  top: 11px;
  position: absolute
  ${
    Media.medium`
      bottom: 10px;
      top: auto;
    `
  }
  ${
    Media.small`
      bottom: auto;
      left: auto;
      top: auto;
      position: relative;
    `
  }
`

export default StyledButton