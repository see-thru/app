import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { 
  Field,
  reduxForm 
} from 'redux-form'
import { geolocated } from 'react-geolocated'

import Icon from '../../../../generic/icon/base'
import StyledSearch from './styled'
import StyledButton from './styled-button'
import StyledInputSearch from './styled-input-search'
import { 
  Input,
  FormWrapper,
  InputGeoSuggest
} from '../../../../generic/form/index'
import isFeatureEnabled from '../../../../../utils/permission/is-feature-enabled'

class SearchForm extends Component {
  constructor () {	
    super()	
    this.onSubmit = this.onSubmit.bind(this)
  }	

  onSubmit (e) {	
    const {	
      coords,
      history	
    } = this.props	
    const query = e.query ? e.query : ''
    const where = e.where ? e.where : null
    let lat = '', lng = '', description = ''
    if ( coords ) {
      lat = coords.latitude
      lng = coords.longitude
      description = 'my current location'
    } 
    if ( where !== null ) {
      lat = where.location.lat
      lng = where.location.lng
      description = where.description
    }
    history.push(`/search?&what=${ query }&lat=${ lat }&lng=${ lng }&description=${ description }`)	
  }

  render () {
    const baseUrl = window.location.host
    const onSubmit = this.onSubmit
    const { handleSubmit } = this.props
    return (
      <StyledSearch>
        <FormWrapper 
          onSubmit={ 
            isFeatureEnabled(baseUrl).feature.homeSearch ? 
              handleSubmit(onSubmit) : 
              handleSubmit(() => console.log('feature is disabled.')) }>
          <StyledInputSearch>
            { false &&
              <Field
                name="query"
                title=""
                hasLabel={ false }
                component={ Input }
                placeholder="What’s going on?" />
            }
            <Field
              name="where"
              title=""
              hasLabel={ false }
              component={ InputGeoSuggest }
              placeholder="Where" />
            <StyledButton type="submit">
              <Icon
                name="search"
                width="50px" 
                height="50px" />
            </StyledButton>
          </StyledInputSearch>
        </FormWrapper>
      </StyledSearch>
    )
  }
}

SearchForm.propTypes = {
  history: PropTypes.object.isRequired
}

export const Form = reduxForm({
  form : 'searchFormBanner'
})( SearchForm ) 

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(Form)