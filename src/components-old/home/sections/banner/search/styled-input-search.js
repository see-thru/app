import styled from 'styled-components'

import Media from '../../../../../utils/media'

const StyledInputSearch = styled.div`
  clear: both;
  float: left;
  position: relative;
  width: 100%;
  & > div {
    clear: none;
    display: inline-block;
    width: 40%;
    ${
      Media.medium`
        width: 100%
      `
    }
    &:first-of-type {
      margin-right: 2%;
      width: 58%;
      ${
        Media.medium`
          margin-right: 0;
          width: 100%
        `
      }      
    }
  }
`

export default StyledInputSearch