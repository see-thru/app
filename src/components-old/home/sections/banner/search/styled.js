import styled from 'styled-components'

const StyledSearch = styled.div`
  margin-left: auto;  
  margin-right: auto;
  margin-top: 20px;
  max-width: 600px;
  width: 100%;
`

export default StyledSearch