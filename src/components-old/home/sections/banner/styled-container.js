import styled from 'styled-components'

import Media from '../../../../utils/media'
import { StyledContainerGrid } from '../../../generic/grid/index'

const StyledContainerGridBanner = styled(StyledContainerGrid)`
  ${
    Media.small`
      grid-template-columns: 1fr;
    `
  }
  & > div {
    ${
      Media.small`
        text-align: center;
      `
    }
  }
`

export default StyledContainerGridBanner