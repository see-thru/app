import styled from 'styled-components'

import Media from '../../../../utils/media'

const StyledTitle = styled.h1`
  font-size: 6rem;
  line-height: 1;
  margin: 10px 0 30px;
  ${
    Media.medium`
      font-size: 3rem;
    `
  }
  ${
    Media.small`
      font-size: 3rem;
      margin-bottom: 20px;
    `
  }
`

export default StyledTitle