import styled from 'styled-components'
import Media from '../../../../utils/media';

const StyledSubtitle = styled.h2`
  font-size: 1.6rem;
  margin: 0;
  ${ 
    Media.medium`
      font-size: 1.4rem;
    `
  }
  ${ 
    Media.small`
      font-size: 1.2rem;
    `
  }  
`

export default StyledSubtitle