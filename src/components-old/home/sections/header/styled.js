import styled from 'styled-components'

const StyledHeader = styled.div`
  margin: 80px 0 60px;
  padding: 0 20px;
  text-align: center;
`

export default StyledHeader