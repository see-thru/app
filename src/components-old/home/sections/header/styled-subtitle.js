import styled from 'styled-components'

const StyledSubtitle = styled.h4`
  font-size: 1.15rem;
  font-weight: 400;
  margin: 0;
`

export default StyledSubtitle