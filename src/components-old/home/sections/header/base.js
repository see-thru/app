import React from 'react'
import PropTypes from 'prop-types'

import StyledTitle from './styled-title'
import StyledHeader from './styled'
import StyledSubtitle from './styled-subtitle'

const Header = ({
  title,
  subtitle
}) => (
  <StyledHeader>
    <StyledTitle>{ title }</StyledTitle>
    { subtitle ? 
      <StyledSubtitle>{ subtitle }</StyledSubtitle> 
      : undefined }
  </StyledHeader>
)

Header.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string
}

export default Header