import styled from 'styled-components'

import { colors } from '../../../../utils/settings'
import Media from '../../../../utils/media';

const Title = styled.h3`
  color: ${ colors.textTitle };
  font-size: 4.5rem;
  margin: 0;
  ${
    Media.medium`
      font-size: 3.2rem;
    `
  }
  ${
    Media.small`
      font-size: 2.2rem;
    `
  }  
`
export default Title