import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../../../utils/media'
import { colors } from '../../../../../utils/settings'

const StyledBox = styled.div`
  background: ${ props => props.boxName === 'Creates' ? 'url(/images/home/graphic5.png)' : undefined };
  background: ${ props => props.boxName === 'Enables' ? 'url(/images/home/graphic6.png)' : undefined };
  background: ${ props => props.boxName === 'Utilizes' ? 'url(/images/home/graphic7.png)' : undefined };
  background-position: bottom;
  background-repeat: no-repeat;
  background-size: cover;
  border: 1px solid ${ colors.border };
  border-radius: 10px;
  padding-bottom: 300px;
  text-align: center;
  ${
    Media.small`
      background-repeat: repeat-x;
      background-size: contain;
      padding-bottom: 250px;
    `
  }
  ${
    Media.small`
      padding-bottom: 200px;
    `
  }
  h4 {
    color: ${ props => props.boxName === 'Creates' ? colors.homeBoxCreates : undefined };
    color: ${ props => props.boxName === 'Enables' ? colors.homeBoxEnables : undefined };
    color: ${ props => props.boxName === 'Utilizes' ? colors.homeBoxUtilizes : undefined };
    font-size: 1.8rem;
    margin: 10px 0 0;
    text-transform: uppercase;
    ${
      Media.medium`
        font-size: 1.2rem;
      `
    }
  }
  p {
    color: ${ colors.textLight };
    font-size: 1.3rem;
    margin-top: 0;
    padding: 0 60px;
    ${
      Media.medium`
        font-size: 1rem;
        padding: 0 20px;
      `
    }
  }
  img {
    width: 100%;
    &:first-of-type {
      margin-top: 40px;
      max-width: 150px;
      ${
        Media.medium`
          max-width: 120px;
        `
      }
      ${
        Media.small`
          max-width: 80px;
        `
      }      
    }
  }
`

StyledBox.propTypes = {
  boxName: PropTypes.string.isRequired
}

export default StyledBox