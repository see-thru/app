import React from 'react'
import PropTypes from 'prop-types'

import StyledBox from './styled'

const Box = ({
  icon,
  title,
  description
}) => (
  <StyledBox boxName={ title }>
    <img 
      alt={ `${ title }'s icon` }
      src={ icon } />
    <h4>{ title }</h4>
    <p>{ description }</p>
  </StyledBox>
)

Box.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
}

export default Box