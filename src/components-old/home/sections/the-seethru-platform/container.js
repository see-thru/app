import React from 'react'

import Box from './box/base'
import Header from '../header/base'
import StyledTheSeeThruPlatform from './styled'
import StyledContainerGridBanner from '../banner/styled-container'

const TheSeeThruPlatform = () => (
  <StyledTheSeeThruPlatform>
    <Header 
      title="Everybody Wins With Seethru"
      subtitle="Brings Patients and Providers closer. SeeThru helps patients by providing a value-based consumer experience in the healthcare industry." />
    <StyledContainerGridBanner
      gridGap="20px"
      templateRows="auto"
      templateColumns="repeat(3, 1fr)"> 
      <Box 
        icon="/images/home/graphic2.png"
        title="Creates"
        description="a marketplace for both patients & providers" />
      <Box 
        icon="/images/home/graphic3.png"
        title="Enables"
        description="instant payment reducing cost & waste" />        
      <Box 
        icon="/images/home/graphic4.png"
        title="Utilizes"
        description="the benefits of blockchain tech" />                
    </StyledContainerGridBanner>
  </StyledTheSeeThruPlatform>
)

export default TheSeeThruPlatform