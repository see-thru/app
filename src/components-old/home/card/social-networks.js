import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Icon from '../../generic/icon/base'

const List = styled.ul`
  margin: 0;
  padding: 0;
`

const Link = styled.a`
  background-color: ${ props => props.type !== 'twitter' ? '#0077B7' : undefined };
  display: inline-block;
  margin: 0 2px;
  padding: 2px;
  transform: scale(1);
  transition: .3s transform;
  &:hover {
    transform: scale(1.2);
  }
`

const SocialNetworks = ({
  socialNetworks
}) => (
  <List>
    { socialNetworks && 
      socialNetworks.map( (x, index) => {
      return (
        <Link 
          key={ index }
          type={ x.type }
          href={ x.url }
          target="_blank">
          <Icon 
            fill={ x.type === 'twitter' ? '#76A9EA' : '#FFF' }
            name={ x.type } 
            width="14px" 
            height="14px"
            display="block" />
        </Link>
      )
    }) }
  </List>
)

const socialNetworksPropTypes = PropTypes.shape({
  type: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
})

SocialNetworks.propTypes = {
  socialNetworks: PropTypes.arrayOf(socialNetworksPropTypes).isRequired
}


export default SocialNetworks