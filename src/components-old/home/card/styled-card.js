import styled from 'styled-components'

const StyledCard = styled.div`
  font-size: 1.15rem;
  text-align: center;
`

export default StyledCard