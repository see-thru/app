import React from 'react'

import Banner from './sections/banner/container'
import TheSeeThruPlatform from './sections/the-seethru-platform/container'
import EverybodyWinsWithSeeThru from './sections/everybody-wins-with-seethru/container'

const Home = (props) => (
  <div>
    <Banner { ...props } /> 
    <EverybodyWinsWithSeeThru />
    <TheSeeThruPlatform />
  </div>
)

export default Home