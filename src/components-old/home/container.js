import React from 'react'

import Home from './base'
import isFeatureEnabled from '../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../has-permission'

export const HomeContainer = (props) => (
  <Home { ...props } />
)

export default HasPermissionHOC(
  HomeContainer, 
  isFeatureEnabled(window.location.href).pages.home
)