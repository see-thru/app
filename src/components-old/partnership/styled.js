import styled from 'styled-components'

import Media from '../../utils/media'
import { StyledContainerGrid } from '../generic/grid'

const StyledContainer = styled(StyledContainerGrid)`
  margin: 40px auto;
  justify-content: center;
  ${ Media.small`
    grid-template-columns: 100%;
  ` }
`
export default StyledContainer
