import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'

import Registration from './basic-registration/container'
import isFeatureEnabled from '../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../has-permission'
import CompleteRegistration from './complete-registration/container'

export const PartnershipContainer = ({ match }) => (
  <Fragment>
    <Route 
      exact
      path={ `${ match.path }` }
      component={ Registration } />
    <Route 
      exact
      path={ `${ match.path }/:providerId` }
      component={ CompleteRegistration } />
  </Fragment>
)

PartnershipContainer.propTypes = {
  match: PropTypes.object.isRequired
}

export default HasPermissionHOC(
  PartnershipContainer, 
  isFeatureEnabled(window.location.href).pages.partnership
)