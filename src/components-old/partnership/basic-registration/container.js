import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import generateDataForCreateToProvider from '../../../utils/generator-data-structure/create-to-provider'
import { postPartnershipProvider, partnershipProviderCreateNew } from '../../../actions-old/partnership'

import Form from './form'
import Loader from '../../generic/loader/base'
import StyledImage from './styled-img'
import StyledContainer from '../styled'
import StyledMessageError from '../../generic/subscribe-general-modal/status-messages/styled-message-error'
import StyledMessageSuccess from './styled-message-success'
import { StyledButtonSuccess } from '../../generic/button/styled'

export class Registration extends Component {
  constructor() {
    super()
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit = e => {
    const {
      user, 
      postPartnershipProvider 
    } = this.props
    const data = generateDataForCreateToProvider(e)
    let token 
    if ( user && user.token ) {
      token = user.token
    }
    postPartnershipProvider(data, token)
  }

  render() {
    const { 
      user,
      partnership 
    } = this.props
    return (
      <StyledContainer
        gridGap="0"
        grid-template-rows="1"
        templateColumns="550px">
        { user && user.token && partnership.data.success ?
          <Fragment>
            <StyledImage src='/images/partnership/reg_thanks.png'/>
            <StyledMessageSuccess>
              <h2>Thanks for registering, we'll contact you shortly.</h2>
              <StyledButtonSuccess 
                to="/user/provider/edit-provider-profile"
                tag={ Link }>
                Complete your profile
              </StyledButtonSuccess>
            </StyledMessageSuccess>
          </Fragment> :
          <Fragment>
            { partnership.loading ? <Loader /> : undefined }
            <h1>Provider Sign-up</h1>
            { partnership.error ? <StyledMessageError>{ partnership.error }</StyledMessageError> : undefined }
            <Form onSubmit={ this.onSubmit } />
          </Fragment>
        }
      </StyledContainer>
    )
  }
}

Registration.propTypes = {
  partnership: PropTypes.shape({
    data: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    error: PropTypes.string,
  }).isRequired
}

const mapStoreToProps = (state) => ({
  user: state.AuthReducer.user,
  partnership: state.PartnershipReducer
})

export default connect(
  mapStoreToProps, 
  { 
    postPartnershipProvider, 
    partnershipProviderCreateNew 
  }
)(Registration)
