import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  Field,
  reduxForm
} from 'redux-form'
import {
  email,
  length,
  required,
  confirmation
} from 'redux-form-validators'

import {
  Input,
  FormWrapper,
  SelectCustomizable
} from '../../generic/form'
import { StyledButtonSuccess } from '../../generic/button/styled'

import { specialities as SpecialitiesCatalog } from '../../../catalogs/index'
import GeneratorInitialValuesPartnershipRegistrationForm from '../../../utils/generator-initial-values-form/partnership-registration'

export let Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper onSubmit={ handleSubmit(onSubmit) }>
    <Field
      title='Email Address'
      name='email'
      component={Input}
      type='email'
      validate={[
        required(),
        length({ max: 255 }),
        email()
      ]}
    />  
    <Field
      title='First Name'
      name='firstName'
      component={Input}
      type='text'
      validate={[
        required()
      ]}
    />
    <Field
      title='Last Name'
      name='lastName'
      component={Input}
      type='text'
      validate={[
        required()
      ]}
    />
    <Field
      title='Specialty'
      name='specialty'
      placeholder='Select specialty'
      options={ SpecialitiesCatalog }
      component={ SelectCustomizable }
      validate={[
        required()
      ]}
    />
    <Field
      title='Zip Code'
      name='zip'
      component={Input}
      type='text'
      validate={[
        required()
      ]}
    />
    <Field
      title='Password'
      name='password'
      component={Input}
      type='password'
      validate={[
        required()
      ]}
    />    
    <Field
      title="Confirm Password"
      name="confirm"
      placeholder="minimum 6 characters"
      component={ Input }
      type="password"
      validate={ [
        length({ minimum: 6 }),
        required(),
        confirmation({ field: 'password' })
      ] }
    />    
    <Field
      title='Referral'
      name='referral'
      component={Input}
      type='text'
    />
    <Field
      title='Phone Number'
      name='phone'
      component={Input}
      type='text'
      validate={[
        required()
      ]}
    />
    <StyledButtonSuccess 
      type='submit'>
      Register
    </StyledButtonSuccess>
  </FormWrapper>          
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

Form = reduxForm({
  form: 'partnershipRegistrationForm',
  initialValues : {},
  enableReinitialize : true
})( Form )

export default connect(
  state => ({
    initialValues: state.AuthReducer.user !== null ? GeneratorInitialValuesPartnershipRegistrationForm(state.AuthReducer.user) : {}
  }),
)(Form)
