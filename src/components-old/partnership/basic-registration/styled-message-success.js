import styled from 'styled-components'

const StyledMessageSuccess = styled.div`
  text-align: center;
`

export default StyledMessageSuccess
