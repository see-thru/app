import { createStore, compose, applyMiddleware } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { responsiveStoreEnhancer } from 'redux-responsive'
import thunk from 'redux-thunk'
import reducer from './reducers'
import { setFCUser, initFCEmpty } from './utils/fresh-chat'
import { setGAUser } from './utils/analytics';

const persistConfig = {
  key: 'root',
  storage: storage ? storage : null,
  whitelist: ['AuthReducer']
}
const enhancers = compose(
  responsiveStoreEnhancer,
  window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
)
const persistedReducer = persistReducer(persistConfig, reducer)
export const store = applyMiddleware(thunk)(createStore)(persistedReducer, enhancers)
export const persistor = persistStore(store, null, () => {
  const auth = store.getState().AuthReducer
  if(auth.user){
    setFCUser(auth.user)
    setGAUser(auth.user._id)
  } else {
    initFCEmpty()
    setGAUser(undefined)
  }
})