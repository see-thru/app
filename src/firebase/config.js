import * as HOSTS from '../constants/hosts'

const configProd = {
  apiKey: "AIzaSyDxeguOt0uaOeRWyK_JlM_a0MwN1CbJtZ4", // Functions API Key
  authDomain: "seethru-app.firebaseapp.com",
  databaseURL: "https://seethru-app.firebaseio.com",
  projectId: "seethru-app",
  storageBucket: "seethru-app.appspot.com",
  messagingSenderId: "119834320561"
}

const configDev = {
  apiKey: "AIzaSyAz-nO0c-JSPPUAEBFGqYdG6S3BVqhwzeo", // Functions API Key
  projectId: "seethru-dev",
  authDomain: "seethru-dev.firebaseapp.com",
  databaseURL: "https://seethru-dev.firebaseio.com",
  storageBucket: "seethru-dev.appspot.com",
  messagingSenderId: "64013815615"
}

const configClaude = {
  apiKey: "AIzaSyBTQzghJ7lbXukEL2Oj4P1N9VOPYifOxfw",
  authDomain: "claude-care.firebaseapp.com",
  databaseURL: "https://claude-care.firebaseio.com",
  projectId: "claude-care",
  storageBucket: "claude-care.appspot.com",
  messagingSenderId: "747605783279"
}

const getFirebaseConfig = (baseUrl = '') => {
  switch (baseUrl) {
    case HOSTS.PROD:
    case HOSTS.PROD2:
      return configProd
    case HOSTS.CLAUDE:
    case HOSTS.CLAUDE2:
    case HOSTS.CLAUDE3:
      return configClaude
    default:
      return configDev
  }
}

export default getFirebaseConfig