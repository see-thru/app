import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, Switch } from 'react-router-dom'
import { PersistGate } from 'redux-persist/lib/integration/react'
import history from './history'
import { store, persistor } from './store'
import { GlobalStyle } from './styles/global'

////////////////////////////////
// Generic Components
////////////////////////////////
import PrivateRoute from './components/private-router'
import HeaderContainer from './components/header/container'
import SignInModalContainer from './components/modals/signin/modal'
import SignUpModalContainer from './components/modals/signup/modal'
import ForgotPasswordContainer from './components/modals/forgot-password/container'

import Footer from './components/footer/container'
import ScrollToTop from './components/common/scroll-to-top/container'

////////////////////
// Platform
////////////////////
import HomeContainer from './components/home/container'
import ProviderRouter from './components/provider/router'
import SearchContainer from './components/search/container'
import AppointmentsRouter from './components/appointments/router'
import CalculatorContainer from './components/calculator/container'
import NoMatchContainer from './components/no-match/container'

// import UserContainer from './components-old/user/container'
// import PatientContainer from './components-old/patient/container'
// import PartnershipContainer from './components-old/partnership/container'
////////////////////
// Website
////////////////////
// import FAQContainer from './components-old/website/faq/container'
// import SiteMapContainer from './components-old/website/site-map/container'
// import AboutUsContainer from './components-old/website/about-us/container'
// import WhySeeTruContainer from './components-old/website/why-seethru/container'
// import PrivacyPolicyContainer from './components-old/website/privacy-policy/container'
// import WorkingWithUsContainer from './components-old/website/working-with-us/container'
// import TermsOfServiceContainer from './components-old/website/terms/container'

// add the icons we're going to need
import { library } from '@fortawesome/fontawesome-svg-core'
import { 
  faChevronDown, faChevronUp, faChevronLeft, faMapMarkerAlt, faSearch, faInfoCircle, 
  faCalendarAlt, faClock, faCheck, faPlus, faLock, faSpinner, faFilter, faTimes
} from '@fortawesome/free-solid-svg-icons'
import { faCalendarCheck, faCalendarPlus } from '@fortawesome/free-regular-svg-icons'
library.add(
  faChevronDown, faChevronUp, faChevronLeft, faMapMarkerAlt, faSearch, faInfoCircle, 
  faCalendarAlt, faClock, faCheck, faPlus, faLock, faSpinner, faFilter, faTimes,
  faCalendarCheck, faCalendarPlus
)

window.SeeThru = { stackSize: 0 }

const MainRoute = ({ children }) => (
  <Route path="/" 
    render={ props => (
      <main style={{backgroundColor: props.match.isExact ? '#2b465a' : null}}>
        { children }
      </main>
    )} />
)

const App = () => (
  <Provider store={ store }>
    <PersistGate loading={ null } persistor={ persistor }>
      <Router history={ history }>
        <ScrollToTop { ...history }>
          <GlobalStyle />
          <Route path="/" component={ HeaderContainer } />
          <div className="full-height-container">
            <MainRoute>
              <Switch>
                <Route exact path="/" component={ HomeContainer } />
                <Route path="/provider" component={ ProviderRouter } />
                <Route exact path="/search"	component={ SearchContainer } />	
                <PrivateRoute path="/appointments" component={ AppointmentsRouter } />
                <Route path="/calculator"	component={ CalculatorContainer } />	
                <Route component={ NoMatchContainer } />
              </Switch>
            </MainRoute>
            <Footer />
          </div>
          <SignInModalContainer />
          <SignUpModalContainer />
          <ForgotPasswordContainer />
        </ScrollToTop>
      </Router>
    </PersistGate>
  </Provider>
)

export default App
