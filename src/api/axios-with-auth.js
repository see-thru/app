import axios from 'axios'
import { store } from '../store'
import { loggedOut } from '../actions/auth'
import { getCurrentUser } from '../firebase'

const axiosWithAuth = () => {
  return getCurrentUser().then(user => {
    if(user){
      return user.getIdToken().then(token => {
        var instance = axios.create({ headers: { Authorization: `Bearer ${token}` } })
        instance.defaults.headers.post['Content-Type'] = 'application/json'
        return instance
      })
    } else {
      // called axiosWithAuth, but no user! might be in trackAction
      store.dispatch(loggedOut()) // he might already be logged out...?
      // return Promise.resolve(axios)
      return Promise.reject({ error: 'User is not logged' });
    }
  })
}

export default axiosWithAuth