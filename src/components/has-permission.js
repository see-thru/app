import React, { Component } from 'react'

const HasPermissionHOC = (
  WrapperComponent, 
  branchHasPermission
) => {
  return class PP extends Component {
    componentWillMount () {
      const { history } = this.props
      if ( ! branchHasPermission ) history.push('/404')
    }

    render () {
      return (
        branchHasPermission && <WrapperComponent { ...this.props } />
      )
    }
  }
}

export default HasPermissionHOC