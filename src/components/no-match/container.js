import React, { Component } from 'react'

import NotMatch from './base'

class NotMatchContainer extends Component {
  render () {
    return <NotMatch />
  }
}

export default NotMatchContainer