import React from 'react'
import { Link } from 'react-router-dom'

import StyledNotMatch from './styled'
import { GridContainer } from '../common/layout';
import { SubmitButton } from '../common/elements';

const NotMatch = () => (
  <StyledNotMatch>
    <GridContainer>
      <img 
        alt=""
        src="/images/404.png"
        width="100%" />
      <SubmitButton
        to="/"
        tag={ Link }>
        Back to Home page
      </SubmitButton>
    </GridContainer>
  </StyledNotMatch>
)

export default NotMatch