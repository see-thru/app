import styled from 'styled-components'

import Media from '../../utils/media'
import { 
  fonts,
  colors 
} from '../../utils/settings'

const StyledNotMatch = styled.div`
  max-width: 1000px;
  margin: 20px auto;
  h3,
  strong {
    font-weight: ${ fonts.weightBold };
  }
  h3 {
    color: ${ colors.successButton };
    font-size: 1.12rem;
    margin-bottom: 10px;
    text-transform: uppercase;
    ${ Media.medium`
      font-size: .9rem;
    ` }    
  }
  p {
    font-size: 1.25rem;
    margin-top: 0;
    ${ Media.medium`
      font-size: 1rem;
    ` }
  }
  a {
    display: inline-block;
    margin-top: 10px;
    text-align: center;
    width: 180px;
  }
`

export default StyledNotMatch