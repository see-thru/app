import styled from 'styled-components'

import Media, { MaxWidth } from '../../utils/media'
import { fonts, colors } from '../../utils/settings'
import { SubmitButton } from '../common/elements'

export const StyledMapContainer = styled.div`
  display: block;
  height: ${ props => props.height };
  max-height: ${ props => props.maxHeight };
  position: relative;
  ${ Media.small`
    display: block;
    margin-top: 0;
  `}
`

export const StyledInfoWindow = styled.div`
  img {
    border-radius: 50%;
    object-fit: cover;
  }
  h3 {
    display: inline-block;
    color: ${ colors.darkBlue };
    font-size: 1rem;
    margin: 4px 8px 0;
  } 
  ${ MaxWidth(600)`
    text-align: center;
  `}
  // a, button {
  //   background-color: ${ colors.successButton };
  //   border-radius: 2px;
  //   color: white;
  //   display: block;
  //   font-size: .8rem;
  //   font-weight: ${ fonts.weightBold };
  //   margin-top: 10px;
  //   padding: 4px 6px;
  //   text-align: center;
  //   text-decoration: none;
  //   text-transform: uppercase;
  //   width: 100px;
  // }
`

export const StyledInfoButton = styled(SubmitButton)`
  display: inline-block;
  margin: 5px 0 0;
  padding: 5px 12px;
`