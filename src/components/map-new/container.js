import React, { Component } from 'react'
import qs from 'qs'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { searchMarkerMouseover, searchMarkerMouseout } from '../../actions/search'
import MapWrapper from './base'
import { StyledMapContainer } from './styles'

// This component is used for all of the functions that are needed on the '/search' map
// i.e. infowindow (infoPlace/infoMarker), search marker hover... 
export class MapContainer extends Component {
  constructor () {
    super() 
    this.state = { infoWindow: {} }
    this.onMarkerClick = this.onMarkerClick.bind(this)
    this.onMapClicked = this.onMapClicked.bind(this) 
    this.onMouseoverMarker = this.onMouseoverMarker.bind(this)
    this.onDragend = this.onDragend.bind(this)
    this.onMapReady = this.onMapReady.bind(this)
    this.timeout = undefined
  }

  onMarkerClick (props, marker) {
    this.setState({
      infoWindow: {
        position: props.position,
        show: true
      }
    })
  }
  
  // TODO: Close info window on results hover as well (redux?)
  onMapClicked () {
    if (this.state.infoWindow.show) {
      this.setState({ infoWindow: {} })
    }
  }

  onMouseoverMarker (marker, map, providerId) {    
    this.props.searchMarkerMouseover(providerId)    
  }

  // todo: on mouse out, doesn't exist in current library
  onDragend(mapProps, map) {
    if(this.timeout){
      clearTimeout(this.timeout)
    }
    this.timeout = setTimeout(() => {
      const { location, history } = this.props
      let filters = qs.parse(location.search.slice(1))
      const bounds = map.getBounds();
      const lat = map.center.lat()
      const lng = map.center.lng()
      // if(activeProvider && activeProvider.office){
      //   if((activeProvider.office.location.lat === lat) && (activeProvider.office.location.lng === lng)){
      //     console.log('moved to active. dont refresh?')
      //   }
      // }
      let distance = '30'
      if (bounds) {
        const ne = bounds.getNorthEast();
        // radius = radius of the earth in statute miles
        const radius = 3963.0;  
        // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
        const lat1 = lat / 57.2958; 
        const lng1 = lng / 57.2958;
        const lat2 = ne.lat() / 57.2958;
        const lng2 = ne.lng() / 57.2958;
        // distance = circle radius from center to Northeast corner of bounds
        distance = radius * Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lng2 - lng1));
      }
      Object.assign(filters, { lat, lng, distance })
      const query = qs.stringify(filters)
      history.push({
        pathname: '/search',
        search: query ? `?${query}` : undefined
      })
    }, 1500)
  }

  onMapReady = (mapProps, map) => {
    const that = this;
    mapProps.google.maps.event.addListener(map, 'zoom_changed', function() {
      that.onDragend(mapProps, map)  
    });
    // window.onresize = () => {
    //   const currCenter = map.getCenter()
    //   mapProps.google.maps.event.trigger(map, 'resize')
    //   map.setCenter(currCenter)
    //   this.onDragend(mapProps, map)
    // };
  }

  // componentWillUnmount() {
  //   window.removeEventListener('resize')
  // }

  render () {
    const { height, location, maxHeight } = this.props
    const { infoWindow } = this.state
    const parsed = qs.parse(location.search.slice(1))
    return (
      <StyledMapContainer
        height={ height } 
        maxHeight={ maxHeight }>
        <MapWrapper 
          infoWindow={ infoWindow }
          onDragend={ this.onDragend }
          onMapReady={ this.onMapReady }
          onMouseoverMarker={ this.onMouseoverMarker }
          onMapClicked={ this.onMapClicked }
          onMarkerClick={ this.onMarkerClick }
          latFilter={ parsed.lat }
          lngFilter={ parsed.lng }
          distanceFilter={ parsed.distance } />
      </StyledMapContainer>      
    )
  }
}

MapContainer.defaultProps = {
  height: '100%',
  location: {},
  maxHeight: '900px'
}

MapContainer.propTypes = {
  // passed in
  height: PropTypes.string,
  history: PropTypes.object,
  location: PropTypes.object,
  maxHeight: PropTypes.string,
  // actions
  searchMarkerMouseover: PropTypes.func.isRequired,
  searchMarkerMouseout: PropTypes.func.isRequired
}

export default connect(
  () => ({}),
  { searchMarkerMouseover, searchMarkerMouseout }
)( MapContainer )