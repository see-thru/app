import { css } from 'styled-components'
import { colors } from '../../utils/settings'

export const borderHover = css`
  border-left: 3px solid transparent;
  transition: border-left-color .2s linear;
  &:hover, &.active {
    border-left-color: ${ colors.darkBlue }
  }
`