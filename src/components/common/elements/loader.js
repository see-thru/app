import React from 'react'
import styled from 'styled-components'
import Icon from './icon'

const StyledLoader = styled.div`
  align-items: center;
  background-color: rgba(255,255,255,.8);
  display: flex;
  left: 0;
  height: 100%;
  justify-content: center;
  top: 0;
  position: absolute;
  width: 100%;
  z-index: 11;
`

const Loader = () => (
  <StyledLoader>
    <Icon 
      name="loader" 
      width="34" 
      height="40" />
  </StyledLoader>
)

export default Loader