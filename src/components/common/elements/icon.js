import React from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'

const StyledIcon = styled.svg`
  display: { props => props.display };
  fill: { props => props.fill };
  height: { props => props.height };
  width: { props => props.width };
`

const Icon = (
  props 
) => (
  <StyledIcon 
    { ...props }
    className={ props.name }>
    <use xlinkHref={ `#${ props.name }` }></use>
  </StyledIcon>
)

Icon.defaultProps = {
  fill: 'black',
  display: 'inline-block'
}

Icon.propTypes = {
  fill: PropTypes.string,
  name: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  display: PropTypes.string
}

export default Icon