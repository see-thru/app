import React from 'react'
import styled from 'styled-components'
import { Alert } from 'reactstrap';

const StyledAlert = styled(Alert)`
  text-align: center;
  max-width: 1100px;
  margin: 15px auto;
`

export const AlertContainer = ({ children }) => {
  return (
    <StyledAlert color="danger">
      { children }
    </StyledAlert>
  )
}

export const OptionalAlert = ({ alert = {}, clearAlert, ...props }) => {
  if(alert.show){
    return <Alert
      color={alert.type || 'danger'}
      onClick={ () => { clearAlert && clearAlert()}}
      style={clearAlert?{cursor: 'pointer'}:{}}
      { ...props }>{ alert.message }</Alert>
  } else return <div></div>
}