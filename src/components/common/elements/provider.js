import React from 'react'
import styled from 'styled-components'
import CurrencyFormat from 'react-currency-format'
import moment from 'moment-timezone'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { colors } from '../../../utils/settings'
// require('moment-timezone')

const StyledRating = styled.span`
  font-weight: 700;
  img {
    width: 15px;
    height: 15px;
    vertical-align: text-top;
    display: inline-block;
    margin-right: 7px;
  }
`

export const Rating = ({ rating }) => (
  <StyledRating>
    <img src="/images/seethru-isotype.png" alt="" />
    { rating }%
  </StyledRating>
)

const StyledServiceCost = styled.span`
  color: white;
  font-weight: 700;
  background-color: ${ colors.darkBlue };
  padding: 4px 7px;
  font-size: 12px;
  border-radius: 3px;
`

export const FormattedCost = ({ cost }) => (
  <CurrencyFormat 
    value={cost}
    displayType={'text'}
    thousandSeparator={true}
    prefix={'$'}
    decimalScale={2} 
    fixedDecimalScale={ (cost%1) > 0 }
    />
)

export const ServiceCost = ({ cost, ...rest }) => (
  <StyledServiceCost { ...rest }>
    { cost ? <FormattedCost cost={cost} /> : 'Free' }
  </StyledServiceCost>
)

// Remove the timezone offset, because we saved it in UTC
export const AppointmentTime = ({ time }) => (
  <span>{ moment.tz(time, 'UTC').format('MMMM D, YYYY - h:mm A') }</span>
)

export const RequestedTime = ({ time, setUTC }) => {
  let date = moment(time.date)
  if(setUTC) date = date.tz('UTC')
  return <span>{ date.format('MMMM D, YYYY') } - { time.time }</span>
}

export const ProviderName = ({ info, tag = 'h1' }) => {
  const Element = styled[tag]``
  return (
    <Element>{ info.name }{ info.degrees &&
      `, ${info.degrees}`
    }</Element>
  )
}

const UnstyledLink = styled.a`
  &, &:hover { color: inherit; }
`
export const AddressLink = ({address}) => {
  const addressHref = `https://www.google.com/maps/place/${encodeURI(address)}`
  return (
    <UnstyledLink href={addressHref} target="_blank">
      <FontAwesomeIcon icon="map-marker-alt" /> { address }
    </UnstyledLink>
  )
}