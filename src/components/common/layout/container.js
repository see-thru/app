import styled, { css } from 'styled-components'
import { colors } from '../../../utils/settings'
import { GridContainer } from './grid'

const pageContainerStyles = css`
  max-width: 1215px;
  margin: 30px auto 15px;
  padding: 0 20px;
`

export const PageContainer = styled.div(pageContainerStyles)

export const GridPageContainer = styled(GridContainer)(pageContainerStyles)

export const ContentBox = styled.div`
  background: white;
  padding: 10px 0;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  margin-bottom: 10px;

  .title {
    padding: 5px 25px 10px;
    display: flex;
    justify-content: space-between;
    align-items: base-line;
    margin-bottom: 10px;
    font-size: .9rem;
    color: ${ colors.titleColor };
    border-bottom: 1px solid $title-border;

    h1, h2 { 
      margin: 0;
      font-size: 1.3rem;
      font-weight: 700;
    }
    &.title-small {
      h1, h2 {
        font-size: 1rem;
      }
    }
  }
  .content {
    padding: 10px 25px;
  }
`

export const ContentBoxPadded = styled(ContentBox)`
  padding: 20px;
  margin-bottom: 20px;
`

export const ContentBoxBlue = styled(ContentBox)`
  padding-top: 0;
  .title {
    background-color: ${ colors.blue};
    color: white;
    padding-top: 15px;
  }
`

export const ContentBoxClickable = styled(ContentBox)`
  cursor: pointer;
  outline: none;
  transition: all 0.3s cubic-bezier(.25,.8,.25,1);
  &:hover {
    box-shadow: 1px 1px 8px rgba(0,0,0,.19), 0 3px 6px rgba(0,0,0,.23);
  }
`