import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledFieldsetWrapper = styled.fieldset`
  border: 0;
  margin: 0;
  padding: 0;
`

const FieldsetWrapper = ({ 
  children 
}) => (
  <StyledFieldsetWrapper>
    { children }
  </StyledFieldsetWrapper>
)

FieldsetWrapper.propTypes = {
  children: PropTypes.node.isRequired
}

export default FieldsetWrapper