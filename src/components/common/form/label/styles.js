import styled from 'styled-components'

const StyledLabel = styled.label`
  display: block;
  font-size: 1rem;
  margin-bottom: 10px;
`

export default StyledLabel