import React from 'react'
import { Input } from 'reactstrap'

export const BootstrapInput = ({ input: { value, onChange } }) => (
  <Input type="checkbox" checked={!!value} onChange={onChange} />
)