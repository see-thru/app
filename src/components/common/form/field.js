import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledFieldWrapper = styled.div`
  clear: both;
  float: left;
  margin: 10px 0;
  width: 100%;
`

const FieldWrapper = ({ 
  children 
}) => (
  <StyledFieldWrapper>
    { children }
  </StyledFieldWrapper>
)

FieldWrapper.propTypes = {
  children: PropTypes.node.isRequired
}

export default FieldWrapper
