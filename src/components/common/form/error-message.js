import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { fonts } from '../../../utils/settings'

const StyledError = styled.span`
  color: ${ props => props.lightColor ? '#d8d8d8' : '#a0a0a0' };
  display: block;
  font-size: .8rem;
  font-weight: ${ fonts.weightBold };
  line-height: 16px;
  margin: 5px 0 10px;
`

const ErrorMessage = ({ children, ...props }) => (
  <StyledError { ...props }>
    { children }
  </StyledError>
)

ErrorMessage.propTypes = {
  children: PropTypes.node.isRequired
}

export default ErrorMessage
