import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { colors } from '../../../utils/settings'
import CurrencyFormat from 'react-currency-format'


const StyledInput = styled(CurrencyFormat)`
  font-size: .9rem;
  ::placeholder {
    color: ${ colors.placeholderFields };
  }
`

const Input = ({
  meta,
  type,
  title,
  input,
  label,
  placeholder
}) => (
  <div className="form-group">
    { label && 
      <label htmlFor={ input.name }>{ label }</label>
    }
    <StyledInput
      title={ title }
      name={ input.name }
      type={ type }
      className="form-control"
      placeholder={ placeholder }
      { ...input } />
    { meta.touched && meta.error &&
      <small className="form-text text-danger">
        { (type !== 'password' && input.value) || title } { meta.error }
      </small>
    }
  </div>
)

Input.defaultProps = {
  type: 'text'
}

Input.propTypes = {
  meta: PropTypes.object.isRequired,
  type: PropTypes.string,
  title: PropTypes.string.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string
}

export default Input
