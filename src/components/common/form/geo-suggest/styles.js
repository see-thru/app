import styled from 'styled-components'
import Geosuggest from 'react-geosuggest'

import { colors } from '../../../../utils/settings'
import { MaxWidth } from '../../../../utils/media'

const StyledInputGeoSuggest = styled(Geosuggest)`
  position: relative;
  input {
    font-size: .9rem;
    ::placeholder {
      color: ${ colors.placeholderFields };
    }
  }
  & * {
    outline: 0;
  }
  .geosuggest__suggests {
    color: ${ colors.text };
    font-size: 0.8rem;
    list-style: none;
    margin: 0;
    padding: 0;
    position: absolute;
    width: 350px;
    border-radius: 5px;
    border-top-left-radius: 0;
    z-index: 1;
    background-color: #f1f1f1;
    ${
      MaxWidth(900)`
        right: 0;
        border-top-right-radius: 0;
      `
    }
    ${ MaxWidth(350)`
      width: 100%;
    `}
    &--hidden {
      display: none;
    }
  }

  .geosuggest__item {
    background-color: transparent;
    border-bottom: 1px solid #dedede;
    color: ${ colors.darkBlue };
    cursor: pointer;
    padding: 10px 15px;
    text-align: left;
    transition: .2s background-color;
    &:hover,
    &--active {
      background-color: #cedce6;
    }    
  }
`

export default StyledInputGeoSuggest