import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { GoogleApiWrapper } from 'google-maps-react'
import StyledInputGeoSuggest from './styles'

export class InputGeoSuggest extends Component {
  onSuggestSelect = value => {
    const { input, onSelect } = this.props
    if(value && value.description && value.location){
      const location = {
        lat: value.location.lat,
        lng: value.location.lng
      }
      input.onChange({
        location,
        description: value.description
      })
      if(onSelect) onSelect()
    } 
  }

  render () {
    const { input, meta, label, title, placeholder } = this.props
    return (
      <div className="form-group">
        { label && 
          <label htmlFor={ input.name }>{ label }</label>
        }
        <StyledInputGeoSuggest
          id={ input.name }
          inputClassName="form-control"
          type="text"
          country={ ['us'] }
          { ...input }
          placeholder={ placeholder || '' }
          title={title}
          initialValue={ input.value.description || ''}
          ignoreEnter
          autoComplete="off"
          onSuggestSelect={this.onSuggestSelect} />
        { meta.touched && meta.error &&
          <small className="form-text text-danger">{ meta.error }</small>
        }
      </div>
    )
  }
}

InputGeoSuggest.defaultProps = {
  type: 'text',
  readOnly: false
}

InputGeoSuggest.propTypes = {
  meta: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  label: PropTypes.string
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAP94H75MYiGqWYg68X3124yzTVJVU7zYs'
})(InputGeoSuggest)