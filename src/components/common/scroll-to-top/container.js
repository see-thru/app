import { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

export class ScrollToTop extends Component {
  componentDidUpdate (prevProps) {
    const { location } = this.props
    if ((location !== prevProps.location) && (location.pathname !== '/search')) {
      window.scrollTo(0, 0)
    }
  }

  render() {
    return this.props.children
  }
}

ScrollToTop.propTypes = {
  location: PropTypes.object.isRequired
} 

export default withRouter(ScrollToTop)