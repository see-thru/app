import React from 'react'
import Modal from 'react-modal'
import styled, { css } from 'styled-components'
import { colors } from '../../utils/settings'

export default ({ size, ...props }) => (
  <Modal
    className={`Modal__Bootstrap modal-dialog modal-dialog-centered ${size||''}`}
    closeTimeoutMS={100}
    style={ { overlay: { zIndex: 100 } } }
    {...props } />
)

const modalContentStyles = css`
  .modal-body {
    padding: 24px;
    font-size: .9rem;
    line-height: 1.3rem;
    .modal-content-text {
      margin-bottom: 20px;
    }
    p {
      margin: 0;
      font-weight: 600;
    }
    form {
      .form-text {
        color: #eee !important;
      }
    }
  }
`

export const ModalBlue = styled.div`
  ${ modalContentStyles }
  background-color: ${ colors.lightBlue };
  color: white;
`

export const ModalWhite = styled.div`
  ${ modalContentStyles }
  background-color: white;
  color: ${ colors.text };
`

// no padding on header
export const ModalBodyWhite = styled.div`
  .modal-body {
    padding: 0;
    font-size: .9rem;
    line-height: 1.3rem;
    
    .modal-body-inner {
      padding: 15px 25px;
    }
    svg {
      margin-right: 5px;
    }
  }
`

export const ModalHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  h1 {
    flex: 1;
    font-size: 40px;
    font-weight: 800;
    margin-bottom: 5px;
  }
  img {
    width: 130px;
    height: 40px;
    object-fit: contain;
  }
`

export const ModalHeaderBlue = styled.div`
  background-color: ${ colors.lightBlue};
  color: white;
  padding: 17px 25px 12px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  h1 {
    font-size: 1rem;
    letter-spacing: 1px;
    font-weight: 600;
    margin: 0;
  }
`

export const ModalFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 25px;
`

export const ModalLink = styled.button`
  background: transparent;
  border: 0;
  border-bottom: 2px solid white;
  color: white;
  padding: 0 2px;
  &:hover {
    border-color: #a6e3ff;
    color: #a6e3ff;
  }
`

export const ModalLinkBlue = styled.button`
  background: transparent;
  border: 0;
  border-bottom: 2px solid ;
  color: ${ colors.darkBlue };
  padding: 0 2px;
  &:hover {
    border-color: ${ colors.lightBlue };
    color: ${ colors.lightBlue };
  }
`