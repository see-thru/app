import styled from 'styled-components'
import Calendar from 'react-calendar'
import { MaxWidth } from '../../../utils/media'
import { colors } from '../../../utils/settings'

export const StyledTimeSelection = styled.div`
  display: flex;
  .column-calendar {
  }
  .column-chosen-dates {
    flex: 1 0 250px;
    margin-left: 20px;
    .chosen-tag {
      display: inline-block;
      margin-bottom: 5px;
      padding: 5px 15px;
      background: rgba(0,110,220,0.1);
      border-radius: 7px;
      font-size: .8rem;
      button {
        padding: 0 5px 0 0;
        font-size: 0.7rem;
      }
    }
  }
  ${MaxWidth(675)`
    flex-wrap: wrap;
  `}
`

export const StyledCalendar = styled(Calendar)`
  &.react-calendar {
    width: auto;
    border: 0;
    box-shadow: 2px 2px 10px rgba(114, 129, 236, 0.5);
    border-radius: 8px;
    .react-calendar__month-view__days__day--weekend:not(.react-calendar__month-view__days__day--neighboringMonth):not(.react-calendar__tile--active) {
      color: initial;
    }
    button {
      transition: background-color 200ms linear;
      &.react-calendar__tile:not(.react-calendar__tile--active):enabled {
        &:hover {
          background-color: rgba(0, 110, 220, 0.1);
        }
      }
    }
  }
`

export const StyledAvailableTimes = styled.div`
  margin: 20px 0;
`

export const StyledAvailableTime = styled.button`
  padding: 8px 15px;
  margin: 10px auto;
  border-radius: 5px;
  display: block;
  border: 1px solid #d4d4d4;
  transition: all 300ms linear;
  &:hover {
    border-color: ${ colors.blue };
  }
  ${ props => props.active ? `
    background-color: ${colors.blue};
    color: white;
  ` : ''}
  > span {
    color: #999;
    font-size: .75rem;
  }
`