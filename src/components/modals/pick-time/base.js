import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { DefaultButton, RequestedTime, OptionalAlert, SubmitButtonScale } from '../../common/elements'
import { ModalHeaderBlue, ModalBodyWhite } from '../styles'
import { StyledCalendar, StyledTimeSelection, StyledAvailableTimes, StyledAvailableTime } from './styles'
import { tomorrow } from './container'
import { timesRequired } from '../../../constants/appointments';

const PickTimeContent = ({
  closeModal,
  details,
  onChangeDate,
  onToggleTime,
  onRemoveTime,
  activeDate,
  isDayDisabled,
  availableTimes,
  alert,
  clearAlert
}) => {
  return (
    <ModalBodyWhite className="modal-content">
      <div className="modal-body">
        <ModalHeaderBlue>
        <h1><FontAwesomeIcon icon="calendar-alt" /> Pick Preferred Times</h1>
          <DefaultButton className="close" onClick={closeModal} aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </DefaultButton>
        </ModalHeaderBlue>
        <StyledTimeSelection className="modal-body-inner">
          <div className="column-calendar">
            <StyledCalendar
              calendarType="ISO 8601"
              onClickDay={onChangeDate}
              minDetail="year"
              minDate={tomorrow}
              tileDisabled={isDayDisabled}
              value={activeDate} />
            
            { availableTimes && 
              <StyledAvailableTimes>
                { availableTimes.map((t, i) => (
                  <StyledAvailableTime 
                    key={i}
                    active={t.selected}
                    onClick={() => onToggleTime(t.name, i)}>{ t.name } <span>({ t.description })</span></StyledAvailableTime>
                ))}
              </StyledAvailableTimes>
            }
          </div>
          <div className="column-chosen-dates">
            <p>Choose 3 times that work for you</p>
            <OptionalAlert alert={alert} className="text-center" clearAlert={clearAlert} />
            { details.map((time, i) => {
              return (
                <div key={i} className="chosen-tag">
                  <DefaultButton onClick={ () => onRemoveTime(i) } aria-label="Remove chosen time">
                    <FontAwesomeIcon icon="times" />
                  </DefaultButton>
                  <RequestedTime time={time} />
                </div>
              )
            })}
            { details.length >= timesRequired &&
              <div className="mt-2 text-center">
                <SubmitButtonScale onClick={closeModal}>Save</SubmitButtonScale>
              </div>
            }
          </div>
        </StyledTimeSelection>
      </div>
    </ModalBodyWhite>
  )
}

PickTimeContent.propTypes = {
  onChangeDate: PropTypes.func.isRequired,
  onToggleTime: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  activeDate: PropTypes.PropTypes.instanceOf(Date),
  availableTimes: PropTypes.array,
  details: PropTypes.array
}

export default PickTimeContent