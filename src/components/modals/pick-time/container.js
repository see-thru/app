import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from '../styles'
import PickTimeContent from './base'
import { createAlert, clearAlert } from '../../../utils/errors'
import { timesMax } from '../../../constants/appointments'
import { getAvailableTimesFromDetails, compareDates } from '../../../utils/dates';

const defaultTimes = [
  { name: 'Morning', description: '9am-12pm' },
  { name: 'Afternoon', description: '12pm-5pm' },
  { name: 'Evening', description: '5pm-8pm' }
]
const defaultAvailabilities = {
  0: defaultTimes, // Sunday
  1: defaultTimes,
  2: defaultTimes,
  3: defaultTimes,
  4: defaultTimes,
  5: defaultTimes,
  6: defaultTimes
}

// min date:
export const tomorrow = new Date() // use moment or dayjs?
tomorrow.setHours(0,0,0,0)
tomorrow.setDate(tomorrow.getDate() + 1)

class PickTimeModal extends Component {
  state = { alert: {} }

  static propTypes = {
    isOpen: PropTypes.bool,
    closeModal: PropTypes.func,
    details: PropTypes.array,
    onPickTime: PropTypes.func,
    totalTime: PropTypes.number,
    availabilities: PropTypes.object
  }

  static defaultProps = {
    availabilities: defaultAvailabilities
  }

  static getDerivedStateFromProps(nextProps, prevState){
    // if(nextProps.details !== prevState){ }
    // set up activeDate/availableTimes/details, based on availabilities etc
    const { availabilities, details = [] } = nextProps
    let date = new Date(tomorrow.getTime())
    console.log('getDerivedStateFromProps.activeDate', date.toISOString())
    return { 
      details,
      availableTimes: getAvailableTimesFromDetails(details, date, availabilities),
      activeDate: date
    }
  }

  onChangeDate = date => {
    this.clearAlert()
    const { details } = this.state
    const { availabilities } = this.props
    this.setState({ 
      availableTimes: getAvailableTimesFromDetails(details, date, availabilities),
      activeDate: date
    })
  }

  isDayDisabled = ({ date, view }) => {
    const { availabilities } = this.props
    return (view === 'month') && !((date >= tomorrow) && availabilities.hasOwnProperty(date.getDay()))
  }

  onToggleTime = (time, i) => { // max it out at three!
    this.clearAlert()
    let { details, activeDate, availableTimes } = this.state
    if(availableTimes[i].selected){
      details = details.filter(d => { // or do a find + splice ?
        return (!compareDates(d.date, activeDate)) || (d.time !== time)
      })
    } else {
      if(details.length >= timesMax){
        this.setState({ alert: createAlert('You have already selected three preferred times.') })
        return
      }
      details.push({ date: activeDate, time })
    }
    availableTimes[i].selected = !availableTimes[i].selected
    this.setState({ details, availableTimes })
  }

  onRemoveTime = (i) => {
    this.clearAlert()
    let { details, activeDate, availableTimes } = this.state
    const time = details[i]
    if(!time){ 
      console.error('Couldn‘t find that time?', i) // create alert
      return
    }
    details.splice(i, 1)
    if(compareDates(activeDate, time.date)){
      availableTimes.forEach(listedTime => {
        if(listedTime.name === time.time){
          listedTime.selected = false
        }
      })
    }
    this.setState({ details, availableTimes })
  }

  onSubmit = () => {
    const { onPickTime, closeModal } = this.props
    onPickTime(this.state.details)
    closeModal()
  }

  clearAlert = clearAlert.bind(this)

  render () {
    const { isOpen } = this.props
    const { availableTimes, activeDate, details, alert } = this.state
    return (
      <Modal
        isOpen={ isOpen }
        onRequestClose={ this.onSubmit }
        size="modal-lg"
        contentLabel="Pick Appointment Time" >
        <PickTimeContent
          activeDate={activeDate}
          availableTimes={availableTimes }
          details={ details }
          alert={ alert }
          clearAlert={this.clearAlert}
          onChangeDate={ this.onChangeDate }
          onToggleTime={ this.onToggleTime }
          onRemoveTime={ this.onRemoveTime }
          isDayDisabled={ this.isDayDisabled }
          closeModal={ this.onSubmit } />
      </Modal>
    )
  }
}

const mapStateToProps = (state) => ({
  availabilities: state.ProviderReducer.data.availabilities
})

export default connect(
  mapStateToProps
)(PickTimeModal)