import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from '../styles'
import SignUpContent from './content'
import { toggleSignUpModal, toggleSignInModal } from '../../../actions/auth'
import { signup } from '../../../actions/auth'

export class SignUpModalContainer extends Component {
  constructor () {
    super()
    this.onSubmit = this.onSubmit.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.showSignInModal = this.showSignInModal.bind(this)
  }

  onSubmit (e) {
    this.props.signup(e)
  }

  closeModal () {
    this.props.toggleSignUpModal(false)
  }

  showSignInModal () {
    this.closeModal()
    this.props.toggleSignInModal(true)
  }

  render () {
    const { isOpen, errors, loading } = this.props
    
    return (
      <Modal
        isOpen={ isOpen }
        onRequestClose={this.closeModal}
        contentLabel="Sign Up">
        <SignUpContent
          errors={ errors }
          loading={ loading }
          onSubmit={ this.onSubmit }
          showSignInModal={ this.showSignInModal } />
      </Modal>
    )
  }
}

SignUpModalContainer.propTypes = {
  isOpen: PropTypes.bool,
  errors: PropTypes.object,
  signup: PropTypes.func.isRequired,
  toggleSignUpModal: PropTypes.func.isRequired,
  toggleSignInModal: PropTypes.func.isRequired,
  loading: PropTypes.bool
}

const mapStateToProps = (state) => ({
  isOpen : state.AuthReducer.signUpModalIsOpen,
  errors : state.AuthReducer.errors,
  loading: state.AuthReducer.loading
})

export default connect(
  mapStateToProps,
  { 
    signup,
    toggleSignUpModal,
    toggleSignInModal
  }
)( SignUpModalContainer )