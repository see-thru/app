import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { date, email, length, required, confirmation } from 'redux-form-validators'
import { FormGroup, Label } from 'reactstrap'
import { Input, FormWrapper, BootstrapInput } from '../../common/form'

let SignupForm = ({
  hasPasswordValue,
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper id="signUpForm" onSubmit={ handleSubmit( onSubmit ) }>
    <Field
      title="Email address"
      placeholder="Email address"
      name="email"
      component={ Input }
      type="email"
      validate={ [
        email(),
        length({ max: 255 }),
        required()
      ] }
    />
    <Field
      title="Invite code"
      placeholder="Invite code"
      name="inviteCode"
      component={ Input }
      validate={ [
        required()
      ] }
    />
    <Field
      title="First name"
      placeholder="First name"
      name="firstName"
      component={ Input }
      validate={ [
        required()
      ] }
    />
    <Field
      title="Last name"
      placeholder="Last name"
      name="lastName"
      component={ Input }
      validate={ [
        required()
      ] }
    />    
    <Field
      title="Password"
      placeholder="Password"
      name="password"
      component={ Input }
      type="password"
      validate={ [
        length({ minimum: 6 }),
        required()
      ] }
    />
    { hasPasswordValue &&
      <Field
        title="Password"
        placeholder="Confirm Password"
        name="confirm"
        component={ Input }
        type="password"
        validate={ [
          required(),
          confirmation({ field: 'password' })
        ] }
      />
    }
    <Field
      title="Date of Birth"
      name="dob"
      placeholder="MM/DD/YYYY"
      component={ Input }
      validate={ [
        date({ format: 'mm/dd/yyyy' }),
        required(),
      ] }
    />
    <Field
      title="Phone Number"
      placeholder="Phone number"
      name="phone"
      component={ Input }
      validate={ [
        required()
      ] }
    />
    <FormGroup check>
      <Label check>
        <Field
          name="remember"
          type="checkbox"
          component={ BootstrapInput } /> Remember me
      </Label>
    </FormGroup>
  </FormWrapper>
)

SignupForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

SignupForm = reduxForm({
  form: 'signUpForm'
})( SignupForm )

const selector = formValueSelector('signUpForm')
SignupForm = connect(state => {
  const hasPasswordValue = selector(state, 'password')
  return {
    hasPasswordValue
  }
})(SignupForm)

export default SignupForm