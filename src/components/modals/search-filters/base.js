import React from 'react'
import PropTypes from 'prop-types'
import SearchForm from '../../search/form/container'

import { ModalWhite, ModalHeader } from './styles'

const FiltersContent = ({ history, onSubmit }) => {
  return (
    <ModalWhite className="modal-content">
      <div className="modal-body">
        <ModalHeader>
          <h1>Search Filters</h1>
        </ModalHeader>
        <SearchForm history={ history } afterSubmit={ onSubmit } />
      </div>
    </ModalWhite>
  )
}

FiltersContent.propTypes = {
  history: PropTypes.object,
  onSubmit: PropTypes.func
}

export default FiltersContent