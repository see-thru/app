import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Modal from '../styles'
import FiltersContent from './base'

export class SearchFiltersModal extends Component {
  onSubmit = () => {
    this.props.closeModal()
  }
  
  render () {
    const { history, isOpen, closeModal } = this.props
    return (
      <Modal
        isOpen={ isOpen }
        onRequestClose={closeModal}
        contentLabel="Search Filters" >
        <FiltersContent
          history={ history }
          onSubmit={ this.onSubmit } />
      </Modal>
    )
  }
}

SearchFiltersModal.propTypes = {
  // component
  history: PropTypes.object,
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func // how to close modal when the submit button is pressed?
}

export default SearchFiltersModal