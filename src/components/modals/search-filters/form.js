import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { injectStripe, CardElement } from 'react-stripe-elements'
import PatientAgreement from './patient-agreement'
import { FormWrapper, ErrorMessage } from '../../common/form'
import { StyledPayment, ModalFooter, StyledButton } from './styles'

class AppointmentForm extends Component {
  constructor() {
    super()
    this.state = {}
  }

  toggleAgree = e => {
    this.setState({ patientAgree: e.target.checked || false })
  }

  handleChange = change => {
    if(change.error && change.error.message){
      this.setState({ cardError: change.error.message })
    } else {
      this.setState({ cardError: undefined })
    }
  }

  handleSubmit = ev => {
    const { totalCost, onSubmit, stripe } = this.props
    ev.preventDefault();
    if(!totalCost){
      onSubmit()
    } else {
      stripe.createSource({
        type: 'card',
        usage: 'reusable'
      }).then(result => {
        if(result.source){
          onSubmit(result.source)
        } else {
          console.error(result.error)
          const cardError = (result.error && result.error.message) || 'There was an error processing your card.'
          this.setState({ cardError })
        }
      }).catch(error => {
          console.error(error)
          this.setState({ cardError: 'There was an error processing your card.' })
        })
    }
  }

  render() {
    const { cardError, patientAgree } = this.state
    const { totalCost } = this.props
    return (
      <FormWrapper onSubmit={ this.handleSubmit }>
        <PatientAgreement toggleAgree={ this.toggleAgree } />
        { !!totalCost && 
          <StyledPayment>
            { cardError && 
              <ErrorMessage>{ cardError }</ErrorMessage>
            }
            <CardElement 
              onChange={ this.handleChange }
              onReady={() => {}}
              onBlur={() => {}}
              onFocus={() => {}}
              style={{base: {fontSize: '1rem'}}} />
          </StyledPayment>
        }
        <ModalFooter>
          <div></div>
          <StyledButton type="submit" disabled={!patientAgree || cardError}>Book Appointment</StyledButton>
        </ModalFooter>
      </FormWrapper>
    )
  }
}

AppointmentForm.propTypes = {
  totalCost: PropTypes.number,
  onSubmit: PropTypes.func.isRequired
}

export default injectStripe(AppointmentForm)