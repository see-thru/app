import styled from 'styled-components'
import { Alert } from 'reactstrap'
import { colors } from '../../../utils/settings'
import { MaxWidth } from '../../../utils/media'
import { SubmitButtonScale } from '../../common/elements'

export const ModalWhite = styled.div`
  .modal-body {
    padding: 0;
    font-size: .9rem;
    line-height: 1.3rem;
    
    .modal-body-inner {
      padding: 10px 25px;
    }
    svg {
      margin-right: 5px;
    }
  }
`

export const ModalHeader = styled.div`
  background-color: ${ colors.lightBlue};
  color: white;
  padding: 17px 25px 12px;
  h1 {
    font-size: 1rem;
    letter-spacing: 1px;
    font-weight: 600;
    margin: 0;
  }
`

export const StyledPayment = styled.div`
  border-top: 1px solid ${ colors.borderGray};
  padding: 15px 0 3px;
  margin-top: 10px;
  .StripeElement {
    display: block;
    margin: 0;
    padding: 15px;
    border: 0;
    outline: 0;
    background: #eee;
    border-radius: 3px;
    height: 46px;
    overflow: hidden;
  }
`

export const ModalFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 10px;
  border-top: 1px solid #eee;
  padding: 10px 0 0;
  text-align: center;
  ${ MaxWidth(500)`
    display: block;
    > button {
      margin: 7px auto;
    }
  `}
`

export const StyledButton = styled(SubmitButtonScale)`
  display: inline-block;
  margin: 0;
`

export const StyledAlert = styled(Alert)`
  margin-top: 10px;
`
