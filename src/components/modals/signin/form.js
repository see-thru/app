import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'
import { email, length, required } from 'redux-form-validators'
import { FormGroup, Label } from 'reactstrap'
import { Input, FormWrapper, BootstrapInput } from '../../common/form'

const Form = ({
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper id="signInForm"
    onSubmit={ handleSubmit( onSubmit ) }>
    <Field
      title="Email address"
      placeholder="Email address"
      name="email"
      component={ Input }
      type="email"
      validate={ [
        email(),
        length({ max: 255 }),
        required()
      ] }
    />
    <Field
      title="Password"
      placeholder="Password"
      name="password"
      component={ Input }
      type="password"
      validate={ [
        required()
      ] }
    />
    <FormGroup check>
      <Label check>
        <Field
          title="Remember Me"
          name="rememberMe"
          type="checkbox"
          component={ BootstrapInput } /> Remember me
      </Label>
    </FormGroup>
  </FormWrapper>
)

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

export default reduxForm({
  form : 'signInForm'
})( Form )