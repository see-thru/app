import React from 'react'
import PropTypes from 'prop-types'
import Form from './form'
import { ErrorMessage } from '../../common/form'
import { Loader, SubmitButton } from '../../common/elements'
import { ModalBlue, ModalHeader, ModalFooter, ModalLink } from '../styles'

const SignInContent = ({
  errors,
  loading,
  onSubmit,
  showSignUpModal,
  showForgotPasswordModal
}) => (
  <ModalBlue className="modal-content">
    { loading ? <Loader /> : undefined }
    <div className="modal-body">
      <ModalHeader>
        <h1>Sign In</h1>
        <img 
          src="/images/logo-white.png" 
          alt="Seethru Logo"
          height="40" />
      </ModalHeader>
      <div className="modal-content-text">
        <p>Book your transparent care now.</p>
      </div>
      { errors.signin && <ErrorMessage lightColor>{ errors.signin }</ErrorMessage> }
      <Form onSubmit={ onSubmit } />
      <ModalFooter>
        <div>
          <p>Not yet registered? <ModalLink onClick={showSignUpModal}>Sign up</ModalLink></p>
          <ModalLink onClick={showForgotPasswordModal}>Forgot password?</ModalLink>
        </div>
        <SubmitButton type="submit" form="signInForm">Sign In</SubmitButton>
      </ModalFooter>
    </div>
  </ModalBlue>
)

SignInContent.propTypes = {
  errors: PropTypes.object,
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  showSignUpModal: PropTypes.func.isRequired,
  showForgotPasswordModal: PropTypes.func.isRequired
}

export default SignInContent