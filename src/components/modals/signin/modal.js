import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from '../styles'
import { login } from '../../../actions/auth'
import { toggleSignInModal, toggleSignUpModal, toggleForgotPasswordModal } from '../../../actions/auth'
import SignInContent from './content'

export class SignInModal extends Component {
  closeModal = () => {
    this.props.toggleSignInModal(false)
  }

  showSignUpModal = () => {
    this.closeModal() // this is done automatically by the action, right?
    this.props.toggleSignUpModal(true)
  }

  showForgotPasswordModal = () => {
    this.closeModal() // this is done automatically by the action
    this.props.toggleForgotPasswordModal(true)
  }

  render () {
    const { errors, isOpen, loading, login } = this.props
    return (
      <Modal
        isOpen={ isOpen }
        onRequestClose={this.closeModal}
        contentLabel="Sign In">
        <SignInContent
          errors={ errors }
          loading={ loading }
          onSubmit={ login }
          showSignUpModal={ this.showSignUpModal }
          showForgotPasswordModal={ this.showForgotPasswordModal } />
      </Modal>
    )
  }
}

SignInModal.propTypes = {
  errors: PropTypes.object,
  isOpen: PropTypes.bool,
  loading: PropTypes.bool,
  login: PropTypes.func.isRequired,
  // location: PropTypes.object.isRequired,
  toggleSignInModal: PropTypes.func.isRequired,
  toggleSignUpModal: PropTypes.func.isRequired,
  toggleForgotPasswordModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  errors: state.AuthReducer.errors,
  isOpen: state.AuthReducer.signInModalIsOpen,
  loading: state.AuthReducer.loading
})

export default connect(
  mapStateToProps,
  { 
    login,
    toggleSignInModal, 
    toggleSignUpModal,
    toggleForgotPasswordModal
  }
)( SignInModal )