import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from '../styles'
import BookAppointmentContent from './base'
import { bookAppointment } from '../../../actions/appointments'

export const PAGES = {
  booked: 'booked',
  checkout: 'checkout'
}

export class BookAppointmentModal extends Component {
  state = { page: PAGES.checkout }

  onSubmit = stripeSource => {
    const { bookAppointment, appointment, onBookAppointment } = this.props
    bookAppointment(appointment, stripeSource).then(appointmentId => {
      if(appointmentId){
        onBookAppointment()
        this.setState({ page: PAGES.booked, appointmentId })
      }
    })
  }

  render () {
    const { isOpen, appointment, error, loadingBooking, closeModal } = this.props
    const { page, appointmentId } = this.state
    return (
      <Modal
        isOpen={ isOpen }
        contentLabel="Book Appointment" >
        <BookAppointmentContent
          appointment={ appointment }
          error={ error }
          loadingBooking={ loadingBooking }
          page={ page }
          appointmentId={ appointmentId }
          closeModal={closeModal}
          onSubmit={ this.onSubmit } />
      </Modal>
    )
  }
}

BookAppointmentModal.propTypes = {
  // component
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  appointment: PropTypes.object,
  onBookAppointment: PropTypes.func,
  // state
  error: PropTypes.string,
  loadingBooking: PropTypes.bool,
  // actions
  bookAppointment: PropTypes.func
}

// why does this need to be in the redux state. it's only used by the current component
const mapStateToProps = (state) => ({ 
  error: state.AppointmentsReducer.booking.error,
  loadingBooking: state.AppointmentsReducer.booking.loading
})

export default connect(
  mapStateToProps,
  { bookAppointment }
)(BookAppointmentModal)