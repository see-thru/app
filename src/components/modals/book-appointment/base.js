import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Elements, StripeProvider } from 'react-stripe-elements'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { STRIPE_API_KEY } from '../../../constants/stripe'

import AppointmentInfo from '../../appointments/info/base'
import AppointmentForm from './form'
import { Loader, DefaultButton } from '../../common/elements';
import { ModalFooter, StyledButton, StyledAlert } from './styles'
import { ModalBodyWhite, ModalHeaderBlue } from '../styles'
import { PAGES } from './container'

const BookAppointmentContent = ({
  appointment,
  error,
  loadingBooking,
  page,
  appointmentId,
  onSubmit,
  closeModal
}) => {
  // TODO: Lazy-load Stripe - https://github.com/stripe/react-stripe-elements#advanced-integrations
  // can also pass in onCheckIn, and paymentDetails. otherwise, it's static
  return (
    <ModalBodyWhite className="modal-content">
      <div className="modal-body">
        { loadingBooking && <Loader /> }
        <ModalHeaderBlue>
          { page === PAGES.checkout && <h1>Checkout Details</h1> }
          { page === PAGES.booked && 
            <h1 className="text-center"><FontAwesomeIcon icon={['far', 'calendar-check']} /> Appointment Booked!</h1>
          }
          <DefaultButton className="close" onClick={closeModal} aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </DefaultButton>
        </ModalHeaderBlue>
        <div className="modal-body-inner">
          <AppointmentInfo appointment={ appointment } />
          { page === PAGES.checkout && 
            <StripeProvider apiKey={ STRIPE_API_KEY }>
              <Elements>
                <AppointmentForm totalCost={ appointment.totalCost } onSubmit={ onSubmit } />
              </Elements>
            </StripeProvider>
          }
          { page === PAGES.booked && 
            <ModalFooter>
              <StyledButton tag={Link} to={`/appointments`}>My Appointments</StyledButton>
              { false && appointmentId &&
                <div className="text-center">
                  <StyledButton tag={Link} to={`/appointments/${appointmentId}/check-in`}>
                    Check In
                  </StyledButton>
                </div>
              }
            </ModalFooter>
          }
          { error && <StyledAlert color="danger">{ error }</StyledAlert>}
        </div>
      </div>
    </ModalBodyWhite>
  )
}

BookAppointmentContent.propTypes = {
  appointment: PropTypes.object,
  error: PropTypes.string,
  loadingBooking: PropTypes.bool,
  page: PropTypes.string,
  appointmentId: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired
}

export default BookAppointmentContent