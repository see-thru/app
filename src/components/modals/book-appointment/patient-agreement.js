import React from 'react'
import { FormGroup, Label, Input, UncontrolledCollapse } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'
import { colors } from '../../../utils/settings'

const StyledPatientAgreement = styled.div`
  margin-top: 10px;
  padding: 15px 0 10px;
  border-top: 1px solid ${ colors.borderGray };
`
const PatientAgreement = ({ toggleAgree }) => {
  return (
    <StyledPatientAgreement>
      <p className="toggle-row" id="toggleAgreement" aria-label="Click to toggle agreement open">
        <span className="section-label">Patient Agreement</span>
        <FontAwesomeIcon icon="chevron-down" />
      </p>
      <UncontrolledCollapse toggler="#toggleAgreement">
        <div>
          <p>1. I understand and agree that this healthcare transaction is an out-of-pocket expense and IS NOT eligible for insurance reimbursement and any money I spend WILL NOT count toward my deductible.</p>
          <p>2. I understand and agree that I CANNOT and WILL NOT submit a claim to my healthcare insurance provider, Medicare, or Medicaid. Doing so may compromise my membership and coverage.</p>
          <p>3. HIPAA Privacy Rule: By utilizing the SeeThru Platform I have enacted my rights under the HIPAA Privacy Rule (45 CFR Parts 160 & 164, Subparts A and E “Right to Restrict Disclosure”) to withhold from third party payors all information about services rendered and details on compensation made via the Platform. Therefore, I nor the Provider shall report or submit bills to third party payors regarding services in connection with this Agreement. Provider will maintain documentation of these instructions regarding the restricting of disclosure to insurance companies as described herein.</p>
          <p>4. Medicare: This agreement stands as a private contract between the Provider and the patient stating that neither one can receive payment from Medicare for the services that were performed.</p>
        </div>
      </UncontrolledCollapse>
      <FormGroup check>
        <Label check>
          <Input type="checkbox" onChange={ toggleAgree } />{' '}
          I accept the Patient Agreement
        </Label>
      </FormGroup>
    </StyledPatientAgreement>
  )
}

export default PatientAgreement