import styled from 'styled-components'
import { colors, values } from '../../utils/settings'
import Media, { MaxWidth, breakpoints } from '../../utils/media'

export const StyledFooterContainer = styled.div`
  display: flex;
  flex-direction: column;

  ${ MaxWidth(breakpoints.collapseFooter)`
    .footer-content {
      display: block;
      .footer-contact { 
        margin-top: 20px;
      }
    }
  `}
`

export const StyledWarning = styled.div`
  background: #eee;
  color: ${ colors.bgFooter }
  padding: 15px 5px;
  text-align: center;
  font-size: 1.1rem;
  line-height: 1.2rem;
  box-shadow: 0px -1px 1px rgba(0,0,0,.2);
  p { 
    margin: 0;
    line-height: 1.6rem;
  }
`

export const StyledFooter = styled.footer`
  background-color: ${ colors.bgFooter };
  color: white;
  flex: 1;
  display: flex;
  align-items: center;
  padding: 15px 10px;

  .footer-content {
    flex: 1;
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 1300px;
    margin: 0 auto;

    .footer-contact {
      flex: 1 0 200px;
    }
  }
  ${ MaxWidth(breakpoints.collapseFooter)`
    position: static;
    height: auto;
    .footer-content {
      display: block;
      .footer-contact { 
        margin-top: 20px;
      }
    }
  `}
`;

export const StyledLinksList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
    
  li {
    display: inline-block;
    margin: 2px 1%;
    vertical-align: top;
    width: 31%;
    ${
      MaxWidth(500)`
        margin: 2px 2%;
        width: 46%;
      `
    }
    a {
      color: white;
      text-decoration: none;
      font-size: .9rem;
      transition: .2s color;
      &:hover {
        color: ${ colors.lightBorder };
      }
    }
  }
`

export const Copyright = styled.p`
  color: ${ colors.textFooter };
  font-size: .9rem;
  margin: 10px 0 0;
  text-align: right;
  ${
    Media.medium`
      text-align: center;
    `
  }
`
