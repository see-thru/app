import React from 'react'
import { StyledFooterContainer, StyledWarning } from './styles'

const year = new Date().getFullYear()
const Footer = () => (
  <StyledFooterContainer>
    <StyledWarning>
      <p>In the event of a medical emergency, dial 9-1-1 or visit your closest emergency room immediately.</p>
      <p>Copyright © { year } - SeeThru</p>
    </StyledWarning>
    { /**
      <StyledFooter>
        <div className="footer-content">
          <StyledLinksList>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/working-with-us">Work With Us</Link></li>
            <li><Link to="/terms">Terms of Service</Link></li>
            <li><Link to="/about-us">About Us</Link></li>
            <li><Link to="/faq">FAQ</Link></li>
            <li><Link to="/privacy-policy">Privacy Policy</Link></li>
            <li><Link to="/why-seethru">Why SeeThru</Link></li>
            <li>
              <a rel="noopener noreferrer"
                href="https://blog.seethru.healthcare/"
                target="_blank">
                Blog
              </a>
            </li>
            <li><Link to="/site-map">Site Map</Link></li>
          </StyledLinksList>
          <div className="footer-contact">
            <SocialNetworks />
            <Copyright>
              Copyright © 2018 - SeeThru
            </Copyright>
          </div>
        </div>
      </StyledFooter>
    */ }
  </StyledFooterContainer>
)

export default Footer