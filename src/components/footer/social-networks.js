import React from 'react'

import styled from 'styled-components'
import { Icon } from '../common/elements'
import Media from '../../utils/media'

const StyledSocialNetworks = styled.div`
  text-align: right;
  ${
    Media.medium`
      text-align: center;
    `
  }
`

const SIZE_ICONS = '36px'

const Link = styled.a`
  border-radius: 100%;
  height: ${ SIZE_ICONS };
  display: inline-block;
  margin: 0 5px;
  text-align: center;
  padding: 6px 0;
  vertical-align: top;
  width: ${ SIZE_ICONS };
  transition: opacity .2s linear;
  &:focus, &:hover {
    opacity: .6;
  }
`

const FILL_ICONS = '#FFF'

const SocialNetworks = () => {
  return (
    <StyledSocialNetworks>
      <Link 
        href="https://twitter.com/SeeThruHealth" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="twitter"
          width="30px"
          height="30px" />
      </Link>
      <Link 
        href="https://www.facebook.com/SeeThruHealth/" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="facebook"
          width="30px"
          height="30px" />
      </Link>
      <Link 
        href="https://www.linkedin.com/company/seethru/" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="linkedin" 
          width="30px"
          height="30px" />
      </Link>
      <Link
        href="mailto:team@seethru.heatlhcare" 
        target="_blank">
        <Icon 
          fill={ FILL_ICONS }
          name="email"
          width="30px"
          height="30px" />
      </Link>
    </StyledSocialNetworks>
  )
}

export default SocialNetworks