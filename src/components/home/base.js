import React from 'react'
import styled from 'styled-components'
import Banner from './banner/base'
import AboutSeeThru from './landing-page/container'

const StyledHome = styled.div`
  background-color: #2b465a;
  flex: 1;
`

const Home = (props) => (
  <StyledHome>
    <Banner { ...props } /> 
    { props.isLogged || true ||
      <AboutSeeThru />
    }
  </StyledHome>
)

export default Home