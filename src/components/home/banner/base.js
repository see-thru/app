import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import SearchForm from '../../search/form/container'
import { StyledBanner } from './styles'

export const Banner = ({ history }) => (
  <StyledBanner>
    <div className="banner-grid">
      <div className="banner-content">
        <h1>Transparent <br />Healthcare</h1>
        <h2>Not Feeling Well? Need a Doctor? <br />Find guaranteed quality and prices for your care.</h2>
        <SearchForm history={ history } />
      </div>
      <img src="/images/home/hero-graphic2.png" alt="" />
    </div>
  </StyledBanner>
)

Banner.propTypes = {
  history: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({})	

export default connect(	
  mapStateToProps,	
  {}	
)( Banner ) 