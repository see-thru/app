import styled from 'styled-components'
import Media, { MaxWidth } from '../../../utils/media';

export const StyledBanner = styled.div`
  background: url('/images/home/hero-bg.png') bottom no-repeat #00adee;
  background-size: cover;
  border-bottom: 25px solid #304658;
  position: relative;
  padding-top: 80px;
  color: white;

  .banner-grid {
    display: grid;
    grid-gap: 0;
    grid-template-rows: auto;
    grid-template-columns: 50% 50%;
    max-width: 1300px;
    margin-left: auto;
    margin-right: auto;
    
    .banner-content {
      padding: 30px 0 0 20px;
      margin-left: auto;
      h1 {
        font-family: 'Montserrat', sans-serif;
        font-weight: 800;
        font-size: 4.3rem;
        line-height: 4.6rem;
        text-shadow: 1px 1px rgba(0,0,0,0.4);
      }
      h2 {
        font-family: 'Montserrat', sans-serif;
        font-weight: 700;
        font-size: 1.3rem;
        line-height: 1.8rem;
        margin: 0;
        text-shadow: 1px 1px rgba(0,0,0,0.4);
      }
    }
    img {
      max-width: 607px;
      top: 15px;
      position: relative;
      width: 100%;
      align-self: flex-end;
    }
  }
  
  ${
    Media.large`
      padding-top: 40px;
      .banner-grid {
        img {
          max-width: 530px;
          margin-left: auto;
        }
      }
    `
  }
  ${
    Media.medium`
      padding-top: 30px;
      .banner-grid {
        .banner-content {
          h1 {
            font-size: 4rem;
            line-height: 4.2rem;
          }
          h2 {
            font-size: 1.1rem;
            line-height: 1.4rem;
          }
        }
        img {
          min-height: 430px;
          object-fit: cover;
          object-position: bottom left;
        }
      }
    `
  }
  ${
    MaxWidth(900)`
      background-image: url('/images/home/hero-bg-mobile.png');

      .banner-grid {
        grid-template-columns: 1fr;
        img {
          margin: 0 auto;
          min-height: initial;
          max-width: 400px;
        }
        .banner-content {
          margin: 0 auto;
          padding: 0 10px;
          text-align: center;
        }
      }
    `
  }
  ${
    MaxWidth(500)`
      .banner-grid {
        .banner-content {
          h1 { 
            font-size: 2.4rem;
            line-height: 2.5rem;
          }
          h2 {
            font-size: 0.9rem;
            line-height: 1.2rem
          }
        }
      }
    `
  }
`

export const StyledContainerGridBanner = styled.div`
  display: grid;
  grid-gap: 0;
  grid-template-rows: auto;
  grid-template-columns: 47% 53%;
  max-width: 1200px;
  margin-left: auto;
  padding: 0 20px;
  ${
    Media.small`
      grid-template-columns: 1fr;
    `
  }
  .banner-content {
    padding: 32px 15px;
    ${
      Media.small`
        text-align: center;
      `
    }
  }
  img {
    max-width: 607px;
    top: 15px;
    position: relative;
    width: 100%;
    ${
      Media.large`
        max-width: 400px;
        top: 90px;
      `
    }
    ${
      Media.medium`
        top: 90px;
      `
    }
    ${
      Media.small`
        max-width: 280px;
        top: 50px;
      `
    }
  }
`

export const StyledTitle = styled.h1`

`

export const StyledSubtitle = styled.h2`
  font-family: 'Montserrat', sans-serif;
  font-weight: 700;
  font-size: 1.2rem;
  line-height: 1.8rem;
  margin: 0;
  ${ 
    Media.medium`
      font-size: 1.4rem;
    `
  }
  ${ 
    Media.small`
      font-size: 1.2rem;
    `
  }  
`