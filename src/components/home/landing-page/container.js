import React, { Fragment } from 'react'

import { GridContainer, GridItem } from '../../common/layout'
import { SubmitButton } from '../../common/elements'
import Title from './title/base'
import Box from './box/base'
import { StyledSection } from './styles'

const AboutSeeThru = () => (
  <Fragment>
    <StyledSection className="section-how">
      <GridContainer
        gridGap="0 20px"
        templateColumns="repeat(2, 1fr)">
        <GridItem alignSelf="end">
          <img src="/images/home/how-we-do.png" alt="" />
        </GridItem>
        <GridItem alignSelf="end">
          <Title title="How We Do It" />
          <p>SeeThru is a blockchain-enabled platform that prioritizes price transparency in order to change the world of healthcare payments. Our ecosystem brings patients the sorely needed value-based consumer experience found in every other service industry.</p>
          <p>SeeThru utilizes smart contracts for direct pricing with providers. In doing so, SeeThru removes third parties, which drives down patient costs, increases provider revenue, and improves access to care around the world.</p>
        </GridItem>
      </GridContainer>
    </StyledSection>
    <StyledSection className="section-stories">
      <Title title="Everybody Wins with Seethru"/>
      <GridContainer
        gridGap="0 20px"
        templateRows="auto"
        templateColumns="repeat(2, 1fr)">
        <div>
          <h4>Meet Joe: The Patient Story</h4>
          <ul>
            <li>Avoid Wait Times</li>
            <li>Know what you’re gonna pay</li>
            <li>Get care today</li>
          </ul>
          <video 
            poster="/images/home/video-placeholder1.jpg"
            controls>
            <source src="/videos/seethru-joe.mp4" />
          </video>
        </div>
        <div>
          <h4>Meet Dr. Jill: The Provider Story</h4>
          <ul>
            <li>Happy patients</li>
            <li>Optimize your schedule</li>
            <li>Avoid billing hassles</li>
          </ul>
          <video 
            poster="/images/home/video-placeholder2.jpg"
            controls>
            <source src="/videos/jills-animated-story-v3.mp4" />
          </video>
        </div>
      </GridContainer>
    </StyledSection>
    <StyledSection className="section-platform">
      <Title 
        title="The SeeThru Platform" 
        subtitle="SeeThru helps patients by providing a value-based consumer experience in the healthcare industry." />
      <GridContainer
        gridGap="20px"
        templateRows="auto"
        templateColumns="repeat(3, 1fr)"
        className="platform-boxes"> 
        <Box
          icon="/images/home/graphic2.png"
          title="Creates"
          description="a marketplace for both patients & providers" />
        <Box 
          icon="/images/home/graphic3.png"
          title="Enables"
          description="instant payment reducing cost & waste" />        
        <Box 
          icon="/images/home/graphic4.png"
          title="Utilizes"
          description="the benefits of blockchain tech" />                
      </GridContainer>
    </StyledSection>
    <StyledSection className="subscribe-newsletter">
      <Title
        title="Keep me posted about SeeThru" 
        subtitle="We are hard at work building the marketplace. Sign up and be the first to know when we are live!" />
      <SubmitButton>Subscribe to our Newsletter</SubmitButton>
    </StyledSection>
  </Fragment>
)

export default AboutSeeThru