import styled from 'styled-components'

import Media from '../../../utils/media'
import { fonts, colors } from '../../../utils/settings'

export const StyledSection = styled.div`
  padding: 40px 20px;
  text-align: center;

  > div {
    max-width: 1150px;
    margin: 0 auto;
  }

  &.section-how {
    * {
      text-align: left;
    }
  }

  &:not(.section-how) {
    background: white;
  }

  &.section-stories {
    h4 {
      color: ${ colors.darkBlue };
      font-family: 
      font-size: 1.6rem;
      font-weight: ${ fonts.weightBold };
      margin: 0 0 10px;
      text-align: center;
      ${
        Media.medium`
          font-size: 1rem;
        `
      }
    }
    ul {
      color: ${ colors.textLight };
      font-size: 1.2rem;
      list-style-position: inside;
      margin: 20px 0;
      text-align: center;
      padding: 0;
      ${
        Media.medium`
          font-size: 1rem;
        `
      }
      ${
        Media.small`
          text-align: left;
          padding: 0 10px;
        `
      }
    }
    video {
      ${
        Media.small`
          margin-bottom: 20px;
        `
      }
    }
  }

  &.section-platform {
    .platform-boxes {
      max-width: 1000px;
    }
  }
`