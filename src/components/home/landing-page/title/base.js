import React from 'react'
import PropTypes from 'prop-types'

import { StyledTitle } from './styles'

const Title = ({
  title,
  subtitle
}) => (
  <StyledTitle>
    <h3>{ title }</h3>
    { subtitle && 
      <p>{ subtitle }</p> 
    }
  </StyledTitle>
)

Title.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string
}

export default Title