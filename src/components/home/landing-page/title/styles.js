import styled from 'styled-components'
import { colors } from '../../../../utils/settings'
import { MaxWidth } from '../../../../utils/media'

export const StyledTitle = styled.div`
  text-align: center;
  margin-bottom: 25px;

  h3 {
    color: ${ colors.darkBlue };
    font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    font-size: 2.8rem;
    margin-bottom: 15px;
  }
  p {
    max-width: 515px;
    margin-left: auto;
    margin-right: auto;
    font-size: 1.1rem;
  }
  ${ MaxWidth(800)`
    h3 {
      font-size: 2.5rem;
    }
    p {
      font-size: 1rem;
    }
  `}
`