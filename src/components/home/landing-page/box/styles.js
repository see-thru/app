import styled from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../../utils/media'
import { colors } from '../../../../utils/settings'

const getBoxImage = name => {
  switch(name){
    case 'Creates': return '/images/home/graphic5.png';
    case 'Enables': return '/images/home/graphic6.png';
    case 'Utilizes': default: return '/images/home/graphic7.png';
  }
}
const getBoxColor = name => {
  switch(name){
    case 'Creates': return colors.homeBoxCreates;
    case 'Enables': return colors.homeBoxEnables;
    case 'Utilizes': default: return colors.homeBoxUtilizes;
  }
}
const StyledBox = styled.div`
  background: ${ props => `url(${ getBoxImage(props.boxName) })` };
  background-position: bottom;
  background-repeat: no-repeat;
  background-size: cover;
  border: 0;
  box-shadow: 0px 1px 6px -1px rgba(0,0,0,.5);
  border-radius: 10px;
  padding-bottom: 300px;
  text-align: center;
  ${
    Media.small`
      background-repeat: repeat-x;
      background-size: contain;
      padding-bottom: 250px;
    `
  }
  ${
    Media.small`
      padding-bottom: 200px;
    `
  }
  h4 {
    color: ${ props => getBoxColor(props.boxName) };
    font-size: 1.8rem;
    margin: 10px 0 0;
    text-transform: uppercase;
    ${
      Media.medium`
        font-size: 1.2rem;
      `
    }
  }
  p {
    color: ${ colors.textLight };
    font-size: 1rem;
    padding: 5px 10px;
    max-width: 200px;
    line-height: 1.3rem;
    margin: 0 auto;
  }
  img {
    width: 100%;
    &:first-of-type {
      margin-top: 40px;
      max-width: 150px;
      ${
        Media.medium`
          max-width: 120px;
        `
      }
      ${
        Media.small`
          max-width: 80px;
        `
      }      
    }
  }
`

StyledBox.propTypes = {
  boxName: PropTypes.string.isRequired
}

export default StyledBox