import React from 'react'
import { connect } from 'react-redux'
import Home from './base'
import isFeatureEnabled from '../../utils/permission/is-feature-enabled'
import HasPermissionHOC from '../has-permission'
import {toggleSignInModal} from '../../actions/auth'

export const HomeContainer = ({
  location,
  toggleSignInModal,
  history,
  isLogged
}) => {
  if(location.hash === '#signin'){
    toggleSignInModal(true)
    history.push({ hash: '' }) // does container rerender? I don't think so...
  }
  return (
    <Home history={ history } isLogged={ isLogged } />
  )
}

const mapStateToProps = state => ({
  isLogged: state.AuthReducer.isLogged
})

export default HasPermissionHOC(
  connect(
    mapStateToProps,
    { toggleSignInModal }
  )(HomeContainer), 
  isFeatureEnabled(window.location.href).pages.home
)