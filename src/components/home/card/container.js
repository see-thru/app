import React from 'react'
import PropTypes from 'prop-types'

import Figure from './figure'
import StyledJob from './styled-job'
import StyledCard from './styled-card'
import SocialNetworks from './social-networks'

const Card = ({
  member
}) => (
  <StyledCard>
    <Figure 
      name={ member.name }
      avatar={ member.avatar } />
    <StyledJob 
      children={ member.job } />
    <SocialNetworks 
      socialNetworks={ member.socialNetworks } />
  </StyledCard>  
)

const socialNetworksPropTypes = PropTypes.shape({
  type: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
})

Card.propTypes = {
  member: PropTypes.shape({
    avatar: PropTypes.string,
    name: PropTypes.string.isRequired,
    job: PropTypes.string,
    socialNetworks: PropTypes.arrayOf(socialNetworksPropTypes)
  }).isRequired
}

export default Card