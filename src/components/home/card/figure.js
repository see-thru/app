import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const AVATAR_SIZE = '150px'

const Wrapper = styled.figure`
  margin: 0;
`

const Avatar = styled.img`
  height: ${ AVATAR_SIZE };
  width: ${ AVATAR_SIZE };
`
 
const Figcaption = styled.figcaption`
  font-weight: 700;
  margin-top: 5px;
`

const Figure = ({
  name, 
  avatar
}) => (
  <Wrapper>
    <Avatar
      alt={ `${ name }'s picture` } 
      src={ avatar ? avatar : 'default.jpg' } 
      width={ AVATAR_SIZE } 
      height={ AVATAR_SIZE } />
    <Figcaption>
      { name }
    </Figcaption>
  </Wrapper>
)

Figure.propTypes = {
  name: PropTypes.string.isRequired,
  avatar: PropTypes.string
}

export default Figure