import React from 'react'
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Rating, ServiceCost, ProviderName } from '../../common/elements'
import { StyledProvider } from './styles'

const RatingSpecialties = ({ info }) => {
  let ratingSpecialties = []
  if(info.rating){
    ratingSpecialties.push(<Rating rating={info.rating} />)
  }
  if(info.specialty){
    ratingSpecialties.push(info.specialty)
  }
  return ratingSpecialties.length ? (
    ratingSpecialties
      .map((item, i) => <span key={i}>{ item }</span>)
      .reduce((el, item) => [el, ' | ', item])
   ) : null
}

const Provider = ({
  item,
  active,
  onHover,
  onSelect
}) => (
  <StyledProvider 
    active={ active }
    onMouseEnter={ () => { onHover(item) }}
    className={ active ? 'active' : '' }
    onClick={() => { onSelect(item._id) }}>
    <div className="provider-image">
      <img src={ item.providerInfo.photo } alt="Provider" />
    </div>
    <div>
      <div className="provider-detail">
        <div className="provider-title">
          <ProviderName info={item.providerInfo} tag="h2" />
          { item.distance && 
            <span>({ item.distance.toFixed(2) } miles)</span>
          }
        </div>
        <p className="provider-rating">
          <RatingSpecialties info={ item.providerInfo } />
        </p>
        { item.address &&
          <p className="provider-address">
            <FontAwesomeIcon icon="map-marker-alt" /> { item.address }
          </p>
        }
      </div>
      { item.baseCost &&
        <ServiceCost className="service-cost" cost={ item.baseCost } />
      }
    </div>
  </StyledProvider>
)

Provider.propTypes = {
  item: PropTypes.shape({
    providerInfo: PropTypes.shape({
      name: PropTypes.string.isRequired,
      photo: PropTypes.string,
      specialty: PropTypes.string
    }).isRequired,
    address: PropTypes.string.isRequired,
    distance: PropTypes.number
  }).isRequired,
  active: PropTypes.bool,
  onHover: PropTypes.func,
  onSelect: PropTypes.func
}

export default Provider