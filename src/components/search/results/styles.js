import styled from 'styled-components'

import { MaxWidth, breakpoints } from '../../../utils/media'
import { colors } from '../../../utils/settings'
import { SubmitButton } from '../../common/elements'
import { borderHover } from '../../common/styles'

export const StyledResultsContainer = styled.div`
  padding: 5px 0;
  .title {
    color: #999;
    font-size: 1rem;
    padding: 5px 20px;
    margin: 0;
    border-bottom: 1px solid #eee;
    display: flex;
    justify-content: space-between;
    align-items: center;

    button {
      color: #8181d2;
      font-size: 0.8rem;
    }
    span {
      font-weight: 700;
    }
  }
  .results-footer {
    padding-top: 10px;
    text-align: center;
    border-top: 1px solid ${colors.borderGray};
  }
`

export const StyledLoadMore = styled(SubmitButton)`
  margin: 0 auto;
`

export const StyledProvider = styled.div`
  padding: 15px 20px;
  position: relative;
  display: flex;
  align-items: flex-start;
  cursor: pointer;
  ${ borderHover }
  &:not(:last-child){
    border-bottom: 1px solid ${ colors.borderGray };
  }

  .provider-image {
    img {
      border-radius: 50%;
      object-fit: cover;
      height: 75px;
      width: 75px;
      margin-right: 15px;
    }
  }
  .provider-detail {
    font-size: .8rem;
    padding-right: 45px;
    .provider-title {
      h2 {
        font-size: 1.2rem;
        color: ${ colors.darkBlue };
        display: inline-block;
        font-weight: 600;
        margin-bottom: 3px;
      }
      span {
        font-size: .8rem;
        color: ${ colors.textMuted };
        margin-left: 5px;
        white-space: nowrap;
      }
    }
    .provider-rating {
      color: #666;
      font-weight: 700;
      margin-bottom: 2px;
    }
    .provider-address {
      color: #888;
      margin-bottom: .4rem;
      svg {
        font-size: .9rem;
        margin-left: 3px;
        margin-right: 5px;
      }
    }
  }
  .base-service {
    font-size: 0.95rem;
    margin-bottom: .5rem;
    svg { 
      color: ${ colors.textMuted }
      font-size: 0.8rem;
      margin-right: 5px;
    }
  }
  .service-cost {
    position: absolute;
    right: 10px;
    top: 10px;
  }

  ${ MaxWidth(breakpoints.extraSmall)`
    display: block;
    text-align: center;
    border-top-width: 3px;
    .provider-detail {
      padding: 0;
      .provider-title h2 {
        font-size: 1rem;
      }
    }
    .provider-image {
      img {
        margin: 0 0 15px;
      }
    }
  `}
`