import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Provider from './provider'
import { StyledResultsContainer, StyledLoadMore } from './styles'
import { DefaultButton } from '../../common/elements';
import { isArrayLength } from '../../../utils/arrays';

export const ResultsContainer = ({
  onClickLoadMore,
  onProviderHover,
  onProviderSelect,
  onShowFilters,
  results,
  loading,
  noMoreResults,
  hoveredMarker
}) => {
  return (
    <StyledResultsContainer className="results-container">
      <div className="title">
        { isArrayLength(results) ?
          <span className="result-count">Results</span>
          :
          <span>{ loading ? 'Loading...' : 'No results found' }</span>
        }
        { onShowFilters && !loading &&
          <DefaultButton onClick={onShowFilters}>FILTERS</DefaultButton>
        }
      </div>
      <div>
        { results.map((item, index) => (
          <Provider 
            key={ index }
            item={ item }
            active={ hoveredMarker === item._id }
            onHover={ onProviderHover }
            onSelect={ onProviderSelect } />
        ))}
      </div>
      { loading && 
        <div className="results-footer">
          <FontAwesomeIcon icon="spinner" spin size="lg" />
        </div>
      }
      { loading || noMoreResults || 
        <div className="results-footer">
          <StyledLoadMore onClick={ onClickLoadMore }>
            Load More
          </StyledLoadMore>
        </div>
      }
    </StyledResultsContainer> 
  )
}

ResultsContainer.defaultProps = {
  results: []
}

ResultsContainer.propTypes = {
  // passed in
  onClickLoadMore: PropTypes.func,
  onProviderHover: PropTypes.func,
  onProviderSelect: PropTypes.func,
  onShowFilters: PropTypes.func,
  // state
  results: PropTypes.array,
  loading: PropTypes.bool,
  noMoreResults: PropTypes.bool,
  hoveredMarker: PropTypes.string
}

const mapStateToProps = (state) => ({
  results: state.SearchReducer.data,
  loading: state.SearchReducer.loading,
  noMoreResults: state.SearchReducer.noMoreResults,
  hoveredMarker: state.SearchReducer.hoveredMarker
})

export default connect(
  mapStateToProps
)(ResultsContainer)