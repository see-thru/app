import styled from 'styled-components'
import { Button } from 'reactstrap';
import { colors } from '../../../utils/settings';
import { ContentBox, GridContainer } from '../../common/layout';
import { MaxWidth } from '../../../utils/media';

export const StyledFiltersContainer = styled(GridContainer)`
  margin-bottom: 30px;
  .filter-buttons {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;

    .filter-sort {
      label {
        font-weight: 600;
        white-space: nowrap;
      }
    }
  }

  .filter-collapse {
    .filter-element-container {
      display: flex;
    }
  }

  ${MaxWidth(650)`
    .filter-buttons {
      display: block;
      .filter-sort { 
        margin-top: 10px;
      }
    }
  `}
`

export const StyledButton = styled(Button)`
  &.btn-secondary {
    color: ${ colors.text };
    background: white;
    border-color: #ddd;
     
    &:focus,
    &:not(:disabled):not(.disabled):active,
    :not(:disabled):not(.disabled).active {
      color: ${ colors.text };
      background-color: #eee;
      border-color: #ddd;
      box-shadow: none;
    }
  }
`

export const StyledFilterElement = styled(ContentBox)`
  width: auto;
`