import React, { Component } from 'react'
import { ButtonGroup, Col, Collapse, FormGroup, Label, Input } from 'reactstrap'
import { GridItem } from '../../common/layout'
import { 
  StyledFiltersContainer,
  StyledButton,
  StyledFilterElement
} from './styles'

export class FiltersContainer extends Component {
  constructor() {
    super()
    this.toggle = this.toggle.bind(this)
    this.state = { showFilter: undefined }
  }

  toggle(filterName) {
    this.setState({ 
      showFilter: this.state.showFilter === filterName ? undefined : filterName
    })
  }

  render () {
    const { showFilter } = this.state
    return (
      <StyledFiltersContainer gridRows="auto" gridColumns="100%">
        <div className="filter-buttons">
          <ButtonGroup>
            <StyledButton onClick={() => {this.toggle('date')}}>Date</StyledButton>
            <StyledButton onClick={() => {this.toggle('time')}}>Time</StyledButton>
            <StyledButton onClick={() => {this.toggle('price')}}>Price</StyledButton>
            <StyledButton onClick={() => {this.toggle('seethru')}}>SeeThru Index</StyledButton>
          </ButtonGroup>
          <div className="filter-sort">
            <FormGroup row>
              <Label for="exampleSelect" sm={4}>Sort by</Label>
              <Col sm={8}>
                <Input type="select" name="select" id="exampleSelect">
                  <option>Best Value</option>
                  <option>Price</option>
                  <option>SeeThru Index</option>
                </Input>
              </Col>
            </FormGroup>
          </div>
        </div>
        <GridItem gridRow="2" gridColumn="1">
          <Collapse isOpen={showFilter === 'date'}>
            <div className="filter-element-container">
              <StyledFilterElement>
                <h1>Date</h1>
                Anim pariatur cliche reprehenderit,
                enim eiusmod high life accusamus terry richardson ad squid. Nihil
                anim keffiyeh helvetica, craft beer labore wes anderson cred
                nesciunt sapiente ea proident.
              </StyledFilterElement>
            </div>
          </Collapse>
        </GridItem>
        <GridItem gridRow="2" gridColumn="1">
          <Collapse isOpen={showFilter === 'time'}>
            <div className="filter-element-container">
              <StyledFilterElement>
                <h1>Time</h1>
                Anim pariatur cliche reprehenderit,
                anim keffiyeh helvetica, craft beer labore wes anderson cred
                nesciunt sapiente ea proident.
              </StyledFilterElement>
            </div>
          </Collapse>
        </GridItem>
      </StyledFiltersContainer> 
    )
  }
}


export default FiltersContainer