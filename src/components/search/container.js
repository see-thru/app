import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import qs from 'qs'
import PropTypes from 'prop-types'

import { search, providerMouseover } from '../../actions/search'

import Search from './base'
import { AlertContainer } from '../common/elements';
import SearchFiltersModal from '../modals/search-filters/container';

const generateDataForSearch = ({ q, lat, lng, distance }) => {
  return {
    q,
    location: lat && lng ? { lat, lng } : undefined,
    distance
  }
}

class SearchContainer extends Component {
  state = {}

  static getDerivedStateFromProps(nextProps, prevState) { // move this to componentDidMount() ? handle the change?
    if(nextProps.location !== prevState.location){
      const parsed = qs.parse(nextProps.location.search.slice(1))
      const data = generateDataForSearch(parsed)
      nextProps.search(data)
      return { location: nextProps.location }
    }
    return null
  }

  onClickLoadMore = () => {
    const { location, search, loading, results } = this.props
    if(loading){
      console.log('already loading')
      return
    }
    const parsed = qs.parse(location.search.slice(1))
    const data = generateDataForSearch(parsed)
    search(data, results.length)
  }

  onProviderHover = activeProvider => {
    this.props.providerMouseover(activeProvider._id)
  }

  onProviderSelect = (providerId, service) => {
    if(!providerId) return
    const { history } = this.props
    const search = service && service.name ? `?select=${service.name}` : undefined
    history.push({ pathname: `/provider/${providerId}`, search })
  }

  toggleFiltersModal = () => {
    console.log('here')
    this.setState({ showFiltersModal: !this.state.showFiltersModal })
  }

  render () {
    const { error, history, location, browser } = this.props
    const { showFiltersModal } = this.state
    return (
      <Fragment>
        { error && <AlertContainer>{ error }</AlertContainer>}
        <Search
          history={ history } 
          location={ location }
          onClickLoadMore={ this.onClickLoadMore }
          onProviderHover={ this.onProviderHover }
          onProviderSelect={ this.onProviderSelect }
          onShowFilters={ this.toggleFiltersModal }
          collapse={ !browser.greaterThan.collapseSearch } />
        <SearchFiltersModal
          isOpen={showFiltersModal}
          closeModal={this.toggleFiltersModal}
          history={history} />
      </Fragment>
    )
  }
}

SearchContainer.defaultProps = {
  history: {},
  location: {}
}

SearchContainer.propTypes = {
  // passed in
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  // state
  results: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.string,
  browser: PropTypes.object,
  // actions
  search: PropTypes.func.isRequired,
  providerMouseover: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  results: state.SearchReducer.data,
  loading: state.SearchReducer.loading,
  error: state.SearchReducer.error,
  browser: state.browser
})

export default connect(
  mapStateToProps,
  { 
    search, 
    providerMouseover
  }
)(SearchContainer)