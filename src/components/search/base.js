import React from 'react'
import PropTypes from 'prop-types'

import ResultsContainer from './results/container'
import MapContainer from '../map/container'
import SearchForm from '../search/form/container'
import { StyledSearchContainer, StyledSearchForm } from './styles'

const Search = ({
  history,
  location,
  onClickLoadMore,
  onProviderHover,
  onProviderSelect,
  onShowFilters,
  collapse
}) => (
  <StyledSearchContainer>
    { collapse ||
      <StyledSearchForm>
        <SearchForm history={ history } inlineForm />
      </StyledSearchForm>
    }
    <div className="search-content">
      <ResultsContainer
        onClickLoadMore={onClickLoadMore}
        onProviderHover={onProviderHover} 
        onProviderSelect={onProviderSelect}
        onShowFilters={collapse ? onShowFilters : undefined } />
      <MapContainer 
        history={history}
        location={location} />
    </div>
  </StyledSearchContainer>
)

Search.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
  collapse: PropTypes.bool,
  onClickLoadMore: PropTypes.func.isRequired,
  onProviderHover: PropTypes.func,
  onProviderSelect: PropTypes.func,
  onShowFilters: PropTypes.func,
}

export default Search