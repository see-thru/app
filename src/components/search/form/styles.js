import styled from 'styled-components'
import { MaxWidth } from '../../../utils/media'
import { SubmitButton } from '../../common/elements'

export const StyledInputs = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  margin-top: 25px;
  
  > div {
    display: inline-block;
    flex-basis: 43%;
    &:first-of-type {
      flex-basis: 55%;
    }
    input {
      border: 0;
      border-radius: 2px;
      font-size: 1rem;
      height: 50px;
      padding: 0 15px;
    }
  }
  ${
    MaxWidth(500)`
      display: block;
      > div {
        width: 100%;
      }
    `
  }
`

export const StyledInputsInline = styled.div`
  justify-content: center;
  input.form-control {
    height: 36px;
    margin: 0 10px;
  }
`

export const StyledButton = styled(SubmitButton)`
  width: 100%;
  font-size: 17px;
  padding: 16px 0;
  margin: 0;
`