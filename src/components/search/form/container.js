import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm, change } from 'redux-form'
import { geolocated } from 'react-geolocated'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import qs from 'qs'
import { StyledInputs, StyledInputsInline, StyledButton } from './styles'
import { Input, FormWrapper, InputGeoSuggest } from '../../common/form'
import { debounce } from '../../../utils/helpers'

class SearchForm extends Component {
  constructor(){
    super()
    this.state = {}
    this.onChange = this.onChange.bind(this)
  }

  componentDidMount() {
    const { history	} = this.props
    let params = qs.parse(history.location.search.slice(1))
    if(params.where && params.lat && params.lng){
      this.props.dispatch(change('searchFormBanner', 'where', {
        description: params.where,
        location: { lat: params.lat, lng: params.lng }
      }))
    }
    if(params.q){
      this.props.dispatch(change('searchFormBanner', 'query', params.q))
    }
  }

  onSubmit = e => {
    const {	coords, isGeolocationEnabled, history, dispatch, afterSubmit	} = this.props	
    console.log('SearchForm submitted', e, coords)
    let filters = { q: e.query }
    const where = e.where || null
    if (where && where.location) {
      filters.lat = where.location.lat
      filters.lng = where.location.lng
      filters.where = where.description
    } else if (isGeolocationEnabled && coords) {
      filters.lat = coords.latitude
      filters.lng = coords.longitude
      filters.where = 'Near me'
      dispatch(change('searchFormBanner', 'where', {
        location: { lat: coords.latitude, lng: coords.longitude },
        description: 'Near me'
      }))
    }
    const query = qs.stringify(filters)
    // don't push when you're already there...
    const historyChange = history.location.pathname === '/search' ? history.replace : history.push
    historyChange({ 
      pathname: '/search',
      search: query ? `?${query}` : undefined
    })
    if(afterSubmit) afterSubmit()
  }

  onChange = debounce(function(){
    console.log('here!')
    const { inlineForm, handleSubmit } = this.props
    if(inlineForm){ handleSubmit(this.onSubmit)() }
  }, 600)

  render () {
    const { handleSubmit, inlineForm } = this.props
    return (
      <FormWrapper onSubmit={ handleSubmit(this.onSubmit) }>
        <div>
          { inlineForm ? (
            <StyledInputsInline className="form-inline">
              <Field
                name="query"
                title="Search Text"
                label="What?"
                component={ Input }
                onChange={this.onChange} />
              <Field
                name="where"
                title="Location"
                label="Where?"
                component={ InputGeoSuggest }
                onSelect={this.onChange} />
            </StyledInputsInline>
          ): (
            <Fragment>
              <StyledInputs>
                <Field
                  name="query"
                  title="Search Text"
                  component={ Input }
                  placeholder="condition, procedure, doctor name..." />
                <Field
                  name="where"
                  title="Location"
                  component={ InputGeoSuggest }
                  placeholder="zip or city" />
              </StyledInputs>
              <StyledButton type="submit">
                <FontAwesomeIcon icon="search" /> Find a Provider
              </StyledButton>
            </Fragment>
          )}
        </div>
      </FormWrapper>
    )
  }
}

SearchForm.propTypes = {
  history: PropTypes.object.isRequired,
  inlineForm: PropTypes.bool,
  afterSubmit: PropTypes.func
}

export const Form = reduxForm({
  form : 'searchFormBanner',
  destroyOnUnmount: false
})( SearchForm ) 

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(Form)