import styled from 'styled-components'
import { MaxWidth, breakpoints } from '../../utils/media'
import { colors, values } from '../../utils/settings'

export const StyledSearchContainer = styled.div`
  position: relative;
  .search-content {
    display: flex;
    height: calc(100vh - ${values.headerHeight} - 50px);
    width: 100vw;
    top: 0;
    left: 0;
    align-items: stretch;
    background: white;
    box-shadow: 0 2px 2px -1px rgba(0,0,0,0.2);
    .results-container {
      height: 100%;
      flex-basis: 450px;
      overflow-y: scroll;
    }
    .search-container {
      flex: 1;
    }
  }
  ${MaxWidth(breakpoints.collapseSearch)`
    .search-content {
      height: calc(100vh - ${values.headerHeight});
      flex-direction: column-reverse;
      .search-container {
        flex-basis: 40vh;
        min-height: 200px;
      }
    }
  `}
`

export const StyledSearchForm = styled.div`
  margin: 0 auto;
  background: ${ colors.headerBackground };
  height: 50px;
  border-bottom: 1px solid #ccc;
  padding: 7px 10px;
  ${MaxWidth(breakpoints.collapseSearch)`
    height: auto;
  `}
`