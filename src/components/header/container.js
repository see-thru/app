import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Logo from './logo'
import NavContainer from './nav/container'
import { StyledHeader } from './styles'
import { DefaultButton } from '../common/elements';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class HeaderContainer extends Component {
  state = {}

  static getDerivedStateFromProps(nextProps, prevState){
    // whenever props (history) change:
    const canGoBack = window.SeeThru.stackSize > 0
    if(canGoBack !== prevState.canGoBack){
      return { canGoBack }
    }
    return null
  }

  render () {
    const { history } = this.props
    const { canGoBack } = this.state
    // TODO: move searchForm into header, instead of Search...!
    return (
      <StyledHeader hasSubNav={ history.location.pathname.startsWith('/search') }>
        <div className="header-content">
          <div>
            { canGoBack && 
              <DefaultButton onClick={history.goBack} aria-label="Navigate back"><FontAwesomeIcon icon="chevron-left" /></DefaultButton>
            }
            <Logo />
          </div>
          <NavContainer history={ history } />
        </div>
      </StyledHeader>
    )
  }
}

HeaderContainer.propTypes = {
  history: PropTypes.object.isRequired
}

export default HeaderContainer