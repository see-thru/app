import React, { Component } from 'react'
import PropTypes from 'prop-types'
import BodyClassName from 'react-body-classname'
import onClickOutside from 'react-onclickoutside'

import Nav from './base'
import MenuButton from './menu-button'

export class NavContainer extends Component {
  state = { isOpened : false }

  toggle = () => {
    this.setState({ isOpened : !this.state.isOpened })
  }

  handleClickOutside = () => {
    this.setState({ isOpened: false })
  }
  
  render () {
    const { isOpened } = this.state
    const { history } = this.props
    return (
      <div>
        { isOpened && <BodyClassName className="shadow-block" /> }
        <MenuButton 
          isActived={ isOpened }
          toggle={ this.toggle } />
        <Nav 
          toggle={ this.toggle }
          history={ history }
          isOpened={ isOpened } />
      </div>
    )
  }
}

NavContainer.propTypes = {
  history: PropTypes.object.isRequired
}

export default onClickOutside(NavContainer)