import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import Media, { MinHide, MaxHide, breakpoints } from '../../../../utils/media'
import { colors, values } from '../../../../utils/settings'
import { Link } from 'react-router-dom'
import { DefaultButton } from '../../../common/elements';

export const StyledUserNav = styled.div`
  display: inline-block;
  position: relative;
  vertical-align: middle;
  ${ MaxHide(breakpoints.small) }
`

export const StyledSmallUserNav = styled.div`
  ${ MinHide(breakpoints.small + 1) }
` 

export const StyledDropdownToggle = styled(DefaultButton)`
  padding: 0;
  margin-left: 10px;
`

export const StyledMockAvatar = styled.img`
  background-color: #eee;
  border-radius: 100%;
  display: block;
  height: 34px;
  width: 34px;
  margin: 8px 0;
  object-fit: cover;
`
StyledMockAvatar.defaultProps = {
  src: '/images/user-avatar.jpg'
}
StyledMockAvatar.propTypes = {
  src: PropTypes.string
}

export const StyledDropdownList = styled.div`
  background-color: white;
  border: 1px solid ${ colors.border };
  box-shadow: 2px 2px 2px rgba(0,0,0,0.3);
  position: absolute;
  top: ${ values.headerHeight };
  right: 0;
  width: 240px;
  z-index: 2;
  ${
    Media.small`
      border: 0;
      border-radius: 0;
      left: 0;
      margin-top: 0;
      right: auto;
      top: 100%;
      padding: 0;
      width: 100%;
    `
  }
  & > svg {
    bottom: 100%;
    right: 25px;
    position: absolute;
    ${
      Media.medium`
        right: 18px;
      `
    }
  }
`

const StyledDropdownAction = css`
  color: ${ colors.text };
  display: block;
  font-size: .9rem;
  margin: 0;
  text-align: left;
  padding: 10px 20px;
  width: 100%;
  position: relative;
  background-color: white;
  transition: background-color .2s linear;
  
  &:hover {
    background-color: #f1f1f1;
    color: #586069;
  }
`

export const StyledDropdownLink = styled(Link)`
  ${ StyledDropdownAction }
`

export const StyledDropdownButton = styled(DefaultButton)`
  ${ StyledDropdownAction }
  color: ${ colors.text };
  font-weight: 400;
  padding-right: 20px;
  svg {
    right: 20px;
    top: 12px;
    position: absolute;
  }
`