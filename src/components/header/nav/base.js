import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { toggleSignInModal, toggleSignUpModal, logout } from '../../../actions/auth'

import { UserNav } from './user/base'
import { StyledNav, StyledButton, StyledButtonBlue } from './styles'

export const Nav = ({
  user,
  logout,
  toggle,
  history,
  isLogged,
  isOpened,
  toggleSignInModal,
  toggleSignUpModal	  
}) => (
  <StyledNav 
    onClick={ toggle }
    isOpened={ isOpened }>
    { isLogged ? 
      <UserNav logout={ logout.bind(this, history) } user={ user } />
      :
      <Fragment>
        <StyledButton
          onClick={ toggleSignUpModal.bind(this, true) }
          type="register">
          Patient Signup
        </StyledButton>
        <StyledButtonBlue
          onClick={ toggleSignInModal.bind(this, true) }
          type="signin">
          Sign in
        </StyledButtonBlue>
      </Fragment>
    }
  </StyledNav>
)

Nav.propTypes = {
  user: PropTypes.object,
  toggle: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  isOpened: PropTypes.bool, 
  isLogged: PropTypes.bool,
  toggleSignInModal: PropTypes.func.isRequired,
  toggleSignUpModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  isLogged: state.AuthReducer.isLogged
})

export default connect(
  mapStateToProps,
  { 
    logout,
    toggleSignInModal,
    toggleSignUpModal
  }
)( Nav )
