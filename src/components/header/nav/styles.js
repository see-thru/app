import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

import Media from '../../../utils/media'
import { colors, fonts } from '../../../utils/settings'
import { HistoryLink } from '../../common/elements'

export const StyledNav = styled.nav`
  ${
    Media.small`
      background-color: white;
      border-top: 2px solid #eee;
      left: 0;
      height: ${ props => props.isOpened ? 'auto' : '0' };
      text-align: center;
      top: 60px;
      overflow: hidden;
      position: absolute;
      width: 100%;
      visibility: ${ props => props.isOpened ? 'visible' : 'hidden' };
      z-index: 2;
    ` 
  }
  ${ Media.small`
    a, button {
      transition: .2s transform;
    }
    a {
      &:nth-child(1) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-10px)' };
      }
      &:nth-child(2) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-20px)' };
      }
      &:nth-child(3) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-30px)' };
      }
      &:nth-child(4) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-40px)' };
      }    
      &:nth-child(5) {
        transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-50px)' };
      }
    }
    button {
      transform: ${ props => props.isOpened ? 'translateY(0)' : 'translateY(-40px)' };
    }  
  ` }
`

StyledNav.propTypes = {
  isOpened: PropTypes.bool
}

const smallHover = css`
  &:hover {
    background: #cfe8fb;
    background: linear-gradient(170deg,#eff5f7,#cfe8fb);
    color: #575d6f;
  }
`
const styles = css`
  border: 0;
  color: #575d6f;
  cursor: pointer;
  display: inline-block;
  line-height: 20px;
  margin: 0 10px;
  padding: 20px 5px;
  transition: .2s color;
  text-decoration: none;
  &:hover {
    color: ${ colors.navBarHoverLink };
  }
  &:focus {
    outline: 0;
  }
  ${ Media.medium`
    font-size: .9rem;
    margin: 0 7px;
  ` }
  ${
    Media.small`
      border-bottom: 1px solid #eee;
      display: block;
      font-size: .9rem;
      margin: 0;
      padding: 13px 20px;
      width: 100%;
      ${smallHover}
    ` 
  }
`

export const StyledButton = styled.button`
  ${ styles }
  background-color: transparent;
  border: 1px solid #9c9c9c;
  border-radius: 4px;
  color: ${ colors.textLight };
  font-size: 13px;
  font-weight: ${ fonts.weightBold };
  padding: 8px 32px;
  transition: .3s color, .3s border-color;
  svg { 
    margin-left: 10px;
  }
  &:hover {
    border-color: ${ colors.navBarHoverLink };
  }
  ${ Media.medium`
    padding: 8px 20px;
  ` }
  ${ Media.small`
    border: 0;
    border-bottom: 1px solid #eee;
    border-radius: 0;
    padding: 13px 20px;
    &:hover {
      border-color: inherit;
    }
  `}
`
export const StyledButtonBlue = styled.button`
  ${ styles }
  background-color: ${ colors.darkButton }; 
  border-radius: 4px;
  color: white;
  font-size: 13px;
  font-weight: ${ fonts.weightBold };
  transition: .35s background-color;
  padding: 8px 32px;
  &:hover {
    background-color: ${ colors.bgMissionBox };
    color: white;
  }
  ${ Media.medium`
    padding: 8px 17px;
  `}
  ${ Media.small`
    background-color: transparent;
    border: 0;
    border-bottom: 1px solid ${ colors.accentNavBar };
    border-radius: 0;
    color: #575d6f;
    padding: 13px 20px;
    ${smallHover}
  `}
  `
  
export const StyledLink = styled.a`${ styles }`
export const StyledLinkRouter = styled(HistoryLink)`${ styles }`