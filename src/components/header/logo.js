import React from 'react'

import styled from 'styled-components'
import Media from '../../utils/media'
import { HistoryLink } from '../common/elements'

const StyledLogo = styled.div`
  display: inline-block;
  margin-left: 10px;
`

const StyledLogoLink = styled(HistoryLink)`
  display: block;
  ${ Media.medium`
    img {
      height: 30px;
    }
  `}
`

const Logo = () => (
  <StyledLogo>
    <StyledLogoLink to="/">
      <img 
        src="/images/header-logo.png" 
        alt="Seethru Logo"
        height="40" />
    </StyledLogoLink>
  </StyledLogo>
)

export default Logo