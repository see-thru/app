import styled from 'styled-components'
import { colors, values } from '../../utils/settings'

export const StyledHeader = styled.header`
  background-color: ${ colors.headerBackground };
  ${ props => props.hasSubNav ? 'border-bottom: 1px solid #ddd' : 'box-shadow: 0 2px 2px rgba(0,0,0,.2)' };
  height: ${ values.headerHeight };
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 2;
  
  .header-content {
    height: inherit;
    margin: auto;
    max-width: 1240px;
    padding: 0 15px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`

export const StyledSearchForm = styled.div`
  background-color: ${ colors.blue }
  height: 50px;
  display: flex;
  padding: 10px 25px;
`