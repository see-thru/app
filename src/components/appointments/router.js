import React from 'react'
import { Route, Switch } from 'react-router-dom'
import PropTypes from 'prop-types'

import AppointmentsContainer from './show/container'
import CheckInContainer from './checkin/container'
import NoMatchContainer from '../no-match/container'

export const ProviderRouter = ({ match }) => (
  <Switch>
    <Route 
      exact
      path={match.path}
      component={ AppointmentsContainer } />
    <Route 
      exact
      path={ `${ match.path }/:appointmentId/check-in` }
      component={ CheckInContainer } />
    <Route component={ NoMatchContainer } />
  </Switch>
)

ProviderRouter.propTypes = {
  match: PropTypes.object.isRequired
}

export default ProviderRouter