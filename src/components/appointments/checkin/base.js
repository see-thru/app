import React from 'react'
import PropTypes from 'prop-types'
import QRCode from 'qrcode.react'
import { Alert } from 'reactstrap'

import { ContentBoxPadded } from '../../common/layout/container'
import { StyledPageContainer, StyledQRContainer } from './styles'
import AppointmentInfo from '../info/base'

const CheckIn = ({ appointment }) => {
  return (
    <StyledPageContainer>
      { appointment.checkInCode && 
        <StyledQRContainer>
          <QRCode
            value={ appointment.checkInCode }
            size={ 230 }
            level="L"
            bgColor="#ffffff"
            fgColor="#000000" /> 
          <p>Show this QR Code when you arrive at your appointment</p>
        </StyledQRContainer>
      }
      <ContentBoxPadded>
        <AppointmentInfo appointment={ appointment } />
      </ContentBoxPadded>
      { false &&
        <div className="thanks">
          <Alert 
            color="primary" 
            onClick={() => { window.open('https://goo.gl/forms/oDlwQzWqdz7HiJux1', '_blank')}}>
              Thanks for testing out SeeThru! Please let us know your feedback here: &nbsp;
              <a href="https://goo.gl/forms/oDlwQzWqdz7HiJux1" target="_blank" rel="noopener noreferrer"
                onClick={e => e.stopPropagation()}>https://goo.gl/forms/oDlwQzWqdz7HiJux1</a>
          </Alert>
        </div>
      }
    </StyledPageContainer>
  )
}

CheckIn.propTypes = {
  appointment: PropTypes.object,
  error: PropTypes.string
}

export default CheckIn