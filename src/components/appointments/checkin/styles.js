import styled from 'styled-components'
import { PageContainer } from '../../common/layout'
import { colors } from '../../../utils/settings'

export const StyledPageContainer = styled(PageContainer)`
  max-width: 600px;
  .thanks {
    text-align: center;
    margin-bottom: 20px;
    .alert {
      cursor: pointer;
      transition: box-shadow .2s linear;
      &:hover {
        box-shadow: 1px 1px 6px -1px;
      }
    }
  }
`

export const StyledQRContainer = styled.div`
  background: ${ colors.lightBlue };
  padding: 20px;
  margin-bottom: 20px;
  canvas {
    display: block;
    margin: 0 auto;
    max-width: 230px;
    width: 96%;
    
  }
  p {
    color: white;
    font-size: 1.1rem;
    max-width: 400px;
    margin: 15px auto 0;
    font-weight: 600;
    line-height: 1.3rem;
    text-align: center;
  }
`