import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { ContentBox, ContentBoxBlue } from '../../common/layout'
import { StyledGridContainer } from './styles'
import AppointmentInfo from '../info/base'
import { isArrayLength } from '../../../utils/arrays';

const HeaderBox = styled(ContentBoxBlue)`
  padding: 0;
  margin: 0;
  .title { margin: 0; }
`

const Appointments = ({ appointments }) => {
  return (
    <StyledGridContainer 
      templateColumns="1fr 1fr"
      gridGap="0 20px">
      <div>
        <HeaderBox>
          <div className="title title-small">
            <h1>Upcoming Appointments</h1>
          </div>
        </HeaderBox>
        { isArrayLength(appointments) ?
            appointments.map((appt, index) => {
              return (
                <ContentBox key={ index }>
                  <div className="content">
                    <AppointmentInfo appointment={ appt } showCheckInButton={ true } />
                  </div>
                </ContentBox>
              )
            }) 
            : (
              <ContentBox>
                <div className="content">
                  <p>You have no upcoming appointments at this time.</p>
                </div>
              </ContentBox>
            )
        }
      </div>
      { false &&
        <ContentBox>
          <div className="title title-small">
            <h1>Past Appointments</h1>
          </div>
          <div className="content">
            <p>You have no past appointments.</p>
          </div>
        </ContentBox>      
      }
    </StyledGridContainer>
  )
}

Appointments.propTypes = {
  appointments: PropTypes.array,
  onCheckIn: PropTypes.func
}

export default Appointments