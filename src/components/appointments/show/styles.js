import styled from 'styled-components'
import { GridPageContainer } from '../../common/layout'
import { MaxWidth } from '../../../utils/media';

export const StyledGridContainer = styled(GridPageContainer)`
  ${ MaxWidth(900)`
    grid-template-columns: 1fr;
  `}
  .content + .content {
    border-top: 3px solid #eee;
    margin-top: 5px;
  }
`