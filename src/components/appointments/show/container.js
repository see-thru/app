import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { loadAppointments } from '../../../actions/appointments'
import { AlertContainer, Loader } from '../../common/elements'
import Appointments from './base'

export class AppointmentsContainer extends Component {
  state = {}

  componentDidMount(){
    this.props.loadAppointments()
  }

  render(){
    const { appointments, loading, error } = this.props
    return (
      <Fragment>
        { loading && <Loader /> }
        { error && <AlertContainer>{ error }</AlertContainer>}
        { appointments &&
          <Appointments appointments={ appointments } />
        }
      </Fragment>
    )
  }
}

AppointmentsContainer.propTypes = {
  appointments: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.string,
  loadAppointments: PropTypes.func
}

const mapStateToProps = (state) => ({
  appointments: state.AppointmentsReducer.appointments.data,
  loading: state.AppointmentsReducer.appointments.loading,
  error: state.AppointmentsReducer.appointments.error
})

export default connect(
  mapStateToProps,
  { loadAppointments }
)( AppointmentsContainer )