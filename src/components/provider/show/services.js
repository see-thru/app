import React from 'react'
import PropTypes from 'prop-types'
import { UncontrolledCollapse } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ServiceCost } from '../../common/elements'
import { StyledMedicalServices, StyledMedicalService } from './styles'

const ServiceTime = ({ time }) => {
  var minutes = parseInt(time)
  if (!isNaN(minutes)) { 
    return (
      <span className="service-time">Estimated Time: <span>{ minutes } Minutes</span></span>
    )
  }
}

const MedicalServiceList = ({ services, toggleService }) => {
  return services.map( (s, index) => (
    <StyledMedicalService key={index} type={ s.selected ? 'selected' : '' } onClick={() => {toggleService(s.index)}}>
      <div className="service-row">
        <span className="service-name">{ s.name }</span>
        { s.time && <ServiceTime time={s.time} /> }
        <ServiceCost className="service-cost" cost={s.cost} />
        <div className="service-toggle" aria-label={s.selected ? 'Service selected' : 'Click to select service' }>
          { s.locked ? <FontAwesomeIcon icon="lock" /> : s.selected ? <FontAwesomeIcon icon="check" /> : <FontAwesomeIcon icon="plus" /> }
        </div>
      </div>
      { s.includes &&
        <div className="service-bundle">
          { s.includes.map((item, index) => {
            return (
              <span key={index}>{ item }</span>
            )
          })}
        </div>
      }
    </StyledMedicalService>
  ))
}

const MedicalServices = ({ services, toggleService, categories }) => (
  <StyledMedicalServices>
    <div className="title">
      <h2>Services</h2>
    </div>
    <div className="services-content">
      <MedicalServiceList 
        services={services.filter(s => !s.category)} 
        toggleService={toggleService} />

      { Array.isArray(categories) && categories.map((c, index) => (
        <div key={ index } >
          <StyledMedicalService type="category">
            <div className="service-row" id={`toggleCat${index}`}>
              <span className="service-name">{ c }</span>
              <FontAwesomeIcon icon="chevron-down" />
            </div>
          </StyledMedicalService>
          <UncontrolledCollapse toggler={`toggleCat${index}`}>
            <MedicalServiceList 
              services={services.filter(s => s.category === c)} 
              toggleService={toggleService} />
          </UncontrolledCollapse>
        </div>
      ))}
    </div>
  </StyledMedicalServices>
)
  
MedicalServices.propTypes = {
  services: PropTypes.array.isRequired,
  categories: PropTypes.array,
  toggleService: PropTypes.func
}

export default MedicalServices