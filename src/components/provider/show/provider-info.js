import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Collapse } from 'reactstrap'
import { 
  StyledProviderInfo,
  StyledProviderDetails
} from './styles'
import { Rating, ProviderName } from '../../common/elements';
import { isArrayLength } from '../../../utils/arrays';

const ProviderSpecialties = ({ specialty, secondary }) => (
  <div className="provider-specialties">
    <p>Specialty: { specialty }</p>
    { isArrayLength(secondary) &&
      <p>Secondary Specialty: { secondary.join(' | ') }</p>
    }
  </div>
)

export class ProviderInfo extends Component {
  constructor() {
    super()
    this.toggle = this.toggle.bind(this);
    this.state = { showSection: {} };
  }

  toggle(section) {
    this.setState({
      showSection: {
        ...this.state.showSection,
        [section]: !this.state.showSection[section]
      }
    })
  }

  renderToggle(key, name) {
    return (
      <p onClick={() => {this.toggle(key)}}>
        { name }
        { this.state.showSection[key] ?
          <FontAwesomeIcon icon="chevron-up" /> : <FontAwesomeIcon icon="chevron-down" />
        }
      </p>
    )
  }
  
  render () {
    const { info } = this.props
    const { showSection } = this.state
    return (
      <StyledProviderInfo>
        <div className="provider-header">
          <div className="provider-image">
            <img src={ info.photo } alt="" />
          </div>
          <div className="provider-title">
            <ProviderName info={info} />
            { info.practiceName &&
              <p className="provider-hospital">{ info.practiceName }</p>
            }
            { info.specialty &&
              <ProviderSpecialties specialty={info.specialty} secondary={info.secondarySpecialties} />
            }
            { info.rating &&
              <Rating rating={info.rating} />
            }
          </div>
        </div>
        <StyledProviderDetails>
          { info.description &&
            <div className="detail-section">
              { this.renderToggle('description', 'Description') }
              <Collapse isOpen={showSection['description']}>
                <div className="collapse-content">
                { info.description }
                </div>
              </Collapse>
            </div>
          }
          { isArrayLength(info.languages) &&
            <div className="detail-section">
              { this.renderToggle('languages', 'Languages') }
              <Collapse isOpen={showSection['languages']}>
                <ul className="collapse-content">
                  { info.languages.map( (lang, index) => (
                    <li key={index}>{ lang }</li>
                  ))}
                </ul>
              </Collapse>
            </div>
          }
          { isArrayLength(info.education) && 
            <div className="detail-section">
              { this.renderToggle('education', 'Education & Affiliations') }
              <Collapse isOpen={showSection['education']}>
                <ul className="collapse-content">
                  { info.education.map( (item, index) => (
                    <li key={index}>{ item }</li>
                  ))}
                </ul>
              </Collapse>
            </div>
          }
        </StyledProviderDetails>
      </StyledProviderInfo>
    )
  }
}

ProviderInfo.propTypes = {
  info: PropTypes.object
}

export default ProviderInfo