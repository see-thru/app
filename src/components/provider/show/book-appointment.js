import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { StyledBookAppointment, StyledBookButton } from './styles'
import PickTimeModal from '../../modals/pick-time/container'
import { RequestedTime, DefaultButton } from '../../common/elements'
import { timesRequired } from '../../../constants/appointments'
import { isArrayLength } from '../../../utils/arrays';
export class BookAppointment extends Component {
  state = {
    chosenTimes: []
  }

  static propTypes = {
    openBookAppointment: PropTypes.func.isRequired,
    totalTime: PropTypes.number
  }

  togglePickTimeModal = () => {
    const { isOpenPickTime } = this.state
    this.setState({ isOpenPickTime: !isOpenPickTime })
  }

  onPickTime = chosenTimes => this.setState({ chosenTimes })

  clickBookAppointment = () => {
    const { openBookAppointment } = this.props
    const { chosenTimes } = this.state
    if(chosenTimes.length < timesRequired){
      this.togglePickTimeModal()
    } else {
      openBookAppointment(chosenTimes)
    }
  }

  render() {
    const { totalTime } = this.props
    const { chosenTimes, isOpenPickTime } = this.state
    return (
      <Fragment>
        <StyledBookAppointment>
          <div className="appointment-content">
            <h2>Schedule Visit</h2>
            <div>
              <DefaultButton onClick={ this.togglePickTimeModal } className="pick-time">
                <p><FontAwesomeIcon icon="calendar-alt" /> Preferred Times:</p>
                { isArrayLength(chosenTimes) &&
                  <ul>
                    { chosenTimes.map((time, i) => (
                      <li key={i}><RequestedTime time={time} /></li>
                    )) }
                  </ul>
                }
              </DefaultButton>
            </div>
          </div>
          <StyledBookButton onClick={this.clickBookAppointment}>Request an Appointment</StyledBookButton>
        </StyledBookAppointment>
        <PickTimeModal 
          isOpen={ isOpenPickTime }
          closeModal={ this.togglePickTimeModal }
          details={ chosenTimes }
          onPickTime={ this.onPickTime }
          totalTime={ totalTime }
        />
      </Fragment>
    )
  }
}

export default BookAppointment