import React from 'react'
import PropTypes from 'prop-types'
import ProviderInfo from './provider-info'
import Location from './location'
import BookAppointment from './book-appointment'
import MedicalServices from './services'
import { GridItem } from '../../common/layout'
import {StyledProviderContainer} from './styles'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { isArrayLength } from '../../../utils/arrays';

const Provider = ({ 
  provider,
  services,
  serviceCategories,
  toggleService,
  openBookAppointment,
  loadingServices,
  totalTime
}) => {
  return (
    <StyledProviderContainer
      gridGap="20px"
      templateRows="auto"
      templateColumns="repeat(auto-fill, minmax(450px, 1fr))">
      <GridItem>
        { provider.providerInfo &&
          <ProviderInfo info={ provider.providerInfo } />
        }
        { provider.address &&
          <Location provider={ provider } />
        }
      </GridItem>
      
      { loadingServices && 
        <GridItem className="text-center">
          <FontAwesomeIcon icon="spinner" spin size="lg" />
        </GridItem>
      }
      { isArrayLength(services) &&
        <GridItem>
          <BookAppointment openBookAppointment={ openBookAppointment } totalTime={ totalTime } />
          <MedicalServices 
            services={ services }
            categories={ serviceCategories }
            toggleService={toggleService} />
        </GridItem>
      }
    </StyledProviderContainer>
  )
}

Provider.propTypes = {
  provider: PropTypes.shape({
    id: PropTypes.string,
    providerInfo: PropTypes.object,
    address: PropTypes.string
  }),
  services: PropTypes.array,
  serviceCategories: PropTypes.array,
  toggleService: PropTypes.func,
  openBookAppointment: PropTypes.func,
  loadingServices: PropTypes.bool,
  totalTime: PropTypes.number
}

export default Provider