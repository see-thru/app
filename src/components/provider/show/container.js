import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import qs from 'qs'

import Provider from './base'
import BookAppointmentModal from '../../modals/book-appointment/container'
import { getProvider } from '../../../actions/provider'
import { toggleSignUpModal } from '../../../actions/auth'
import { AlertContainer, Loader } from '../../common/elements'
import { defaultInterval } from '../../../constants/appointments'
import { convertToUTC } from '../../../utils/dates';

const getTotalTime = services => {
  if(!Array.isArray(services)) return defaultInterval
  return services.filter(s => s.selected).reduce((total, s) => total + (s.time || 0), 0) || defaultInterval
}

export class ProviderContainer extends Component {
  state = { services: [] }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { match, getProvider, providerServices, location } = nextProps

    if(!match || !match.params) return {}
    if(match.params.providerId !== prevState.providerId){
      // Changed providerId. Dispatch getProvider // could probably do this in componentDidMount to make it simpler
      getProvider(match.params.providerId)
      return { 
        providerId: match.params.providerId,
        loadingServices: true
      }
    }
    // loadingServices is a dummy variable, to define when the props are new and need to be processed. 
    if(providerServices && prevState.loadingServices){
      // Loaded Provider. Updating services.
      let services = providerServices.map((s, index) => {
        s.index = index
        if(s.isDefault){
          s.selected = true
          s.locked = true
        }
        return s
      })
      let serviceCategories
      if(services.length){
        let scatMap = services.reduce((cats, service) => {
          if(service.category && !cats[service.category]){
            cats[service.category] = true
          }
          return cats
        }, {})
        let scats = Object.keys(scatMap)
        if(scats.length){
          serviceCategories = scats
        }
      }
      if(location.search){
        let parsed = qs.parse(location.search.slice(1))
        if(parsed.select){
          let selected = services.findIndex(s => s.name === parsed.select)
          if(selected > -1){
            services[selected].selected = true
          }
        }
      }
      return {
        services,
        serviceCategories,
        totalTime: getTotalTime(services),
        loadingServices: false
      }
    }
    return null
  }

  toggleService = index => {
    const { services } = this.state
    if(!Number.isInteger(index) || services[index].locked){ 
      return
    }
    // TODO: Logic to determine whether service can be toggled (based on baseService and service.time)
    services[index].selected = !services[index].selected
    let newState = { services }
    if(services[index].time){
      newState.totalTime = getTotalTime(services)
    }
    this.setState(newState)
  }

  toggleBookAppointment = chosenTimes => {
    const { isLogged, toggleSignUpModal } = this.props
    if(!isLogged){
      toggleSignUpModal(true) // set continueTo field in reducer. add book appointment stuff to state..?
      return
    }
    const { services, isOpenBookAppointment } = this.state
    var newState = { isOpenBookAppointment: !isOpenBookAppointment }
    if(!isOpenBookAppointment){
      if(!services.some(s => s.selected)) return
      let selected = services.filter(s => s.selected).map(s => {
        delete s.index
        return s
      })
      let totalCost = selected.reduce((total, s) => total + (s.cost || 0), 0)
      // let totalTime = selected.reduce((total, s) => total + (s.time || 0), 0) || 30 // this is already stored in this.state
      // let start = moment().hours(9).startOf('hour')
      // let end = start.clone().add(totalTime, 'm')
      // details.time = `${start.format('LT')} - ${end.format('LT')}`
      
      const chosenTimesUTC = chosenTimes.map(t => ({ date: convertToUTC(t.date), time: t.time }))
      newState.appointment = {
        provider: this.props.provider,
        services: selected,
        details: {
          times: chosenTimesUTC
        },
        totalCost
      }
    }
    this.setState(newState)
  }

  onBookAppointment = () => {
    let services = this.state.services.map(s => {
      s.selected = s.locked
      return s
    })
    this.setState({ services })
  }

  render () {
    const { provider, error, loading } = this.props
    // is this just all of this.state?
    const { appointment, services, serviceCategories, isOpenBookAppointment, loadingServices, totalTime } = this.state
    return (
      <div>
        { loading ? <Loader /> :
          <Fragment>
            { error && <AlertContainer>{ error }</AlertContainer> }
            { provider &&
              <Fragment>
                <Provider 
                  provider={ provider } 
                  services={ services }
                  serviceCategories={ serviceCategories }
                  toggleService={ this.toggleService }
                  openBookAppointment={ this.toggleBookAppointment }
                  loadingServices={ loadingServices && !error } 
                  totalTime={ totalTime } />
                <BookAppointmentModal 
                  isOpen={ isOpenBookAppointment }
                  closeModal={ this.toggleBookAppointment }
                  appointment={ appointment }
                  onBookAppointment={this.onBookAppointment}
                />
              </Fragment>
            }
          </Fragment>
        }
      </div>
    )
  }
}

ProviderContainer.propTypes = {
  provider: PropTypes.object,
  providerServices: PropTypes.array,
  error: PropTypes.string,
  loading: PropTypes.bool,
  isLogged: PropTypes.bool,
  getProvider: PropTypes.func,
  toggleSignUpModal: PropTypes.func
}

const mapStateToProps = (state) => ({
  provider: state.ProviderReducer.data,
  providerServices: state.ProviderReducer.services,
  error: state.ProviderReducer.error,
  loading: state.ProviderReducer.loading,
  isLogged: state.AuthReducer.isLogged
})

export default connect(
  mapStateToProps,
  {
    getProvider,
    toggleSignUpModal
  }
)( ProviderContainer )