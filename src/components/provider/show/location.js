import React from 'react'
import PropTypes from 'prop-types'

import MapWrapper from '../../map/base'
import {StyledMapContainer} from '../../map/styles'
import { colors } from '../../../utils/settings'

import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import generateMapLocations from '../../../utils/generate-map-locations';

const StyledLocation = styled.div`
  position: relative;
  & > div:last-of-type {
    margin-top: 0;
  }
`

const StyledDescription = styled.div`
  background-color: ${ colors.blue };
  color: white;
  font-size: .9rem;
  padding: 10px;
  svg {
    margin: 0 10px;
  }
`

// TODO: Should Update() = check address
const Location = ({ provider }) => {
  return (
    <StyledLocation>
      <StyledDescription>
        <FontAwesomeIcon icon="map-marker-alt" /> { provider.address }
      </StyledDescription>
      <StyledMapContainer
        height="400px"
        maxHeight="900px">
        <MapWrapper locations={ generateMapLocations([provider]) } />
      </StyledMapContainer>  
    </StyledLocation>
  )
}

Location.propTypes = {
  provider: PropTypes.object
}

export default Location