import React from 'react'
import { Route, Switch } from 'react-router-dom'
import PropTypes from 'prop-types'

import ProviderContainer from './show/container'
import ProviderListContainer from './list'
import NoMatchContainer from '../no-match/container'

export const ProviderRouter = ({
  match
}) => (
  <Switch>
    <Route path={`${ match.path }/all` } component={ ProviderListContainer } />
    <Route 
      exact
      path={ `${ match.path }/:providerId` }
      component={ ProviderContainer } />
    <Route component={ NoMatchContainer } />
  </Switch>
)

ProviderRouter.propTypes = {
  match: PropTypes.object.isRequired
}

export default ProviderRouter