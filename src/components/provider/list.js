import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { PageContainer } from '../common/layout'
import { getProviderIds } from '../../actions/provider'
import { Loader } from '../common/elements';

export class ProviderListContainer extends Component {
  constructor() {
    super()
    this.state = { loading: true }
  }

  componentWillMount() {
    getProviderIds().then(response => {
      console.log(response)
      if(response && response.data) {
        this.setState({ providers: response.data, loading: false })
      }
    })
  }

  render () {
    const { providers, loading } = this.state
    return (
      <PageContainer>
        { loading && <Loader /> }
        { providers && providers.map((p, index) => (
          <div key={index}><Link to={`/provider/${p._id}`}>{ p.name }</Link></div>
        ))}
      </PageContainer>
    )
  }
}

export default ProviderListContainer