import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Map, Marker, InfoWindow, GoogleApiWrapper } from 'google-maps-react'
import { StyledInfoWindow, StyledInfoButton } from './styles'

// TODO: Load map to distance in query on page reload
class MapWrapper extends Component {
  constructor(){
    super()
    this.state = {}
  }
  
  // when the actual result ids have changed, set the center and the ids, so that map knows to update in shouldComponentUpdate
  static getDerivedStateFromProps(nextProps, prevState){ // how to tell whether they are new results?
    const { latFilter, lngFilter, locations } = nextProps
    const ids = (locations || []).map(r => r.provider._id).join(',')
    if(ids !== prevState.ids){
      // only reset location when ids are different
      let center
      if (latFilter && lngFilter) {
        center = { lat: latFilter, lng: lngFilter } // TODO: set bounds by distanceFilter as well
      } else if(locations.length){
        center = { lat: locations[0].lat, lng: locations[0].lng }
      } else {
        center = { lat: 40.7128, lng: -74.0060 }
      }
      return { center, ids }
    }
    return null
  }

  // TODO: make marker its own component which pulls from redux, so that we don't need to update the whole map whenever one changes
  // Unfortunately, google-maps-react needs Marker to be a direct child of Map :(
  // shouldComponentUpdate(nextProps, nextState){
  //   const scu = nextState.ids !== this.state.ids
  //   console.log('MapWrapper.scu', scu, nextState.ids, this.state.ids)
  //   return scu
  // }

  render() {
    const { google, onDragend, onMapReady, onMouseoverMarker, onMapClicked, onMarkerClick, infoWindow, locations } = this.props
    const { center } = this.state
    return (
      <Map 
        zoom={ 12 }
        google={ google }
        onReady={ onMapReady }
        onClick={ onMapClicked }
        onDragend={ onDragend }
        initialCenter={ center }
        center={ center }>
        { locations.map(location => {
        return (
          <Marker
            zIndex={ location.active ? 2 : null }
            key={ location.provider._id }
            icon={{
              url: location.active ? "/images/seethru-isotype-dark.png" : "/images/seethru-isotype.png",
              anchor: new google.maps.Point(15,15),
              scaledSize: location.active ? new google.maps.Size(21,24) : new google.maps.Size(20,22)
            }} 
            onClick={ onMarkerClick }
            position={ location }
            onMouseover={ (marker, map) => onMouseoverMarker(marker, map, location.provider._id) } />
          )
        })}
          <InfoWindow
            marker={ infoWindow.marker }
            visible={ infoWindow.show }>
            <StyledInfoWindow>
              { infoWindow.show &&
              <div>
                { infoWindow.provider.photo &&
                  <img
                    alt=""
                    src={infoWindow.provider.photo}
                    width="30"
                    height="30" />
                }
                <h3>{ infoWindow.provider.name }</h3>
                <div className="text-center">
                  <StyledInfoButton tag="a" href={`/provider/${infoWindow.provider._id}`}>
                    View Provider
                  </StyledInfoButton>
                </div>
              </div>
              }
            </StyledInfoWindow>
          </InfoWindow>
      </Map>
    )
  }
}

MapWrapper.defaultProps = {
  onDragend: () => {},
  onMapReady: () => {},
  onMouseoverMarker: () => {},
  onMapClicked: () => {},
  onMarkerClicked: () => {},
  locations: [],
  infoWindow: {}
}

MapWrapper.propTypes = {
  // MapContainer functions
  onDragend: PropTypes.func,
  onMapReady: PropTypes.func,
  onMouseoverMarker: PropTypes.func,
  onMapClicked: PropTypes.func,
  onMarkerClick: PropTypes.func,
  // parsed
  latFilter: PropTypes.string,
  lngFilter: PropTypes.string,
  // MapContainer.state (all related to info window)
  infoWindow: PropTypes.object,
  // For Provider Page / From search reducer
  locations: PropTypes.array
}

const mapStateToProps = (state, ownProps) => ({
  locations: ownProps.locations || state.SearchReducer.locations
})

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAP94H75MYiGqWYg68X3124yzTVJVU7zYs'
})(connect(mapStateToProps)(MapWrapper))