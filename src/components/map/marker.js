// import React, { Component } from 'react'
// import { connect } from 'react-redux'
// import { Marker, GoogleApiWrapper } from 'google-maps-react'

// class MarkerComponent extends Component {
//   shouldComponentUpdate(nextProps) {
//     const shouldUpdate = (this.props.active !== nextProps.active) || (this.props.location !== nextProps.location)
//     console.log('Marker.shouldComponentUpdate', shouldUpdate)
//     return shouldUpdate
//   }

//   render(){
//     const { onMouseoverMarker, onMarkerClick, active, location, google } = this.props
//     console.log('rendering marker', location)
//     return (
//       <Marker
//         zIndex={ active ? 2 : null }
//         id={ location.provider._id }
//         key={ location.provider._id }
//         icon={{
//           url: active ? "/images/seethru-isotype-dark.png" : "/images/seethru-isotype.png",
//           anchor: new google.maps.Point(15,15),
//           scaledSize: active ? new google.maps.Size(21,24) : new google.maps.Size(20,22)
//         }} 
//         onClick={ onMarkerClick }
//         position={ location }
//         onMouseover={ (marker, map) => onMouseoverMarker(marker, map, location.provider._id) } />
//     )
//   }
// }

// const mapStateToProps = (state, ownProps) => ({
//   location: state.SearchReducer.locations.find(l => l.provider._id === ownProps.id)
// })

// export default GoogleApiWrapper({
//   apiKey: 'AIzaSyAP94H75MYiGqWYg68X3124yzTVJVU7zYs'
// })(connect(mapStateToProps)(MarkerComponent))