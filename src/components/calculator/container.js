import React, { Component } from 'react'

import Calculator from './base'
import GetTotalHours from '../../utils/calculator/get-total-hours'
import GetCostPerHour from '../../utils/calculator/get-cost-per-hour'
import { PageContainer } from '../common/layout'
import ConvertToNumbers from '../../utils/calculator/convert-to-numbers'
import GetCostPerProcedure from '../../utils/calculator/get-cost-per-procedure'
import GetNumberOfProcedure from '../../utils/calculator/get-number-of-procedure'

class CalculatorContainer extends Component {
  constructor() {
    super()
    this.state = {
      totalHours: null,
      costPerHour: null,
      costPerProcedure: null,
      numberOfProcedure: null
    }
  }

  onSubmit = values => {
    console.log(values)
    const valuesConvertedToNumbers = ConvertToNumbers(
      values.annualOverhead,
      values.hoursPerWeek,
      values.weeksPerYear,
      values.minutesPerProcedure,
      values.desiredAnnualIncome
    )
    const { 
      annualOverhead,
      hoursPerWeek,
      weeksPerYear,
      minutesPerProcedure,
      desiredAnnualIncome,
     } = valuesConvertedToNumbers

    const hourPerProcedure = minutesPerProcedure / 60.0
    const totalHours = GetTotalHours(hoursPerWeek, weeksPerYear)
    const numberOfProcedure = GetNumberOfProcedure(totalHours, hourPerProcedure)
    const costPerHour = GetCostPerHour(desiredAnnualIncome, annualOverhead, totalHours)
    const costPerProcedure = GetCostPerProcedure(costPerHour, hourPerProcedure)
    this.setState({
      totalHours,
      costPerHour,
      costPerProcedure,
      numberOfProcedure
    })
  }

  onReset = reset => {
    return () => {
      this.setState({
        totalHours: null,
        costPerHour: null,
        numberOfProcedure: null,
        costPerProcedure: null
      })
      reset()
    }
  }

  render() {
    return (
      <PageContainer>
        <Calculator 
          onReset={ this.onReset }
          onSubmit={ this.onSubmit }
          { ...this.state } />
      </PageContainer>
    )
  }
}

export default CalculatorContainer
