import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Form from './form'
import CurrencyFormat from 'react-currency-format'

const StyledDiv = styled.div`
  form {
    max-width: 550px;
  }
  h3 {
    font-size: 1.2rem;
  }
`

const Cost = ({ cost }) => cost ? (
  <CurrencyFormat 
    value={cost}
    displayType={'text'}
    thousandSeparator={true}
    prefix={'$'}
    decimalScale={2} 
    fixedDecimalScale={ (cost%1) > 0 } />
) : null

const Number = ({ value }) => value ? (
  <CurrencyFormat 
    value={value}
    displayType={'text'}
    thousandSeparator={true} />
) : null

const Calculator = ({
  onReset,
  onSubmit,
  totalHours,
  costPerHour,
  costPerProcedure,
  numberOfProcedure
}) => (
  <StyledDiv>
    <Form
      onReset={ onReset }
      onSubmit={ onSubmit } />
    <br/>
    <h3>Your Hourly Rate: <Cost cost={costPerHour} /></h3>
    <h3>Total Work Hours: <Number value={totalHours} /></h3>
    <hr />
    <h3>Cost Per Procedure/Service: <Cost cost={costPerProcedure} /> </h3>
    <h3>Estimated Number of Procedures/Services Per Year: <Number value={numberOfProcedure} /> </h3>
  </StyledDiv>
)

Calculator.propTypes = {
  onReset: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  totalHours: PropTypes.number,
  costPerHour: PropTypes.number,
  costPerProcedure: PropTypes.number,
  numberOfProcedure: PropTypes.number
}

export default Calculator
