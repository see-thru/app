export const SEARCH_LOADING = 'SEARCH_LOADING'
export const SET_SEARCH_RESULTS = 'SET_SEARCH_RESULTS'
export const SEARCH_FAILED = 'SEARCH_FAILED'
export const PROVIDER_MOUSEOVER = 'PROVIDER_MOUSEOVER'
export const SEARCH_MARKER_MOUSEOVER = 'SEARCH_MARKER_MOUSEOVER'
export const SEARCH_MARKER_MOUSEOUT = 'SEARCH_MARKER_MOUSEOUT'