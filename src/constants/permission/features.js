// Generic:
export const HEADER = 'header'
export const ENROLLMENT_FORM = 'enrollmentForm'
// Home:
export const HOME_SEARCH = 'homeSearch'
// Payment info:
export const TEST_CREDIT_CARDS_LIST = 'testCreditCardsList'