// user
export const USER = 'user/me'
export const CREATE_USER = 'user/create'
export const USER_PHOTO = 'user/photo'

// booking
export const CREATE_APPOINTMENT = 'booking/create'
export const CHECK_IN = 'booking/patient/check-in'
export const LOAD_APPOINTMENTS = 'booking/all'

// provider
export const PROVIDER = 'provider/provider'
export const PROVIDER_ALL = 'provider/all'

// search
export const SEARCH_PROVIDERS = 'search/search'