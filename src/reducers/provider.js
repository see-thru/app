import * as types from '../constants/provider'

const initialState = {
  data: undefined,
  services: undefined,
  error: undefined,
  loading: false
}

const ProviderReducer = (state = initialState, action) => {
  switch ( action.type ) {
    case types.PROVIDER_LOADING:
      return { 
        ...state,
        data: undefined,
        services: undefined,
        error: undefined,
        loading: true
      }
    case types.SET_PROVIDER:
      return { 
        ...state, 
        data: action.data,
        loading: false
      }
    case types.SET_PROVIDER_SERVICES:
      return { 
        ...state, 
        services: action.data
      }
    case types.PROVIDER_FAILED:
      return { 
        ...state, 
        error: action.error,
        loading: false
      }
    default:
      return state
  }
}

export default ProviderReducer