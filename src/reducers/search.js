import * as types from '../constants/search'
import combineSearchData from '../utils/combine-received-data'
import generateMapLocations from '../utils/generate-map-locations';

const initialState = {
  data: [],
  locations: [], 
  loading: false,
  // error,
  // hoveredMarker,
}

const SearchReducer = (state = initialState,  action) => {
  switch ( action.type ) {
    case types.SEARCH_LOADING:
      return { 
        ...state,
        loading: true,
        noMoreResults: false
      }
    case types.SET_SEARCH_RESULTS:
      let data = action.clearResults ? action.data : combineSearchData(action.data, state.data)
      let locations = generateMapLocations(data)
      return {
        data,
        locations,
        noMoreResults: action.noMoreResults,
        loading: false,
        error: undefined,
        hoveredMarker: undefined,
      }
    case types.SEARCH_FAILED:
      return { 
        ...state, 
        // data: [],
        noMoreResults: true,
        loading: false,
        error: action.error,
        hoveredMarker: undefined,
      }
    case types.SEARCH_MARKER_MOUSEOVER: // When hovering over a map marker
      return{
          ...state,
          hoveredMarker: action.providerId
      }
    case types.SEARCH_MARKER_MOUSEOUT: // When hovering over a map marker
      return{
          ...state,
          hoveredMarker: undefined
      }
    case types.PROVIDER_MOUSEOVER: // When hovering over a result item
      const locationsDeepCopy = state.locations.map(loc => {
        return Object.assign({}, loc, {
          active: loc.provider._id === action.providerId
        })
      })
      return {
        ...state,
        locations: locationsDeepCopy,
        hoveredMarker: undefined,
      }
    default:
      return state
  }
}

export default SearchReducer