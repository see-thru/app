import * as types from '../constants/auth'

const emptyAuth = {
  errors: {},
  loading: false,
  signInModalIsOpen: false,
  signUpModalIsOpen: false,
  resetModalIsOpen: false
}

const initialState = {
  user: null,
  isLogged: false,
  ...emptyAuth
}

const AuthReducer = ( 
  state = initialState, 
  action 
) => {
  switch ( action.type ) {
    case types.TOGGLE_SIGN_IN:
      return { 
        ...state,
        ...emptyAuth,
        signInModalIsOpen: action.modalIsOpen
      }
    case types.TOGGLE_SIGN_UP:
      return { 
        ...state,
        ...emptyAuth,
        signUpModalIsOpen: action.modalIsOpen
      }
    case types.TOGGLE_FORGOT_PASSWORD:
      return { 
        ...state,
        ...emptyAuth,
        resetModalIsOpen: action.modalIsOpen,
        resetModal: {}
      }         
    case types.AUTH_LOADING: 
      return {
        ...state,
        errors: {},
        loading: true
      }
    case types.SET_USER:
      return {
        ...state,
        ...emptyAuth,
        user: action.data,
        isLogged: true
      }
    case types.SET_TOKEN: 
      return {
        ...state,
        user: { 
          ...state.user,
          token: action.token
        }
      }
    case types.SIGNUP_FAILED:
      return {
        ...state,
        errors: { signup: action.error },
        loading: false
      }
    case types.LOGIN_FAILED:
      return {
        ...state,
        errors: { signin: action.error },
        loading: false
      }
    case types.USER_FAILED:
      return {
        ...state,
        errors: { current: action.error },
        loading: false
      }
    case types.PASSWORD_RESET_FAILED:
      return {
        ...state,
        resetModal: { error: action.error },
        loading: false
      }
    case types.PASSWORD_RESET_DONE:
      return {
        ...state,
        loading: false,
        resetModal: { success: action.data }
      }
    case types.LOGGED_IN: 
      return {
        ...state,
        isLogged: true
      }
    case types.LOGGED_OUT: 
      return {
        ...state,
        user: null,
        errors: {},
        loading: false,
        isLogged: false
      }
    default:
      return state
  }
}

export default AuthReducer