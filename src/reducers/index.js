import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { createResponsiveStateReducer } from 'redux-responsive'
import { breakpoints } from '../utils/media'
import AuthReducer from './auth'
import SearchReducer from './search'
import ProviderReducer from './provider'
import AppointmentsReducer from './appointments'

export default combineReducers({
  AuthReducer,
  SearchReducer,
  ProviderReducer,
  AppointmentsReducer,
  form: formReducer,
  browser: createResponsiveStateReducer(breakpoints)
})
